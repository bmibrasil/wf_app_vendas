package PDFManager;

import java.awt.Label;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import Actions.TMN_Action;
import Beans.Config;
import Beans.Elemento;
import Beans.Grupo;
import Beans.Decisao;
import Beans.LE;
import Bundle.EmbaralhamentoBundle;
import Service.ConfigService;
import Service.ElementoService;
import Service.DecisaoService;
import Service.GrupoService;
import Service.LEService;
import Util.Util;

public class PDFDecisoesTomadas {
	private int grupo;
	private String nomeGrupo;
	private Document document;
	private ElementoService es;
	private DecisaoService ds, dsLid;
	private LEService les;
	private GrupoService gr;
	private int periodo;
	private String organizacao;  
	private List<Elemento> lista; 
	private Decisao decisao; 
	private List<LE> listaLE;
	private List<Decisao> sitLid;
	private List<Grupo> listaGr; 
	Hashtable<String, Elemento> hash = new Hashtable<String, Elemento>();
	private String empresas[] = { "DentalByte", "DigiMetrics", "HardTech", "CleanSystem", "BioMaster" };
	private int valor;
	private EmbaralhamentoBundle emb= new EmbaralhamentoBundle(); 

	public PDFDecisoesTomadas(String organizacao, int grupo, String nomeGrupo){
		this.grupo=grupo;
		this.nomeGrupo=nomeGrupo;
		document = new Document();
		es= new ElementoService();
		ds= new DecisaoService();
		les= new LEService(grupo);
		gr= new GrupoService();
		ConfigService configService = new ConfigService();
		Config config = configService.getConfig(grupo);
		periodo = config.getPeriodo();
		this.organizacao=organizacao;  
		lista=new ArrayList<Elemento>(); 
		listaLE=new ArrayList<LE>();
		listaGr=new ArrayList<Grupo>();
		
	}

	private  String[][] PlanejamentoEstrategico= {
			{"Planejamento Estrat�gico","BioMaster"},
			{"","Alvo P4"},
			{"Excel�ncia em Opera��o",""},
			{"Excel�ncia em Inova��o",""},
			{"Excel�ncia em Customiza��o",""},
			{"Excel�ncia em Integra��o",""},
	};
	
	private  String[][] Decisoes= {
			{"Decis�es do Planejamento Estrat�gico", "P1","P2","P3","P4","", "P1.","P2","P3","P4","","P1","P2","P3","P4","","P1","P2","P3","P4","","P1","P2","P3","P4"},
			{"OPCAO 1",  "","","","","","","","","","","","","","","","","","","","","","","",""},
			{"OPCAO 1", "","","","","","","","","","","","","","","","","","","","","","","",""},
			{"OPCAO 1", "","","","","","","","","","","","","","","","","","","","","","","",""},
			{"OPCAO 1",    "","","","","","","","","","","","","","","","","","","","","","","",""},
			{"OPCAO 1",   "","","","","","","","","","","","","","","","","","","","","","","",""}
	};

	private  String[][] Proposito= {
			{"Tipo de Prop�sito", "Prop�sito"},
			{" ", " "},
	};
	
//	private  String[][] Proposito= {
//			{"Tipo de Prop�sito", "Prop�sito","Tipo de Multineg�cio","Sucess�o do Presidente"},
//			{" ", " "," "," "},
//	};
//	
	
	private  String[][] AlocacaoTempo= {
			{"Colaboradores","Acionistas","Sociedade","Clientes","DentalByte","DigiMetrics","HardTech","CleanSystem","BioMaster"},
			{"","","","","","","","",""}
	   }; 
	

	private  String[][] GestaoPlatHolding= {
			{"Recursos Humanos","Finan�as","Projeto","Tecnologia","","Recursos Humanos","Projetos","Tecnologia","Finan�as"},
			{" "," "," "," "," "," "," "," "," "}
	   }; 
	
	
	private  String[][] IndicadoresHolding= {
			{"Metas","Meta Per�odo","Pondera��o"},
			{"Receita L�quida Holding","",""},
			{"Pre�o da A��o Holding","","",},
			{"Receita L�quida BioMaster","","",},
			{"EBITDA BioMaster","",""},
			{"Necessidade de Capital de Giro BioMaster","",""},
			{"Satisfa��o de Clientes BioMaster","",""},
			{"Satisfa��o da Sociedade BioMaster","",""},
			{"Satisfa��o da Equipe BioMaster","",""},
			{"Execu��o Estrat�gica BioMaster","",""},
			{"Pilares da Sustenta��o BioMaster","",""}
	};
	
	private  String comunicacaoExecutiva[][]={
		//	{"Comunica��o Estrat�gica da Diretoria Executiva"},
			{" "}
	};
	
	private  String projetosEspeciais[][]={
			//{"Projetos Especiais", " "},
			{"Escolha","Descri��o"},
			{" ", " "}
	};
	
	private  String[][] AlocacaoGerencial= {
			{"Aloca��o de Tempo Gerencial","BioMaster"},
			{"Gest�o de Pessoas",""},
			{"Gest�o de Finan�as",""},
			{"Gest�o de Clientes",""},
			{"Gest�o de Opera��es",""},
	
	};

	
	private  String[][] gestaoPessoas= {
			{"Gest�o de Pessoas","BioMaster"},
			{"Compet�ncias",""},
			{"Motiva��o",""},
			{"Remunera��o",""},
			{"Produtividade",""},
	
	};

	private  String[][] gestaoFinancas= {
			{"Gest�o de Finan�as","BioMaster"},
			{"Or�amento",""},
			{"Lucro Operacional",""},
			{"Riscos",""},
			{"Necessidade de Capital de Giro",""},
	
	};	
	
	private  String[][] gestaoClientes= {
			{"Gest�o de Clientes","BioMaster"},
			{"Rentabilidade",""},
			{"Base de Clientes",""},
			{"Ciclo de Vida",""},
			{"Venda Cruzada",""},
	
	};
	
	private  String[][] gestaoOperacoes= {
			{"Gest�o de Opera��es","BioMaster"},
			{"Processos",""},
			{"Tecnologia",""},
			{"Custos",""},
			{"Riscos",""},
	
	};

	
	
	private  String[][] alocacaoTempoCEO= {
			{"Aloca��o de Tempo do CEO","BioMaster"},
			{"Sociedade",""},
			{"Acionistas",""},
			{"Clientes",""},
			{"Colaboradores",""},
			{"Atividades Rotineiras",""},
			{"Temas Urgentes e Importantes",""},
			{"Ess�ncia Individual",""},
			{"Coaching",""},
	};	

	
	private  String[][] planoTatico= {
			{"Plano T�tico","BioMaster"},
			{"Pol�tica de Recrutamento",""},
			{"N�vel de Terceiriza��o",""},
			{"Estrat�gia Comercial",""},
			{"Pol�tica Comercial",""},
	};
	
	
	
		
	private  String[][] projetos= {
			{"Projetos e Iniciativas","BioMaster"},
			{"Gest�o de Pessoas - Parte 1",""},
			{"Gest�o de Pessoas - Parte 2",""},
			{"Gest�o de Pessoas - Parte 3",""},
			{"Gest�o de Pessoas - Parte 4",""},
			{"Gest�o de Clientes - Parte 1",""},
			{"Gest�o de Clientes - Parte 2",""},
			{"Gest�o de Clientes - Parte 3",""},
			{"Gest�o de Clientes - Parte 4",""},
			{"Gest�o de Opera��es - Parte 1",""},
			{"Gest�o de Opera��es - Parte 2",""},
			{"Gest�o de Opera��es - Parte 3",""},
			{"Gest�o de Opera��es - Parte 4",""},
			{"Gest�o de Finan�as - Parte 1",""},
			{"Gest�o de Finan�as - Parte 2",""},
			{"Gest�o de Finan�as - Parte 3",""},
			{"Gest�o de Finan�as - Parte 4",""},
			
	};	
	
	private  String[][] liderancaAcao= {
			{"Lideran�a em A��o","BioMaster"},
			{"Lideran�a em A��o - Parte 1",""},
			{"Lideran�a em A��o - Parte 2",""},
			{"Lideran�a em A��o - Parte 3",""},
			{"Lideran�a em A��o - Parte 4",""},
			
			
			
	};	
	
	private  String[][] situacaoLid= {
			{"Situa��o de Lideran�a","BioMaster"},
			{"Situa��o de Lideran�a - Parte 1",""},
			{"Situa��o de Lideran�a - Parte 2",""},
			
			
	};	
	
	
	private  String[][] alocacaoOrcamentoOpex= {
			{"Aloca��o de Or�amento Opex","BioMaster"},
			{"ADM",""},
			{"Pessoal",""},
			{"PD",""},
			{"Vendas e Marketing",""},
			{"Tecnologia",""},
			{"Opera��es",""}
			
	};	
	
	
	private  String[][] alocacaoOrcamentoCapex= {
			{"Aloca��o de Or�amento Capex","BioMaster"},
			{"ADM",""},
			{"Pessoal",""},
			{"PD",""},
			{"Vendas e Marketing",""},
			{"Tecnologia",""},
			{"Opera��es",""}
			
	};	
	
	private  String[][] DecisoesFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//{"B 8 L 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0","B 6 C 8 0.0"},
			//{"B 8 L 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 0.5", "B 8 C 8 1.5"},
			{"B 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5","B 8 C 3 0.5", "B 8 C 3 0.5","B 8 C 8 0.5","B 8 C 8 0.5","B 8 C 8 0.5","B 8 C 8 0.5", "B 8 C 8 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 8 0.5","N 8 C 8 0.5","N 8 C 8 0.5","N 8 C 8 0.5", "B 8 C 8 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 8 0.5","N 8 C 8 0.5","N 8 C 8 0.5","N 8 C 8 0.5", "B 8 C 8 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 8 0.5","N 8 C 8 0.5","N 8 C 8 0.5","N 8 C 8 0.5", "B 8 C 8 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 8 0.5","N 8 C 8 0.5","N 8 C 8 0.5","N 8 C 8 0.5", "B 8 C 8 0.5"}
	};

	
	private  String[][] PlanejamentoFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"}
	};
	
	private  String[][] PropositoFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha

			//
			{"B 8 C 8 1.5", "B 8 C 8 1.5"},
			{"N 8 C 3 0.5", "N 8 C 3 0.5"},
			
	};
	
	
	private  String[][] AlocacaoFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha

			//
			{"B 8 C 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"}
			
	};
	
	
	private  String[][] GestaoPlatHoldingFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			
			//
			{"B 8 C 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"N 8 C 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"}
			
	};
	
	
	private  String[][] IndicadoresHoldingFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5"},
			{"N 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5"},
			{"N 8 L 3 0.5", "B 8 C 3 0.5","B 8 C 3 0.5"},
	};
	
	
	private  String[][] comunicacaoExecutivaFormato = {

			//Comunicacao
			{"N 8  L 11  0.0"}
	};
	
	
	private  String[][] ProjetosEspeciaisFormato = {

			//Situacao Estrategica
			{"B 8  L 8  1.5","B 8  L 8  1.5"},
			{"N 8  L 3  0.5","N 8  L 3  0.5"}
			
	};

	
	private  String[][] alocacaoGerencialFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","B 8 C 3 0.5"}
	};


	private  String[][] gestaoPessoasFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"}
	};

	private  String[][] gestaoFinancasFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"}
	};

	private  String[][] gestaoClientesFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"}
	};

	private  String[][] gestaoOperacoesFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"}
	};


	private  String[][] AlocacaoTempoCEOFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"}
	};

	
	private  String[][] planoTaticoFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5","B 8 C 8 1.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5","N 8 C 3 0.5"}
	};
	
	
	
	private  String[][] projetosFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},	
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 1.0", "N 8 C 3 1.0"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 1.0", "N 8 C 3 1.0"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 1.0", "N 8 C 3 1.0"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 1.0", "N 8 C 3 1.0"},
			
	};
	
	
	
	private  String[][] liderancaAcaoFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5"},
			{"B 8 L 3 1.0", "N 8 C 3 1.0"},
			{"B 8 L 3 1.0", "N 8 C 3 1.0"},
			{"B 8 L 3 1.0", "N 8 C 3 1.0"},
			{"B 8 L 3 1.0", "N 8 C 3 1.0"},
			
	};

	
	private  String[][] situacaoLidFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5"},
			{"B 8 L 3 1.0", "N 8 C 3 1.0"},
			{"B 8 L 3 1.0", "N 8 C 3 1.0"},
			
			
	};
	
	private  String[][] alocacaoOrcamentoOpexFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"}
		
	};
	
	
	private  String[][] alocacaoOrcamentoCapexFormato = { //Bold|Italic|Normal tamanho_fonte alinhamento espa�amento_vertical(padding)espessura da linha
			//Din�mica Organizacional

			//
			{"B 8 L 8 1.5", "B 8 C 8 1.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"},
			{"B 8 L 3 0.5", "N 8 C 3 0.5"}
		
	};
	public  void preenchePlanejamento(String[][] matriz)
	{ 
	
			matriz[2][1] =Math.round(hash.get("DO_P_E_EO"+empresas[4]+""+(0)).getValor()*100)+"%";
			
			matriz[3][1] =Math.round(hash.get("DO_P_E_EI"+empresas[4]+""+(0)).getValor()*100)+"%";
			
			matriz[4][1] =Math.round(hash.get("DO_P_E_EC"+empresas[4]+""+(0)).getValor()*100)+"%";
			
			matriz[5][1] =Math.round(hash.get("DO_P_E_EIt"+empresas[4]+""+(0)).getValor()*100)+"%";
			
	}

	
	public  void preencheProposito(String[][] matriz)
	{ 
	       
	        valor=(int) Math.round(hash.get("DO_P_TP"+"Holding"+""+(0)).getValor());
	        switch (valor)
	        {
	        case 1 : matriz[1][0] ="Universaliza��o"; break;
	        case 2 : matriz[1][0] ="Responsabilidade"; break;
	        case 3 : matriz[1][0] ="Excel�ncia"; break;
	        case 4 : matriz[1][0] ="Vanguarda"; break;
	        }
			
			matriz[1][1] =hash.get("DO_P_Txt"+"Holding"+""+(0)).getConteudo();
	

	}
	
	public void preencheMetas(String[][] matriz)
	{
		matriz[1][1] =""+Math.round(hash.get("DH_Metas_M1_M"+"BioMaster"+""+(periodo-1)).getValor());
		matriz[1][2] =""+Math.round(hash.get("DH_Metas_M1_P"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[2][1] =""+Math.round(hash.get("DH_Metas_M2_M"+"BioMaster"+""+(periodo-1)).getValor());
		matriz[2][2] =""+Math.round(hash.get("DH_Metas_M2_P"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[3][1] =""+Math.round(hash.get("DH_Metas_M3_M"+"BioMaster"+""+(periodo-1)).getValor());
		matriz[3][2] =""+Math.round(hash.get("DH_Metas_M3_P"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[4][1] =""+Math.round(hash.get("DH_Metas_M4_M"+"BioMaster"+""+(periodo-1)).getValor());
		matriz[4][2] =""+Math.round(hash.get("DH_Metas_M4_P"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[5][1] =""+Math.round(hash.get("DH_Metas_M5_M"+"BioMaster"+""+(periodo-1)).getValor());
		matriz[5][2] =""+Math.round(hash.get("DH_Metas_M5_P"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[6][1] =""+Math.round(hash.get("DH_Metas_M6_M"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[6][2] =""+Math.round(hash.get("DH_Metas_M6_P"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[7][1] =""+Math.round(hash.get("DH_Metas_M7_M"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[7][2] =""+Math.round(hash.get("DH_Metas_M7_P"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[8][1] =""+Math.round(hash.get("DH_Metas_M8_M"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[8][2] =""+Math.round(hash.get("DH_Metas_M8_P"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[9][1] =""+Math.round(hash.get("DH_Metas_M9_M"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[9][2] =""+Math.round(hash.get("DH_Metas_M9_P"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[10][1] =""+Math.round(hash.get("DH_Metas_M10_M"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
		matriz[10][2] =""+Math.round(hash.get("DH_Metas_M10_P"+"BioMaster"+""+(periodo-1)).getValor()*100)+"%";
	}
	
	public void preencheComunicaoExecutiva(String[][] matriz)
	{
		matriz[0][0] =  hash.get("DH_Com_Txt"+"Holding"+""+(periodo-1)).getConteudo();
	}
	
	public void preencheProjetosEspeciais(String[][] matriz)
	{
	      valor=(int) Math.round(hash.get("DUN_PE"+"BioMaster"+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PE",valor))
	        {
	        case 1 : matriz[1][0] ="A"; matriz[1][1]=decisao.getOp1();break;
	        case 2 : matriz[1][0] ="B"; matriz[1][1]=decisao.getOp2(); break;
	        case 3 : matriz[1][0] ="C"; matriz[1][1]= decisao.getOp3(); break;
	        case 4 : matriz[1][0] ="D"; matriz[1][1]=decisao.getOp4(); break;
	        
	        
	        }
	}     
	
	public void preencheAlocacaoGerencial(String[][] matriz)
	{
	
	 matriz[1][1] =Math.round(hash.get("DUN_ATGL_GP"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	 matriz[2][1] =Math.round(hash.get("DUN_ATGL_GF"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	 
	 matriz[3][1] =Math.round(hash.get("DUN_ATGL_GC"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	 
	 matriz[4][1] =Math.round(hash.get("DUN_ATGL_GO"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	 
	}
	
	
	public void preencheGestaoPessoas(String[][] matriz)
	{
	
	 matriz[1][1] =Math.round(hash.get("DUN_AEE_GP_Comp"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	 
	 matriz[2][1] =Math.round(hash.get("DUN_AEE_GP_Mot"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	 
	 matriz[3][1] =Math.round(hash.get("DUN_AEE_GP_Rem"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	 
	 matriz[4][1] =Math.round(hash.get("DUN_AEE_GP_Prod"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	 
	}
	
	
	public void preencheGestaoFinancas(String[][] matriz)
	{
	
	 matriz[1][1] =Math.round(hash.get("DUN_AEE_GF_Orc"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	 matriz[2][1] =Math.round(hash.get("DUN_AEE_GF_LO"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	 matriz[3][1] =Math.round(hash.get("DUN_AEE_GF_Rsc"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	 matriz[4][1] =Math.round(hash.get("DUN_AEE_GF_NCG"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	}
	
	public void preencheGestaoClientes(String[][] matriz)
	{
	
	 matriz[1][1] =Math.round(hash.get("DUN_AEE_GC_Rent"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	 matriz[2][1] =Math.round(hash.get("DUN_AEE_GC_BC"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	 matriz[3][1] =Math.round(hash.get("DUN_AEE_GC_CV"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	 matriz[4][1] =Math.round(hash.get("DUN_AEE_GC_VC"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	}
	
	public void preencheGestaoOperacoes(String[][] matriz)
	{
	
	 matriz[1][1] =Math.round(hash.get("DUN_AEE_GO_Proc"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	 matriz[2][1] =Math.round(hash.get("DUN_AEE_GO_Tec"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	 matriz[3][1] =Math.round(hash.get("DUN_AEE_GO_Cst"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	 matriz[4][1] =Math.round(hash.get("DUN_AEE_GO_Rsc"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
	
	}
	
	

	public void preencheAlocacaoTempoCEO(String[][] matriz)
	{
		
		 matriz[1][1] =""+Math.round(hash.get("DUN_ATL_Soc"+empresas[4]+""+(periodo-1)).getValor()*40)+"h";
		
		 matriz[2][1] =""+Math.round(hash.get("DUN_ATL_Ac"+empresas[4]+""+(periodo-1)).getValor()*40)+"h";
		
		 matriz[3][1] =""+Math.round(hash.get("DUN_ATL_Col"+empresas[4]+""+(periodo-1)).getValor()*40)+"h";
		
		 matriz[4][1] =""+Math.round(hash.get("DUN_ATL_Cli"+empresas[4]+""+(periodo-1)).getValor()*40)+"h";
		
		 matriz[5][1] =""+Math.round(hash.get("DUN_ATL_AtRot"+empresas[4]+""+(periodo-1)).getValor()*40)+"h";
		
		 matriz[6][1] =""+Math.round(hash.get("DUN_ATL_TemasUrgImp"+empresas[4]+""+(periodo-1)).getValor()*40)+"h";
		 
		 matriz[7][1] =""+Math.round(hash.get("DUN_ATL_EI"+empresas[4]+""+(periodo-1)).getValor()*40)+"h";
		 
		 matriz[8][1] =""+Math.round(hash.get("DUN_ATL_Coach"+empresas[4]+""+(periodo-1)).getValor()*40)+"h";
		 
	}
	
	public void preenchePlanoTatico(String[][] matriz)
	{   
		//for(int j=1; j<=5;j++)
		//{
		int j=1;
		   valor=(int) Math.round(hash.get("DUN_PT_PR"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (valor)
	        {
	        case 1 : matriz[1][j] ="A"; break;
	        case 2 : matriz[1][j] ="B"; break;
	        case 3 : matriz[1][j] ="C"; break;
	        case 4 : matriz[1][j] ="D"; break;
	        case 5 : matriz[1][j] ="E"; break;
	        
	        }
	       
	       valor=(int) Math.round(hash.get("DUN_PT_NT"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (valor)
	        {
	        case 1 : matriz[2][j] ="A"; break;
	        case 2 : matriz[2][j] ="B"; break;
	        case 3 : matriz[2][j] ="C"; break;
	        case 4 : matriz[2][j] ="D"; break;
	        case 5 : matriz[2][j] ="E"; break;
	        
	        }
	       
	       valor=(int) Math.round(hash.get("DUN_PT_EC"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (valor)
	        {
	        case 1 : matriz[3][j] ="A"; break;
	        case 2 : matriz[3][j] ="B"; break;
	        case 3 : matriz[3][j] ="C"; break;
	        case 4 : matriz[3][j] ="D"; break;
	        case 5 : matriz[3][j] ="E"; break;
	        
	        }
	
	       matriz[4][j]=Math.round(hash.get("DUN_PT_PC_CR"+empresas[4]+""+(periodo-1)).getValor())+"m";   
	        
	       //}
	   }     
	
	
	
	public void preencheLiderancaAcao(String[][] matriz)
	{
		 //for(int j=1; j<=5;j++)
		  //{
		   int j=1;
	       valor=(int) Math.round(hash.get("DUN_LA_RC"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU"+j+"_LA_RC",valor))
	        {
	        case 1 : matriz[1][j] ="A"; break;
	        case 2 : matriz[1][j] ="B"; break;
	        case 3 : matriz[1][j] ="C"; break;
	       	        
	        }
	       
	       valor=(int) Math.round(hash.get("DUN_LA_Conflitos"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU"+j+"_LA_CONF",valor))
	        {
	        case 1 : matriz[2][j] ="A"; break;
	        case 2 : matriz[2][j] ="B"; break;
	        case 3 : matriz[2][j] ="C"; break;
	       	        
	        }
	       
	       valor=(int) Math.round(hash.get("DUN_LA_Prioridades"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU"+j+"_LA_PRI",valor))
	        {
	        case 1 : matriz[3][j] ="A"; break;
	        case 2 : matriz[3][j] ="B"; break;
	        case 3 : matriz[3][j] ="C"; break;
	       	        
	        }
	       
	       valor=(int) Math.round(hash.get("DUN_LA_Decisoes"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU"+j+"_LA_DEC",valor))
	        {
	        case 1 : matriz[4][j] ="A"; break;
	        case 2 : matriz[4][j] ="B"; break;
	        case 3 : matriz[4][j] ="C"; break;
	       	        
	        }
	}
	
	
	
	public void preencheProjetos(String[][] matriz)
	{
	      int j=1;
	       
	       valor=(int) Math.round(hash.get("DUN_PI_GP_Cmp"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GP_Cmp",valor))
	        {
	        case 1 : matriz[1][j] ="A"; break;
	        case 2 : matriz[1][j] ="B"; break;
	        case 3 : matriz[1][j] ="C"; break;
	        case 4 : matriz[1][j] ="D"; break;
	      
	        
	        }
	       
	       valor=(int) Math.round(hash.get("DUN_PI_GP_Mot"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GP_Mot",valor))
	        {
	        case 1 : matriz[2][j] ="A"; break;
	        case 2 : matriz[2][j] ="B"; break;
	        case 3 : matriz[2][j] ="C"; break;
	        case 4 : matriz[2][j] ="D"; break;
	      
	        
	        }
	       
	       
	       valor=(int) Math.round(hash.get("DUN_PI_GP_Prd"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GP_Prd",valor))
	        {
	        case 1 : matriz[3][j] ="A"; break;
	        case 2 : matriz[3][j] ="B"; break;
	        case 3 : matriz[3][j] ="C"; break;
	        case 4 : matriz[3][j] ="D"; break;
	      
	        
	        }
	      
	       
	       valor=(int) Math.round(hash.get("DUN_PI_GP_Rem"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GP_Rem",valor))
	        {
	        case 1 : matriz[4][j] ="A"; break;
	        case 2 : matriz[4][j] ="B"; break;
	        case 3 : matriz[4][j] ="C"; break;
	        case 4 : matriz[4][j] ="D"; break;
	      
	        
	        }
	       
	       
	     	     
	       	       
	       valor=(int) Math.round(hash.get("DUN_PI_GC_BC"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GC_BC",valor))
	        {
	        case 1 : matriz[5][j] ="A"; break;
	        case 2 : matriz[5][j] ="B"; break;
	        case 3 : matriz[5][j] ="C"; break;
	        case 4 : matriz[5][j] ="D"; break;
	      
	        
	        }
	      
	     
	       
	       valor=(int) Math.round(hash.get("DUN_PI_GC_CV"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GC_CV",valor))
	        {
	        case 1 : matriz[6][j] ="A"; break;
	        case 2 : matriz[6][j] ="B"; break;
	        case 3 : matriz[6][j] ="C"; break;
	        case 4 : matriz[6][j] ="D"; break;
	      
	        
	        }
	       valor=(int) Math.round(hash.get("DUN_PI_GC_Rent"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GC_Rent",valor))
	        {
	        case 1 : matriz[7][j] ="A"; break;
	        case 2 : matriz[7][j] ="B"; break;
	        case 3 : matriz[7][j] ="C"; break;
	        case 4 : matriz[7][j] ="D"; break;
	      
	        
	        }
	      
	       
	       	       
	       valor=(int) Math.round(hash.get("DUN_PI_GC_VC"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GC_VC",valor))
	        {
	        case 1 : matriz[8][j] ="A"; break;
	        case 2 : matriz[8][j] ="B"; break;
	        case 3 : matriz[8][j] ="C"; break;
	        case 4 : matriz[8][j] ="D"; break;
	      
	        
	        }
	      
	       
	       valor=(int) Math.round(hash.get("DUN_PI_GO_Cst"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GO_Cst",valor))
	        {
	        case 1 : matriz[9][j] ="A"; break;
	        case 2 : matriz[9][j] ="B"; break;
	        case 3 : matriz[9][j] ="C"; break;
	        case 4 : matriz[9][j] ="D"; break;
	               
	        }
	       
	       valor=(int) Math.round(hash.get("DUN_PI_GO_Proc"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GO_Proc",valor))
	        {
	        case 1 : matriz[10][j] ="A"; break;
	        case 2 : matriz[10][j] ="B"; break;
	        case 3 : matriz[10][j] ="C"; break;
	        case 4 : matriz[10][j] ="D"; break;
	               
	        }
	       
	       
	       valor=(int) Math.round(hash.get("DUN_PI_GO_Rsk"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GO_Rsk",valor))
	        {
	        case 1 : matriz[11][j] ="A"; break;
	        case 2 : matriz[11][j] ="B"; break;
	        case 3 : matriz[11][j] ="C"; break;
	        case 4 : matriz[11][j] ="D"; break;
	               
	        }
	       valor=(int) Math.round(hash.get("DUN_PI_GO_Tec"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GO_Tec",valor))
	        {
	        case 1 : matriz[12][j] ="A"; break;
	        case 2 : matriz[12][j] ="B"; break;
	        case 3 : matriz[12][j] ="C"; break;
	        case 4 : matriz[12][j] ="D"; break;
	               
	        }
	       
	      
	       valor=(int) Math.round(hash.get("DUN_PI_GF_LO"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GF_LO",valor))
	        {
	        case 1 : matriz[13][j] ="A"; break;
	        case 2 : matriz[13][j] ="B"; break;
	        case 3 : matriz[13][j] ="C"; break;
	        case 4 : matriz[13][j] ="D"; break;
	      
	        
	        }
	       
	       
	       valor=(int) Math.round(hash.get("DUN_PI_GF_Orc"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GF_Orc",valor))
	        {
	        case 1 : matriz[14][j] ="A"; break;
	        case 2 : matriz[14][j] ="B"; break;
	        case 3 : matriz[14][j] ="C"; break;
	        case 4 : matriz[14][j] ="D"; break;
	      
	        
	        }
	      
	      
	       
	       valor=(int) Math.round(hash.get("DUN_PI_GF_Rsk"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GF_Rsk",valor))
	        {
	        case 1 : matriz[15][j] ="A"; break;
	        case 2 : matriz[15][j] ="B"; break;
	        case 3 : matriz[15][j] ="C"; break;
	        case 4 : matriz[15][j] ="D"; break;
	      
	        
	        }
	      
	       valor=(int) Math.round(hash.get("DUN_PI_GF_wc"+empresas[4]+""+(periodo-1)).getValor());   
	       switch (emb.convert("A"+(periodo-1)+"_DBU_PI_GF_wc",valor))
	        {
	        case 1 : matriz[16][j] ="A"; break;
	        case 2 : matriz[16][j] ="B"; break;
	        case 3 : matriz[16][j] ="C"; break;
	        case 4 : matriz[16][j] ="D"; break;
	      
	        
	        }
	      
	       
	}
	
	
	
	public void preencheSituacaoLid(String[][] matriz)
	{
		String pergunta1="",pergunta2="";
		String campo1="",campo2="";
		switch (periodo -1)
		{
		case 1:
		 pergunta1="A1_DBU5_SL_A";
		 pergunta2="A2_DBU5_SL_I";
		 campo1="DUN_SL_A";
		 campo2="DUN_SL_I";
		  break;
		case 2:
			 pergunta1="A2_DBU5_SL_I";
			 pergunta2="A1_DBU5_SL_A";
			 campo1="DUN_SL_I";
			 campo2="DUN_SL_A";
			 
			 break;
			
		case 3:
			 pergunta1="A3_DBU5_SL_IE";
			 pergunta2="A4_DBU5_SL_CP";
			 campo1="DUN_SL_IE";
			 campo2="DUN_SL_CP";
			 
			  break;
			
		case 4:
			 pergunta1="A4_DBU5_SL_CP";
			 pergunta2="A3_DBU5_SL_IE";
			 campo1="DUN_SL_CP";
			 campo2="DUN_SL_IE";
			 
			 break;
			
		}	  
		
	             
	  	 //for(int j=1; j<=5;j++)
			  //{
			   int j=1;
		       valor=(int) Math.round(hash.get(campo1+empresas[4]+""+(periodo-1)).getValor());   
		       switch (emb.convert(pergunta1,valor))
		        {
		        case 1 : matriz[1][j] ="A"; break;
		        case 2 : matriz[1][j] ="B"; break;
		        case 3 : matriz[1][j] ="C"; break;
		       	        
		        }
		       
		       valor=(int) Math.round(hash.get(campo2+empresas[4]+""+(periodo-1)).getValor());   
		       switch (emb.convert(pergunta2,valor))
		        {
		        case 1 : matriz[2][j] ="A"; break;
		        case 2 : matriz[2][j] ="B"; break;
		        case 3 : matriz[2][j] ="C"; break;
		       	        
		        }
   }     
	
	
	
	public void preencheAlocacaoOrcamentoOpex(String[][] matriz)
	{
		//for(int i=1; i<=5;i++)
		//{
		   int i=1;	
		   matriz[1][i]= ""+ Math.round(hash.get("DUN_AO_ADM_Opex"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";   
		   matriz[2][i]= ""+ Math.round(hash.get("DUN_AO_Pessoal_Opex"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
		   matriz[3][i]= ""+ Math.round(hash.get("DUN_AO_PD_Opex"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
		   matriz[4][i]= ""+ Math.round(hash.get("DUN_AO_VMkt_Opex"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
		   matriz[5][i]= ""+ Math.round(hash.get("DUN_AO_TI_Opex"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
		   matriz[6][i]= ""+ Math.round(hash.get("DUN_AO_Oper_Opex"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
			 
		      
	     //}
	}		
	
	
	public void preencheAlocacaoOrcamentoCapex(String[][] matriz)
	{
		//for(int i=1; i<=5;i++)
		//{
		   int i=1;	
		   matriz[1][i]= ""+ Math.round(hash.get("DUN_AO_ADM_Capex"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";   
		   matriz[2][i]= ""+ Math.round(hash.get("DUN_AO_Pessoal_Capex"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
		   matriz[3][i]= ""+ Math.round(hash.get("DUN_AO_PD_Capex"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
		   matriz[4][i]= ""+ Math.round(hash.get("DUN_AO_VMkt_Capex"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
		   matriz[5][i]= ""+ Math.round(hash.get("DUN_AO_TI_Capex"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
		   matriz[6][i]= ""+ Math.round(hash.get("DUN_AO_Oper_Capex"+empresas[4]+""+(periodo-1)).getValor()*100)+"%";
			 
		      
	    // }
	}	
	public   void desenhaRelatorio(int numeroColunas, String[][] matriz, String[][] formato , int[] widths, boolean marcar,int relatorio) throws DocumentException
	{       
		double maior=0,menor=101;
		PdfPCell maiorCelula =new PdfPCell();
		PdfPCell menorCelula =new PdfPCell();
		switch(relatorio)
		{
		
		case 2: preenchePlanejamento(matriz);break;
		case 3: preencheProposito(matriz);break;
	//	case 4: preencheAlocacaoHolding(matriz);break;
	//	case 5: preencheGestaoPlatHolding(matriz);break;
		case 6: preencheMetas(matriz);break;
		case 7: preencheComunicaoExecutiva(matriz);break;
		case 8: preencheProjetosEspeciais(matriz);break;
		case 9: preencheAlocacaoGerencial(matriz);break;
		case 10: preencheGestaoPessoas(matriz);break;
		case 11: preencheGestaoOperacoes(matriz);break;
		case 12: preencheGestaoFinancas(matriz);break;
		case 13: preencheGestaoClientes(matriz);break;
		case 14: preencheAlocacaoTempoCEO(matriz);break;
		case 15: preenchePlanoTatico(matriz);break;
		case 16: preencheProjetos(matriz);break;
		case 17: preencheAlocacaoOrcamentoOpex(matriz);break;
		case 18: preencheAlocacaoOrcamentoCapex(matriz);break;
		case 19: preencheLiderancaAcao(matriz);
		case 20: preencheSituacaoLid(matriz);
		}
		//Relat�rios Financeiros - BMI Simulador Octopus
		//document.add(new Paragraph(""));
		//determina o numero de colunas da tabela
		//cria a tabela com o numero de colunas passado
		PdfPTable table = new PdfPTable(numeroColunas);
		//seta o tamanho das colunas de acordo com o vetor de tamanho passado
		table.setWidths(widths);

		// document.add(linebreak);
		//document.add(new Paragraph(" "));
		for (int i = 0; i<matriz.length; i++){
			for (int j = 0; j<matriz[i].length; j++){
				StringTokenizer st = new StringTokenizer(formato[i][j]);
				char tipo=st.nextToken().charAt(0);
				int tamanho = Integer.parseInt(st.nextToken());
				char align=st.nextToken().charAt(0);
				int padding = Integer.parseInt(st.nextToken());
				float espessura =Float.parseFloat(st.nextToken());
				Font f = new Font();
				//seta o tipo de formata��o da fonte
				f.setSize(tamanho);
				switch(tipo){
				case 'I': f.setStyle(Font.ITALIC);break;
				case 'B': f.setStyle(Font.BOLD);break;
				}
				Chunk c = new Chunk(matriz[i][j]);
				//System.out.println("primeira"+matriz[i][j]); 
				c.setFont(f);
				Phrase ph = new Phrase();
				ph.add(c);
				//configura a celula para bordas, espessura da borda e espa�amento antes e depois da c�lula.
				PdfPCell cell = new PdfPCell(ph);
				cell.setBorder(Rectangle.BOTTOM);
				cell.setBorderWidth(espessura);
				cell.setPaddingTop(padding);
				cell.setPaddingBottom(padding);
				//if ((i==2)&&(j==2)) cell.setBackgroundColor(BaseColor.ORANGE);
				//configura o alinhamento do texto
				switch(align){
				case 'R': cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);break;
				case 'L': cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);break;
				case 'C': cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);break;
				}
				//faz uma mescla nas c�lulas da primeira linha
				//  if ((j==0) && (i==0)){
				// cell.setColspan(numeroColunas-2);
				// }

				table.addCell(cell);
				//guarda a maior e a menor c�lula da linha
				if (marcar ==true)
				{
					//System.out.println("marcar" +matriz[i][j]); 
					try
					{

						if(Double.parseDouble(matriz[i][j].replace("%","")) > maior)  
						{ maior= Double.parseDouble(matriz[i][j].replace("%",""));
						//System.out.println("maior:" +maior);
						maiorCelula=cell ;
						//System.out.println(cell);
						}
						if(Double.parseDouble(matriz[i][j].replace("%",""))  < menor)  
						{ menorCelula=cell ;
						menor= Double.parseDouble(matriz[i][j].replace("%",""));
						//System.out.println("menor:" +menor);
						//System.out.println(cell);
						}  
					}
					catch(Exception e)
					{//System.out.println("n�o � numero" +matriz[i][j]);}

					}

				}

			}
			if (marcar ==true)
			{ 
				maiorCelula.setBackgroundColor(BaseColor.ORANGE);
				menorCelula.setBackgroundColor(BaseColor.RED);   
			}
		}
		document.add(table);
		maior=0;
		menor=101;
	}

	public void inicio() throws DocumentException  
	{ 
		
		
		
		System.out.println("IN�CIO GERA��O");
		
		
		//cria hassh de elementos do balan�o
		lista =es.getHerdeiros("%%","%%",0,+grupo);
		for (Elemento e:lista){
					hash.put(e.getCampo()+e.getOrganizacao()+e.getPeriodo(), e);
		}
		lista =es.getHerdeiros("%%","%%",periodo-1,+grupo);
		for (Elemento e:lista){
					hash.put(e.getCampo()+e.getOrganizacao()+e.getPeriodo(), e);
		}
		
		
		decisao = ds.getDecisao(periodo-1, "_DBU_PE");
		
		
		//this.organizacao = organizacao;
		//Cria��o do Relat�rio

		// cria��o do DR

		try {

			//LineSeparator separator = new LineSeparator();
			//separator.setLineWidth((float) 1);
			//Chunk linebreak = new Chunk(separator);
			document.setPageSize(PageSize.A4.rotate());

			//Verifica se o diret�rio da turma existe
			int turma = gr.getGrupo(grupo).getTurma();
			File dir_turma = new File("turma_"+turma);
			if (!dir_turma.exists()){
				dir_turma.mkdir();
			}
			
			File dir_grupo = new File("turma_"+turma+"/grupo_"+grupo);
			if (!dir_grupo.exists()){
				dir_grupo.mkdir();
			}

			ConfigService configService = new ConfigService();
			Config config = configService.getConfig(grupo);
			periodo = config.getPeriodo();

			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("turma_"+turma+"/grupo_"+grupo+"/"+"Decis�es"+"_grupo"+grupo+"_periodo"+(periodo-1)+".pdf"));
			writer.setPageEvent(new CabecalhoRodape(nomeGrupo, periodo-1,"Relat�rio de Decis�es"));
			document.open();
			document.addSubject("Simulador Eagle - BMI"); 
			document.addKeywords("www.bluemanagement.institute");
			document.addCreator("Eagle");
			document.addAuthor("BMI Tecnologia");
			Image img1 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/biomaster.png");
			Image img11 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/octopus.png");
			Image img2 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/cleansystem.png");
			Image img3 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/dentalbyte.png");
			Image img4 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/digimetrics.png");
			Image img5 = Image.getInstance("../applications/BMI_SIM_Eagle/img/relatorios/hardtech.png");
			img1.scaleToFit(100,100);
			img11.scaleToFit(70,70);
			img2.scaleToFit(70,70);
			img3.scaleToFit(70,70);
			img4.scaleToFit(70,70);
			img5.scaleToFit(70,70);
			img1.setAbsolutePosition(350,350);
			img11.setAbsolutePosition(180,200);
			img2.setAbsolutePosition(280,200);
			img3.setAbsolutePosition(380,200);
			img4.setAbsolutePosition(480,200);
			img5.setAbsolutePosition(580,200);
		    PdfContentByte cb = writer.getDirectContent();
			Phrase header = new Phrase("Relat�rio de Decis�es do grupo "+ nomeGrupo + " - do periodo: "+""+(periodo-1)+  " - BioMaster " ,  new Font(Font.FontFamily.UNDEFINED, 14, Font.BOLD));
            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, header,(document.right() - document.left()) / 2 + document.leftMargin() ,document.top() - 30, 0);
			document.add(img1);
			document.add(img11);
			document.add(img2);
			document.add(img3);
			document.add(img4);
			document.add(img5);

			

		}
		//  document.add(linebreak);
		catch(DocumentException de) {
			System.err.println(de.getMessage());
		}
		catch(IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		document.newPage();
		//P�gina P0]
		titulo("Prop�sito","");
		this.desenhaRelatorio(2, Proposito, PropositoFormato,new int[]{2,6},false,3);
		espaco();
		//titulo("Decis�es do Planejamento Estrat�gico","");
		this.desenhaRelatorio(2, PlanejamentoEstrategico, PlanejamentoFormato,new int[]{4,2},false,2);
		espaco();
		titulo("Defini��o de Metas - Holding","");
		this.desenhaRelatorio(3, IndicadoresHolding, IndicadoresHoldingFormato,new int[]{2,2,2},false,6);
		espaco();
		document.newPage();
		//titulo("Gest�o de Pessoas","");
		this.desenhaRelatorio(2, gestaoPessoas, gestaoPessoasFormato,new int[]{4,2},false,10);
		espaco();
		//titulo("Gest�o de Opera��es","");
		this.desenhaRelatorio(2, gestaoOperacoes, gestaoOperacoesFormato,new int[]{4,2},false,11);
	    //titulo("Gest�o de Finan�as","");
		espaco();
		this.desenhaRelatorio(2, gestaoFinancas, gestaoFinancasFormato,new int[]{4,2},false,12);
		//titulo("Gest�o de Clientes","");
		espaco();
		this.desenhaRelatorio(2, gestaoClientes, gestaoClientesFormato,new int[]{4,2},false,13);
		document.newPage();
		titulo("Projetos Especiais","");
		this.desenhaRelatorio(2, projetosEspeciais, ProjetosEspeciaisFormato,new int[]{2,10},false,8);
		//titulo("Plano T�tico","");
		document.newPage();
		espaco();
		this.desenhaRelatorio(2, planoTatico, planoTaticoFormato,new int[]{4,2},false,15);
		//titulo("Aloca��o de Or�amento - Opex","");
		espaco();
		this.desenhaRelatorio(2, alocacaoOrcamentoOpex, alocacaoOrcamentoOpexFormato,new int[]{4,2},false,17);
		espaco();
		//titulo("Aloca��o de Or�amento - Capex","");
		this.desenhaRelatorio(2, alocacaoOrcamentoCapex, alocacaoOrcamentoCapexFormato,new int[]{4,2},false,18);
		espaco();
		document.newPage();
		//titulo("Projetos e Iniciativas","");
		this.desenhaRelatorio(2, projetos, projetosFormato,new int[]{10,2},false,16);
		espaco();
		//titulo("Aloca��o de Tempo do CEO","");
		this.desenhaRelatorio(2, alocacaoTempoCEO, AlocacaoTempoCEOFormato,new int[]{4,2},false,14);
	    espaco();
		//titulo("Aloca��o de Tempo Gerencial","");
		this.desenhaRelatorio(2, AlocacaoGerencial, alocacaoGerencialFormato,new int[]{4,2},false,9);
		espaco();
		document.newPage();
		titulo("Comunica��o Estrat�gica da Diretoria Executiva","");
		this.desenhaRelatorio(1, comunicacaoExecutiva, comunicacaoExecutivaFormato,new int[]{10},false,7);
		document.newPage();
		//titulo("Lideran�a em A��o","");
		this.desenhaRelatorio(2, liderancaAcao, liderancaAcaoFormato,new int[]{10,2},false,19);
		//titulo("Situa��o de Lideran�a","");
		espaco();
		this.desenhaRelatorio(2, situacaoLid, situacaoLidFormato,new int[]{10,2},false,20);
		//titulo("Aloca��o de Esfor�o de Execu��o","");
		
		
		
	
		//	this.desenhaRelatorio(6, AlocacaoEsfor�o, alocacaoEsforcoFormato,new int[]{4,2,2,2,2,2},false,9);
		//espaco();
		
		
		document.close();    
		System.out.println("FIM GERA��O");
	}
	
	
	public void titulo(String descricao, String comentario) throws DocumentException{
		String[][] titulo = {{descricao,comentario}};
		String[][] formato = {{"B 8 L 0 0","N 6 R 0 0"}};
		int[] larguras = {3,1};
		espaco();
		this.desenhaRelatorio(2,titulo, formato, larguras, false, 0);
		espaco();

	}
	

		
	public void subTituloGestao() throws DocumentException{
		String[][] titulo = {{"Gest�o de Plataforma","N�vel de Centraliza��o Funcional"}};
		String[][] formato = {{"B 8 C 0 0","B 8 C 0 0"}};
		int[] larguras = {5,5};
		this.desenhaRelatorio(2,titulo, formato, larguras, false, 0);
	}
	

	public void subTituloDecisoes() throws DocumentException{
		String[][] titulo = {{" ","BioMaster", "CleanSystem", "DentalByte", "DigiMetrics","HardTech"}};
		String[][] formato = {{"B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0"}};
		int[] larguras = {2,4,4,4,4,4};
		this.desenhaRelatorio(6,titulo, formato, larguras, false, 0);
	}
	
	
	public void rodapeMultiNegocio() throws DocumentException{
		String[][] rodape = {{"1-Portfolio Management", "2-Strategic Design", "3-Operational Check & Balance", "4-Functional Alignment","5-Corporate Engagement"}};
		String[][] formato = {{"B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0"}};
		int[] larguras = {6,6,6,6,6};
		this.desenhaRelatorio(5,rodape, formato, larguras, false, 0);
	}
	public void subTituloATL() throws DocumentException{
		String[][] titulo = {{" ",empresas[0],empresas[1],empresas[2],empresas[3],empresas[4]}};
		String[][] formato = {{"N 8 L 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0","B 8 C 0 0"}};
		int[] larguras = {3,1,1,1,1,1};
		this.desenhaRelatorio(6,titulo, formato, larguras, false, 0);
	}
	public void espaco() throws DocumentException{
		document.add(new Paragraph(" "));

	}

}