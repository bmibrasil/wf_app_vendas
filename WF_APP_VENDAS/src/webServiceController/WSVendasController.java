package webServiceController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import action.ColaboradorAction;
import action.NegocioAction;
import action.OrganizacaoAction;
import action.PessoaAction;
import action.SolicitacaoOrganizacaoAction;
import action.SolicitacaoPessoaAction;
import action.UnidadeNegocioAction;
import action.AtividadeAction;


public class WSVendasController {

	public String process(String jsonRecebido) throws Exception 
	{
		String result="";
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObj = (JsonObject)jsonParser.parse(jsonRecebido);
		String action = jsonObj.get("action").getAsString();


		if (action.equals( "salvarAtividade")) {
			AtividadeAction cfg = new AtividadeAction();
			result=""+cfg.salvarAtividade(jsonObj);
		}

		if (action.equals( "buscarUnidadesNegocio")) {
			UnidadeNegocioAction cfg = new UnidadeNegocioAction();
			result=""+cfg.buscarUnidadesNegocio(jsonObj);
		}

		

		if (action.equals( "buscarAtividade")) {
			AtividadeAction cfg = new AtividadeAction();
			result=""+cfg.buscarAtividade(jsonObj);
		}
		
		
		if (action.equals( "buscarTodasAtividadesProprietarioNegocio")) {
			AtividadeAction cfg = new AtividadeAction();
			result=""+cfg.buscarTodasAtividadesProprietarioNegocio(jsonObj);
		}
		
		if (action.equals( "buscarTodasAtividadesProprietarioPessoa")) {
			AtividadeAction cfg = new AtividadeAction();
			result=""+cfg.buscarTodasAtividadesProprietarioPessoa(jsonObj);
		}
		
		
		if (action.equals( "buscarTodosRelacionamentosProprietarioPessoa")) {
			AtividadeAction cfg = new AtividadeAction();
			result=""+cfg.buscarTodosRelacionamentosProprietarioPessoa(jsonObj);
		}
		
		

//		if (action.equals( "buscarAtividadesNegocioPessoa")) {
//			AtividadeAction cfg = new AtividadeAction();
//			result=""+cfg.buscarAtividadesNegocioPessoa(jsonObj);
//		}


		if (action.equals( "removerAtividade")) {
			AtividadeAction cfg = new AtividadeAction();
			result=""+cfg.removerAtividade(jsonObj);
		}	



		if (action.equals( "salvarNegocio")) {
			NegocioAction cfg = new NegocioAction();
			result=""+cfg.salvarNegocio(jsonObj);
		}
		
		if (action.equals( "atualizarFaseNegocio")) {
			NegocioAction cfg = new NegocioAction();
			result=""+cfg.atualizarFaseNegocio(jsonObj);
		}

		
		
		if (action.equals( "buscarLancamentosNegocioStatus")) {
			NegocioAction cfg = new NegocioAction();
			result=""+cfg.buscarLancamentosNegocioStatus(jsonObj);
		}

		if (action.equals( "buscarNegociosProprietario")) {
			NegocioAction cfg = new NegocioAction();
			result=""+cfg.buscarNegociosProprietario(jsonObj);
		}

		if (action.equals( "buscarNegocio")) {
			NegocioAction cfg = new NegocioAction();
			result=""+cfg.buscarNegocio(jsonObj);
		}


		if (action.equals( "salvarSolicitacaoPessoa")) {
			SolicitacaoPessoaAction cfg = new SolicitacaoPessoaAction();
			result=""+cfg.salvarSolicitacaoPessoa(jsonObj);
		}


		if (action.equals( "buscarSolicitacaoPessoa")) {
			SolicitacaoPessoaAction cfg = new SolicitacaoPessoaAction();
			result=""+cfg.buscarSolicitacaoPessoa(jsonObj);
		}


//		if (action.equals( "buscarLancamentosSolicitacaoPessoaStatus")) {
//			SolicitacaoPessoaAction cfg = new SolicitacaoPessoaAction();
//			result=""+cfg.buscarLancamentosSolicitacaoPessoaStatus(jsonObj);
//		}


		if (action.equals( "removerSolicitacaoPessoa")) {
			SolicitacaoPessoaAction cfg = new SolicitacaoPessoaAction();
			//result=""+cfg.removerSolicitacaoPessoa(jsonObj);
		}	


		if (action.equals("salvarSolicitacaoOrganizacao")) {
			SolicitacaoOrganizacaoAction cfg = new SolicitacaoOrganizacaoAction();
			result=""+cfg.salvarSolicitacaoOrganizacao(jsonObj);
		}


		if (action.equals( "buscarSolicitacaoOrganizacao")) {
			SolicitacaoOrganizacaoAction cfg = new SolicitacaoOrganizacaoAction();
			result=""+cfg.buscarSolicitacaoOrganizacao(jsonObj);
		}
			
		if (action.equals("buscarSolicitacoesStatusTipo")) {
			int tipoSolicitacao = jsonObj.get("tipoSolicitacao").getAsInt();
			if (tipoSolicitacao==0)
			{
				SolicitacaoPessoaAction cfg = new SolicitacaoPessoaAction();
				result=""+cfg.buscarLancamentosSolicitacaoPessoaStatus(jsonObj);
			}
			else
			{
			SolicitacaoOrganizacaoAction cfg = new SolicitacaoOrganizacaoAction();
			result=""+cfg.buscarLancamentosSolicitacaoOrganizacaoStatus(jsonObj);
			}
			
		}

		if (action.equals( "removerSolicitacaoOrganizacao")) {
			SolicitacaoOrganizacaoAction cfg = new SolicitacaoOrganizacaoAction();
		//	result=""+cfg.removerSolicitacaoOrganizacao(jsonObj);
		}	

		if (action.equals( "buscarColaboradores")) {
			ColaboradorAction cfg = new ColaboradorAction();
			result=""+cfg.buscarColaboradores(jsonObj);
		}

		if (action.equals( "buscarColaborador")) {
			ColaboradorAction cfg = new ColaboradorAction();
			result=""+cfg.buscarColaborador(jsonObj);
		}

		if (action.equals( "buscarTodosColaboradores")) {
			ColaboradorAction cfg = new ColaboradorAction();
			result=""+cfg.buscarTodosColaboradores(jsonObj);
		}

		if (action.equals( "buscarOrganizacoes")) {
			OrganizacaoAction cfg = new OrganizacaoAction();
			result=""+cfg.buscarOrganizacoes(jsonObj);
		}

		if (action.equals( "buscarTodasPessoasOrganizacoes")) {
			PessoaAction cfg = new PessoaAction();
			result=""+cfg.buscarTodasPessoasOrganizacoes(jsonObj);
		}

		if (action.equals( "buscarPessoasOrganizacao")) {
			PessoaAction cfg = new PessoaAction();
			result=""+cfg.buscarPessoasOrganizacao(jsonObj);
		}

		if (action.equals( "buscarPessoasNegocioProprietario")) {
			PessoaAction cfg = new PessoaAction();
			result=""+cfg.buscarPessoasNegocioProprietario(jsonObj);
		}		
		
		if (action.equals( "buscarPessoasNegociosProprietario")) {
			PessoaAction cfg = new PessoaAction();
			result=""+cfg.buscarPessoasNegociosProprietario(jsonObj);
		}		
		
		
		
		
		if (action.equals("avaliarSolicitacao")) {
			int tipoSolicitacao = jsonObj.get("tipoSolicitacao").getAsInt();
			if (tipoSolicitacao==0)
			{
				SolicitacaoPessoaAction cfg = new SolicitacaoPessoaAction();
				result=""+cfg.avaliarSolicitacaoPessoa(jsonObj);
			}
			else
			{
				SolicitacaoOrganizacaoAction cfg = new SolicitacaoOrganizacaoAction();
				result=""+cfg.avaliarSolicitacaoOrganizacao(jsonObj);	

			}
		}

		if (action.equals( "buscarPessoasProprietario")) {
			PessoaAction cfg = new PessoaAction();
			result=""+cfg.buscarPessoasProprietario(jsonObj);
		}
		
		if (action.equals( "buscarPessoascomSolicitacao")) {
			PessoaAction cfg = new PessoaAction();
			result=""+cfg.buscarPessoascomSolicitacao(jsonObj);
		}

		//busca as organizações que existem pessoas
		if (action.equals( "buscaOrganizacaocomPessoa")) {
			OrganizacaoAction cfg = new OrganizacaoAction();
			result=""+cfg.buscaOrganizacaocomPessoa(jsonObj);
		}
		
		if (action.equals( "buscarOrganizacoescomSolicitacao")) {
			OrganizacaoAction cfg = new OrganizacaoAction();
			result=""+cfg.buscarOrganizacoescomSolicitacao(jsonObj);
		}

		if (action.equals( "buscaOrganizacaocomNegocio")) {
			OrganizacaoAction cfg = new OrganizacaoAction();
			result=""+cfg.buscaOrganizacaocomNegocio(jsonObj);
		}


		System.out.println(result);
		return result;        

	}


}