package webServiceController;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import Exceptions.ProxyNaoIniciadoException;
import action.ApplicationAction;

public class WSApplicationController {

	public String process(String jsonRecebido) throws FileNotFoundException, IOException, ProxyNaoIniciadoException, SQLException{
		String result="";
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObj = (JsonObject)jsonParser.parse(jsonRecebido);
		String action = jsonObj.get("action").getAsString();

		if (action.equals( "verificaConexao")) {
			ApplicationAction cfg = new ApplicationAction();
			try {
				result=""+cfg.verificaConexao(jsonObj);
			} catch (ParseException e) {
				//e.printStackTrace();
			}
		}
		return result;        
	}

}