package webServiceController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import action.ColaboradorAction;


public class WSCadastroAcessoController {

	public String process(String jsonRecebido) throws Exception {
		String result="";
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObj = (JsonObject)jsonParser.parse(jsonRecebido);
		String action = jsonObj.get("action").getAsString();

		if (action.equals( "cadastroAcesso")){
			ColaboradorAction cfg = new ColaboradorAction();
			result=""+cfg.cadastroAcesso(jsonObj);
		} 
		System.out.println(result);
		return result;        		
	}
}