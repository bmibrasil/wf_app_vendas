package Util;

import java.util.Date;
import java.util.StringTokenizer;

public class Logger {
	public static boolean echo=true;
	public static Date hora = new Date();
	
	public static void rawlog(String msg){
		if (echo) {
			System.out.println(msg);
		}
	}
	
	public static void log(String msg){
		if (echo) {
			System.out.println(time()+" ("+Runtime.getRuntime().freeMemory()/(1024*1024) + "MB) " + msg);
			System.out.println("Elapsed time: "+crono());
		}
	}
	
	public static void memDisp(){
		if (echo) {
			System.out.println("Available memory: "+Runtime.getRuntime().freeMemory()/(1024*1024) + "MB");
		}
	}
	
	public static String crono(){
		Date nova = new Date();
		long l = nova.getTime() - hora.getTime();
		hora = nova;
		return "("+l+")";
	}

	public static String time(){
		return ""+new Date();
	}
	
	public static String[][] names;
	public static String[][] rel;

	public static String getName(String variavel){
		String result = getDescricao(variavel);
		if (!result.equals("nome n�o encontrado!")){
			String name="";
			StringTokenizer st = new StringTokenizer(result,"|");
			while (st.hasMoreTokens()){
				name = st.nextToken().trim();
			}
			result = name;
		}
		return result;
	}
	
	public static String getDescricao(String variavel){
		String result = "nome n�o encontrado!";
		for (int i=0;i<names.length;i++){
			if (names[i][0].equals(variavel)) result = names[i][1];
		}
		return result;
	}
}

