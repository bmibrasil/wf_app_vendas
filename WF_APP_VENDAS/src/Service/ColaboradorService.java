package Service;

import java.sql.Time;
import java.util.Hashtable;
import java.util.List;

import Beans.Colaborador;
import Dao.ColaboradorDao;


public class ColaboradorService {

	//	public List<Colaborador> getColaboradorsHolding(int turma){
	//		ColaboradorDao dao = new ColaboradorDao();
	//		List<Colaborador> list = dao.b(turma);
	//		return list;
	//	}

	public void delete(int id){
		ColaboradorDao dao = new ColaboradorDao();
		dao.delete(id);
	}




	public List<Colaborador> findByFields(Hashtable<String,Object> hash) {
		List<Colaborador> result = null;
		ColaboradorDao dao = new ColaboradorDao();
		result = dao.findByFields(hash);
		return result;
	}

	public List<Colaborador> findByField(String fieldName, Object content) {
		List<Colaborador> result = null;
		ColaboradorDao dao = new ColaboradorDao();
		result = dao.findByField(fieldName, content);
		return result;
	}

	public List<Colaborador> findByFieldOrdered(String fieldName, Object content, String fieldNameOrder) {
		List<Colaborador> result = null;
		ColaboradorDao dao = new ColaboradorDao();
		result = dao.findByFieldOrdered(fieldName, content,fieldNameOrder);
		return result;
	}
	public void update(Colaborador Colaborador)
	{
		ColaboradorDao dao = new ColaboradorDao();
		dao.save(Colaborador);
	}

	public void insert(String nome,  double cargaHoraria, double horasEsperadas, Time realizado, double valor, String email, String senha){
		Colaborador obj = new Colaborador();
		obj.setNome(nome);
		obj.setCargaHoraria(cargaHoraria);
		obj.setHorasEsperadas(horasEsperadas);
		obj.setRealizado(realizado);
		obj.setValor(valor);
		obj.setEmail(email);
		obj.setSenha(senha);
		ColaboradorDao dao = new ColaboradorDao();
		dao.insert(obj);
	}


	public void save(int id, String nome,  double cargaHoraria, double horasEsperadas, Time realizado, double valor, String email, String senha){
		Colaborador obj = new Colaborador();
		obj.setId(id);
		obj.setNome(nome);
		obj.setCargaHoraria(cargaHoraria);
		obj.setHorasEsperadas(horasEsperadas);
		obj.setRealizado(realizado);
		obj.setValor(valor);
		obj.setEmail(email);
		obj.setSenha(senha);
		ColaboradorDao dao = new ColaboradorDao();
		dao.save(obj);
	}


	public List<Colaborador> findAll(){
		List<Colaborador> result = null;
		ColaboradorDao dao = new ColaboradorDao();
		result = dao.findAll();
		return result;
	}
	

	public List<Colaborador> findAllOrdered(){
		List<Colaborador> result = null;
		ColaboradorDao dao = new ColaboradorDao();
		result = dao.findAllOrdered("nome");
		return result;
	}
	
	public List<Colaborador> buscaTodosColaborador(){
		List<Colaborador> result = null;
		ColaboradorDao dao = new ColaboradorDao();
		result = dao.findAll();
		return result;
	}


	public List<Colaborador> buscaColaboradorEquipe(int colaborador){
		List<Colaborador> result = null;
		ColaboradorDao dao = new ColaboradorDao();
		result = dao.buscaColaboradorEquipe(colaborador);
		return result;
	}

	public int bloqueiaColaborador(String acesso, String bloqueio){
		ColaboradorDao dao = new ColaboradorDao();
		dao.bloqueiaColaborador(acesso,bloqueio);
		return 0;
	}

	
}
