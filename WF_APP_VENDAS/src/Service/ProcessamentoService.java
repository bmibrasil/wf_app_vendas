package Service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import Beans.Processamento;
import Dao.ProcessamentoDao;

public class ProcessamentoService {

	public static int counter=0;
	public int grupo;
	
	public ProcessamentoService(int grupo){
		this.grupo = grupo;
	}
	
	public boolean exists(int grupo, String campo){
		ProcessamentoDao dao = new ProcessamentoDao();
		Hashtable<String, Object> hash = new Hashtable<String,Object>();
		hash.put("campo", campo);
		hash.put("grupo", grupo);
		List<Processamento>list = dao.findByFields(hash);
		return list.size()>0;
	}
	
	public Processamento getProcessamento(int grupo, String campo){
		Processamento resultItem = null;
		List<Processamento> result = new ArrayList<Processamento>();
		ProcessamentoDao dao = new ProcessamentoDao();
		Hashtable<String, Object> hash = new Hashtable<String,Object>();
		hash.put("campo", campo);
		hash.put("grupo", grupo);
		result = dao.findByFields(hash);
		if (result.size()==0) {
			
			String s = campo;
			String r = "";
			StringTokenizer st = new StringTokenizer(s,"_");
			while (st.hasMoreTokens()){
				if (r.length()>0) {
					r=r+"_"+st.nextToken();
				} else {
					r=st.nextToken();
				}
				
				//verificar se o campo j� existe
				if (!exists(grupo,r)) {
					//se n�o incluir
					resultItem = new Processamento();
					resultItem.setGrupo(grupo);
					resultItem.setCampo(r);
					resultItem.setValor(0);
					insert(resultItem);
				}
			}
			resultItem = getProcessamento(resultItem.getGrupo(),resultItem.getCampo());
		} else {
			resultItem = result.get(0);
		}
		return resultItem;
	}

	public List<Processamento> getHerdeiros(Processamento e, int grupo) {
		List<Processamento> result;
		ProcessamentoDao dao = new ProcessamentoDao();
		result = dao.getHerdeiros(e, grupo);
		return result;
	}

	public List<Processamento> getHerdeiros(String campo, int grupo) {
		List<Processamento> result;
		ProcessamentoDao dao = new ProcessamentoDao();
		result = dao.getHerdeiros(campo, grupo);
		return result;
	}

	public void update(Processamento from, Processamento to){
		to.setCampo(from.getCampo());
		to.setConteudo(from.getConteudo());
		to.setDescricao(from.getDescricao());
		to.setGrupo(from.getGrupo());
		to.setValor(from.getValor());
	}

	public void save(Processamento e){
		ProcessamentoDao dao = new ProcessamentoDao();
		dao.save(e);
	}

	public void insert(Processamento e){
		ProcessamentoDao dao = new ProcessamentoDao();
		dao.insert(e);
	}

	public void save(Processamento e, double novoValor){
		ProcessamentoDao dao = new ProcessamentoDao();
		e.setValor(novoValor);
		dao.save(e);
	}

}
