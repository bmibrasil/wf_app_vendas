package Service;

import java.util.Hashtable;
import java.util.List;

import Beans.Parametros;
import Dao.ParametrosDao;


public class ParametrosService {

//	public List<Parametros> getParametrossHolding(int turma){
//		ParametrosDao dao = new ParametrosDao();
//		List<Parametros> list = dao.b(turma);
//		return list;
//	}

	public void delete(int id){
		ParametrosDao dao = new ParametrosDao();
		dao.delete(id);
	}
	
	
	
	
	public List<Parametros> findByFields(Hashtable<String,Object> hash) {
		List<Parametros> result = null;
		ParametrosDao dao = new ParametrosDao();
		result = dao.findByFields(hash);
		return result;
	}
	
	public List<Parametros> findByField(String fieldName, Object content) {
		List<Parametros> result = null;
		ParametrosDao dao = new ParametrosDao();
		result = dao.findByField(fieldName, content);
		return result;
	}
	
	public List<Parametros> findByFieldOrdered(String fieldName, Object content,String fieldNameOrder) {
		List<Parametros> result = null;
		ParametrosDao dao = new ParametrosDao();
		result = dao.findByFieldOrdered(fieldName, content,fieldNameOrder);
		return result;
	}
	
	public void update(Parametros Parametros)
	{
	  ParametrosDao dao = new ParametrosDao();
	  dao.save(Parametros);
	 }
	
	public void insert(String nome, String valor){
		Parametros obj = new Parametros();
		obj.setNomeparametro(nome);
		obj.setValor(valor);
		ParametrosDao dao = new ParametrosDao();
		dao.insert(obj);
	}
	
	
	public void save(int id, String nome, String valor){
		Parametros obj = new Parametros();
		obj.setId(id);
		obj.setNomeparametro(nome);
		obj.setValor(valor);
		ParametrosDao dao = new ParametrosDao();
		dao.save(obj);
	}
	
	public List<Parametros> findAllOrdered(String fieldNameOrder){
		List<Parametros> result = null;
		ParametrosDao dao = new ParametrosDao();
		result = dao.findAllOrdered(fieldNameOrder);
		return result;
	}
	
	public List<Parametros> findAll(){
		List<Parametros> result = null;
		ParametrosDao dao = new ParametrosDao();
		result = dao.findAll();
		return result;
	}
	

}
