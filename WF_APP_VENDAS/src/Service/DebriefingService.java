package Service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import Beans.Debriefing;
import Dao.DebriefingDao;

public class DebriefingService {
	
	public List<Debriefing> findByField(String campo, Object conteudo){
		List<Debriefing> result = new ArrayList<Debriefing>();
		DebriefingDao dao = new DebriefingDao();
		result = dao.findByField(campo, conteudo);
		return result;
	}
	
	public List<Debriefing> findAll(){
		List<Debriefing> result = new ArrayList<Debriefing>();
		DebriefingDao dao = new DebriefingDao();
		result = dao.findAll();
		return result;
	}

	public void insert(Debriefing debriefing){
		DebriefingDao dao = new DebriefingDao();
		dao.insert(debriefing);
	}
	
	public void save(Debriefing debriefing){
		DebriefingDao dao = new DebriefingDao();
		dao.save(debriefing);
	}
	
	public List<Debriefing> findByFields(Hashtable<String,Object> hash){
		List<Debriefing> result = new ArrayList<Debriefing>();
		DebriefingDao dao = new DebriefingDao();
		result = dao.findByFields(hash);
		return result;
	}
	
	public int getId(Debriefing debriefing){
		int result = -1;
		Hashtable<String,Object> hash = new Hashtable<String,Object>();
		hash.put("turma", debriefing.getTurma());
		hash.put("nome", debriefing.getNome());
		hash.put("link", debriefing.getLink());
	//	hash.put("descricao", debriefing.getDescricao());
	//	hash.put("status", debriefing.getStatus());
//		hash.put("nivelInicial", debriefing.getNivelInicial());
//		hash.put("nivelFinal", debriefing.getNivelFinal());
		hash.put("empresa", debriefing.getEmpresa());
		List<Debriefing> list = findByFields(hash);
		if (list.size()>0){
			result = list.get(0).getId();
		}
		return result;
	}
}
