package Service;

import java.util.Hashtable;
import java.util.List;

import Beans.SolicitacaoOrganizacao;
import Dao.SolicitacaoOrganizacaoDao;


public class SolicitacaoOrganizacaoService {

//	public List<SolicitacaoOrganizacao> getSolicitacaoOrganizacaosHolding(int turma){
//		SolicitacaoOrganizacaoDao dao = new SolicitacaoOrganizacaoDao();
//		List<SolicitacaoOrganizacao> list = dao.b(turma);
//		return list;
//	}

	public void delete(int id){
		SolicitacaoOrganizacaoDao dao = new SolicitacaoOrganizacaoDao();
		dao.delete(id);
	}
	
	
	
	
	public List<SolicitacaoOrganizacao> findByFields(Hashtable<String,Object> hash) {
		List<SolicitacaoOrganizacao> result = null;
		SolicitacaoOrganizacaoDao dao = new SolicitacaoOrganizacaoDao();
		result = dao.findByFields(hash);
		return result;
	}
	
	public List<SolicitacaoOrganizacao> findByField(String fieldName, Object content) {
		List<SolicitacaoOrganizacao> result = null;
		SolicitacaoOrganizacaoDao dao = new SolicitacaoOrganizacaoDao();
		result = dao.findByField(fieldName, content);
		return result;
	}
	
	
	public List<SolicitacaoOrganizacao> findByFieldOrdered(String fieldName, Object content,String fieldNameOrder) {
		List<SolicitacaoOrganizacao> result = null;
		SolicitacaoOrganizacaoDao dao = new SolicitacaoOrganizacaoDao();
		result = dao.findByFieldOrdered(fieldName, content,fieldNameOrder);
		return result;
	}
	
	public List<SolicitacaoOrganizacao> findByFieldsOrdered(Hashtable<String,Object> hash,String fieldNameOrder) {
		List<SolicitacaoOrganizacao> result = null;
		SolicitacaoOrganizacaoDao dao = new SolicitacaoOrganizacaoDao();
		result = dao.findByFieldsOrdered(hash,fieldNameOrder);
		return result;
	}
	
	
	public void update(SolicitacaoOrganizacao SolicitacaoOrganizacao)
	{
	  SolicitacaoOrganizacaoDao dao = new SolicitacaoOrganizacaoDao();
	  dao.save(SolicitacaoOrganizacao);
	 }
	
	public void insert(String nome, String fantasia, String cnpj,String logradouro, int numero, String bairro, String cidade, String uf, 
			String cep,String segmento, int statusSolicitacaoOrganizacao, String observacao, String dataSolicitacaoOrganizacao, String proprietario)
	{
		SolicitacaoOrganizacao obj = new SolicitacaoOrganizacao();
		obj.setNome(nome);
		obj.setCnpj(cnpj);
		obj.setLogradouro(logradouro);
		obj.setBairro(bairro);
		obj.setCidade(cidade);
		obj.setUf(uf);
		obj.setCep(cep);
		obj.setSegmento(segmento);
		obj.setStatusSolicitacaoOrganizacao(statusSolicitacaoOrganizacao);
		obj.setObservacao(observacao);
		obj.setDataSolicitacaoOrganizacao(dataSolicitacaoOrganizacao);
		obj.setProprietario(proprietario);
		SolicitacaoOrganizacaoDao dao = new SolicitacaoOrganizacaoDao();
		dao.insert(obj);
	}
	
	
	public void save(String id,String nome, String fantasia,String cnpj,String logradouro, int numero, String bairro, String cidade, String uf, 
			 String cep,String segmento, int statusSolicitacaoOrganizacao, String observacao, String dataSolicitacaoOrganizacao,String proprietario){
		SolicitacaoOrganizacao obj = new SolicitacaoOrganizacao();
		obj.setId(id);
		obj.setNome(nome);
		obj.setCnpj(cnpj);
		obj.setLogradouro(logradouro);
		obj.setBairro(bairro);
		obj.setCidade(cidade);
		obj.setUf(uf);
		obj.setCep(cep);
		obj.setSegmento(segmento);
		obj.setStatusSolicitacaoOrganizacao(statusSolicitacaoOrganizacao);
		obj.setObservacao(observacao);
		obj.setDataSolicitacaoOrganizacao(dataSolicitacaoOrganizacao);
		obj.setProprietario(proprietario);		
		SolicitacaoOrganizacaoDao dao = new SolicitacaoOrganizacaoDao();
		dao.save(obj);
	}
	
	
	public List<SolicitacaoOrganizacao> findAll(){
		List<SolicitacaoOrganizacao> result = null;
		SolicitacaoOrganizacaoDao dao = new SolicitacaoOrganizacaoDao();
		result = dao.findAll();
		return result;
	}
	
	public List<SolicitacaoOrganizacao> findAllOrdered(String fieldNameOrder){
		List<SolicitacaoOrganizacao> result = null;
		SolicitacaoOrganizacaoDao dao = new SolicitacaoOrganizacaoDao();
		result = dao.findAllOrdered(fieldNameOrder);
		return result;
	}

}
