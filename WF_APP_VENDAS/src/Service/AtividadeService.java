package Service;

import java.util.Hashtable;
import java.util.List;

import Beans.Atividade;
import Dao.AtividadeDao;


public class AtividadeService {



	public void delete(int id){
		AtividadeDao dao = new AtividadeDao();
		dao.delete(id);
	}
	
	
	
	
	public List<Atividade> findByFields(Hashtable<String,Object> hash) {
		List<Atividade> result = null;
		AtividadeDao dao = new AtividadeDao();
		result = dao.findByFields(hash);
		return result;
	}
	
	public List<Atividade> findByField(String fieldName, Object content) {
		List<Atividade> result = null;
		AtividadeDao dao = new AtividadeDao();
		result = dao.findByField(fieldName, content);
		return result;
	}
	
	public List<Atividade> findByFieldOrdered(String fieldName, Object content, String fieldNameOrder) {
		List<Atividade> result = null;
		AtividadeDao dao = new AtividadeDao();
		result = dao.findByFieldOrdered(fieldName, content,fieldNameOrder);
		return result;
	}
	
	public List<Atividade> findByFieldsOrdered(Hashtable<String,Object> hash, String fieldNameOrder) {
		List<Atividade> result = null;
		AtividadeDao dao = new AtividadeDao();
		result = dao.findByFieldsOrdered(hash,fieldNameOrder);
		return result;
	}
	
	
	public void update(Atividade Projeto)
	{
	  AtividadeDao dao = new AtividadeDao();
	  dao.save(Projeto);
	 }
	
	public void insert(String negocio, String dataAtividade, String diaSemana, String descricao, String tipoAtividade, int conexaoAtividade, String nomeProprietario){
		Atividade obj = new Atividade();
		obj.setNegocio(negocio);
		obj.setDataAtividade(dataAtividade);
		obj.setTipoAtividade(tipoAtividade);
		obj.setDescricao(descricao);
		obj.setDiaSemana(diaSemana);
		obj.setNomeProprietario(nomeProprietario);
		AtividadeDao dao = new AtividadeDao();
		dao.insert(obj);
	}
	
	
	public void save(String id, String negocio, String dataAtividade,String diaSemana, String descricao, String tipoAtividade, int conexaoAtividade,String nomeProprietario){
		Atividade obj = new Atividade();
		obj.setId(id);
		obj.setNegocio(negocio);
		obj.setDataAtividade(dataAtividade);
		obj.setTipoAtividade(tipoAtividade);
		obj.setDiaSemana(diaSemana);
		obj.setDescricao(descricao);
		obj.setNomeProprietario(nomeProprietario);
		AtividadeDao dao = new AtividadeDao();
		dao.save(obj);
	}
	
	
	public List<Atividade> findAll(){
		List<Atividade> result = null;
		AtividadeDao dao = new AtividadeDao();
		result = dao.findAll();
		return result;
	}
	
	public List<Atividade> findAllOrdered(String fieldNameOrder){
		List<Atividade> result = null;
		AtividadeDao dao = new AtividadeDao();
		result = dao.findAllOrdered(fieldNameOrder);
		return result;
	}
	

}
