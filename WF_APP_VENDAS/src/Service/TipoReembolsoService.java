package Service;

import java.util.Hashtable;
import java.util.List;

import Beans.TipoReembolso;
import Dao.TipoReembolsoDao;


public class TipoReembolsoService {

//	public List<TipoReembolso> getTipoReembolsosHolding(int turma){
//		TipoReembolsoDao dao = new TipoReembolsoDao();
//		List<TipoReembolso> list = dao.b(turma);
//		return list;
//	}

	public void delete(int id){
		TipoReembolsoDao dao = new TipoReembolsoDao();
		dao.delete(id);
	}
	
	
	
	
	public List<TipoReembolso> findByFields(Hashtable<String,Object> hash) {
		List<TipoReembolso> result = null;
		TipoReembolsoDao dao = new TipoReembolsoDao();
		result = dao.findByFields(hash);
		return result;
	}
	
	public List<TipoReembolso> findByField(String fieldName, Object content) {
		List<TipoReembolso> result = null;
		TipoReembolsoDao dao = new TipoReembolsoDao();
		result = dao.findByField(fieldName, content);
		return result;
	}
	
	public List<TipoReembolso> findByFieldOrdered(String fieldName, Object content,String fieldNameOrder) {
		List<TipoReembolso> result = null;
		TipoReembolsoDao dao = new TipoReembolsoDao();
		result = dao.findByFieldOrdered(fieldName, content,fieldNameOrder);
		return result;
	}
	
	
	
	public void update(TipoReembolso TipoReembolso)
	{
	  TipoReembolsoDao dao = new TipoReembolsoDao();
	  dao.save(TipoReembolso);
	 }
	
	public void insert(String nome){
		TipoReembolso obj = new TipoReembolso();
		obj.setNome(nome);
		TipoReembolsoDao dao = new TipoReembolsoDao();
		dao.insert(obj);
	}
	
	
	public void save(int id, String nome){
		TipoReembolso obj = new TipoReembolso();
		obj.setId(id);
		obj.setNome(nome);
		TipoReembolsoDao dao = new TipoReembolsoDao();
		dao.save(obj);
	}
	
	
	public List<TipoReembolso> findAll(){
		List<TipoReembolso> result = null;
		TipoReembolsoDao dao = new TipoReembolsoDao();
		result = dao.findAll();
		return result;
	}
	
	public List<TipoReembolso> findAllOrdered(String fieldNameorder){
		List<TipoReembolso> result = null;
		TipoReembolsoDao dao = new TipoReembolsoDao();
		result = dao.findAllOrdered(fieldNameorder);
		return result;
	}
	

}
