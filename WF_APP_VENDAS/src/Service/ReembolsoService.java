package Service;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.TimeZone;

import org.primefaces.json.JSONObject;

import com.google.gson.JsonObject;

import Beans.Reembolso;
import Beans.Valor;
import Dao.ReembolsoDao;
import Dao.ValorDao;


public class ReembolsoService {



	public int buscarArquivos(String reembolso)
	{
		int tamanho=0;
		try{
				Class.forName("com.mysql.jdbc.Driver");
				Connection con=DriverManager.getConnection("jdbc:mysql://localhost/timesheet","root","octo3417");

					byte b[];
				Blob blob;

				PreparedStatement ps=con.prepareStatement("select * from arquivoreembolso where numeroReembolso="+reembolso); 
				ResultSet rs=ps.executeQuery();
				FileOutputStream fos=null;
				while(rs.next()){
					
					blob=rs.getBlob("arquivo");
					b=blob.getBytes(1,(int)blob.length());
					File file=new File("C:\\servers\\glassfish4\\glassfish\\domains\\domain1\\applications\\TimeSheet\\arquivosReembolso\\reembolso_"+reembolso+"_"+tamanho+".png");
					fos=new FileOutputStream(file);
				
					fos.write(b);
					tamanho=tamanho+1;
				}

				ps.close();
				if (fos!=null)
				 {fos.close();}
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		
		return tamanho;
	}


	public void salvarReembolso(int id,String data, int colaborador,String diaSemana, int cliente,int  projeto,int  tipo,  int etapa, String local, int quilometragem,String origem,String destino, int quantidadeDias, double valorsolicitado,double valorreembolso,String observacao,int status) throws Exception{
		int result;
		Reembolso reembolsoObj = new Reembolso();

		reembolsoObj.setId(id);
		reembolsoObj.setData(data);
		reembolsoObj.setColaborador(colaborador);
		reembolsoObj.setDiaSemana(diaSemana);
		reembolsoObj.setCliente(cliente);
		reembolsoObj.setProjeto(projeto);
		reembolsoObj.setTipo(tipo);
		reembolsoObj.setLocal(local);
		reembolsoObj.setQuilometragem(quilometragem);
		reembolsoObj.setOrigem(origem);
		reembolsoObj.setDestino(destino);
		reembolsoObj.setQuantidadeDias(quantidadeDias);
		reembolsoObj.setValorSolicitado(valorsolicitado);
		reembolsoObj.setValorReembolso(valorreembolso);
		reembolsoObj.setObservacao(observacao);
		reembolsoObj.setStatusreembolso(status);
		reembolsoObj.setEtapa(etapa);

		//Verifica se j� existe um valor com este nome nesta turma


		ReembolsoDao dao = new ReembolsoDao();


		List<Reembolso> list = dao.findByField("id",id);


		//Insere grupo caso n�o exista outro com o mesmo nome
		if (list.size()>0){
			//Grupo j� existe, n�o pode ser criado
			reembolsoObj.setId(list.get(0).getId());
			dao.save(reembolsoObj);
		} else {
			dao.insert(reembolsoObj);
		}



	}


	public void avaliarReembolso(JsonObject jsonObj)
	{

		int idreembolso     = jsonObj.get("reembolso").getAsInt();
		int status     = jsonObj.get("status").getAsInt();
		String justificativa     = jsonObj.get("justificativa").getAsString();


		List<Reembolso> lista = findByField("id", idreembolso);
		ReembolsoService reembolsoService = new ReembolsoService(); 
		Reembolso reembolso = new Reembolso();
		reembolso=lista.get(0);
		reembolso.setStatusreembolso(status);
		reembolso.setJustificativa(justificativa);
		reembolsoService.save(reembolso);      

	}

	public void delete(int id){
		ReembolsoDao dao = new ReembolsoDao();
		dao.delete(id);
	}




	public List<Reembolso> findByFields(Hashtable<String,Object> hash) {
		List<Reembolso> result = null;
		ReembolsoDao dao = new ReembolsoDao();
		result = dao.findByFields(hash);
		return result;
	}

	public List<Reembolso> findByFieldsOrdered(Hashtable<String,Object> hash,String fieldNameOrder) {
		List<Reembolso> result = null;
		ReembolsoDao dao = new ReembolsoDao();
		result = dao.findByFieldsOrdered(hash, fieldNameOrder);
		return result;
	}



	public List<Reembolso> findByField(String fieldName, Object content) {
		List<Reembolso> result = null;
		ReembolsoDao dao = new ReembolsoDao();
		result = dao.findByField(fieldName, content);
		return result;
	}


	public List<Reembolso> findByFieldOrdered(String fieldName, Object content, String fieldNameOrder) {
		List<Reembolso> result = null;
		ReembolsoDao dao = new ReembolsoDao();
		result = dao.findByFieldOrdered(fieldName, content,fieldNameOrder);
		return result;
	}


	public void update(Reembolso Reembolso)
	{
		ReembolsoDao dao = new ReembolsoDao();
		dao.save(Reembolso);
	}

	public void insert(Reembolso obj)
	{
		ReembolsoDao dao = new ReembolsoDao();
		dao.insert(obj);
	}


	public void save(Reembolso obj)
	{

		ReembolsoDao dao = new ReembolsoDao();
		dao.save(obj);
	}


	public List<Reembolso> findAll(){
		List<Reembolso> result = null;
		ReembolsoDao dao = new ReembolsoDao();
		result = dao.findAll();
		return result;
	}


	public List<Reembolso> findAllOrdered(String campo){
		List<Reembolso> result = null;
		ReembolsoDao dao = new ReembolsoDao();
		result = dao.findAllOrdered(campo);
		return result;
	}

	public ResultSet consolidaReembolso(String listaColaboradores, String periodo)
	{
		ReembolsoDao timeSheetDao= new ReembolsoDao();
		return timeSheetDao.consolidaReembolso(listaColaboradores,periodo);
	}


	public ResultSet buscaResumoHorasProjetoPeriodo(String periodo)
	{
		ReembolsoDao timeSheetDao= new ReembolsoDao();
		return timeSheetDao.buscaResumoHorasProjetoPeriodo(periodo);
	}

	public ResultSet buscaResumoHorasProjetoColaboradorTipoHoraPeriodo(String periodo)
	{
		ReembolsoDao timeSheetDao= new ReembolsoDao();
		return timeSheetDao.buscaResumoHorasProjetoColaboradorTipoHoraPeriodo(periodo);
	}


	public ResultSet buscaLancamentosColaboradorPeriodo(String dataInicio, String dataFim) 
	{
		ReembolsoDao timeSheetDao= new ReembolsoDao();
		return timeSheetDao.buscaLancamentosColaboradorPeriodo(dataInicio,dataFim); 
	}



}
