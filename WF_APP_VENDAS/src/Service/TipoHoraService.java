package Service;

import java.util.Hashtable;
import java.util.List;

import Beans.TipoHora;
import Dao.TipoHoraDao;


public class TipoHoraService {

//	public List<TipoHora> getTipoHorasHolding(int turma){
//		TipoHoraDao dao = new TipoHoraDao();
//		List<TipoHora> list = dao.b(turma);
//		return list;
//	}

	public void delete(int id){
		TipoHoraDao dao = new TipoHoraDao();
		dao.delete(id);
	}
	
	
	
	
	public List<TipoHora> findByFields(Hashtable<String,Object> hash) {
		List<TipoHora> result = null;
		TipoHoraDao dao = new TipoHoraDao();
		result = dao.findByFields(hash);
		return result;
	}
	
	public List<TipoHora> findByField(String fieldName, Object content) {
		List<TipoHora> result = null;
		TipoHoraDao dao = new TipoHoraDao();
		result = dao.findByField(fieldName, content);
		return result;
	}
	
	public List<TipoHora> findByFieldOrdered(String fieldName, Object content,String fieldNameOrder) {
		List<TipoHora> result = null;
		TipoHoraDao dao = new TipoHoraDao();
		result = dao.findByFieldOrdered(fieldName, content,fieldNameOrder);
		return result;
	}
	
	
	
	public void update(TipoHora TipoHora)
	{
	  TipoHoraDao dao = new TipoHoraDao();
	  dao.save(TipoHora);
	 }
	
	public void insert(String nome){
		TipoHora obj = new TipoHora();
		obj.setNome(nome);
		TipoHoraDao dao = new TipoHoraDao();
		dao.insert(obj);
	}
	
	
	public void save(int id, String nome){
		TipoHora obj = new TipoHora();
		obj.setId(id);
		obj.setNome(nome);
		TipoHoraDao dao = new TipoHoraDao();
		dao.save(obj);
	}
	
	
	public List<TipoHora> findAll(){
		List<TipoHora> result = null;
		TipoHoraDao dao = new TipoHoraDao();
		result = dao.findAll();
		return result;
	}
	
	public List<TipoHora> findAllOrdered(String fieldNameorder){
		List<TipoHora> result = null;
		TipoHoraDao dao = new TipoHoraDao();
		result = dao.findAllOrdered(fieldNameorder);
		return result;
	}
	

}
