package Service;

import java.util.Hashtable;
import java.util.List;

import Beans.Periodo;
import Dao.PeriodoDao;


public class PeriodoService {

//	public List<Periodo> getPeriodosHolding(int turma){
//		PeriodoDao dao = new PeriodoDao();
//		List<Periodo> list = dao.b(turma);
//		return list;
//	}

	public void delete(int id){
		PeriodoDao dao = new PeriodoDao();
		dao.delete(id);
	}
	
	
	
	
	public List<Periodo> findByFields(Hashtable<String,Object> hash) {
		List<Periodo> result = null;
		PeriodoDao dao = new PeriodoDao();
		result = dao.findByFields(hash);
		return result;
	}
	
	public List<Periodo> findByField(String fieldName, Object content) {
		List<Periodo> result = null;
		PeriodoDao dao = new PeriodoDao();
		result = dao.findByField(fieldName, content);
		return result;
	}
	
	public List<Periodo> findByFieldOrdered(String fieldName, Object content,String fieldNameOrder) {
		List<Periodo> result = null;
		PeriodoDao dao = new PeriodoDao();
		result = dao.findByFieldOrdered(fieldName, content,fieldNameOrder);
		return result;
	}
	
	public void update(Periodo Periodo)
	{
	  PeriodoDao dao = new PeriodoDao();
	  dao.save(Periodo);
	 }
	
	public void insert(String datainicial,String datafinal){
		Periodo obj = new Periodo();
		obj.setDatafinal(datafinal);
		obj.setDatainicial(datainicial);
	
		
		PeriodoDao dao = new PeriodoDao();
		dao.insert(obj);
	}
	
	
	public void save(int id, String datainicial,String datafinal){
		Periodo obj = new Periodo();
		obj.setId(id);
		obj.setDatainicial(datainicial);
		obj.setDatafinal(datafinal);
		PeriodoDao dao = new PeriodoDao();
		dao.save(obj);
	}
	
	public List<Periodo> findAllOrdered(String fieldNameOrder){
		List<Periodo> result = null;
		PeriodoDao dao = new PeriodoDao();
		result = dao.findAllOrdered(fieldNameOrder);
		return result;
	}
	
	public List<Periodo> findAll(){
		List<Periodo> result = null;
		PeriodoDao dao = new PeriodoDao();
		result = dao.findAll();
		return result;
	}
	

}
