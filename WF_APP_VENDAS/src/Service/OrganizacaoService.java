package Service;

import java.util.Hashtable;
import java.util.List;

import Beans.Organizacao;
import Dao.OrganizacaoDao;


public class OrganizacaoService {

//	public List<Organizacao> getOrganizacaosHolding(int turma){
//		OrganizacaoDao dao = new OrganizacaoDao();
//		List<Organizacao> list = dao.b(turma);
//		return list;
//	}

	public void delete(int id){
		OrganizacaoDao dao = new OrganizacaoDao();
		dao.delete(id);
	}
	
	
	
	
	public List<Organizacao> findByFields(Hashtable<String,Object> hash) {
		List<Organizacao> result = null;
		OrganizacaoDao dao = new OrganizacaoDao();
		result = dao.findByFields(hash);
		return result;
	}
	
	
	
	public List<Organizacao> findByField(String fieldName, Object content) {
		List<Organizacao> result = null;
		OrganizacaoDao dao = new OrganizacaoDao();
		result = dao.findByField(fieldName, content);
		return result;
	}
	
	
	public List<Organizacao> findByFieldOrdered(String fieldName, Object content,String fieldNameOrder) {
		List<Organizacao> result = null;
		OrganizacaoDao dao = new OrganizacaoDao();
		result = dao.findByFieldOrdered(fieldName, content,fieldNameOrder);
		return result;
	}
	
	public List<Organizacao> findByFieldsOrdered(Hashtable<String,Object> hash,String fieldNameOrder) {
		List<Organizacao> result = null;
		OrganizacaoDao dao = new OrganizacaoDao();
		result = dao.findByFieldsOrdered(hash,fieldNameOrder);
		return result;
	}
	
	public void update(Organizacao Organizacao)
	{
	  OrganizacaoDao dao = new OrganizacaoDao();
	  dao.save(Organizacao);
	 }
	
	public void insert(String nome, String fantasia,String rua, int numero, String bairro, String cidade, String estado, 
			String pais, String cep,String segmento, int estagio, String statusOrganizacao)
	{
		Organizacao obj = new Organizacao();
		obj.setNome(nome);
		obj.setFantasia(fantasia);
		obj.setRua(rua);
		obj.setNumero(numero);
		obj.setBairro(bairro);
		obj.setCidade(cidade);
		obj.setEstado(estado);
		obj.setPais(pais);
		obj.setCep(cep);
		obj.setSegmento(segmento);
		obj.setEstagio(estagio);
		obj.setStatusOrganizacao(statusOrganizacao);
				
		OrganizacaoDao dao = new OrganizacaoDao();
		dao.insert(obj);
	}
	
	
	public void save(String id,String nome, String fantasia,String rua, int numero, String bairro, String cidade, String estado, 
			String pais, String cep,String segmento, int estagio, String statusOrganizacao){
		Organizacao obj = new Organizacao();
		obj.setId(id);
		obj.setNome(nome);
		obj.setFantasia(fantasia);
		obj.setRua(rua);
		obj.setNumero(numero);
		obj.setBairro(bairro);
		obj.setCidade(cidade);
		obj.setEstado(estado);
		obj.setPais(pais);
		obj.setCep(cep);
		obj.setSegmento(segmento);
		obj.setEstagio(estagio);
		obj.setStatusOrganizacao(statusOrganizacao);
		
				
		OrganizacaoDao dao = new OrganizacaoDao();
		dao.save(obj);
	}
	
	
	public List<Organizacao> findAll(){
		List<Organizacao> result = null;
		OrganizacaoDao dao = new OrganizacaoDao();
		result = dao.findAll();
		return result;
	}
	
	public List<Organizacao> findAllOrdered(String fieldNameOrder){
		List<Organizacao> result = null;
		OrganizacaoDao dao = new OrganizacaoDao();
		result = dao.findAllOrdered(fieldNameOrder);
		return result;
	}
	

	public List<Organizacao> buscaOrganizacaocomPessoa(int proprietario){
		List<Organizacao> result = null;
		OrganizacaoDao dao = new OrganizacaoDao();
		result = dao.buscaOrganizacaocomPessoa(proprietario);
		return result;
	}
	
	public List<Organizacao> buscaOrganizacaocomNegocio(int proprietario){
		List<Organizacao> result = null;
		OrganizacaoDao dao = new OrganizacaoDao();
		result = dao.buscaOrganizacaocomPessoa(proprietario);
		return result;
	}
	

}
