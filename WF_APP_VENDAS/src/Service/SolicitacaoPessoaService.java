package Service;


import java.util.Hashtable;
import java.util.List;

import Beans.SolicitacaoPessoa;
import Dao.SolicitacaoPessoaDao;


public class SolicitacaoPessoaService {


	public void delete(int id){
		SolicitacaoPessoaDao dao = new SolicitacaoPessoaDao();
		dao.delete(id);
	}




	public List<SolicitacaoPessoa> findByFields(Hashtable<String,Object> hash) {
		List<SolicitacaoPessoa> result = null;
		SolicitacaoPessoaDao dao = new SolicitacaoPessoaDao();
		result = dao.findByFields(hash);
		return result;
	}

	public List<SolicitacaoPessoa> findByField(String fieldName, Object content) {
		List<SolicitacaoPessoa> result = null;
		SolicitacaoPessoaDao dao = new SolicitacaoPessoaDao();
		result = dao.findByField(fieldName, content);
		return result;
	}

	public List<SolicitacaoPessoa> findByFieldOrdered(String fieldName, Object content, String fieldNameOrder) {
		List<SolicitacaoPessoa> result = null;
		SolicitacaoPessoaDao dao = new SolicitacaoPessoaDao();
		result = dao.findByFieldOrdered(fieldName, content,fieldNameOrder);
		return result;
	}
	
	
	public List<SolicitacaoPessoa> findByFieldsOrdered(Hashtable<String,Object> hash,String fieldNameOrder) {
		List<SolicitacaoPessoa> result = null;
		SolicitacaoPessoaDao dao = new SolicitacaoPessoaDao();
		result = dao.findByFieldsOrdered(hash,fieldNameOrder);
		return result;
	}
	public void update(SolicitacaoPessoa SolicitacaoPessoa)
	{
		SolicitacaoPessoaDao dao = new SolicitacaoPessoaDao();
		dao.save(SolicitacaoPessoa);
	}

	public void insert(String nome, String organizacao, String cargo, String email, String celular, String telefone, int status, String proprietario,String observacao,String dataSolicitacaoPessoa){
		SolicitacaoPessoa obj = new SolicitacaoPessoa();
		obj.setNome(nome);
		obj.setOrganizacao(organizacao);
		obj.setCargo(cargo);
		obj.setEmail(email);
		obj.setCelular(celular);
		obj.setTelefone(telefone);
		obj.setStatusSolicitacaoPessoa(status);
		obj.setProprietario(proprietario);
		obj.setObservacao(observacao);
		obj.setDataSolicitacaoPessoa(dataSolicitacaoPessoa);
		SolicitacaoPessoaDao dao = new SolicitacaoPessoaDao();
		dao.insert(obj);
	}


	public void save(String id, String nome, String organizacao, String cargo, String email, String celular, String telefone,int status, String proprietario, String observacao,String dataSolicitacaoPessoa){
		SolicitacaoPessoa obj = new SolicitacaoPessoa();
		obj.setId(id);
		obj.setNome(nome);
		obj.setOrganizacao(organizacao);
		obj.setCargo(cargo);
		obj.setEmail(email);
		obj.setCelular(celular);
		obj.setTelefone(telefone);
		obj.setStatusSolicitacaoPessoa(status);
		obj.setProprietario(proprietario);
		obj.setObservacao(observacao);
		obj.setDataSolicitacaoPessoa(dataSolicitacaoPessoa);
		SolicitacaoPessoaDao dao = new SolicitacaoPessoaDao();
		dao.save(obj);
	}


	public List<SolicitacaoPessoa> findAll(){
		List<SolicitacaoPessoa> result = null;
		SolicitacaoPessoaDao dao = new SolicitacaoPessoaDao();
		result = dao.findAll();
		return result;
	}
	

	public List<SolicitacaoPessoa> findAllOrdered(){
		List<SolicitacaoPessoa> result = null;
		SolicitacaoPessoaDao dao = new SolicitacaoPessoaDao();
		result = dao.findAllOrdered("nome");
		return result;
	}
	
	public List<SolicitacaoPessoa> buscaTodasSolicitacoesPessoa(){
		List<SolicitacaoPessoa> result = null;
		SolicitacaoPessoaDao dao = new SolicitacaoPessoaDao();
		result = dao.findAll();
		return result;
	}



	
}
