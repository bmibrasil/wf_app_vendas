package Service;

import java.util.Hashtable;
import java.util.List;

import Beans.Negocio;
import Dao.NegocioDao;


public class NegocioService {

//	public List<Negocio> getNegociosHolding(int turma){
//		NegocioDao dao = new NegocioDao();
//		List<Negocio> list = dao.b(turma);
//		return list;
//	}

	public void delete(int id){
		NegocioDao dao = new NegocioDao();
		dao.delete(id);
	}
	
	
	
	
	public List<Negocio> findByFields(Hashtable<String,Object> hash) {
		List<Negocio> result = null;
		NegocioDao dao = new NegocioDao();
		result = dao.findByFields(hash);
		return result;
	}
	
	public List<Negocio> findByFieldsOrdered(Hashtable<String,Object> hash, String fieldName){
		List<Negocio> result = null;
		NegocioDao dao = new NegocioDao();
		result = dao.findByFieldsOrdered(hash, fieldName);
		return result;
	}
	
	public List<Negocio> findByField(String fieldName, Object content) {
		List<Negocio> result = null;
		NegocioDao dao = new NegocioDao();
		result = dao.findByField(fieldName, content);
		return result;
	}
	
	public List<Negocio> findByFieldOrdered(String fieldName, Object content,String fieldNameOrder) {
		List<Negocio> result = null;
		NegocioDao dao = new NegocioDao();
		result = dao.findByFieldOrdered(fieldName, content,fieldNameOrder);
		return result;
	}
	
	public void update(Negocio Negocio)
	{
	  NegocioDao dao = new NegocioDao();
	  dao.save(Negocio);
	 }
	
	public void insert(String dataAbertura,String diaSemana, String organizacao, String pessoa, String nome, String escopo, String briefing, double potencial, int estagio, String statusNegocio, String proprietario){
		Negocio obj = new Negocio();
		obj.setDataAbertura(dataAbertura);
		obj.setDiaSemana(diaSemana);
		obj.setOrganizacao(organizacao);
		obj.setPessoa(pessoa);
		obj.setEscopo(escopo);
		obj.setNome(nome);
		obj.setBriefing(briefing);
		obj.setPotencial(potencial);
		obj.setEstagio(estagio);
		obj.setStatusNegocio(statusNegocio);
	    obj.setProprietario(proprietario);	
		NegocioDao dao = new NegocioDao();
		dao.insert(obj);
	}
	
	
	public void save(String id, String dataAbertura,String diaSemana, String organizacao, String pessoa, String nome, String escopo, String briefing, double potencial, int estagio, String statusNegocio, String proprietario){
		Negocio obj = new Negocio();
		obj.setId(id);
		obj.setDataAbertura(dataAbertura);
		obj.setDiaSemana(diaSemana);
		obj.setOrganizacao(organizacao);
		obj.setPessoa(pessoa);
		obj.setNome(nome);
		obj.setEscopo(escopo);
		obj.setBriefing(briefing);
		obj.setPotencial(potencial);
		obj.setEstagio(estagio);
		obj.setStatusNegocio(statusNegocio);
	    obj.setProprietario(proprietario);
		NegocioDao dao = new NegocioDao();
		dao.save(obj);
	}
	
	public List<Negocio> findAllOrdered(String fieldNameOrder){
		List<Negocio> result = null;
		NegocioDao dao = new NegocioDao();
		result = dao.findAllOrdered(fieldNameOrder);
		return result;
	}
	
	public List<Negocio> findAll(){
		List<Negocio> result = null;
		NegocioDao dao = new NegocioDao();
		result = dao.findAll();
		return result;
	}
	

}
