package Service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import Beans.Acesso;
import Beans.Turma;
import Dao.AcessoDao;
//import Dao.TurmaDao;
import Exceptions.SQLDuplicatedRecordException;

public class AcessoService {

	public Acesso getAcesso(int acesso) {
		Acesso obj = null;
		AcessoDao dao = new AcessoDao();
		List<Acesso> list = dao.findByField("id", acesso);
		if (list.size()>0) obj = list.get(0);
		return obj;
	}
	
	public int getCodigoAcesso(int turma, String email) {
		int result = 0;
		AcessoDao dao = new AcessoDao();
		Hashtable<String,Object> hash = new Hashtable<String,Object>();
		hash.put("turma", turma);
		hash.put("email", email);
		List<Acesso> list = dao.findByFields(hash);
		if (list.size()>0) result = list.get(0).getId();
		return result;
	}
	
	public int findAcesso(int turma, int empresa) {
		AcessoDao dao = new AcessoDao();
		int status = dao.findAcesso(turma,empresa);
		return status;
	}
	
	public int verificaStatusTurma(int turma) {
		int status = 0;
//		TurmaDao dao = new TurmaDao();
//		List<Turma> list = dao.findByField("id", turma);
//		if (list.size()>0) status=list.get(0).getStatus();
		return status;
	}
	
	public int validaDominio(int turma,String email) {
		AcessoDao dao = new AcessoDao();
		int status = dao.validaDominio(turma, email);
		return status;
	}
	
	public Acesso getAcessoTurma(String acesso) {
		Acesso obj = null;
		AcessoDao dao = new AcessoDao();
		List<Acesso> list = dao.findByField("turma", acesso);
		if (list.size()>0) obj = list.get(0);
		return obj;
	}

	public Acesso getAcesso(String nome) {
		Acesso obj = null;
		AcessoDao dao = new AcessoDao();
		List<Acesso> list = dao.findByField("nome", nome);
		if (list.size()>0) obj = list.get(0);
		return obj;
	}

	public int insert(int turma, String nome) throws Exception{
		int result;
		Acesso obj = new Acesso();
		obj.setTurma(turma);
		obj.setNome(nome);
		
		//Verifica se j� existe um grupo com este nome nesta turma
		AcessoDao dao = new AcessoDao();
		Hashtable<String, Object> hash = new Hashtable<String, Object>();
		hash.put("turma", turma);
		hash.put("nome", nome);
		List<Acesso> list = dao.findByFields(hash);
		
		//Insere grupo caso n�o exista outro com o mesmo nome
		if (list.size()>0){
			//Grupo j� existe, n�o pode ser criado
			throw new SQLDuplicatedRecordException();
		} else {
			dao.insert(obj);
			list = dao.findByFields(hash);
			
			result = list.get(0).getId();
		}
		return result;
	}
	
	public void insert(Acesso acesso){
		AcessoDao dao = new AcessoDao();
		dao.insert(acesso);
	}
	
	public List<Acesso> listaAcessoAtivos(int turma, int empresa) {
		AcessoDao dao = new AcessoDao();
		return dao.listaAcessoAtivos(turma, empresa);
	}
	
	
	public List<Acesso> findByTurma(int turma){
		List<Acesso> result=null;
		AcessoDao dao = new AcessoDao();
		result = dao.findByField("turma", turma);
		return result;
	}
	
//	public List<Acesso> getGruposTurma(int turma) {
//		List<Acesso> result;
//		AcessoDao dao = new AcessoDao();
//		result = dao. getAcessosTurma(turma);
//		return result;
//	}
//	
	public void save(Acesso grupo){
		AcessoDao dao = new AcessoDao();
		dao.save(grupo);
	}
	
	
	public void delete(int acesso){
		AcessoDao dao = new AcessoDao();
		dao.delete(acesso);
	}
	public List<Acesso> findByFields(Hashtable<String,Object> hash){
		AcessoDao dao = new AcessoDao();
		return dao.findByFields(hash);
	}
	
	public List<Acesso> findByField(String campo, Object conteudo){
		List<Acesso> result = new ArrayList<Acesso>();
		AcessoDao dao = new AcessoDao();
			result = dao.findByField(campo, conteudo);
		return result;
	}
	
	public int getID(Acesso acesso){
		int result = -1;
		Hashtable<String,Object> hash = new Hashtable<String,Object>();
		List<Acesso> list = findByFields(hash);
		if (list.size()>0){
			result = list.get(0).getId();
		}
		return result;
	}

}
