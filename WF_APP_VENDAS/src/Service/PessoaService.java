package Service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import Beans.Organizacao;
import Beans.Pessoa;
import Dao.PessoaDao;


public class PessoaService {


	public void delete(int id){
		PessoaDao dao = new PessoaDao();
		dao.delete(id);
	}




	public List<Pessoa> findByFields(Hashtable<String,Object> hash) {
		List<Pessoa> result = null;
		PessoaDao dao = new PessoaDao();
		result = dao.findByFields(hash);
		return result;
	}

	public List<Pessoa> findByField(String fieldName, Object content) {
		List<Pessoa> result = null;
		PessoaDao dao = new PessoaDao();
		result = dao.findByField(fieldName, content);
		return result;
	}

	public List<Pessoa> findByFieldOrdered(String fieldName, Object content, String fieldNameOrder) {
		List<Pessoa> result = null;
		PessoaDao dao = new PessoaDao();
		result = dao.findByFieldOrdered(fieldName, content,fieldNameOrder);
		return result;
	}
	public void update(Pessoa Pessoa)
	{
		PessoaDao dao = new PessoaDao();
		dao.save(Pessoa);
	}

	public void insert(String nome, String organizacao, String cargo, String email, String celular, String telefone, int estagio, String status, String proprietario){
		Pessoa obj = new Pessoa();
		obj.setNome(nome);
		obj.setOrganizacao(organizacao);
		obj.setCargo(cargo);
		obj.setEmail(email);
		obj.setCelular(celular);
		obj.setTelefone(telefone);
		obj.setEstagio(estagio);
		obj.setStatusPessoa(status);
		obj.setProprietario(proprietario);
		PessoaDao dao = new PessoaDao();
		dao.insert(obj);
	}


	public void save(String id, String nome, String organizacao, String cargo, String email, String celular, String telefone, int estagio, String status, String proprietario){
		Pessoa obj = new Pessoa();
		obj.setId(id);
		obj.setNome(nome);
		obj.setOrganizacao(organizacao);
		obj.setCargo(cargo);
		obj.setEmail(email);
		obj.setCelular(celular);
		obj.setTelefone(telefone);
		obj.setEstagio(estagio);
		obj.setStatusPessoa(status);
		obj.setProprietario(proprietario);
		PessoaDao dao = new PessoaDao();
		dao.save(obj);
	}


	public List<Pessoa> findAll(){
		List<Pessoa> result = null;
		PessoaDao dao = new PessoaDao();
		result = dao.findAll();
		return result;
	}
	

	public List<Pessoa> findAllOrdered(){
		List<Pessoa> result = null;
		PessoaDao dao = new PessoaDao();
		result = dao.findAllOrdered("nome");
		return result;
	}
	
//	public List<Pessoa> buscarTodasPessoasOrganizacoes(String proprietario){
//		List<Pessoa> result = null;
//		PessoaDao dao = new PessoaDao();
//		result = dao.findByFieldOrdered("proprietario", proprietario, "nome");
//		return result;
//	}
//	
	public List<Pessoa>  findByFieldsOrdered(Hashtable<String,Object> hash,String fieldNameOrder)
	{
		List<Pessoa> result = null;
		PessoaDao dao = new PessoaDao();
		result = dao. findByFieldsOrdered( hash,fieldNameOrder);
		return result;
	}
	



	
}
