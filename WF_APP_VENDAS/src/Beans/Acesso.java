package Beans;

import java.io.Serializable;

public class Acesso implements Serializable{
	private Integer id;
	private String nome;
	private String codigoAcesso;
	private Integer turma;
	private int tipo;
	private int fase;
	
	public Acesso() {
		
	}

	/**
	 * @param id
	 * @param turma
	 * @param nome
	 */
	public Acesso(Integer turma, String nome, String email, int hierarquia) {
		super();
		this.id = null;
		this.turma = turma;
		this.nome = nome;
	
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the turma
	 */
	public Integer getTurma() {
		return turma;
	}

	/**
	 * @param turma the turma to set
	 */
	public void setTurma(Integer turma) {
		this.turma = turma;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigoAcesso() {
		return codigoAcesso;
	}

	public void setCodigoAcesso(String codigoAcesso) {
		this.codigoAcesso = codigoAcesso;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getFase() {
		return fase;
	}

	public void setFase(int fase) {
		this.fase = fase;
	}


	
}
