package Beans;

import java.io.Serializable;

public class Valor implements Serializable{
	private Integer id;
	private String valor;
	private int acesso;
	private int empresa;
	private int votacao;
	private double classificacaoCliente;
	private double classificacaoEmpresa;
	private int turma ;
	private int fase ;
	
	//0 - ativa  ; 1- removida ; 2 incluida da lista geral
	
	public Valor() {
		
	}

	/**
	 * @param id
	 * @param turma
	 * @param nome
	 */
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public int getAcesso() {
		return acesso;
	}

	public void setAcesso(int acesso) {
		this.acesso = acesso;
	}

	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}

	public int getVotacao() {
		return votacao;
	}

	public void setVotacao(int votacao) {
		this.votacao = votacao;
	}

	public double getClassificacaoEmpresa() {
		return classificacaoEmpresa;
	}

	public void setClassificacaoEmpresa(double classificacaoEmpresa) {
		this.classificacaoEmpresa = classificacaoEmpresa;
	}

	public double getClassificacaoCliente() {
		return classificacaoCliente;
	}

	public void setClassificacaoCliente(double classificacaoCliente) {
		this.classificacaoCliente = classificacaoCliente;
	}

	public int getTurma() {
		return turma;
	}

	public void setTurma(int turma) {
		this.turma = turma;
	}

	public int getFase() {
		return fase;
	}

	public void setFase(int fase) {
		this.fase = fase;
	}

	

	
}
