package Beans;

public class Atividade{
	/**
	 * 
	 */
	private String id;
	private String negocio;
	private String dataAtividade;
	private String dataInicio;
	private String dataFim;
	private String diaSemana;
	private String descricao;
	private String tipoAtividade;
	private String pessoaAtividade;
	private String nomePessoaAtividade;
	private String proprietario;
	private String nomeProprietario;
	
	
	public Atividade(){
		
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	
	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public String getDiaSemana() {
		return diaSemana;
	}


	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}


	public String getTipoAtividade() {
		return tipoAtividade;
	}


	public void setTipoAtividade(String tipoAtividade) {
		this.tipoAtividade = tipoAtividade;
	}


	public String getDataAtividade() {
		return dataAtividade;
	}


	public void setDataAtividade(String dataAtividade) {
		this.dataAtividade = dataAtividade;
	}

	public String getDataInicio() {
		return dataInicio;
	}


	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}


	public String getDataFim() {
		return dataFim;
	}


	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}


	public String getNegocio() {
		return negocio;
	}


	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}


	
	public String getPessoaAtividade() {
		return pessoaAtividade;
	}


	public void setPessoaAtividade(String pessoaAtividade) {
		this.pessoaAtividade = pessoaAtividade;
	}


	public String getNomePessoaAtividade() {
		return nomePessoaAtividade;
	}


	public void setNomePessoaAtividade(String nomePessoaAtividade) {
		this.nomePessoaAtividade = nomePessoaAtividade;
	}


	public String getProprietario() {
		return proprietario;
	}


	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}


	public String getNomeProprietario() {
		return nomeProprietario;
	}


	public void setNomeProprietario(String nomeProprietario) {
		this.nomeProprietario = nomeProprietario;
	}



}
