package Beans;

import java.io.Serializable;

public class Especificacao implements Serializable{
	
	private String campo;
	private String descricao;
	private String calculo;
	private Double fator;
	
	public Especificacao() {
	}

	/**
	 * @param campo
	 * @param descricao
	 * @param calculo
	 * @param fator
	 */
	public Especificacao(String campo, String descricao, String calculo, Double fator) {
		super();
		this.campo = campo;
		this.descricao = descricao;
		this.calculo = calculo;
		this.fator = fator;
	}

	/**
	 * @return the campo
	 */
	
	
	public String getCampo() {
		return campo;
	}

	/**
	 * @param campo the campo to set
	 */
	public void setCampo(String campo) {
		this.campo = campo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the calculo
	 */
	public String getCalculo() {
		return calculo;
	}

	/**
	 * @param calculo the calculo to set
	 */
	public void setCalculo(String calculo) {
		this.calculo = calculo;
	}

	/**
	 * @return the fator
	 */
	public Double getFator() {
		return fator;
	}

	/**
	 * @param fator the fator to set
	 */
	public void setFator(Double fator) {
		this.fator = fator;
	}
	
}
