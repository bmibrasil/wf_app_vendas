package Beans;

public class SolicitacaoOrganizacao {
	/**
	 * 
	 */
	private String id;
	private String nome;
	private String cnpj;
	private String logradouro;
	private String bairro;
	private String cidade;
	private String uf;
	private String cep;
	private String segmento;
	private int statusSolicitacaoOrganizacao;
	private String observacao;
	private String dataSolicitacaoOrganizacao;
	private String proprietario;
	private String unidade;	
	
	
	
	
	public SolicitacaoOrganizacao(){
		
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getBairro() {
		return bairro;
	}


	public void setBairro(String bairro) {
		this.bairro = bairro;
	}


	public String getCidade() {
		return cidade;
	}


	public void setCidade(String cidade) {
		this.cidade = cidade;
	}


	public String getUf() {
		return uf;
	}


	public void setUf(String uf) {
		this.uf = uf;
	}


	public String getLogradouro() {
		return logradouro;
	}


	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}


	
	public String getCep() {
		return cep;
	}


	public void setCep(String cep) {
		this.cep = cep;
	}


	public String getSegmento() {
		return segmento;
	}


	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}


	public String getObservacao() {
		return observacao;
	}


	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}


	public int getStatusSolicitacaoOrganizacao() {
		return statusSolicitacaoOrganizacao;
	}


	public void setStatusSolicitacaoOrganizacao(int statusSolicitacaoOrganizacao) {
		this.statusSolicitacaoOrganizacao = statusSolicitacaoOrganizacao;
	}


	public String getCnpj() {
		return cnpj;
	}


	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}


	public String getDataSolicitacaoOrganizacao() {
		return dataSolicitacaoOrganizacao;
	}


	public void setDataSolicitacaoOrganizacao(String dataSolicitacaoOrganizacao) {
		this.dataSolicitacaoOrganizacao = dataSolicitacaoOrganizacao;
	}


	public String getProprietario() {
		return proprietario;
	}


	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}


	public String getUnidade() {
		return unidade;
	}


	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}



}
