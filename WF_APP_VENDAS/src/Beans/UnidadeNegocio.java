package Beans;

import java.io.Serializable;

public class UnidadeNegocio implements Serializable{
	private String id;
	private String nome;
	
	
	
	public UnidadeNegocio() {
		
	}

	/**
	 * @param id
	 * @param turma
	 * @param nome
	 */
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

		
}
