package Beans;

import java.sql.Time;

public class Colaborador {
	/**
	 * 
	 */
	private Integer id;
	private String nome;
	private double valor;
	private double cargaHoraria;
	private Time realizado;
	private double horasEsperadas;
	private String email;
	private String senha;
	private int bloqueado;
	private int distanciaCasaTrabalho;
	private String permissoes;

	
	
	public Colaborador(){
		
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	

	public double getCargaHoraria() {
		return cargaHoraria;
	}


	public void setCargaHoraria(double cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}


	public double getValor() {
		return valor;
	}


	public void setValor(double valor) {
		this.valor = valor;
	}


	public double getHorasEsperadas() {
		return horasEsperadas;
	}


	public void setHorasEsperadas(double horasEsperadas) {
		this.horasEsperadas = horasEsperadas;
	}


	public Time getRealizado() {
		return realizado;
	}


	public void setRealizado(Time realizado) {
		this.realizado = realizado;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getSenha() {
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	}


	public int getBloqueado() {
		return bloqueado;
	}


	public void setBloqueado(int bloqueado) {
		this.bloqueado = bloqueado;
	}



	public int getDistanciaCasaTrabalho() {
		return distanciaCasaTrabalho;
	}


	public void setDistanciaCasaTrabalho(int distanciaCasaTrabalho) {
		this.distanciaCasaTrabalho = distanciaCasaTrabalho;
	}


	public String getPermissoes() {
		return permissoes;
	}


	public void setPermissoes(String permissoes) {
		this.permissoes = permissoes;
	}






}
