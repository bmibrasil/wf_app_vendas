package Beans;

import java.util.Date;
import java.sql.Time;
import java.util.Calendar;


public class TimeSheet {
	private String id; 
	private String data; 
	private String colaborador;
	private String diaSemana;
	private String inicio;
	private String termino;
	private String horasTrabalhadas;
	private String cliente;
	private String projeto;
	private String tipo;
	private String descricao;
	private String etapa;
	private String periodo;
	
	
	
	/**
	 * @param id
	 * @param turma
	 * @param nome
	 */

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getColaborador() {
		return colaborador;
	}

	public void setColaborador(String colaborador) {
		this.colaborador = colaborador;
	}

	public String getInicio() {
		return inicio;
	}

	public void setInicio(String inicio) {
		this.inicio = inicio;
	}

	public String getTermino() {
		return termino;
	}

	public void setTermino(String termino) {
		this.termino = termino;
	}

	public String getHorasTrabalhadas() {
		return horasTrabalhadas;
	}

	public void setHorasTrabalhadas(String horasTrabalhadas) {
		this.horasTrabalhadas = horasTrabalhadas;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getProjeto() {
		return projeto;
	}

	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	
	public String getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}

	public String getId() {
		return id;
	}

	public void setId(String idTimeSheet) {
		this.id = idTimeSheet;
	}

	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}


	
}

