package Beans;

public class Periodo {
	/**
	 * 
	 */
	private Integer id;
	private String datainicial;
	private String datafinal;
	private int diasLancamento;
	private String nomePeriodo;
	private Integer status;
	
	public Periodo(){
		
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getDatainicial() {
		return datainicial;
	}


	public void setDatainicial(String datainicial) {
		this.datainicial = datainicial;
	}


	public String getDatafinal() {
		return datafinal;
	}


	public void setDatafinal(String datafinal) {
		this.datafinal = datafinal;
	}


	public int getDiasLancamento() {
		return diasLancamento;
	}


	public void setDiasLancamento(int diasLancamento) {
		this.diasLancamento = diasLancamento;
	}


	public String getNomePeriodo() {
		return nomePeriodo;
	}


	public void setNomePeriodo(String nomePeriodo) {
		this.nomePeriodo = nomePeriodo;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	
}
