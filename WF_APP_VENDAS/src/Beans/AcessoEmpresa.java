package Beans;

import java.io.Serializable;

public class AcessoEmpresa implements Serializable{
	private Integer id;
	private Integer acesso;
	private int empresa;
	private int fase;
	public AcessoEmpresa() {
		
	}

	/**
	 * @param id
	 * @param turma
	 * @param nome
	 */
	public AcessoEmpresa(Integer acesso, Integer empresa, int fase) {
		super();
		this.id = null;
		this.empresa=empresa;
		this.acesso=acesso;
		fase=0;

	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	
	public Integer getAcesso() {
		return acesso;
	}

	public void setAcesso(Integer acesso) {
		this.acesso = acesso;
	}

	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}

	public int getFase() {
		return fase;
	}

	public void setFase(int fase) {
		this.fase = fase;
	}


	
}
