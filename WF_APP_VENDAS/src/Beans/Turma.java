package Beans;

import java.io.Serializable;

public class Turma implements Serializable{
	/**
	 * 
	 */
	private Integer id;
	private String nome;
	private int estagio;
	private int empresa;
	private int status;
	private int percentualRemocao;
	private int totalValores;
		
	public Turma(){
		
	}

	/**
	 * @param id
	 * @param nome
	 */
	public Turma(Integer id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	
	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}

	public int getEstagio() {
		return estagio;
	}

	public void setEstagio(int estagio) {
		this.estagio = estagio;
	}

	
	public int getPercentualRemocao() {
		return percentualRemocao;
	}

	public void setPercentualRemocao(int percentualRemocao) {
		this.percentualRemocao = percentualRemocao;
	}

	public int getTotalValores() {
		return totalValores;
	}

	public void setTotalValores(int totalValores) {
		this.totalValores = totalValores;
	}

		
}
