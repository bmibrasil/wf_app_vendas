package Beans;

import java.util.Date;
import java.sql.Time;
import java.util.Calendar;


public class Reembolso {
	private int id; 
	private String data; 
	private int colaborador;
	private String diasemana;
	private int cliente;
	private int projeto;
	private int etapa;
	private int tipo;
	private String local;
	private double quilometragem;
	private String origem;
	private String destino;
	private int quantidadeDias;
	private double valorsolicitado;
	private double valorreembolso;
	private String observacao;
	private int statusreembolso;
	private int numeroArquivos;
	private String justificativa;
	
	
	/**
	 * @param id
	 * @param turma
	 * @param nome
	 */

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getColaborador() {
		return colaborador;
	}

	public void setColaborador(int colaborador) {
		this.colaborador = colaborador;
	}


	public int getCliente() {
		return cliente;
	}

	public void setCliente(int cliente) {
		this.cliente = cliente;
	}

	public int getProjeto() {
		return projeto;
	}

	public void setProjeto(int projeto) {
		this.projeto = projeto;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	
	
	public String getDiaSemana() {
		return diasemana;
	}

	public void setDiaSemana(String diaSemana) {
		this.diasemana = diaSemana;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEtapa() {
		return etapa;
	}

	public void setEtapa(int etapa) {
		this.etapa = etapa;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public double getQuilometragem() {
		return quilometragem;
	}

	public void setQuilometragem(double quilometragem) {
		this.quilometragem = quilometragem;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}


	public double getValorSolicitado() {
		return valorsolicitado;
	}

	public void setValorSolicitado(double valorSolicitado) {
		this.valorsolicitado = valorSolicitado;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}


	public double getValorReembolso() {
		return valorreembolso;
	}

	public void setValorReembolso(double valorreembolso) {
		this.valorreembolso = valorreembolso;
	}

	public int getQuantidadeDias() {
		return quantidadeDias;
	}

	public void setQuantidadeDias(int quantidadeDias) {
		this.quantidadeDias = quantidadeDias;
	}

	public int getStatusreembolso() {
		return statusreembolso;
	}

	public void setStatusreembolso(int statusreembolso) {
		this.statusreembolso = statusreembolso;
	}

	
	public int getNumeroArquivos() {
		return numeroArquivos;
	}

	public void setNumeroArquivos(int numeroArquivos) {
		this.numeroArquivos = numeroArquivos;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}


	
}

