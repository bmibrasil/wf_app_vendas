package Beans;

public class Debriefing {
	private Integer id;
	private Integer turma;
	private String nome;
	private String link;
	private String descricao;
	private Integer status;
	private Integer nivelInicial;
	private Integer nivelFinal;
	private int empresa;	
	
	public Debriefing(){
		
	}
	
	public Debriefing(Integer id, Integer turma, String nome, String link, String descricao, Integer status,
			Integer nivelInicial, Integer nivelFinal, int empresa) {
		super();
		this.id = id;
		this.turma = turma;
		this.nome = nome;
		this.link = link;
		this.descricao = descricao;
		this.status = status;
		this.nivelInicial = nivelInicial;
		this.nivelFinal = nivelFinal;
		this.empresa = empresa;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTurma() {
		return turma;
	}
	public void setTurma(Integer turma) {
		this.turma = turma;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getNivelInicial() {
		return nivelInicial;
	}
	public void setNivelInicial(Integer nivelInicial) {
		this.nivelInicial = nivelInicial;
	}
	public Integer getNivelFinal() {
		return nivelFinal;
	}
	public void setNivelFinal(Integer nivelFinal) {
		this.nivelFinal = nivelFinal;
	}
	
	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}
	
}
