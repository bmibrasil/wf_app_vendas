package Beans;



public class SolicitacaoPessoa {
	/**
	 * 
	 */
	private String id;
	private String nome;
	private String organizacao;
	private String cargo;
	private String email;
	private String celular;
	private String telefone;
	private int statusSolicitacaoPessoa;
	private String proprietario;
	private String observacao;
	private String dataSolicitacaoPessoa;
	private String origemPessoa;
	private String unidade;
	
	
	public SolicitacaoPessoa(){
		
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getOrganizacao() {
		return organizacao;
	}


	public void setOrganizacao(String organizacao) {
		this.organizacao = organizacao;
	}


	public String getCargo() {
		return cargo;
	}


	public void setCargo(String cargo) {
		this.cargo = cargo;
	}


		
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTelefone() {
		return telefone;
	}


	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}


	
	public String getProprietario() {
		return proprietario;
	}


	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}


	public int getStatusSolicitacaoPessoa() {
		return statusSolicitacaoPessoa;
	}


	public void setStatusSolicitacaoPessoa(int statusSolicitacaoPessoa) {
		this.statusSolicitacaoPessoa = statusSolicitacaoPessoa;
	}


	public String getObservacao() {
		return observacao;
	}


	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}


	public String getDataSolicitacaoPessoa() {
		return dataSolicitacaoPessoa;
	}


	public void setDataSolicitacaoPessoa(String dataSolicitacaoPessoa) {
		this.dataSolicitacaoPessoa = dataSolicitacaoPessoa;
	}


	public String getOrigemPessoa() {
		return origemPessoa;
	}


	public void setOrigemPessoa(String origemPessoa) {
		this.origemPessoa = origemPessoa;
	}


	public String getUnidade() {
		return unidade;
	}


	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}


	

}
