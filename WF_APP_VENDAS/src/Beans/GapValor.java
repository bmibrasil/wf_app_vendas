package Beans;

import java.io.Serializable;

public class GapValor implements Serializable{
	private Integer id;
	private int valor;
	private String acao;
	private int camada;
	private int empresa;
	private int acesso;
   	private int turma ;
	
	//0 - ativa  ; 1- removida ; 2 incluida da lista geral
	
	public GapValor() {
		
	}

	/**
	 * @param id
	 * @param turma
	 * @param nome
	 */
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	

	public int getAcesso() {
		return acesso;
	}

	public void setAcesso(int acesso) {
		this.acesso = acesso;
	}

	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}


	public int getCamada() {
		return camada;
	}

	public void setCamada(int camada) {
		this.camada = camada;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public int getTurma() {
		return turma;
	}

	public void setTurma(int turma) {
		this.turma = turma;
	}

	

	
}
