package Beans;

public class Negocio {
	/**
	 * 
	 */
	private String id;
	private String dataAbertura;
	private String diaSemana;
	private String organizacao;
	private String pessoa;
	private String nome;
	private String escopo;
	private String briefing;
	private double potencial;
	private int estagio;
	private String statusNegocio;
	private String proprietario;
	private boolean briefingAprovado;
	private int quantidadeTurmas;
	private int quantidadePessoas;
	private String dataPrevistaEntrega;
	private String dataPrevistaFechamento;
	private String tipoSolucao;
	private String publicoAlvo;
	public String limiteTempoCliente;
	public String obsAdm;
	public String numeracao;
	private String temperatura;
	

	public Negocio(){
		
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


		public String getDataAbertura() {
		return dataAbertura;
	}


	public void setDataAbertura(String dataAbertura) {
		this.dataAbertura = dataAbertura;
	}


	public String getOrganizacao() {
		return organizacao;
	}


	public void setOrganizacao(String organizacao) {
		this.organizacao = organizacao;
	}


	public String getPessoa() {
		return pessoa;
	}


	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}


	public String getBriefing() {
		return briefing;
	}


	public void setBriefing(String briefing) {
		this.briefing = briefing;
	}


	public String getEscopo() {
		return escopo;
	}


	public void setEscopo(String escopo) {
		this.escopo = escopo;
	}


	public double getPotencial() {
		return potencial;
	}


	public void setPotencial(double potencial) {
		this.potencial = potencial;
	}


	public String getStatusNegocio() {
		return statusNegocio;
	}


	public void setStatusNegocio(String statusNegocio) {
		this.statusNegocio = statusNegocio;
	}


	public int getEstagio() {
		return estagio;
	}


	public void setEstagio(int estagio) {
		this.estagio = estagio;
	}


	public String getProprietario() {
		return proprietario;
	}


	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}


	public String getDiaSemana() {
		return diaSemana;
	}


	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public boolean isBriefingAprovado() {
		return briefingAprovado;
	}


	public void setBriefingAprovado(boolean briefingAprovado) {
		this.briefingAprovado = briefingAprovado;
	}


	public int getQuantidadePessoas() {
		return quantidadePessoas;
	}


	public void setQuantidadePessoas(int quantidadePessoas) {
		this.quantidadePessoas = quantidadePessoas;
	}


	public int getQuantidadeTurmas() {
		return quantidadeTurmas;
	}


	public void setQuantidadeTurmas(int quantidadeTurmas) {
		this.quantidadeTurmas = quantidadeTurmas;
	}


	public String getDataPrevistaFechamento() {
		return dataPrevistaFechamento;
	}


	public void setDataPrevistaFechamento(String dataPrevistaFechamento) {
		this.dataPrevistaFechamento = dataPrevistaFechamento;
	}


	public String getDataPrevistaEntrega() {
		return dataPrevistaEntrega;
	}


	public void setDataPrevistaEntrega(String dataPrevistaEntrega) {
		this.dataPrevistaEntrega = dataPrevistaEntrega;
	}


	public String getPublicoAlvo() {
		return publicoAlvo;
	}


	public void setPublicoAlvo(String publicoAlvo) {
		this.publicoAlvo = publicoAlvo;
	}


	public String getTipoSolucao() {
		return tipoSolucao;
	}


	public void setTipoSolucao(String tipoSolucao) {
		this.tipoSolucao = tipoSolucao;
	}

	
	public String getlimiteTempoCliente() {
		return limiteTempoCliente;
	}


	public void setlimiteTempoCliente(String limiteTempoCliente) {
		this.limiteTempoCliente = limiteTempoCliente;
	}


	public String getObsAdm() {
		return obsAdm;
	}


	public void setObsAdm(String obsAdm) {
		this.obsAdm = obsAdm;
	}
	

	public String getNumeracao() {
		return numeracao;
	}


	public void setNumeracao(String numeracao) {
		this.numeracao = numeracao;
	}


	public String getTemperatura() {
		return temperatura;
	}


	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}
	
}
