package Beans;

public class LoginBean {
	private String codigoAcesso;
	private String codigoSimulacao;
	
	public LoginBean(String codigoAcesso, String codigoSimulacao) {
		super();
		this.codigoAcesso = codigoAcesso;
		this.codigoSimulacao = codigoSimulacao;
	}
	public String getCodigoAcesso() {
		return codigoAcesso;
	}
	public void setCodigoAcesso(String codigoAcesso) {
		this.codigoAcesso = codigoAcesso;
	}
	public String getCodigoSimulacao() {
		return codigoSimulacao;
	}
	public void setCodigoSimulacao(String codigoSimulacao) {
		this.codigoSimulacao = codigoSimulacao;
	}
	
}
