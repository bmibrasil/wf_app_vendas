package Beans;

public class Pessoa{
	/**
	 * 
	 */
	private String id;
	private String nome;
	private String organizacao;
	private String cargo;
	private String email;
	private String celular;
	private String telefone;
	private Integer estagio;
	private String statusPessoa;
	private String proprietario;
	
	
	public Pessoa(){
		
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getOrganizacao() {
		return organizacao;
	}


	public void setOrganizacao(String organizacao) {
		this.organizacao = organizacao;
	}


	public String getCargo() {
		return cargo;
	}


	public void setCargo(String cargo) {
		this.cargo = cargo;
	}


	public Integer getEstagio() {
		return estagio;
	}


	public void setEstagio(Integer estagio) {
		this.estagio = estagio;
	}


	
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTelefone() {
		return telefone;
	}


	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}


	public String getStatusPessoa() {
		return statusPessoa;
	}


	public void setStatusPessoa(String statusPessoa) {
		this.statusPessoa = statusPessoa;
	}


	public String getProprietario() {
		return proprietario;
	}


	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}

}
