package Beans;

import java.io.Serializable;

public class Aprovacao implements Serializable{
	private Integer id;
	private Integer colaborador;
	private Integer periodo;
	private Integer aprovador;
	private String data;
	private Integer valorAprovacao;
	
	public Aprovacao() {
		
	}

	/**
	 * @param id
	 * @param turma
	 * @param nome
	 */
	public Aprovacao(Integer colaborador, Integer periodo, Integer aprovador, String data) {
		super();
		this.setId(null);
		this.setColaborador(colaborador);
		this.setPeriodo(periodo);
		this.setAprovador(aprovador);
		this.setData(data);
	
	}

	public Integer getColaborador() {
		return colaborador;
	}

	public void setColaborador(Integer colaborador) {
		this.colaborador = colaborador;
	}

	public Integer getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAprovador() {
		return aprovador;
	}

	public void setAprovador(Integer aprovador) {
		this.aprovador = aprovador;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}


	public Integer getValorAprovacao() {
		return valorAprovacao;
	}

	public void setValorAprovacao(Integer valorAprovacao) {
		this.valorAprovacao = valorAprovacao;
	}
}

	