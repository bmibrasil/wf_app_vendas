package Beans;

public class TipoReembolso{
	/**
	 * 
	 */
	private Integer id;
	private String nome;
	private String configuracao;
	
	
	
	public TipoReembolso(){
		
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getConfiguracao() {
		return configuracao;
	}


	public void setConfiguracao(String configuracao) {
		this.configuracao = configuracao;
	}



}
