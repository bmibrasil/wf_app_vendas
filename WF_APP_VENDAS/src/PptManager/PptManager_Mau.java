package PptManager;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.poi.POIXMLDocumentPart;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xslf.usermodel.TextAlign;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFChart;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTable;
import org.apache.poi.xslf.usermodel.XSLFTableRow;
import org.apache.poi.xslf.usermodel.XSLFTextBox;
import org.apache.poi.xslf.usermodel.XSLFTextParagraph;
import org.apache.poi.xslf.usermodel.XSLFTextRun;
import org.apache.poi.xslf.usermodel.XSLFTextShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBarChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBarSer;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTDouble;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTNumData;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTNumVal;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTStrVal;

//import Beans.Grupo;
//import Service.GrupoService;

public class PptManager_Mau {

	private String templateName;
	private String filename;
	private int slideNum;
	private int elementNum;
	private boolean opened = false;

	private File inputFile;
	private File outputFile;
	private FileInputStream inputstream;

	private Sheet sheet;
	private XMLSlideShow ppt;
	private XSLFSlide slide;
	private XSLFChart chart;
	private XSLFTable table;
	private XSLFTextBox text;
	private XSSFWorkbook wb;

	private FileOutputStream out;
	private InputStream input;
	private OutputStream output;

	public PptManager_Mau() {
		super();
		templateName = "exemplo_template.pptx";
		filename = "exemplo_out.pptx";
		slideNum=0;
		elementNum=1;
	}

	public PptManager_Mau(String templateName, String filename, int slideNum, int elementNum) {
		super();
		this.templateName = templateName;
		this.filename = filename;
		this.slideNum = slideNum;
		this.elementNum = elementNum;
	}

	public void readOpenChart() throws FileNotFoundException, IOException, PptChartNotFoundException{
		inputFile = new File(templateName);
		outputFile = new File(filename);
		inputstream = new FileInputStream(inputFile);
		ppt = new XMLSlideShow(inputstream);
		slide = ppt.getSlides()[slideNum];
		chart = findChart(elementNum,slide);
		if (chart!=null){
			POIXMLDocumentPart xlsPart = chart.getRelations().get(2);
			input = xlsPart.getPackagePart().getInputStream();
			output = xlsPart.getPackagePart().getOutputStream();
			wb = new org.apache.poi.xssf.usermodel.XSSFWorkbook(input);
			sheet = wb.getSheetAt(0);
			opened = true;
		} else {
			throw new PptChartNotFoundException();
		}	
	}	

	public void readOpenChart(double min) throws FileNotFoundException, IOException, PptChartNotFoundException{
		inputFile = new File(templateName);
		outputFile = new File(filename);
		inputstream = new FileInputStream(inputFile);
		ppt = new XMLSlideShow(inputstream);
		slide = ppt.getSlides()[slideNum];
		chart = findChart(elementNum,slide);
		CTDouble d = CTDouble.Factory.newInstance();
		d.setVal(min);
		chart.getCTChart().getPlotArea().getValAxArray()[0].getScaling().setMin(d);
		if (chart!=null){
			POIXMLDocumentPart xlsPart = chart.getRelations().get(2);
			input = xlsPart.getPackagePart().getInputStream();
			output = xlsPart.getPackagePart().getOutputStream();
			wb = new org.apache.poi.xssf.usermodel.XSSFWorkbook(input);
			sheet = wb.getSheetAt(0);
			opened = true;
		} else {
			throw new PptChartNotFoundException();
		}	
	}	

	public void readOpenTable() throws FileNotFoundException, IOException, PptTableNotFoundException{
		inputFile = new File(templateName);
		outputFile = new File(filename);
		inputstream = new FileInputStream(inputFile);
		ppt = new XMLSlideShow(inputstream);
		slide = ppt.getSlides()[slideNum];
		table = findTable(elementNum,slide);
		if (table!=null){
			opened = true;
		} else {
			throw new PptTableNotFoundException();
		}
	}

	public void readOpenText() throws FileNotFoundException, IOException, PptTextNotFoundException{
		inputFile = new File(templateName);
		outputFile = new File(filename);
		inputstream = new FileInputStream(inputFile);
		ppt = new XMLSlideShow(inputstream);
		slide = ppt.getSlides()[slideNum];
		text = findText(elementNum,slide);
		if (text!=null){
			opened = true;
		} else {
			throw new PptTextNotFoundException();
		}
	}

/*
	public void removeSlides(int slides[], int turma) throws FileNotFoundException, IOException, PptTextNotFoundException{
		inputFile = new File(templateName);
		outputFile = new File(filename);
		inputstream = new FileInputStream(inputFile);
		ppt = new XMLSlideShow(inputstream);
		
		GrupoService grupoService = new GrupoService();
		List<Grupo> listGrupos = grupoService.getGruposTurma(turma);
		int numGrupos = listGrupos.size();
		
		List<Integer> listSlides = new ArrayList<Integer>();
		for (int slide:slides){
			listSlides.add(slide);
		}
		Collections.reverse(listSlides);
		for (int i=10;i>=6+numGrupos;i--){
			listSlides.add(i);
		}
		Collections.sort(listSlides);
		Collections.reverse(listSlides);
		for (int s:listSlides) {
			ppt.removeSlide(s-1);
		}
		Collections.sort(listSlides);
		Collections.reverse(listSlides);
		out = new FileOutputStream(outputFile);
		ppt.write(out);
		out.close();
	}
*/
	public void saveClose(){
		try {
			wb.write(output);
			out = new FileOutputStream(outputFile);
			ppt.write(out);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			opened = false;
		}
	}

	public void saveCloseTable(){
		try {
			out = new FileOutputStream(outputFile);
			ppt.write(out);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			opened = false;
		}
	}

	public void saveCloseText(){
		try {
			out = new FileOutputStream(outputFile);
			out.flush();
			ppt.write(out);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			opened = false;
		}
	}

	@SuppressWarnings("deprecation")
	public String getBarCell(int lin, int col) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		String result = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[col].getVal().getNumRef().getNumCache().getPtArray()[lin].getV();
		return result;
	}

	@SuppressWarnings("deprecation")
	public void setBarCell(int lin, int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		sheet.getRow(lin).getCell(col).setCellValue(Long.parseLong(value));
		chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[col-1].getVal().getNumRef().getNumCache().getPtArray()[lin-1].setV(value);
	}

	@SuppressWarnings("deprecation")
	public void removeRowSheet(int rowindex)  throws PptNotOpenedException, InvalidFormatException, IOException { //CORRIGIR OU UNIFICAR COM changeBarCellDouble
	int xls_folder=0;
	int lineIndex = sheet.getRow(rowindex).getRowNum();
	if (!opened) throw new PptNotOpenedException();
	int lastRowNum=sheet.getLastRowNum();
	    if(lineIndex>=0&&lineIndex<lastRowNum){
	        sheet.shiftRows(lineIndex+1,lastRowNum, -1);
	    }
	     int lastRowNum1=sheet.getLastRowNum();
	    if(lineIndex==lastRowNum){
	        HSSFRow removingRow=(HSSFRow) sheet.getRow(lineIndex);
	        if(removingRow!=null){
	            sheet.removeRow(removingRow);
	        }
	    }
	}
	
	public void print() {
		System.out.println(chart.getCTChart());
	}
	
	public void removeBarLine(int numLines)  throws PptNotOpenedException {
		int xls_folder=0;
		System.out.println(sheet.getWorkbook().getSheetAt(0).getWorkbook());
		if (!opened) throw new PptNotOpenedException();
		int cols = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray().length;
		int lines=0;
		//Remove dados
		for (int c = 0;c<cols;c++) {
			CTNumVal arr[] = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().getNumCache().getPtArray();
			CTNumVal new_arr[] = new CTNumVal[arr.length-numLines];
			lines=arr.length-numLines;
			for (int k=0;k<lines;k++) {
				new_arr[k]=arr[k];
			}
			chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().getNumCache().setPtArray(new_arr);
		}
		//Remove categorias
		for (int c = 0;c<cols;c++) {
			
			CTStrVal[] arr = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().getStrCache().getPtArray();
			CTStrVal new_arr[] = new CTStrVal[arr.length-numLines];
			for (int k=0;k<new_arr.length;k++) {
				new_arr[k]=arr[k];
			}
			chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().getStrCache().setPtArray(new_arr);
			int j = 0;
			//CAT 
			String s = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().getF();
			s = s.substring(0, s.length()-(lines+numLines>9?2:1))+(lines+1);
			chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().setF(s);
			chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().getStrCache().getPtCount().setVal(new_arr.length);
			//VAL
			s = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().getF();
			s = s.substring(0, s.length()-(lines+numLines>9?2:1))+(lines+1);
			chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().setF(s);
			chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().getNumCache().getPtCount().setVal(new_arr.length);
		
		
		}
	}	
	
	
	public void removeBarColumn(int numColumns)  throws PptNotOpenedException {
		int xls_folder=0;
		System.out.println(chart.getCTChart());
		if (!opened) throw new PptNotOpenedException();
		
		int i = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray().length;
		CTBarSer[] arr = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray();
		CTBarSer[] arr_new = new CTBarSer[i-numColumns];
		for (int c = 0; c<arr_new.length; c++) {
			arr_new[c] = arr[c];
		}
		chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].setSerArray(arr_new);
	}	
	
	
	public void setBarCellDouble(int lin, int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		sheet.getRow(lin).getCell(col).setCellValue(Double.parseDouble(value));
		chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[col-1].getVal().getNumRef().getNumCache().getPtArray()[lin-1].setV(value);
	}
	
	@SuppressWarnings("deprecation")
	public void setLineCell(int lin, int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		sheet.getRow(lin).getCell(col).setCellValue(Double.parseDouble(value));
		chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[col].getVal().getNumRef().getNumCache().getPtArray()[lin].setV(value);

		//verifica se o gr�fico possui informa��es abaixo da escala m�nima
		double min = chart.getCTChart().getPlotArea().getValAxArray()[0].getScaling().getMin().getVal();
		try {
			double x = Double.parseDouble(value);
			if (x<min){
				if (x>0) {
					CTDouble d = CTDouble.Factory.newInstance();
					d.setVal(x-1);
					chart.getCTChart().getPlotArea().getValAxArray()[0].getScaling().setMin(d);
				}
			}
		} catch (Exception e){
			//N�o conseguiu converter o valor em double, ignorar
		}
	}

	@SuppressWarnings("deprecation")
	public String getPieCell(int lin, int col) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		return chart.getCTChart().getPlotArea().getPieChartArray()[xls_folder].getSerArray()[col].getVal().getNumRef().getNumCache().getPtArray()[lin].getV();
	}

	@SuppressWarnings("deprecation")
	public void setPieCell(int lin, int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		sheet.getRow(lin).getCell(col).setCellValue(Double.parseDouble(value));
		chart.getCTChart().getPlotArea().getPieChartArray()[xls_folder].getSerArray()[col].getVal().getNumRef().getNumCache().getPtArray()[lin].setV(value);
	}

	@SuppressWarnings({ "deprecation", "unused" })
	private String getBarCategory(int lin) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		return chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[0].getCat().getStrRef().getStrCache().getPtArray()[lin].getV();
	}

	@SuppressWarnings("deprecation")
	public void setBarCategory(int lin, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[0].getCat().getStrRef().getStrCache().getPtArray()[lin].setV(value);
	}

	@SuppressWarnings("deprecation")
	public void setLineCategory(int lin, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[0].getCat().getStrRef().getStrCache().getPtArray()[lin].setV(value);
	}

	@SuppressWarnings({ "deprecation", "unused" })
	private String getPieCategory(int lin) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		return chart.getCTChart().getPlotArea().getPieChartArray()[xls_folder].getSerArray()[0].getCat().getStrRef().getStrCache().getPtArray()[lin].getV();
	}

	@SuppressWarnings("deprecation")
	public void setPieCategory(int lin, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		chart.getCTChart().getPlotArea().getPieChartArray()[xls_folder].getSerArray()[0].getCat().getStrRef().getStrCache().getPtArray()[lin].setV(value);
	}

	@SuppressWarnings({ "deprecation", "unused" })
	private String getBarSerie(int col) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		return chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[1].getTx().getStrRef().getStrCache().getPtArray()[0].getV();
	}

	@SuppressWarnings("deprecation")
	public void setBarSerie(int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[col].getTx().getStrRef().getStrCache().getPtArray()[0].setV(value);
	}

	@SuppressWarnings("deprecation")
	public void setLineSerie(int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[col].getTx().getStrRef().getStrCache().getPtArray()[0].setV(value);
	}

	@SuppressWarnings({ "deprecation" })
	public String getLineSerie(int col) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		return chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[1].getTx().getStrRef().getStrCache().getPtArray()[0].getV();
	}

	@SuppressWarnings({ "deprecation" })
	public String getPieSerie(int col) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		return chart.getCTChart().getPlotArea().getPieChartArray()[xls_folder].getSerArray()[1].getTx().getStrRef().getStrCache().getPtArray()[0].getV();
	}

	@SuppressWarnings("deprecation")
	public void setPieSerie(int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		chart.getCTChart().getPlotArea().getPieChartArray()[xls_folder].getSerArray()[col].getTx().getStrRef().getStrCache().getPtArray()[0].setV(value);
	}

	public String getTableCell(int lin, int col) throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		List<XSLFTableRow> row = table.getRows();
		String value = row.get(lin).getCells().get(col).getText();
		return value;
	}

	public void setTableCell(int lin, int col, String value)  throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		List<XSLFTableRow> row = table.getRows();
		row.get(lin).getCells().get(col).setText(value);

	}

	//escreva na tabela em bold
	public void setTableCell(int lin, int col,boolean bold,int size, Color color, String alignment, String value)  throws PptNotOpenedException 
	{
		if (!opened) throw new PptNotOpenedException();
		List<XSLFTableRow> row = table.getRows();
		setTableCell(lin,col,value);
		//formata o texto da tabela
		this.setTextBold(value, color, size, bold, alignment,row.get(lin).getCells().get(col)); 
		//row.get(lin).getCells().get(col).setText(value);
		
	}


	public String getText() throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		return text.getText();
	}

	public void setText(String value)  throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		text.clearText();
		XSLFTextParagraph p = text.addNewTextParagraph();

		XSLFTextRun r1 = p.addNewTextRun();
		r1.setText(value);
		r1.setFontColor(Color.white);
		r1.setFontSize(28);
	}

	public void setText(String value, Color color,String fonte, int tamanho)  throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		text.clearText();
		XSLFTextParagraph p = text.addNewTextParagraph();

		XSLFTextRun r1 = p.addNewTextRun();
		r1.setText(value);
		r1.setFontFamily(fonte);
		r1.setFontColor(color);
		r1.setFontSize(tamanho);
	}


	public void setText(String value, Color color,String fonte, int tamanho, String alignment)  throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		text.clearText();
		XSLFTextParagraph p = text.addNewTextParagraph(); 

		XSLFTextRun r1 = p.addNewTextRun();
		r1.setText(value);
		r1.setFontFamily(fonte);
		r1.setFontColor(color);
		r1.setFontSize(tamanho);
		if (alignment.equals("left")) p.setTextAlign(TextAlign.LEFT);
		if (alignment.equals("right")) p.setTextAlign(TextAlign.RIGHT);
		if (alignment.equals("center")) p.setTextAlign(TextAlign.CENTER);
	}


	public void setTextBold(String value, Color color, int size, boolean bold, String alignment, XSLFTextShape text )  throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		text.clearText();
		XSLFTextParagraph p = text.addNewTextParagraph();
		XSLFTextRun r1 = p.addNewTextRun();
		r1.setText(value);
		if (bold==true)
		{r1.setBold(bold);};
		r1.setFontColor(color);
		r1.setFontSize(size);
		if (alignment.equals("left")) p.setTextAlign(TextAlign.LEFT);
		if (alignment.equals("right")) p.setTextAlign(TextAlign.RIGHT);
		if (alignment.equals("center")) p.setTextAlign(TextAlign.CENTER);
	}



	public void setTextColor(Color color) throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
	}

	public void setTableCell(int lin, int col, int finalCol, String value, Color color)  throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		setTableCell(lin, col, value);
		List<XSLFTableRow> row = table.getRows();
		row.get(lin).getCells().get(col).setFillColor(color);
		if (col==0){
			row.get(lin).getCells().get(col).setBorderLeft(1);
			row.get(lin).getCells().get(col).setBorderLeftColor(Color.black);
		}
		if (col==finalCol){
			row.get(lin).getCells().get(col).setBorderRight(1);
			row.get(lin).getCells().get(col).setBorderRightColor(Color.black);
		}
		row.get(lin).getCells().get(col).setBorderTop(1);
		row.get(lin).getCells().get(col).setBorderTopColor(Color.black);
		row.get(lin).getCells().get(col).setBorderBottom(1);
		row.get(lin).getCells().get(col).setBorderBottomColor(Color.black);
	}

	private XSLFChart findChart(int chartNum, XSLFSlide slide){
		int currentChart=1;
		for(POIXMLDocumentPart part : slide.getRelations()){
			if(part instanceof XSLFChart){
				if (currentChart==chartNum){
					chart = (XSLFChart) part;
					break;
				} else {
					currentChart++;
				}
			}
		}
		return chart;
	}

	private XSLFTable findTable(int tableNum, XSLFSlide slide){
		int currentTable=1;
		for(XSLFShape shape : slide){
			if (shape instanceof XSLFTable) {
				if (currentTable==tableNum) {
					shape.getAnchor();
					table = (XSLFTable) shape;
				} else {
					currentTable++;
				}
			}
		}
		return table;
	}

	private XSLFTextBox findText(int textNum, XSLFSlide slide){
		int currentText=1;
		for(XSLFShape shape : slide){
			if (shape instanceof XSLFTextBox) {
				if (currentText==textNum) {
					shape.getAnchor();
					text = (XSLFTextBox) shape;
				}
				currentText++;
			}
		}
		return text;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public int getSlideNum() {
		return slideNum;
	}

	public void setSlideNum(int slideNum) {
		this.slideNum = slideNum;
	}

	public int getChartNum() {
		return elementNum;
	}

	public void setChartNum(int chartNum) {
		this.elementNum = chartNum;
	}

}