package PptManager;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.poi.POIXMLDocumentPart;
import org.apache.poi.hslf.model.Line;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xslf.usermodel.TextAlign;
import org.apache.poi.xslf.usermodel.VerticalAlignment;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFAutoShape;
import org.apache.poi.xslf.usermodel.XSLFChart;
import org.apache.poi.xslf.usermodel.XSLFGraphicFrame;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFShapeType;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTable;
import org.apache.poi.xslf.usermodel.XSLFTableCell;
import org.apache.poi.xslf.usermodel.XSLFTableRow;
import org.apache.poi.xslf.usermodel.XSLFTextBox;
import org.apache.poi.xslf.usermodel.XSLFTextParagraph;
import org.apache.poi.xslf.usermodel.XSLFTextRun;
import org.apache.poi.xslf.usermodel.XSLFTextShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBarSer;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTLineSer;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTNumVal;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTStrVal;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSRgbColor;




public class PptManager {

	private String templateName;
	private String filename;
	private int slideNum;
	private int elementNum;
	private boolean opened = false;

	private File inputFile;
	private File outputFile;
	private FileInputStream inputstream;

	private Sheet sheet;
	private XMLSlideShow ppt;
	private XSLFSlide slide;
	private XSLFChart chart;
	private XSLFTable table;
	private XSLFTextBox text;
	private XSSFWorkbook wb;

	private FileOutputStream out;
	private InputStream input;
	private OutputStream output;

	public PptManager() {
		super();
		templateName = "exemplo_template.pptx";
		filename = "exemplo_out.pptx";
		slideNum=0;
		elementNum=1;
	}

	
	
	public PptManager(String templateName, String filename, int slideNum, int elementNum) {
		super();
		this.templateName = templateName;
		this.filename = filename;
		this.slideNum = slideNum;
		this.elementNum = elementNum;
	}

	public void readOpenChart() throws FileNotFoundException, IOException, PptChartNotFoundException{
		inputFile = new File(templateName);
		outputFile = new File(filename);
		inputstream = new FileInputStream(inputFile);
		ppt = new XMLSlideShow(inputstream);
		slide = ppt.getSlides()[slideNum];
		chart = findChart(elementNum,slide);
		if (chart!=null){
			POIXMLDocumentPart xlsPart = chart.getRelations().get(2);
			input = xlsPart.getPackagePart().getInputStream();
			output = xlsPart.getPackagePart().getOutputStream();
			wb = new org.apache.poi.xssf.usermodel.XSSFWorkbook(input);
			sheet = wb.getSheetAt(0);
			opened = true;
		} else {
			throw new PptChartNotFoundException();
		}	
	}	

	public void readOpenTable() throws FileNotFoundException, IOException, PptTableNotFoundException{
		inputFile = new File(templateName);
		outputFile = new File(filename);
		inputstream = new FileInputStream(inputFile);
		ppt = new XMLSlideShow(inputstream);
		slide = ppt.getSlides()[slideNum];
		table = findTable(elementNum,slide);
		if (table!=null){
			opened = true;
		} else {
			throw new PptTableNotFoundException();
		}
	}

	public void readOpenText() throws FileNotFoundException, IOException, PptTextNotFoundException{
		inputFile = new File(templateName);
		outputFile = new File(filename);
		inputstream = new FileInputStream(inputFile);
		ppt = new XMLSlideShow(inputstream);
		slide = ppt.getSlides()[slideNum];
		text = findText(elementNum,slide);
		if (text!=null){
			opened = true;
		} else {
			throw new PptTextNotFoundException();
		}
	}

	public void saveClose(){
		try {
			wb.write(output);
			out = new FileOutputStream(outputFile);
			ppt.write(out);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			opened = false;
		}
	}

	public void saveCloseTable(){
		try {
			out = new FileOutputStream(outputFile);
			ppt.write(out);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			opened = false;
		}
	}

	public void saveCloseText(){
		try {
			out = new FileOutputStream(outputFile);
			out.flush();
			ppt.write(out);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			opened = false;
		}
	}


	public void removeSlides(int slides[]) throws FileNotFoundException, IOException, PptTextNotFoundException{
		inputFile = new File(templateName);
		outputFile = new File(filename);
		inputstream = new FileInputStream(inputFile);
		ppt = new XMLSlideShow(inputstream);

		List<Integer> listSlides = new ArrayList<Integer>();
		for (int slide:slides){
			listSlides.add(slide);
		}


		Collections.reverse(listSlides);
		for (int s:listSlides) {
			ppt.removeSlide(s-1);
		}
		Collections.sort(listSlides);
		Collections.reverse(listSlides);
		out = new FileOutputStream(outputFile);
		ppt.write(out);
		out.close();
	}

	@SuppressWarnings("deprecation")
	public String getBarCell(int lin, int col) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		String result = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[col].getVal().getNumRef().getNumCache().getPtArray()[lin].getV();
		return result;
	}

	@SuppressWarnings("deprecation")
	public void setBarCell(int lin, int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		sheet.getRow(lin).getCell(col).setCellValue(Double.parseDouble(value));
		chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[col].getVal().getNumRef().getNumCache().getPtArray()[lin].setV(value);
		 
      }
	
	
	@SuppressWarnings("deprecation")
	public void setBarCell(int lin, int col, String value, int red, int green,int blue)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		sheet.getRow(lin).getCell(col).setCellValue(Double.parseDouble(value));
		chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[col].getVal().getNumRef().getNumCache().getPtArray()[lin].setV(value);
		 
         CTSRgbColor rgb = CTSRgbColor.Factory.newInstance();
         rgb.setVal(new byte[]{(byte)red, (byte)green, (byte)blue});
         chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[col].getSpPr().getSolidFill().setSrgbClr(rgb);
	}
	
	@SuppressWarnings("deprecation")
	public void removeRowSheet(int rowindex)  throws PptNotOpenedException, InvalidFormatException, IOException { //CORRIGIR OU UNIFICAR COM changeBarCellDouble
		int xls_folder=0;
		int lineIndex = sheet.getRow(rowindex).getRowNum();
		if (!opened) throw new PptNotOpenedException();
		int lastRowNum=sheet.getLastRowNum();
		if(lineIndex>=0&&lineIndex<lastRowNum){
			sheet.shiftRows(lineIndex+1,lastRowNum, -1);
		}
		int lastRowNum1=sheet.getLastRowNum();
		if(lineIndex==lastRowNum){
			HSSFRow removingRow=(HSSFRow) sheet.getRow(lineIndex);
			if(removingRow!=null){
				sheet.removeRow(removingRow);
			}
		}
	}

	public void print() {
		System.out.println(chart.getCTChart());
	}

	public void removeBarLine(int numLines)  throws PptNotOpenedException {
		int xls_folder=0;
		//System.out.println(chart.getCTChart());
		if (!opened) throw new PptNotOpenedException();
		int cols = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray().length;
		int lines=0;
		//Remove dados
		for (int c = 0;c<cols;c++) {
			CTNumVal arr[] = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().getNumCache().getPtArray();
			CTNumVal new_arr[] = new CTNumVal[arr.length-numLines];
			lines=arr.length-numLines;
			for (int k=0;k<lines;k++) {
				new_arr[k]=arr[k];
			}
			chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().getNumCache().setPtArray(new_arr);
		}
		//Remove categorias
		for (int c = 0;c<cols;c++) {

			CTStrVal[] arr = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().getStrCache().getPtArray();
			CTStrVal new_arr[] = new CTStrVal[arr.length-numLines];
			for (int k=0;k<new_arr.length;k++) {
				new_arr[k]=arr[k];
			}
			chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().getStrCache().setPtArray(new_arr);
			int j = 0;
			//CAT 
			String s = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().getF();
			s = s.substring(0, s.length()-(lines+numLines>9?2:1))+(lines+1);
			chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().setF(s);
			chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().getStrCache().getPtCount().setVal(new_arr.length);
			//VAL
			s = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().getF();
			s = s.substring(0, s.length()-(lines+numLines>9?2:1))+(lines+1);
			chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().setF(s);
			chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().getNumCache().getPtCount().setVal(new_arr.length);


		}
	}	

	
	public void removeLineChartLine(int numLines)  throws PptNotOpenedException {
		int xls_folder=0;
		//System.out.println(chart.getCTChart());
		if (!opened) throw new PptNotOpenedException();
		int cols = chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray().length;
		int lines=0;
		//Remove dados
		for (int c = 0;c<cols;c++) {
			CTNumVal arr[] = chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().getNumCache().getPtArray();
			CTNumVal new_arr[] = new CTNumVal[arr.length-numLines];
			lines=arr.length-numLines;
			for (int k=0;k<lines;k++) {
				new_arr[k]=arr[k];
			}
			chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().getNumCache().setPtArray(new_arr);
		}
		//Remove categorias
		for (int c = 0;c<cols;c++) {

			CTStrVal[] arr = chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().getStrCache().getPtArray();
			CTStrVal new_arr[] = new CTStrVal[arr.length-numLines];
			for (int k=0;k<new_arr.length;k++) {
				new_arr[k]=arr[k];
			}
			chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().getStrCache().setPtArray(new_arr);
			int j = 0;
			//CAT 
			String s = chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().getF();
			s = s.substring(0, s.length()-(lines+numLines>9?2:1))+(lines+1);
			chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().setF(s);
			chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[c].getCat().getStrRef().getStrCache().getPtCount().setVal(new_arr.length);
			//VAL
			s = chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().getF();
			s = s.substring(0, s.length()-(lines+numLines>9?2:1))+(lines+1);
			chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().setF(s);
			chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[c].getVal().getNumRef().getNumCache().getPtCount().setVal(new_arr.length);


		}
	}	

	public void removeBarColumn(int numColumns)  throws PptNotOpenedException {
		int xls_folder=0;
		//System.out.println(chart.getCTChart());
		if (!opened) throw new PptNotOpenedException();

		int i = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray().length;
		CTBarSer[] arr = chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray();
		CTBarSer[] arr_new = new CTBarSer[i-numColumns];
		for (int c = 0; c<arr_new.length; c++) {
			arr_new[c] = arr[c];
		}
		chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].setSerArray(arr_new);
	}	

	public void removeLIneChartColumn(int numColumns)  throws PptNotOpenedException {
		int xls_folder=0;
		//System.out.println(chart.getCTChart());
		if (!opened) throw new PptNotOpenedException();

		int i = chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray().length;
		CTLineSer[] arr = chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray();
		CTLineSer[] arr_new = new CTLineSer[i-numColumns];
		for (int c = 0; c<arr_new.length; c++) {
			arr_new[c] = arr[c];
		}
		chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].setSerArray(arr_new);
	}	


	@SuppressWarnings("deprecation")
	public void setLineCell(int lin, int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		sheet.getRow(lin).getCell(col).setCellValue(Double.parseDouble(value));
		chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[col].getVal().getNumRef().getNumCache().getPtArray()[lin].setV(value);
	}

	@SuppressWarnings("deprecation")
	public String getPieCell(int lin, int col) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		return chart.getCTChart().getPlotArea().getPieChartArray()[xls_folder].getSerArray()[col].getVal().getNumRef().getNumCache().getPtArray()[lin].getV();
	}

	@SuppressWarnings("deprecation")
	public void setPieCell(int lin, int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		sheet.getRow(lin).getCell(col).setCellValue(Double.parseDouble(value));
		chart.getCTChart().getPlotArea().getPieChartArray()[xls_folder].getSerArray()[col].getVal().getNumRef().getNumCache().getPtArray()[lin].setV(value);
	}

	@SuppressWarnings({ "deprecation", "unused" })
	private String getBarCategory(int lin) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		return chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[0].getCat().getStrRef().getStrCache().getPtArray()[lin].getV();
	}

	@SuppressWarnings("deprecation")
	public void setBarCategory(int lin, String value)  throws PptNotOpenedException {

		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[0].getCat().getStrRef().getStrCache().getPtArray()[lin].setV(value);
	}

	@SuppressWarnings("deprecation")
	public void setLineCategory(int lin, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[0].getCat().getStrRef().getStrCache().getPtArray()[lin].setV(value);
	}

	@SuppressWarnings({ "deprecation", "unused" })
	private String getPieCategory(int lin) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		return chart.getCTChart().getPlotArea().getPieChartArray()[xls_folder].getSerArray()[0].getCat().getStrRef().getStrCache().getPtArray()[lin].getV();
	}

	@SuppressWarnings("deprecation")
	public void setPieCategory(int lin, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		chart.getCTChart().getPlotArea().getPieChartArray()[xls_folder].getSerArray()[0].getCat().getStrRef().getStrCache().getPtArray()[lin].setV(value);
	}

	@SuppressWarnings({ "deprecation", "unused" })
	private String getBarSerie(int col) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		return chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[1].getTx().getStrRef().getStrCache().getPtArray()[0].getV();
	}

	@SuppressWarnings("deprecation")
	public void setBarSerie(int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[col].getTx().getStrRef().getStrCache().getPtArray()[0].setV(value);
	}

	@SuppressWarnings("deprecation")
	public void setLineSerie(int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[col].getTx().getStrRef().getStrCache().getPtArray()[0].setV(value);
	}

	@SuppressWarnings({ "deprecation" })
	public String getLineSerie(int col) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		return chart.getCTChart().getPlotArea().getLineChartArray()[xls_folder].getSerArray()[1].getTx().getStrRef().getStrCache().getPtArray()[0].getV();
	}

	@SuppressWarnings({ "deprecation" })
	public String getPieSerie(int col) throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		return chart.getCTChart().getPlotArea().getPieChartArray()[xls_folder].getSerArray()[1].getTx().getStrRef().getStrCache().getPtArray()[0].getV();
	}

	@SuppressWarnings("deprecation")
	public void setPieSerie(int col, String value)  throws PptNotOpenedException {
		int xls_folder=0;
		if (!opened) throw new PptNotOpenedException();
		chart.getCTChart().getPlotArea().getPieChartArray()[xls_folder].getSerArray()[col].getTx().getStrRef().getStrCache().getPtArray()[0].setV(value);
	}

	public void createTable(int x, int y, int width, int heigth, Color cellColor, Color borderColor, double borderSizeRight,double borderSizeLeft,double borderSizeTop,double borderSizeBottom, int rowHeight, int numLines, int numCols)

	{
		XSLFTable tableC=slide.createTable();
		tableC.setAnchor(new java.awt.Rectangle(x,y,width,heigth));


		for (int i =0;i <numLines;i++) 
		{
			XSLFTableRow row = tableC.addRow();
			row.setHeight(rowHeight);
			for (int j =0;j <numCols;j++) 
			{
				XSLFTableCell cell = row.addCell();
				cell.setBorderBottom(borderSizeBottom);
				cell.setBorderBottomColor(borderColor);
				cell.setBorderTop(borderSizeTop);
				cell.setBorderTopColor(borderColor);
				cell.setBorderRight(borderSizeRight);
				cell.setBorderRightColor(borderColor);
				cell.setBorderLeft(borderSizeLeft);
				cell.setBorderLeftColor(borderColor);
				cell.setFillColor(cellColor);
			}
		}
	}

	public void removeTableLine(int lin)  throws PptNotOpenedException {
		//		if (!opened) throw new PptNotOpenedException();
		//		List<XSLFTableRow> row = table.getRows();
		//		table.
		//		row.remove(lin);
		XSLFTable t = null;
		List<XSLFTableRow> r = null;
		//encontra a table
		for (XSLFShape shape : slide) {
			if (shape instanceof XSLFTable) {
				t = (XSLFTable) shape;
				r = t.getRows();
			}
		}
		//System.out.println(t.getNumberOfRows());
		//remove a table
		t.getCTTable().getTrList().remove(lin);
		//t.getAnchor();
		//t.setAnchor(new Anchor());

	}


	public void removeGraph(int numGraph)  throws PptNotOpenedException {
		//		if (!opened) throw new PptNotOpenedException();
		//		List<XSLFTableRow> row = table.getRows();
		//		table.
		//		row.remove(lin);
		XSLFGraphicFrame t;
		int i=0;
		for (XSLFShape shape : slide) {

			//System.out.println(slide.getShapes());
			if (shape instanceof XSLFGraphicFrame) {
				t = (XSLFGraphicFrame) shape;
				System.out.println(t.getShapeName());   
				System.out.println(t.getShapeId());
				//		        slide.removeShape(slide.getShapes()[i]);
			}
			i=i+1;    
		}

		//  t.		

		//		System.out.println(t.getNumberOfRows());
		//		
		//		t.getCTTable().getTrList().remove(lin);


	}


	public void setTableCell(int lin, int col, String value)  throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		List<XSLFTableRow> row = table.getRows();
		row.get(lin).getCells().get(col).setText(value);

	}

	//escreva na tabela em bold
	public void setTableCell(int lin, int col,boolean bold,int size, Color color, String alignment, String value)  throws PptNotOpenedException 
	{
		if (!opened) throw new PptNotOpenedException();
		List<XSLFTableRow> row = table.getRows();
		setTableCell(lin,col,value);
		//formata o texto da tabela
		this.setTextBold(value, color, size, bold, alignment,row.get(lin).getCells().get(col)); 
		//row.get(lin).getCells().get(col).setText(value);
	}
	
	public void setTableCell(int lin, int col,boolean bold,int size, Color color, String alignment, String value, Color celColor)  throws PptNotOpenedException 
	{
		if (!opened) throw new PptNotOpenedException();
		List<XSLFTableRow> row = table.getRows();
		setTableCell(lin,col,value);
		row.get(lin).getCells().get(col).setFillColor(celColor);
		//formata o texto da tabela
		this.setTextBold(value, color, size, bold, alignment,row.get(lin).getCells().get(col)); 
		//row.get(lin).getCells().get(col).setText(value);
	}



	public String getText() throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		return text.getText();
	}

	public void setText(String value)  throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		text.clearText();
		XSLFTextParagraph p = text.addNewTextParagraph();

		XSLFTextRun r1 = p.addNewTextRun();
		r1.setText(value);
		r1.setFontColor(Color.white);
		r1.setFontSize(28);
	}

	public void setTextBold(String value, Color color, int size, boolean bold, String alignment, XSLFTextShape text )  throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		text.clearText();
		XSLFTextParagraph p = text.addNewTextParagraph();
		XSLFTextRun r1 = p.addNewTextRun();
		r1.setText(value);
		if (bold==true)
		{r1.setBold(bold);};
		r1.setFontColor(color);
		r1.setFontSize(size);
		if (alignment.equals("left")) p.setTextAlign(TextAlign.LEFT);
		if (alignment.equals("right")) p.setTextAlign(TextAlign.RIGHT);
		if (alignment.equals("center")) p.setTextAlign(TextAlign.CENTER);
	}



	public void setTextColor(Color color) throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
	}

	public void setText(String value, Color color,String fonte, int tamanho, String alignment)  throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		text.clearText();
		XSLFTextParagraph p = text.addNewTextParagraph(); 

		XSLFTextRun r1 = p.addNewTextRun();
		r1.setText(value);
		r1.setFontFamily(fonte);
		r1.setFontColor(color);
		r1.setFontSize(tamanho);
		if (alignment.equals("left")) p.setTextAlign(TextAlign.LEFT);
		if (alignment.equals("right")) p.setTextAlign(TextAlign.RIGHT);
		if (alignment.equals("center")) p.setTextAlign(TextAlign.CENTER);
	}


	public void setTableCell(int lin, int col, int finalCol, String value, Color color)  throws PptNotOpenedException {
		if (!opened) throw new PptNotOpenedException();
		setTableCell(lin, col, value);
		List<XSLFTableRow> row = table.getRows();
		row.get(lin).getCells().get(col).setFillColor(color);
		if (col==0){
			row.get(lin).getCells().get(col).setBorderLeft(1);
			row.get(lin).getCells().get(col).setBorderLeftColor(Color.black);
		}
		if (col==finalCol){
			row.get(lin).getCells().get(col).setBorderRight(1);
			row.get(lin).getCells().get(col).setBorderRightColor(Color.black);
		}
		row.get(lin).getCells().get(col).setBorderTop(1);
		row.get(lin).getCells().get(col).setBorderTopColor(Color.black);
		row.get(lin).getCells().get(col).setBorderBottom(1);
		row.get(lin).getCells().get(col).setBorderBottomColor(Color.black);
	}

	private XSLFChart findChart(int chartNum, XSLFSlide slide){
		int currentChart=1;
		for(POIXMLDocumentPart part : slide.getRelations()){
			if(part instanceof XSLFChart){
				if (currentChart==chartNum){
					chart = (XSLFChart) part;
					break;
				} else {
					currentChart++;
				}
			}
		}
		return chart;
	}

	private XSLFTable findTable(int tableNum, XSLFSlide slide){
		int currentTable=1;
		for(XSLFShape shape : slide){
			if (shape instanceof XSLFTable) {
				if (currentTable==tableNum) {
					shape.getAnchor();
					table = (XSLFTable) shape;
				} else {
					currentTable++;
				}
			}
		}
		return table;
	}

	private XSLFTextBox findText(int textNum, XSLFSlide slide){
		int currentText=1;
		for(XSLFShape shape : slide){
			if (shape instanceof XSLFTextBox) {
				if (currentText==textNum) {
					shape.getAnchor();
					text = (XSLFTextBox) shape;
				}
				currentText++;
			}
		}
		return text;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public int getSlideNum() {
		return slideNum;
	}

	public void setSlideNum(int slideNum) {
		this.slideNum = slideNum;
	}

	public int getChartNum() {
		return elementNum;
	}

	public void setChartNum(int chartNum) {
		this.elementNum = chartNum;
	}



	public void insertRemoveShape() throws IOException, InvalidFormatException {


		Line line = new Line();
		line.setAnchor(new java.awt.Rectangle(50, 50, 100, 20));
		line.setLineColor(new Color(0, 128, 0));
		line.setLineStyle(Line.LINE_DOUBLE);
		System.out.println(slide.getShapes()[2].getShapeName());
		System.out.println(slide.getShapes()[2].getShapeId());
		System.out.println(slide.getShapes()[2].getAnchor());
		System.out.println(slide.getShapes()[2].getAnchor().getX());
		System.out.println(slide.getShapes()[2].getAnchor().getY());
		slide.getShapes()[2].setAnchor(new java.awt.Rectangle(250, 250, 100, 20));
		System.out.println(slide.getShapes()[2].getFlipHorizontal());
		System.out.println(slide.getShapes()[2].getFlipVertical());
		System.out.println(slide.getCommonSlideData());
		slide.getShapes();

		int i;
		for(i=1;i<slide.getShapes().length-1;i++)
		{
			slide.getShapes()[i]=slide.getShapes()[i+1];

		}
		//remove shape da tela.		
		slide.removeShape(slide.getShapes()[i]);

	}

	public void insertTextBox (String text, int x, int y, int heigth, int width, Color color, String fontName, int fontSize, Boolean negrito, int alinhamentoTexto, int alinhamentoVertical)
	
	{
		
		
		XSLFTextBox caixaTexto= slide.createTextBox();
	    //seta  o alinhamento vertical
		switch(alinhamentoVertical)
	    {
	    case 1: caixaTexto.setVerticalAlignment(VerticalAlignment.MIDDLE); break;
	    case 2: caixaTexto.setVerticalAlignment(VerticalAlignment.TOP); break;
	    case 3: caixaTexto.setVerticalAlignment(VerticalAlignment.BOTTOM); break;
	    }
		
		//retira as margens
		caixaTexto.setLeftInset(0);
		caixaTexto.setRightInset(0);
		caixaTexto.setBottomInset(0);
		caixaTexto.setTopInset(0);
		caixaTexto.setAnchor(new java.awt.Rectangle(x, y, heigth, width));
	    XSLFTextParagraph paragrafo=caixaTexto.addNewTextParagraph();
	    switch(alinhamentoTexto)
	    {
	    case 1: paragrafo.setTextAlign(TextAlign.CENTER); break;
	    case 2: paragrafo.setTextAlign(TextAlign.LEFT); break;
	    case 3: paragrafo.setTextAlign(TextAlign.RIGHT); break;
	    }
	    XSLFTextRun elementTextRun =  paragrafo.addNewTextRun();
		elementTextRun.setFontFamily(fontName); 
		elementTextRun.setFontSize(fontSize); 
		elementTextRun.setFontColor(color);
		elementTextRun.setBold(negrito);
		elementTextRun.setText(text);	
		
	}




	public void insertShape(String text,int x, int y, int width, int heigth, Color colorShape,String fontName, int fontSize, Color colorFont, int type) 
	{
		XSLFAutoShape shape= slide.createAutoShape();
		switch(type)
		{
		case 1: shape.setShapeType(XSLFShapeType.UP_ARROW); break;
		case 2: shape.setShapeType(XSLFShapeType.DOWN_ARROW); break;
		case 3: shape.setShapeType(XSLFShapeType.LEFT_ARROW); break;
		case 4: shape.setShapeType(XSLFShapeType.RIGHT_ARROW); break;
		case 5: shape.setShapeType(XSLFShapeType.RECT); break;
		case 6: shape.setShapeType(XSLFShapeType.ROUND_RECT); break;
		case 7: shape.setShapeType(XSLFShapeType.LINE); break;
		case 8: shape.setShapeType(XSLFShapeType.LINE_INV); break;
		case 9: shape.setShapeType(XSLFShapeType.ELLIPSE); break;
		}

		shape.setAnchor(new java.awt.Rectangle(x,y,width,heigth));
		shape.setFillColor(colorShape);
		XSLFTextRun elementTextRun =  shape.addNewTextParagraph().addNewTextRun(); 
		elementTextRun.setFontFamily(fontName); 
		elementTextRun.setFontSize(fontSize); 
		elementTextRun.setFontColor(colorFont);
		elementTextRun.setText(text);	
	
	}	
	//		caixaTexto.setAnchor(new java.awt.Rectangle(x, y, heigth, width));
	//		XSLFTextRun elementTextRun =  caixaTexto.addNewTextParagraph().addNewTextRun(); 
	//		elementTextRun.setFontFamily(fontName); 
	//		elementTextRun.setFontSize(fontSize); 
	//		elementTextRun.setFontColor(color);
	//		elementTextRun.setText(text);		
	//}

	//    slide.getCommonSlideData()9

	//      chart.getCTChart().getPlotArea().getBarChartArray()[xls_folder].getSerArray()[1].getTx().getStrRef().getStrCache().getPtArray()[0].getV();




	//,
	//		wb.s
	//		f.close();

	//		System.out.println("Number Of Sheets" + wb.getNumberOfSheets());
	//		Sheet s = wb.getSheetAt(0);
	//		System.out.println("Number Of Rows:" + s.getLastRowNum());

 public void removeChart() throws InvalidFormatException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
 {


	  Map<String, XSLFGraphicFrame> chartFramesToRemove = new HashMap<String, XSLFGraphicFrame>();

	  for (XSLFShape shape : slide.getShapes()) {
	   if (shape instanceof XSLFGraphicFrame) {
	    XSLFGraphicFrame graphicframe = (XSLFGraphicFrame)shape;
	    XmlObject xmlobject = graphicframe.getXmlObject();
	    XmlObject[] graphics = xmlobject.selectPath(
	                            "declare namespace a='http://schemas.openxmlformats.org/drawingml/2006/main' " +
	                            ".//a:graphic");
	    if (graphics.length > 0) { //we have a XSLFGraphicFrame containing a:graphic
	     XmlObject graphic = graphics[0];
	     XmlObject[] charts = graphic.selectPath(
	                           "declare namespace c='http://schemas.openxmlformats.org/drawingml/2006/chart' " +
	                           ".//c:chart");
	     if (charts.length > 0) { //we have a XSLFGraphicFrame containing c:chart
	      XmlObject chart = charts[0];
	      String rid = chart.selectAttribute(
	                          "http://schemas.openxmlformats.org/officeDocument/2006/relationships", "id")
	                          .newCursor().getTextValue();
	      chartFramesToRemove.put(rid, graphicframe);
	     }
	    }
	   }
	  }

	  PackagePart slidepart = slide.getPackagePart();
	  OPCPackage opcpackage = ppt.getPackage();

	  for (String rid : chartFramesToRemove.keySet()) {
	   //at frist remove the XSLFGraphicFrame
	   XSLFGraphicFrame chartFrame = chartFramesToRemove.get(rid);
	   slide.removeShape(chartFrame);
	   //Here is the problem in my opinion. This **should** remove all related parts too.
	   //But since XSLFChart is @Beta, it does not.

	   //So we try doing removing the related parts manually.

	   //we get the PackagePart of the chart
	   PackageRelationship relship = slidepart.getRelationships().getRelationshipByID(rid);
	   PackagePart chartpart = slidepart.getRelatedPart(relship);

	   //now we get and remove all the relations and related PackageParts from this chartpart
	   //this are /ppt/embeddings/Microsoft_Excel_WorksheetN.xlsx, /ppt/charts/colorsN.xml 
	   //and /ppt/charts/styleN.xml
	   for (PackageRelationship chartrelship : chartpart.getRelationships()) {
	    String partname = chartrelship.getTargetURI().toString();
	    PackagePart part = opcpackage.getPartsByName(Pattern.compile(partname)).get(0);
	    opcpackage.removePart(part);
	    chartpart.removeRelationship(chartrelship.getId());
	   }

	   //now we remove the chart part from the slide part
	   //We need doing this on POIXMLDocumentPart level. 
	   //Since POIXMLDocumentPart.removeRelation is protected, we need doing this using reflection
	   XSLFChart chart = (XSLFChart)slide.getRelationById(rid);
	   Method removeRelation = POIXMLDocumentPart.class.getDeclaredMethod("removeRelation", POIXMLDocumentPart.class); 
	   removeRelation.setAccessible(true); 
	   removeRelation.invoke(slide, chart);

	  }

 }

// public void moveChart()
// {
//  chart.remo
// }	
	
}