package webService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

//wla:
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
//wla:

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

//import webServiceController.WSAdminController;
import webServiceController.WSApplicationController;
import webServiceController.WSCadastroAcessoController;
//import webServiceController.WSFacilitadorController;
import webServiceController.WSLoginController;
import webServiceController.WSTurmaController;
//import webServiceController.WSValorController;
import webServiceController.WSVendasController;


/**
 *
 * @author BMI Tecnologia
 */

@Path("/srv_vendas")
public class Resource {

	@POST
	@Path("ws_vendas_post")
	@Consumes("application/json; charset=utf-8")
	@Produces("application/json; charset=utf-8")
	public String webServiceRegistrar( String jsonRecebido ) throws Throwable {

		String retorno="";
		String result = "";
		try {
			JsonParser jsonParser = new JsonParser();
			JsonObject jsonObj = (JsonObject)jsonParser.parse(jsonRecebido);

			if (jsonObj.get("page").getAsString().equals("vendas")){
				WSVendasController controller = new WSVendasController();
				retorno = controller.process(jsonRecebido);
			}

			//define m�todo de tratamento da resposta no client
			//result = "callback( "+retorno+" )";
			result=retorno;
			if (jsonObj.get("jsonp")!=null) {
				result = ""+jsonObj.get("jsonp");
				result = result.trim().replaceAll("\"","")+"( "+retorno+" )";
			}
		} catch (Throwable tx) {
			tx.printStackTrace();
			throw tx;
		}
		return result;
	}

	@GET
	@Path("ws_vendas/{valor}")
	public String webservice(@PathParam("valor") String jsonRecebido) 
			throws Throwable{

		String retorno="";

		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObj = (JsonObject)jsonParser.parse(jsonRecebido);




		if (jsonObj.get("page").getAsString().equals("application")){
			WSApplicationController controller = new WSApplicationController();
			retorno = controller.process(jsonRecebido);
		}

		if (jsonObj.get("page").getAsString().equals("vendas")){
			WSVendasController controller = new WSVendasController();
			retorno = controller.process(jsonRecebido);
		}


		if (jsonObj.get("page").getAsString().equals("login")){
			WSLoginController controller = new WSLoginController();
			retorno = controller.process(jsonRecebido);
		}

		/*  if (jsonObj.get("page").getAsString().equals("facilitador")){
        	WSFacilitadorController controller = new WSFacilitadorController();
        	retorno = controller.process(jsonRecebido);
        }

        if (jsonObj.get("page").getAsString().equals("turma")){
        	WSTurmaController controller = new WSTurmaController();
        	retorno = controller.process(jsonRecebido);
        }

        if (jsonObj.get("page").getAsString().equals("admin")){
        	WSAdminController controller = new WSAdminController();
        	retorno = controller.process(jsonRecebido);

        }

        if (jsonObj.get("page").getAsString().equals("valor")){
        	WSValorController controller = new WSValorController();
        	retorno = controller.process(jsonRecebido);
        }*/

		if (jsonObj.get("page").getAsString().equals("cadastroAcesso")){
			WSCadastroAcessoController controller = new WSCadastroAcessoController();
			retorno = controller.process(jsonRecebido);
		}

		//define m�todo de tratamento da resposta no client
		String result = "callback( "+retorno+" )";
		if (jsonObj.get("jsonp")!=null) {
			result = ""+jsonObj.get("jsonp");
			result = result.trim().replaceAll("\"","")+"( "+retorno+" )";
		}
		return result;
	}
}