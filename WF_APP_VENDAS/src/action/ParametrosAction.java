package action;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import Beans.Parametros;
import Service.ParametrosService;




public class ParametrosAction {

	
//
//	public String buscarParametrossProjeto(JsonObject jsonObj) {
//		List<Parametros> listaParametros = new ArrayList<Parametros>();
//		JsonObject jsonObjResult = new JsonObject() ;
//		String result,lista;
//		ParametrosService parametrosService=new ParametrosService();
//		int projeto     = jsonObj.get("projeto").getAsInt();
//		listaParametros=parametrosService.findByFieldOrdered("projeto", projeto,"nome");
//		Gson gson = new Gson();
//
//		// converte objetos Java para JSON e retorna JSON como String
//		lista = gson.toJson(listaParametros);
//		jsonObjResult.addProperty("action", "buscarParametrossProjeto");
//		jsonObjResult.addProperty("retorno", lista);
//		result = jsonObjResult.toString();
//		System.out.println(result);
//		return result;
//	}
//	
	public String buscarParametros(JsonObject jsonObj) {
		List<Parametros> listaParametros = new ArrayList<Parametros>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		ParametrosService parametrosService=new ParametrosService();
		listaParametros=parametrosService.findAll();
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaParametros);
		jsonObjResult.addProperty("action", "buscarParametros");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	public String removerParametros(JsonObject jsonObj) {
		
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		ParametrosService parametrosService=new ParametrosService();
		
		int parametros     = jsonObj.get("parametros").getAsInt();
		parametrosService.delete(parametros);

		jsonObjResult.addProperty("action", "removerParametros");
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
	
	
	
	

}