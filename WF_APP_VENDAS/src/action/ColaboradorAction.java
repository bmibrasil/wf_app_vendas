package action;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.Acesso;
import Beans.Colaborador;
import Beans.Organizacao;
import Dao.AcessoDao;
import Dao.ColaboradorDao;
import Service.ColaboradorService;
import webService.DMLSalesForce;




public class ColaboradorAction {




	public String bloqueiaColaborador(JsonObject jsonObj) {
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		ColaboradorService colaboradorService=new ColaboradorService();
		String acesso     = jsonObj.get("acesso").getAsString();
		String bloqueio     = jsonObj.get("bloqueio").getAsString();
		colaboradorService.bloqueiaColaborador(acesso,bloqueio);

		jsonObjResult.addProperty("action", "bloqueiaColaborador");
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}



	public String buscarTodosColaboradores(JsonObject jsonObj) {
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		ColaboradorService colaboradorService=new ColaboradorService();
		listaColaborador=colaboradorService.buscaTodosColaborador();
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaColaborador);
		jsonObjResult.addProperty("action", "buscarTodosColaboradores");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}


	public String buscarColaboradores(JsonObject jsonObj) {
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		ColaboradorService colaboradorService=new ColaboradorService();
		int colaborador     = jsonObj.get("colaborador").getAsInt();
		listaColaborador=colaboradorService.buscaColaboradorEquipe(colaborador);
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaColaborador);
		jsonObjResult.addProperty("action", "buscarColaboradores");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}

	public String buscarColaborador(JsonObject jsonObj) 
	{
		String idSalesForce="";

		JsonObject jsonObjSelecao = new JsonObject() ;
		JsonObject jsonObjSelecaoUnidade = new JsonObject() ;
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista="",campo="";
		int retorno;
		ColaboradorService colaboradorService=new ColaboradorService();
		String email     = jsonObj.get("email").getAsString();
		String senha     = jsonObj.get("senha").getAsString();
		String unidade     = jsonObj.get("unidade").getAsString();


		String token;
		String primeiroNome ="";
		String ultimoNome ="";
		String[] campos= {"Usuario__r.id","Usuario__r.Token__c","Unidade_Negocio__c","Usuario__r.FirstName","Usuario__r.LastName"};
		//tirar o .nexisim
		String condicaoWhere="Usuario__r.email='"+email+"'%20AND%20Usuario__r.Token__c='"+senha+"'"+"%20AND%20Unidade_Negocio__c='"+unidade+"'"; 
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Unidade_de_Negocio_do_Usuario__c", campos,condicaoWhere,"Usuario__r.id");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		Colaborador colaboradorSalesForce = new Colaborador();

		//JsonObject organizacaoJson     = jsonObjElement.get("User").getAsJsonObject();

		JsonElement jsonElementArray = jsonArray.get(0);
		JsonObject jsonObjElement = jsonElementArray.getAsJsonObject();
		JsonObject usuarioJson     = jsonObjElement.get("Usuario__r").getAsJsonObject();

		try{idSalesForce= usuarioJson.get("Id").getAsString();}
		catch(Exception ex){idSalesForce="";}

		try{token= usuarioJson.get("Token__c").getAsString();}
		catch(Exception ex){token="";}

		try{primeiroNome= usuarioJson.get("FirstName").getAsString();}
		catch(Exception ex){primeiroNome="";}
		
		try{ultimoNome= usuarioJson.get("LastName").getAsString();}
		catch(Exception ex){ultimoNome="";}

		if (idSalesForce.equals("")==true)
		{
			retorno=1;
		}
		else
		{
			retorno=0;
		}

		//	}


		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaColaborador);

		jsonObjResult.addProperty("action", "buscarColaborador");
		jsonObjResult.addProperty("retorno", retorno);
		jsonObjResult.addProperty("salesForce", idSalesForce);	
		jsonObjResult.addProperty("unidade", unidade);	
		jsonObjResult.addProperty("primeiroNome", primeiroNome);	
		jsonObjResult.addProperty("ultimoNome", ultimoNome);	
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;

	}


	public String removerColaborador(JsonObject jsonObj) {

		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		ColaboradorService colaboradorService=new ColaboradorService();

		int colaborador     = jsonObj.get("colaborador").getAsInt();
		colaboradorService.delete(colaborador);

		jsonObjResult.addProperty("action", "removerColaborador");
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}


	public String cadastroAcesso(JsonObject jsonObj) throws Exception 
	{   
		String idSalesForce="";
		JsonObject jsonObjSelecao = new JsonObject() ;
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista,email,nome,senha;
		int codigoAcesso=0;

		ColaboradorService colaboradorService= new ColaboradorService();
		ColaboradorDao colaboradorDao= new ColaboradorDao();
		Colaborador colaborador= new Colaborador();

		email   = jsonObj.get("email").getAsString();	
		senha   = jsonObj.get("senha").getAsString();
		nome   = jsonObj.get("nome").getAsString();
		Hashtable<String,Object> hash = new Hashtable<String,Object>();
		hash.put("nome", nome);
		hash.put("email", email);
		List<Colaborador> listPart = new  ColaboradorDao().findByFields(hash);

		colaborador=listPart.get(0);
		colaborador.setSenha(senha);
		colaboradorService.save(colaborador.getId(),colaborador.getNome(), colaborador.getCargaHoraria(), colaborador.getHorasEsperadas(),colaborador.getRealizado(),colaborador.getValor(),colaborador.getEmail(),senha);

		if (listPart.size() >0)
		{
			String[] campos= {"Id"};
			//tirar o .nexisim
			jsonObjSelecao=DMLSalesForce.selecionaSalesForce("User", campos,"Username='"+listPart.get(0).getEmail()+".nexisim"+"'","");
			JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
			Colaborador colaboradorSalesForce = new Colaborador();
			JsonElement jsonElement = jsonArray.get(0);
			JsonObject jsonObjElement = jsonElement.getAsJsonObject();
			//JsonObject organizacaoJson     = jsonObjElement.get("User").getAsJsonObject();


			try{idSalesForce= jsonObjElement.get("Id").getAsString();}
			catch(Exception ex){idSalesForce="";}

		}


		Gson gson = new Gson();

		//			// converte objetos Java para JSON e retorna JSON como String
		//			lista = gson.toJson(listaEmpresa);

		//result = jsonObjResult.toString();
		//System.out.println("result:"+result);
		jsonObjResult.addProperty("action", "cadastroAcesso");
		jsonObjResult.addProperty("retorno", 0);
		jsonObjResult.addProperty("salesForce", idSalesForce);	
		result = jsonObjResult.toString();
		System.out.println(result);


		return result;
	}





}