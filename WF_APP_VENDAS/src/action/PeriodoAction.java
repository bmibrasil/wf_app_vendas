package action;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import Beans.Pessoa;
import Beans.Periodo;
import Service.PessoaService;
import Service.PeriodoService;




public class PeriodoAction {
	

	
	public String buscarPeriodos(JsonObject jsonObj) {
		List<Periodo> listaPeriodo = new ArrayList<Periodo>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		PeriodoService periodoService=new PeriodoService();
		listaPeriodo=periodoService.findAllOrdered("datainicial");
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaPeriodo);
		jsonObjResult.addProperty("action", "buscarPeriodos");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
	public String buscarPeriodo(JsonObject jsonObj) {
		List<Periodo> listaPeriodo= new ArrayList<Periodo>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista="",campo="";
		int retorno;
		PeriodoService periodoService=new PeriodoService();
		int periodo     = jsonObj.get("periodo").getAsInt();
		
		listaPeriodo=periodoService.findByField("id", periodo);
		// converte objetos Java para JSON e retorna JSON como String
		Gson gson = new Gson();
		
		lista = gson.toJson(listaPeriodo);
		jsonObjResult.addProperty("action", "buscarPeriodo");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	public String removerPeriodo(JsonObject jsonObj) {
		
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		PeriodoService periodoService=new PeriodoService();
		
		int periodo     = jsonObj.get("periodo").getAsInt();
		periodoService.delete(periodo);

		jsonObjResult.addProperty("action", "removerPeriodo");
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
	
	
	
	

}