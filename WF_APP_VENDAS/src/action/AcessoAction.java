package action;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import Beans.Acesso;
import Beans.AcessoEmpresa;
import Beans.Turma;
import Service.AcessoEmpresaService;
import Service.AcessoService;
//import Service.TurmaService;
import webService.DMLSalesForce;

public class AcessoAction {
	
	public String verificaAcesso(JsonObject jsonObj) throws ParseException, SQLException {
		int codigo=0;
	
		int retorno;
		String result="";
		String lista="";
		int finalAssessment=0;
		int liberacaoFase=0;
		
		//int validaDominio = 0;  
		//int codigoAcesso  = 0;  
		int statusTurma   = 0;
		int numeroRemocoes=0;
		JsonObject jsonObjResult = new JsonObject();
		JsonObject jsonObjSelecao = new JsonObject() ;
		
		codigo = jsonObj.get("codigo").getAsInt();	
		List<Acesso> acesso= new ArrayList<Acesso>();

		AcessoService   acessoService   = new AcessoService();
		AcessoEmpresaService   acessoEmpresaService   = new AcessoEmpresaService();
		//TurmaService turmaService = new TurmaService();
		Turma turma= new Turma();
        
		String[] campos= {"Organizacao__r.Id", "Organizacao__r.Name", "Organizacao__r.CEP__c",  "Organizacao__r.rua__c",  "Organizacao__r.cidade__c",  "Organizacao__r.estado__c",  "Organizacao__r.CNPJ__c",  "Organizacao__r.Industry",  "name" };
        System.out.println("cheguei aqui");
		
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("StatusOrganizacao__c", campos,"","Organizacao__r.Name");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		
		
		acesso=acessoService.findByField("codigoAcesso", codigo);
		//senha não existe
		if (acesso.size()==0)
		{
			retorno=2;
			
		}
		else
		{
			//verifica se turma esta aberta
			//turma=turmaService.findByField("id", acesso.get(0).getTurma()).get(0);
			//statusTurma=turma.getStatus();
			numeroRemocoes=turma.getPercentualRemocao();
			//turma aberta
			if (statusTurma==0)
			{
				retorno=0;
			}	
			else
			{ //turma fechada
				retorno=1;
			}
			Gson gson = new Gson();

			// converte objetos Java para JSON e retorna JSON como String
			lista = gson.toJson(acesso);
		}
	
		//busca  o acesso do facilitador
		// int faseFacilitador=acessoEmpresaService.consultarFaseFacilitador(acesso.get(0).getTurma());
		
		
		//if (acesso.get(0).getFase() >faseFacilitador)
		//{
			liberacaoFase=1;
	//	}
		
		
		jsonObjResult.addProperty("action", "verificaAcesso");
		jsonObjResult.addProperty("acesso", lista);
		jsonObjResult.addProperty("tipoAcesso", acesso.get(0).getTipo());
		jsonObjResult.addProperty("turma", turma.getId());
		jsonObjResult.addProperty("percentualRemocao", turma.getPercentualRemocao());
		jsonObjResult.addProperty("totalValores", turma.getTotalValores());
		jsonObjResult.addProperty("fase", acesso.get(0).getFase());
		jsonObjResult.addProperty("liberacaoFase",liberacaoFase );
		jsonObjResult.addProperty("retorno", retorno);

		result = jsonObjResult.toString();
		return result;
	}

	public String criaAcesso(JsonObject jsonObj) throws ParseException, SQLException {
		AcessoEmpresaService     acessoEmpresaService     = new AcessoEmpresaService();
		AcessoService     acessoService     = new AcessoService();
		
		int    turma,tipo;
		String nome;
		
		
		String result;
		
		JsonObject jsonObjResult = new JsonObject();
		
		turma      = jsonObj.get("turma").getAsInt();
		nome       = jsonObj.get("nome").getAsString();
		tipo      = jsonObj.get("tipo").getAsInt();
		
		
		Acesso acesso = new Acesso();
		acesso.setTurma(turma);
		acesso.setNome(nome);
		acesso.setTipo(tipo);
		
		//verifica se o usuario já existe
		int acessoId =0;
		acessoId=acessoService.getID(acesso);//busca o id do elemento rec�m criado criado
		
		if (acessoId !=0)
		{ 
		  acessoService.insert(acesso);
		  acessoId=acessoService.getID(acesso);//busca o id do elemento rec�m criado criado
		}
		
		
		AcessoEmpresa acessoEmpresa = new AcessoEmpresa();
		acessoEmpresa.setAcesso(acessoId);
		acessoEmpresa.setFase(1);
		acessoEmpresa.setAcesso(acessoId);
	//	acessoEmpresaService.insert(acessoEmpresa);
		
		jsonObjResult.addProperty("action", "criaAcesso");
		
		result = jsonObjResult.toString();
		
		System.out.println(result);
		
		return result;
	}
	
	
	public String buscarAcessos(JsonObject jsonObj) 
	{
	AcessoService     acessoService     = new AcessoService();	
	AcessoEmpresaService     acessoEmpresaService     = new AcessoEmpresaService();
	List<Acesso> lista = new ArrayList<Acesso>(); 
	String result;
	JsonObject jsonObjResult = new JsonObject();
	
	int turma     = jsonObj.get("turma").getAsInt();
	//apaga o acesso
	
	lista= acessoService.findByField("turma", turma);
	Gson gson = new Gson();

	// converte objetos Java para JSON e retorna JSON como String
	result = gson.toJson(lista);
	
	jsonObjResult.addProperty("action", "buscarAcessos");
	jsonObjResult.addProperty("retorno", result);
	
	result = jsonObjResult.toString();
	
	System.out.println(result);
	
	return result;
}
	
	
	public String excluirAcesso(JsonObject jsonObj) 
	{
	AcessoService     acessoService     = new AcessoService();	
	AcessoEmpresaService     acessoEmpresaService     = new AcessoEmpresaService();
	List<AcessoEmpresa> lista = new ArrayList<AcessoEmpresa>(); 
	String result;
	JsonObject jsonObjResult = new JsonObject();
	
	int acesso     = jsonObj.get("acesso").getAsInt();
	//apaga o acesso
	acessoService.delete(acesso);
//	lista= acessoEmpresaService.findByField("acesso", acesso);
	//apaga os negocios do acesso
	//for(int i = 0 ; i <lista.size();i++)
	//{
	//	acessoEmpresaService.delete(lista.get(i).getId());
	//}
	
	
	jsonObjResult.addProperty("action", "removeAcesso");
	jsonObjResult.addProperty("retorno", 0);
	
	result = jsonObjResult.toString();
	
	System.out.println(result);
	
	return result;
}
}