package action;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import Beans.Acesso;
import Beans.AcessoEmpresa;

import Beans.Turma;
import Dao.AcessoEmpresaDao;
import Service.AcessoEmpresaService;
import Service.AcessoService;


//import Service.TurmaService;

public class AcessoEmpresaAction {

/*
	public String submeterFaseAcesso(JsonObject jsonObj) throws ParseException, SQLException {
		AcessoEmpresa     acessoEmpresa     = new AcessoEmpresa();
		AcessoEmpresaService     acessoEmpresaService     = new AcessoEmpresaService();
		
        AcessoService acessoService= new AcessoService();
        
			
		String result,lista;
		
		JsonObject jsonObjResult = new JsonObject();
		

		Acesso acessoObj = new Acesso();
		int acesso = jsonObj.get("acesso").getAsInt();
		int empresa = jsonObj.get("empresa").getAsInt();
		int fase = jsonObj.get("fase").getAsInt();
				
		
		Hashtable<String, Object> hash = new Hashtable<String, Object>();
		hash.put("acesso", acesso);
		hash.put("empresa", empresa);
		List<AcessoEmpresa> list = acessoEmpresaService.findByFields(hash);
	    acessoEmpresa=list.get(0);
		acessoEmpresa.setFase(fase);
	    acessoEmpresaService.save(acessoEmpresa);
	    
	    
	        
	    
	    acessoObj=acessoService.findByField("id",acesso).get(0);
		if ((acessoObj.getTipo()==2) && ((fase==2) || (fase==3)))
		{
			ValorAction valorAction= new ValorAction();
			valorAction.atualizaListaAcessos(acessoObj.getTurma(), fase,empresa,acesso);
			
		}
		
		if ((acessoObj.getTipo()==2) && ((fase>3)))
		{
			ValorAction valorAction= new ValorAction();
			valorAction.atualizaFaseAcesso(acessoObj.getTurma(), fase,empresa);
			
		}
		
		//atualiza a fase geral do grupo para todos os negocios
		
		acessoObj.setFase(acessoEmpresaService.consultarMenorFaseGrupo(acessoObj.getTurma(), acessoObj.getId()));
	    acessoService.save(acessoObj);
		
		
		jsonObjResult.addProperty("action", "submeterFaseAcesso");
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		
		System.out.println(result);
		
		return result;
	}
	

	public String consultarFaseAcesso(JsonObject jsonObj) throws ParseException, SQLException {
		Acesso     acessoObj    = new Acesso();
		AcessoService     acessoService     = new AcessoService();
		AcessoEmpresa     acessoEmpresa    = new AcessoEmpresa();
		AcessoEmpresaService     acessoEmpresaService     = new AcessoEmpresaService();
		int liberacaoFase=0;
			
		String result,lista, listaAcessoEmpresa;
		
		JsonObject jsonObjResult = new JsonObject();
		

		
		int acesso = jsonObj.get("acesso").getAsInt();
		int empresa = jsonObj.get("empresa").getAsInt();
				

		List<Acesso> list = acessoService.findByField("id",acesso);
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(list);
		
		
		//busca os acesso do usuario		
		Hashtable<String, Object> hash = new Hashtable<String, Object>();
		hash.put("acesso", acesso);
		hash.put("empresa", empresa);
		List<AcessoEmpresa> listAcessoEmpresa = acessoEmpresaService.findByFields(hash);
		listaAcessoEmpresa= gson.toJson(listAcessoEmpresa);
		
		//busca  o acesso do facilitador
		 int faseFacilitador=acessoEmpresaService.consultarFaseFacilitador(list.get(0).getTurma());
		
		
		if (listAcessoEmpresa.get(0).getFase() >faseFacilitador)
		{
			liberacaoFase=1;
		}

		jsonObjResult.addProperty("action", "consultarFaseAcesso");
		jsonObjResult.addProperty("retorno", lista);
		jsonObjResult.addProperty("faseNegocio", listaAcessoEmpresa);
		jsonObjResult.addProperty("liberacaoFase", liberacaoFase);
		result = jsonObjResult.toString();
		
		System.out.println(result);
		
		return result;
	}


	

	public String consultarMenorFaseAcessoTurma(JsonObject jsonObj) throws ParseException, SQLException {
		AcessoEmpresa     acessoEmpresa     = new AcessoEmpresa();
		AcessoEmpresaService     acessoEmpresaService     = new AcessoEmpresaService();
		
			
		String result,lista;
		
		JsonObject jsonObjResult = new JsonObject();
				
		int turma = jsonObj.get("turma").getAsInt();
		int acesso = jsonObj.get("acesso").getAsInt();		
		
		int menorFase = acessoEmpresaService.consultarMenorFaseAcessoTurma(turma,acesso);
		int turmaFase=acessoEmpresaService.consultarFaseFacilitador(turma);
		// converte objetos Java para JSON e retorna JSON como String
		
		
		jsonObjResult.addProperty("action", "consultarMenorFaseAcessoTurma");
		jsonObjResult.addProperty("menorFase", menorFase);
		jsonObjResult.addProperty("turmaFase", turmaFase);
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		
		System.out.println(result);
		
		return result;
	}
	
	
	public String consultarMenorFaseTurma(JsonObject jsonObj) throws ParseException, SQLException {
		AcessoEmpresa     acessoEmpresa     = new AcessoEmpresa();
		AcessoEmpresaService     acessoEmpresaService     = new AcessoEmpresaService();
		
			
		String result,lista;
		
		JsonObject jsonObjResult = new JsonObject();
				
		int turma = jsonObj.get("turma").getAsInt();
				
		
		int menorFase = acessoEmpresaService.consultarMenorFaseTurma(turma);
		//int turmaFase=acessoEmpresaService.consultarFaseFacilitador(turma);
		// converte objetos Java para JSON e retorna JSON como String
		
		
		jsonObjResult.addProperty("action", "consultarMenorFaseTurma");
		jsonObjResult.addProperty("turmaFase", menorFase);
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		
		System.out.println(result);
		
		return result;
	}
	
	
	
	public String buscarNegociosParticipante(JsonObject jsonObj) throws ParseException, SQLException {
		AcessoEmpresa     acessoEmpresa     = new AcessoEmpresa();
		AcessoEmpresaService     acessoEmpresaService     = new AcessoEmpresaService();
		List<AcessoEmpresa> list = new ArrayList<AcessoEmpresa>();
			
		String result,lista;
		
		JsonObject jsonObjResult = new JsonObject();
				
		int acesso = jsonObj.get("acesso").getAsInt();
				
		list = acessoEmpresaService.findByField("acesso", acesso);
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(list);
		
		jsonObjResult.addProperty("action", "buscarNegociosParticipante");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		
		System.out.println(result);
		
		return result;
	}
	
	*/
}