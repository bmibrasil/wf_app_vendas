package action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.Colaborador;
import Beans.Organizacao;
import Service.OrganizacaoService;
import webService.DMLSalesForce;

public class OrganizacaoAction {


	public List<Organizacao> montaOrganizacao(JsonArray jsonArray, String nomeReferenciaTabela)
	{
		List<Organizacao> listaOrganizacao = new ArrayList<Organizacao>();
		Organizacao organizacao= new Organizacao();

		for (int i=0;i<jsonArray.size();i++)
		{
			organizacao = new Organizacao();
			JsonElement jsonElement = jsonArray.get(i);
			JsonObject jsonObjElement = jsonElement.getAsJsonObject();
			JsonObject organizacaoJson     = jsonObjElement.get(nomeReferenciaTabela).getAsJsonObject();

			String id;
			try{id=organizacaoJson.get("Id").getAsString();}
			catch(Exception ex){id="";}
			organizacao.setId(id);
			
			String nome;
			try {nome = organizacaoJson.get("Name").getAsString();}
			catch(Exception ex){nome="";}
			organizacao.setNome(nome);

			String cep;
			try { cep = organizacaoJson.get("CEP__c").getAsString();}
			catch(Exception ex){cep="";}
			organizacao.setCep(cep);

			String rua;
			try{rua=organizacaoJson.get("Rua__c").getAsString();}
			catch(Exception ex) {rua="";}
			organizacao.setRua(rua);

			String cidade;
			try{cidade = organizacaoJson.get("Cidade__c").getAsString();}
			catch(Exception e) {cidade="";}
			organizacao.setCidade(cidade);

			String estado;
			try{estado = organizacaoJson.get("Estado__c").getAsString();}
			catch(Exception e) {estado="";}
			organizacao.setEstado(estado);

			String cnpj;
			try{cnpj = organizacaoJson.get("CNPJ__c").getAsString();}
			catch(Exception e) {cnpj="";}
			organizacao.setCNPJ(cnpj);

			String segmento;
			try {segmento = organizacaoJson.get("Industry").getAsString();}
			catch(Exception e) {segmento="";}
			organizacao.setSegmento(segmento); 
			
			String bairro;
			try {bairro = organizacaoJson.get("Bairro__c").getAsString();}
			catch(Exception e) {bairro="";}
			organizacao.setBairro(bairro);
			
			String status;
			try{status = jsonObjElement.get("Status__c").getAsString();}
			catch(Exception e) {status="";}
			organizacao.setStatusOrganizacao(status);


			listaOrganizacao.add(organizacao);
		}
		
		return listaOrganizacao;

	}


	public String buscarOrganizacoes(JsonObject jsonObj) {
		List<Organizacao> listaOrganizacao = new ArrayList<Organizacao>();
		JsonObject jsonObjResult = new JsonObject() ;
		JsonObject jsonObjSelecao = new JsonObject() ;
		String result,lista;
		JsonObject organizacaoJson;
		Organizacao organizacao;
		
		

		OrganizacaoService organizacaoService=new OrganizacaoService();
		String unidade     = jsonObj.get("unidade").getAsString();
		//listaOrganizacao=organizacaoService.findAllOrdered("nome");
		String[] campos= {"Organizacao__r.Id", "Organizacao__r.Name", "Organizacao__r.CEP__c",  "Organizacao__r.rua__c",  "Organizacao__r.cidade__c",  "Organizacao__r.estado__c",  "Organizacao__r.CNPJ__c",  "Organizacao__r.Industry",  "name" ,"Organizacao__r.Bairro__c"};
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("StatusOrganizacao__c", campos,"%20Unidade_Negocio__c='"+unidade+"'","Organizacao__r.Name");
		
		if(jsonObjSelecao!=null)
		{
		 	JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
  		    listaOrganizacao=montaOrganizacao(jsonArray,"Organizacao__r");
		} 
		else
		  {listaOrganizacao=null;}
		
		Gson gson = new Gson();
		
		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaOrganizacao);
		jsonObjResult.addProperty("action", "buscarOrganizacoes");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}

	public String buscaOrganizacaocomPessoa(JsonObject jsonObj) {
		List<Organizacao> listaOrganizacao = new ArrayList<Organizacao>();
		JsonObject jsonObjResult = new JsonObject() ;
		JsonObject jsonObjSelecao = new JsonObject() ;
		String result,lista;

		String proprietario     = jsonObj.get("proprietario").getAsString();
		//listaOrganizacao=organizacaoService.buscaOrganizacaocomPessoa(proprietario);
		


		//listaOrganizacao=organizacaoService.findAllOrdered("nome");
		String[] campos= {"Account.Id", "Account.Name", "Account.CEP__c",  "Account.rua__c",  "Account.cidade__c",  "Account.estado__c",  "Account.CNPJ__c",  "Account.Industry",  "name" ,"Account.Bairro__c"};
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Contact", campos,"OwnerId__c='"+proprietario+"'","Account.Name");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		listaOrganizacao=montaOrganizacao(jsonArray,"Account");

		Gson gson = new Gson();
		
		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaOrganizacao);
		jsonObjResult.addProperty("action", "buscaOrganizacaocomPessoa");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	public String buscarOrganizacoescomSolicitacao(JsonObject jsonObj) {
		List<Organizacao> listaOrganizacao = new ArrayList<Organizacao>();
		JsonObject jsonObjResult = new JsonObject() ;
		JsonObject jsonObjSelecao = new JsonObject() ;
		String result,lista;

		String[] campos= {"OrganizacaoId__r.Id", "OrganizacaoId__r.Name", "OrganizacaoId__r.CEP__c",  "OrganizacaoId__r.rua__c",  "OrganizacaoId__r.cidade__c",  "OrganizacaoId__r.estado__c",  "OrganizacaoId__r.CNPJ__c",  "OrganizacaoId__r.Industry",  "name" ,"OrganizacaoId__r.Bairro__c"};
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("WFOXCRM_SolicitacaoPessoa__c", campos,"","OrganizacaoId__r.Name");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		listaOrganizacao=montaOrganizacao(jsonArray,"OrganizacaoId__r");
	
		
		Gson gson = new Gson();
		
		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaOrganizacao);
		jsonObjResult.addProperty("action", "buscarOrganizacoescomSolicitacao");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}	
	

	public String buscaOrganizacaocomNegocio(JsonObject jsonObj) {
		JsonObject jsonObjSelecao = new JsonObject() ;
		List<Organizacao> listaOrganizacao = new ArrayList<Organizacao>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		OrganizacaoService organizacaoService=new OrganizacaoService();
		String proprietario     = jsonObj.get("proprietario").getAsString();
	//	listaOrganizacao=organizacaoService.buscaOrganizacaocomNegocio(proprietario);

		//listaOrganizacao=organizacaoService.findAllOrdered("nome");
		String[] campos= {"Account.Id", "Account.Name", "Account.CEP__c",  "Account.rua__c",  "Account.cidade__c",  "Account.estado__c",  "Account.CNPJ__c",  "Account.Industry",  "name" };
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Opportunity", campos,"OwnerId__c='"+proprietario+"'","Account.Name");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		listaOrganizacao=montaOrganizacao(jsonArray,"Account");
		
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaOrganizacao);
		jsonObjResult.addProperty("action", "buscaOrganizacaocomNegocio");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}	





	public String removerOrganizacao(JsonObject jsonObj) {

		JsonObject jsonObjResult = new JsonObject() ;
		String result;
		OrganizacaoService organizacaoService=new OrganizacaoService();

		int organizacao     = jsonObj.get("organizacao").getAsInt();
		organizacaoService.delete(organizacao);

		jsonObjResult.addProperty("action", "removerOrganizacao");
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}



	public String salvarOrganizacao(JsonObject jsonObj) throws Exception {

		JsonObject jsonObjResult = new JsonObject() ;
		JsonObject jsonObjSelecao = new JsonObject() ;
		
		String result,idSalesForce;
		OrganizacaoService organizacaoService=new OrganizacaoService();
		int retorno=0;

		String id     = jsonObj.get("id").getAsString();
		String nome     = jsonObj.get("nome").getAsString();
		String fantasia     = jsonObj.get("fantasia").getAsString();
		String rua     = jsonObj.get("rua").getAsString();
		int numero     = jsonObj.get("numero").getAsInt();
		String bairro     = jsonObj.get("bairro").getAsString();
		String cidade     = jsonObj.get("cidade").getAsString();
		String estado     = jsonObj.get("estado").getAsString();
		String pais     = jsonObj.get("pais").getAsString();
		String proprietario     = jsonObj.get("proprietario").getAsString();
		String cep     = jsonObj.get("cep").getAsString();
		String cnpj = jsonObj.get("cnpj").getAsString();
		String segmento     = jsonObj.get("segmento").getAsString();
		int statusOrganizacao = jsonObj.get("statusOrganizacao").getAsInt();
		String unidade = jsonObj.get("unidade").getAsString();


		String[] vetorCamposInsert= {"Name", "CEP__c",  "rua__c",  "cidade__c",  "estado__c",  "CNPJ__c",  "Industry", "ownerID","Bairro__c"};
		String[] vetorValores= {nome,cep, rua,cidade,estado,cnpj,segmento,proprietario,bairro};
		String[] vetorConsulta = {"Id"};
		
		DMLSalesForce.insereSalesForce("Account", vetorCamposInsert, vetorValores);
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Account", vetorConsulta,"%20CNPJ__c='"+cnpj+"'","");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		Colaborador colaboradorSalesForce = new Colaborador();
		JsonElement jsonElement = jsonArray.get(0);
		JsonObject jsonObjElement = jsonElement.getAsJsonObject();
		//JsonObject organizacaoJson     = jsonObjElement.get("User").getAsJsonObject();


		try{idSalesForce= jsonObjElement.get("Id").getAsString();}
		catch(Exception ex){idSalesForce="";}
		
		String[] vetorCamposInsertStatus= {"Organizacao__c", "Status__c",  "Unidade_Negocio__c",  "CreatedById",  "Foco"};
		String[] vetorValoreStatus= {idSalesForce,"C0", unidade,proprietario,"Market"};
		DMLSalesForce.insereSalesForce("StatusOrganizacao__c", vetorCamposInsertStatus, vetorValoreStatus);
		
		
		
		
		jsonObjResult.addProperty("action", "salvarOrganizacao");
		jsonObjResult.addProperty("retorno", retorno);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}




}