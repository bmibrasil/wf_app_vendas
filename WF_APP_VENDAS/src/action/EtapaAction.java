package action;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import Beans.Negocio;
import Service.NegocioService;




public class EtapaAction {

	

	public String buscarEtapasProjeto(JsonObject jsonObj) {
		List<Negocio> listaEtapa = new ArrayList<Negocio>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		NegocioService etapaService=new NegocioService();
		int projeto     = jsonObj.get("projeto").getAsInt();
		listaEtapa=etapaService.findByFieldOrdered("projeto", projeto,"nome");
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaEtapa);
		jsonObjResult.addProperty("action", "buscarEtapasProjeto");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	public String buscarEtapas(JsonObject jsonObj) {
		List<Negocio> listaEtapa = new ArrayList<Negocio>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		NegocioService etapaService=new NegocioService();
		listaEtapa=etapaService.findAllOrdered("nome");
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaEtapa);
		jsonObjResult.addProperty("action", "buscarEtapas");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	public String removerEtapa(JsonObject jsonObj) {
		
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		NegocioService etapaService=new NegocioService();
		
		int etapa     = jsonObj.get("etapa").getAsInt();
		etapaService.delete(etapa);

		jsonObjResult.addProperty("action", "removerEtapa");
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
	
	
	
	

}