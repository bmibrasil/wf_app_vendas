package action;

import java.sql.SQLException;
import java.text.ParseException;

import com.google.gson.JsonObject;

public class ApplicationAction {
	
	public String verificaConexao(JsonObject jsonObj) throws ParseException, SQLException {
		JsonObject jsonObjResult = new JsonObject();
		jsonObjResult.addProperty("action", "verificaConexao");
		jsonObjResult.addProperty("conexao", "ok");
		String result = jsonObjResult.toString();
		return result;
	}

}