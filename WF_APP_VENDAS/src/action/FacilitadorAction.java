package action;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.enterprise.inject.*;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.primefaces.context.RequestContext;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import Dados.Data;
//import PptDebriefing.PptExporter;
import PptManager.PptChartNotFoundException;
import PptManager.PptNotOpenedException;
import PptManager.PptTableNotFoundException;
import PptManager.PptTextNotFoundException;
//import Service.ParticipanteStatusService;
//import Service.ParticipanteStatusService;
//import Service.TurmaService;
import Beans.ParticipanteStatus;
import Beans.Turma;
//import Beans.ParticipanteStatus;

/**
 *
 * @author akoskm
 */


public class FacilitadorAction {
    
/*
	public String buscaTurmas(JsonObject jsonObj) throws ParseException
	{
		JsonObject jsonObjResult= new JsonObject();
		//System.out.println(result);
		String resultLista;
		String result;
		
		
		TurmaService service= new TurmaService();
		List<Turma> listaTurmas=service.findAll();
		Gson gson = new Gson();
		 // converte objetos Java para JSON e retorna JSON como String
		jsonObjResult.addProperty("page", "facilitador");
		jsonObjResult.addProperty("action", "buscaTurmas");
		resultLista = gson.toJson(listaTurmas);
		result = jsonObjResult.toString();
		result=result.substring(0, result.length()-1)+",\"listaTurmas\":" +resultLista+"}";
        System.out.println("final"+result);
		 
	  return result;
    }
	
	
	
	public String buscaParticipantesStatus(JsonObject jsonObj) throws ParseException
	{
		JsonObject jsonObjResult= new JsonObject();
		String resultLista;
		String result;
		
		int turma =jsonObj.get("turma").getAsInt();
		ParticipanteStatusService service= new ParticipanteStatusService();
		List<ParticipanteStatus> listaParticipantes=service.getParticipantesStatus(turma);
		Gson gson = new Gson();
		 // converte objetos Java para JSON e retorna JSON como String
		jsonObjResult.addProperty("page", "facilitador");
		jsonObjResult.addProperty("action", "buscaParticipantesStatus");
		resultLista = gson.toJson(listaParticipantes);
		result = jsonObjResult.toString();
		result=result.substring(0, result.length()-1)+",\"listaParticipantes\":" +resultLista+"}";
        System.out.println("final"+result);
		 
	  return result;
    }
	
	
	public String processaRelatorio(JsonObject jsonObj) throws Throwable
	{
		JsonObject jsonObjResult= new JsonObject();
		String resultLista;
		String result;
		String nomeTurma="";
		
		String turma =jsonObj.get("turma").toString();
		turma=turma.replaceAll("\"","");
		String vetorTurma[]= turma.split(","); 
		TurmaService turmaService = new TurmaService();
		for (int i=0; i<vetorTurma.length;i++)
		{
		 nomeTurma=nomeTurma+ turmaService.findByField("id", vetorTurma[i]).get(0).getNome();
		}
		//String nomeTurma =jsonObj.get("nometurma").getAsString();
		//RequestContext context = RequestContext.getCurrentInstance();
		//context.execute("PF('statusDialog').show();");
		Gson gson= new Gson();
		ArquivosFacilitadorAction relatorio= new ArquivosFacilitadorAction();
		relatorio.criaDiretorio(turma,nomeTurma);
		Data.log("In�cio do processamento do debriefing- Turma "+turma);
		PptExporter ppt = new PptExporter();
		ppt.inicioDebriefing(jsonObj);
		//remove o aguarde da tela
		//context.execute("PF('statusDialog').hide();");
		jsonObjResult.addProperty("page", "facilitador");
		jsonObjResult.addProperty("action", "processaRelatorio");
		jsonObjResult.addProperty("turma", turma);
		//jsonObjResult.addProperty("link", relatorio.getListaNomes());
		resultLista = gson.toJson(relatorio.getListaNomes());
		result = jsonObjResult.toString();
		result=result.substring(0, result.length()-1)+",\"listaArquivos\":" +resultLista+"}";
        System.out.println("final"+result);
		 
	  return result;
    }
	
	
	
//	public String alterarEstagio(JsonObject jsonObj) throws ParseException
//	{
//		JsonObject jsonObjResult= new JsonObject();
//		//System.out.println(result);
//		String resultLista;
//		String result=null;
//		int valor=0;
//		
//		
//		TurmaService service= new TurmaService();
//		valor= service.updateEstagioTurma(jsonObj.get("turma").getAsInt(),jsonObj.get("estagio").getAsInt());
//		//Gson gson = new Gson();
//		 // converte objetos Java para JSON e retorna JSON como String
//		jsonObjResult.addProperty("page", "facilitador");
//		jsonObjResult.addProperty("action", "alterarEstagio");
//		jsonObjResult.addProperty("resultado", valor);
//		jsonObjResult.addProperty("estagio", jsonObj.get("estagio").getAsInt());
//		jsonObjResult.addProperty("turma", jsonObj.get("turma").getAsInt());
//		 System.out.println("final"+result);
//		result = jsonObjResult.toString();
//	  return result;
//    }
	
	public String buscaArquivos(JsonObject jsonObj) throws ParseException
	{
		JsonObject jsonObjResult= new JsonObject();
		String resultLista;
		String result;
		
		String turma =jsonObj.get("turma").getAsString();
		//String nomeTurma =jsonObj.get("nometurma").getAsString();
		String nomeTurma="";
		String vetorTurma[]= turma.split(","); 
		TurmaService turmaService = new TurmaService();
		for (int i=0; i<vetorTurma.length;i++)
		{
		 nomeTurma=nomeTurma+ turmaService.findByField("id", vetorTurma[i]).get(0).getNome();
		}
		//RequestContext context = RequestContext.getCurrentInstance();
		//context.execute("PF('statusDialog').show();");
		Gson gson= new Gson();
		ArquivosFacilitadorAction relatorio= new ArquivosFacilitadorAction();
		relatorio.consultaDiretorio(turma,nomeTurma);
		jsonObjResult.addProperty("page", "facilitador");
		jsonObjResult.addProperty("action", "buscaArquivos");
		jsonObjResult.addProperty("turma", turma);
		jsonObjResult.addProperty("nometurma", nomeTurma);
		//jsonObjResult.addProperty("link", relatorio.getListaNomes());
		resultLista = gson.toJson(relatorio.getListaNomes());
		result = jsonObjResult.toString();
		result=result.substring(0, result.length()-1)+",\"listaArquivos\":" +resultLista+"}";
        System.out.println("final"+result);
		 
	  return result;
    }
	*/
}
    
