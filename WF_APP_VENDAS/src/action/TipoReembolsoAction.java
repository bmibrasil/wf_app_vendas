package action;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import Beans.TipoReembolso;
import Service.TipoReembolsoService;




public class TipoReembolsoAction {

	

	public String buscarTipoReembolsos(JsonObject jsonObj) {
		List<TipoReembolso> listaTipoReembolso = new ArrayList<TipoReembolso>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		TipoReembolsoService tipoReembolsoService=new TipoReembolsoService();
		listaTipoReembolso=tipoReembolsoService.findAllOrdered("nome");
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaTipoReembolso);
		jsonObjResult.addProperty("action", "buscarTipoReembolso");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
	public String removerTipoReembolsos(JsonObject jsonObj) {
		
		JsonObject jsonObjResult = new JsonObject() ;
		String result;
		TipoReembolsoService tipoReembolsoService=new TipoReembolsoService();
		
		int tipoReembolso     = jsonObj.get("tipoReembolso").getAsInt();
		tipoReembolsoService.delete(tipoReembolso);

		jsonObjResult.addProperty("action", "removerTipoReembolso");
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
	
	
	
	

}