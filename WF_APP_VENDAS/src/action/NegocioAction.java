package action;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.Negocio;
import Service.NegocioService;
import webService.DMLSalesForce;




public class NegocioAction {

	public List<Negocio> montaNegocio(JsonArray jsonArray, String nomeReferenciaTabela)
	{
		List<Negocio> listaNegocio= new ArrayList<Negocio>();
		Negocio negocio= new Negocio();		

		for (int i=0;i<jsonArray.size();i++)
		{
			negocio = new Negocio();
			JsonElement jsonElement = jsonArray.get(i);
			JsonObject negocioJson = jsonElement.getAsJsonObject();
			//JsonObject negocioJson     = jsonObjElement.get(nomeReferenciaTabela).getAsJsonObject();

			String id;
			try{id=negocioJson.get("Id").getAsString();}
			catch(Exception ex){id="";}
			negocio.setId(id);

			String nome;
			try {nome = negocioJson.get("Name").getAsString();}
			catch(Exception ex){nome="";}
			negocio.setNome(nome);

			String organizacao;
			try { organizacao = negocioJson.get("AccountId").getAsString();}
			catch(Exception ex){organizacao="";}
			negocio.setOrganizacao(organizacao);

			String proprietario;
			try{proprietario=negocioJson.get("ownerId__c").getAsString();}
			catch(Exception ex) {proprietario="";}
			negocio.setProprietario(proprietario);

			String status;
			try{status = negocioJson.get("StageName").getAsString();}
			catch(Exception e) {status="";}
			negocio.setStatusNegocio(status);

			String escopo;
			try{escopo = negocioJson.get("Escopo__c").getAsString();}
			catch(Exception e) {escopo="";}
			negocio.setEscopo(escopo);

			String briefing; 
			try{briefing = negocioJson.get("Briefing__c").getAsString();}
			catch(Exception e) {briefing="";}
			negocio.setBriefing(briefing);

			String dataAbertura;
			try {dataAbertura = negocioJson.get("Data_cadastro__c").getAsString();}
			catch(Exception e) {dataAbertura="";}
			negocio.setDataAbertura(dataAbertura); 


			String pessoa;
			try {pessoa = negocioJson.get("Pessoa__c").getAsString();}
			catch(Exception e) {pessoa="";}
			negocio.setPessoa(pessoa); 
			
			double potencial;
			try {potencial = negocioJson.get("Amount").getAsDouble();}
			catch(Exception e) {potencial=0;}
			negocio.setPotencial(potencial); 
			
			boolean briefingAprovado;
			try {briefingAprovado = negocioJson.get("Briefing_aprovado__c").getAsBoolean();}
			catch(Exception e) {briefingAprovado=false;}
			negocio.setBriefingAprovado(briefingAprovado); 
		
			
			int quantidadeTurmas;
			try {quantidadeTurmas = negocioJson.get("Quantidade_Turmas__c").getAsInt();}
			catch(Exception e) {quantidadeTurmas=0;}
			negocio.setQuantidadeTurmas(quantidadeTurmas); 
			
			int quantidadePessoas;
			try {quantidadePessoas = negocioJson.get("Quantidade_Pessoas__c").getAsInt();}
			catch(Exception e) {quantidadePessoas=0;}
			negocio.setQuantidadePessoas(quantidadePessoas); 
			
			
			String dataPrevistaEntrega;
			try {dataPrevistaEntrega = negocioJson.get("Data_prevista_de_inicio__c").getAsString();}
			catch(Exception e) {dataPrevistaEntrega="";}
			negocio.setDataPrevistaEntrega(dataPrevistaEntrega); 
			
			String dataPrevistaFechamento;
			try {dataPrevistaFechamento = negocioJson.get("CloseDate").getAsString();}
			catch(Exception e) {dataPrevistaFechamento="";}
			negocio.setDataPrevistaFechamento(dataPrevistaFechamento); 
			
			String tipoSolucao;
			try {tipoSolucao = negocioJson.get("Tipo_Solucao__c").getAsString();}
			catch(Exception e) {tipoSolucao="";}
			negocio.setTipoSolucao(tipoSolucao); 
			
			
			String temperatura;
			try {temperatura = negocioJson.get("Temperatura__c").getAsString();}
			catch(Exception e) {temperatura="";}
			negocio.setTemperatura(temperatura); 
	
			
			String publicoAlvo;
			try {publicoAlvo = negocioJson.get("Publico_Alvo__c").getAsString();}
			catch(Exception e) {publicoAlvo="";}
			negocio.setPublicoAlvo(publicoAlvo); 
	
			
			String limiteTempoCliente;
			try {limiteTempoCliente = negocioJson.get("Limite_Tempo_Cliente__c").getAsString();}
			catch(Exception e) {limiteTempoCliente="Limite...";}
			negocio.setlimiteTempoCliente(limiteTempoCliente);
			
			String obsAdm;
			try {obsAdm = negocioJson.get("Obs_Adm__c").getAsString();}
			catch(Exception e) {obsAdm = "";}
			negocio.setObsAdm(obsAdm); 
	
			
			String numeracao;
			try {numeracao = negocioJson.get("Numeracao_do_negocio__c").getAsString();}
			catch(Exception e) {numeracao="";}
			negocio.setNumeracao(numeracao); 
			
			
			listaNegocio.add(negocio);
		}
		return listaNegocio;

	}


	

	public String buscarLancamentosNegocioStatus(JsonObject jsonObj) {
		List<Negocio> listaNegocio = new ArrayList<Negocio>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		JsonObject jsonObjSelecao = new JsonObject() ;
		NegocioService negocioService=new NegocioService();
		String proprietario    = jsonObj.get("proprietario").getAsString();
		String statusNegocio    = jsonObj.get("statusNegocio").getAsString();
		String unidade    = jsonObj.get("unidade").getAsString();
	    String temperatura    = jsonObj.get("temperatura").getAsString();
		System.out.println("TEMPERATURA" +temperatura);
	   String filtroTemperatura;
		if (temperatura.equals("") || temperatura.equals("Temperatura...") )
		{
			filtroTemperatura="";
		}
		else
		{ 
			filtroTemperatura="%20AND%20Temperatura__c='"+temperatura+"'";
			
		}

		String[] campos= {"Id","Name","AccountId","ownerId__c","StageName","Escopo__c","Briefing__c","Data_cadastro__c","Pessoa__c","Amount","Briefing_aprovado__c","Quantidade_Turmas__c","Quantidade_Pessoas__c","Data_prevista_de_inicio__c","CloseDate","Tipo_Solucao__c","Publico_Alvo__c","Limite_Tempo_Cliente__c","Obs_Adm__c","Numeracao_do_negocio__c","Temperatura__c"};
		String condicaoWhere="OwnerId__c='"+proprietario+"'%20AND%20StageName='"+statusNegocio+"'"+"%20AND%20Pessoa__c<>''"+"%20AND%20Unidade_Negocio__c='"+unidade+"'"+filtroTemperatura;
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Opportunity", campos,condicaoWhere,"Name");

		if(jsonObjSelecao!=null)
		{
			JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));

			listaNegocio=montaNegocio(jsonArray,"Opportunity");
		}
		else
		{
			listaNegocio=null;	
		}


		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaNegocio);
		
		jsonObjResult.addProperty("action", "buscarLancamentosNegocioStatus");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}



	public String buscarNegociosProprietario(JsonObject jsonObj) {
		List<Negocio> listaNegocio = new ArrayList<Negocio>();
		JsonObject jsonObjResult = new JsonObject() ;
		JsonObject jsonObjSelecao = new JsonObject() ;
		String result,lista;
		NegocioService negocioService=new NegocioService();
		String proprietario    = jsonObj.get("proprietario").getAsString();
		String unidade    = jsonObj.get("unidade").getAsString();
		

		String[] campos= {"Id","Name","AccountId","ownerId__c","StageName","Escopo__c","Briefing__c","Data_cadastro__c","Pessoa__c", "Amount","Briefing_aprovado__c","Quantidade_Turmas__c","Quantidade_Pessoas__c","Data_prevista_de_inicio__c","CloseDate","Tipo_Solucao__c","Publico_Alvo__c","Limite_Tempo_Cliente__c","Obs_Adm__c","Numeracao_do_negocio__c","Temperatura__c"};
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Opportunity", campos,"Unidade_Negocio__c='"+unidade+"%20AND%20ownerid__c=\'"+proprietario+"\'","organizacao__c");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));

		listaNegocio=montaNegocio(jsonArray,"Opportunity");

		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaNegocio);
		jsonObjResult.addProperty("action", "buscarNegociosProprietario");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}


	public String buscarNegocio(JsonObject jsonObj) {
		List<Negocio> listaNegocio = new ArrayList<Negocio>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		NegocioService negocioService=new NegocioService();
		JsonObject jsonObjSelecao = new JsonObject() ;
		String id    = jsonObj.get("id").getAsString();

		//List<Colaborador> listPart = new  ColaboradorDao().findByFields(hash);
		//		listaNegocio = negocioService.findByFieldOrdered("id", id, "dataAbertura");

		String[] campos= {"Id","Name","AccountId","ownerId__c","StageName","Escopo__c","Briefing__c","Data_cadastro__c","Pessoa__c","Amount","Briefing_aprovado__c","Quantidade_Turmas__c","Quantidade_Pessoas__c","Data_prevista_de_inicio__c","CloseDate","Tipo_Solucao__c","Publico_Alvo__c","Limite_Tempo_Cliente__c","Obs_Adm__c","Numeracao_do_negocio__c", "Temperatura__c"};
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Opportunity", campos,"id='"+id+"'","Name");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));

		listaNegocio=montaNegocio(jsonArray,"Opportunity");

		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaNegocio);
		
		jsonObjResult.addProperty("action", "buscarNegocio");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}


	public String removerNegocio(JsonObject jsonObj) {

		JsonObject jsonObjResult = new JsonObject() ;
		String result;
		NegocioService negocioService=new NegocioService();

		int negocio     = jsonObj.get("negocio").getAsInt();
		negocioService.delete(negocio);

		jsonObjResult.addProperty("action", "removerNegocio");
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	              
	public String atualizarFaseNegocio(JsonObject jsonObj) {

		List<Negocio> listaNegocio = new ArrayList<Negocio>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		NegocioService negocioService=new NegocioService();
		JsonObject jsonObjSelecao = new JsonObject() ;
		String id    = jsonObj.get("lancamento").getAsString();
		String fase    = jsonObj.get("fase").getAsString();
		String motivoPerda    = jsonObj.get("motivoPerda").getAsString();

		//List<Colaborador> listPart = new  ColaboradorDao().findByFields(hash);
		//		listaNegocio = negocioService.findByFieldOrdered("id", id, "dataAbertura");

		String[] campos= {"Id","Name","AccountId","ownerId__c","StageName","Escopo__c","Briefing__c","Data_cadastro__c","Pessoa__c","Amount","Briefing_aprovado__c","Quantidade_Turmas__c","Quantidade_Pessoas__c","Data_prevista_de_inicio__c","CloseDate","Tipo_Solucao__c","Publico_Alvo__c","Limite_Tempo_Cliente__c","Obs_Adm__c","Numeracao_do_negocio__c","Temperatura__c"};
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Opportunity", campos,"id='"+id+"'","Name");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));

		listaNegocio=montaNegocio(jsonArray,"Opportunity");
	

		String[] vetorCamposAtualizar= {"StageName","Loss_Reason__c"};
		String[] vetorValores= {fase, motivoPerda};
		DMLSalesForce.atualizaSalesForce("Opportunity", id, vetorCamposAtualizar, vetorValores);
		

		jsonObjResult.addProperty("action", "atualizarFaseNegocio");
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	




	public String salvarNegocio(JsonObject jsonObj) throws Exception {
		
		JsonObject jsonObjResult = new JsonObject() ;
		String result;
		NegocioService negocioService=new NegocioService();
		int retorno=0;
		int estagio=0;
		String statusNegocio="BQ";

		String id     = jsonObj.get("id").getAsString();
		String dataAbertura     = jsonObj.get("dataAbertura").getAsString();
		String diaSemana     = jsonObj.get("diaSemana").getAsString();
		String organizacao     = jsonObj.get("organizacao").getAsString();
		String pessoa     = jsonObj.get("pessoa").getAsString();
		String nome     =jsonObj.get("nome").getAsString();
		String escopo     = jsonObj.get("escopo").getAsString();
		String briefing     = jsonObj.get("briefing").getAsString();
		double potencial;
		try {  potencial   = jsonObj.get("potencial").getAsDouble();}
		catch(Exception e){ potencial=0;}
		String proprietario     = jsonObj.get("proprietario").getAsString();
		String quantidadeTurmas     = jsonObj.get("quantidadeTurmas").getAsString();
		String quantidadePessoas     = jsonObj.get("quantidadePessoas").getAsString();
		String dataPrevistaEntrega     = jsonObj.get("dataPrevistaEntrega").getAsString();
		String dataPrevistaFechamento     = jsonObj.get("dataPrevistaFechamento").getAsString();
		String tipoSolucao     = jsonObj.get("tipoSolucao").getAsString();
		String publicoAlvo     = jsonObj.get("publicoAlvo").getAsString();
		String limiteTempoCliente     = jsonObj.get("limiteTempoCliente").getAsString();
		String unidade     = jsonObj.get("unidadeNegocio").getAsString();
		String temperatura     = jsonObj.get("temperatura").getAsString();
		
		
		

		String[] vetorCampos= {"Name","AccountId","ownerId__c","StageName","Escopo__c","Briefing__c","Data_cadastro__c","Pessoa__c","StageName","Amount","Briefing_aprovado__c","Quantidade_Turmas__c","Quantidade_Pessoas__c","Data_prevista_de_inicio__c","CloseDate","Tipo_Solucao__c","Publico_Alvo__c","Limite_Tempo_Cliente__c","ownerId","unidade_negocio__c","Temperatura__c"};
		String[] vetorValores= {nome,organizacao,proprietario,statusNegocio,escopo,briefing,dataAbertura,pessoa,statusNegocio,""+potencial,"false",quantidadeTurmas,quantidadePessoas,dataPrevistaEntrega,dataPrevistaFechamento,tipoSolucao, publicoAlvo,limiteTempoCliente,"0051N000005t5P8QAI",unidade,temperatura};

		if (id.equals("0")==true)
			{

				DMLSalesForce.insereSalesForce("Opportunity", vetorCampos, vetorValores);
			}
			else
			{
				DMLSalesForce.atualizaSalesForce("Opportunity", id, vetorCampos, vetorValores);

			}		
		
		jsonObjResult.addProperty("action", "salvarNegocio");
		jsonObjResult.addProperty("retorno", retorno);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}




}