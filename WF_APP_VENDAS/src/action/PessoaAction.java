package action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.Colaborador;
import Beans.Organizacao;
import Beans.Pessoa;
import Beans.Pessoa;
import Beans.Pessoa;
import Service.PessoaService;
import webService.DMLSalesForce;
import Service.PessoaService;




public class PessoaAction {




	public List<Pessoa> montaPessoa(JsonArray jsonArray,String tabela)
	{
		List<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		Pessoa pessoa= new Pessoa();

		for (int i=0;i<jsonArray.size();i++)
		{
			pessoa = new Pessoa();
			JsonElement jsonElement = jsonArray.get(i);
			JsonObject jsonObjElement = jsonElement.getAsJsonObject();
			JsonObject pessoaJson;
			
			try{pessoaJson= jsonObjElement.get(tabela).getAsJsonObject();}
			catch(Exception e) {pessoaJson=null;}

			String id;
			try{id= pessoaJson.get("Id").getAsString();}
			catch(Exception e) {id="";}
			pessoa.setId(id);

			String nome;
			try {nome = pessoaJson.get("Name").getAsString();}
			catch(Exception ex){nome="";}
			pessoa.setNome(nome);

			String cargo;
			try { cargo = pessoaJson.get("Title").getAsString();}
			catch(Exception ex){cargo="";}
			pessoa.setCargo(cargo);

			String telefone;
			try{telefone=pessoaJson.get("Phone").getAsString();}
			catch(Exception ex) {telefone="";}
			pessoa.setTelefone(telefone);

			String email;
			try{email = pessoaJson.get("Email").getAsString();}
			catch(Exception e) {email="";}
			pessoa.setEmail(email);

			String status;
			try{status = jsonObjElement.get("Status__c").getAsString();}
			catch(Exception e) {status="";}
			pessoa.setStatusPessoa(status);


			String organizacao;
			try{organizacao = pessoaJson.get("AccountId").getAsString();}
			catch(Exception e) {organizacao="";}
			pessoa.setOrganizacao(organizacao);

			String celular;
			try{celular = pessoaJson.get("MobilePhone").getAsString();}
			catch(Exception e) {celular="";}
			pessoa.setCelular(celular);



			listaPessoa.add(pessoa);
		}
		
		return listaPessoa;

	}



	public String buscarTodasPessoasOrganizacoes(JsonObject jsonObj) {
		List<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		JsonObject jsonObjResult = new JsonObject() ;
		JsonObject jsonObjSelecao = new JsonObject() ;
		String result,lista;
		PessoaService pessoaService=new PessoaService();
		String proprietario     = jsonObj.get("proprietario").getAsString();
		//listaPessoa=pessoaService.buscarTodasPessoasOrganizacoes(proprietario);


		Pessoa pessoa;
		String unidade     = jsonObj.get("unidade").getAsString();
		//listaOrganizacao=organizacaoService.findAllOrdered("nome");
		String[] campos=  {"Pessoa__r.Id","Pessoa__r.Name", "Pessoa__r.Title","Pessoa__r.MobilePhone", "Pessoa__r.Phone","Pessoa__r.Email","Status__c","Pessoa__r.Origem_do_contato__c","Pessoa__r.AccountId"};
		String condicaoWhere="Pessoa__r.OwnerId__c='"+proprietario+"'%20AND%20Unidade_Negocio__c='"+unidade+"'";
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("StatusPessoa__c", campos,condicaoWhere,"Pessoa__r.Name");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		listaPessoa=montaPessoa(jsonArray,"Pessoa__r");

		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaPessoa);
		jsonObjResult.addProperty("action", "buscarTodasPessoasOrganizacoes");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}


	public String salvarPessoa(JsonArray jsonArray) throws Exception {
		JsonObject jsonObjSelecao = new JsonObject() ;
		JsonObject jsonObjResult = new JsonObject() ;
		String result,idSalesForce;
		PessoaService pessoaService=new PessoaService();
		int retorno=0;
		
		JsonElement jsonElement = jsonArray.get(0);
		JsonObject jsonObj = jsonElement.getAsJsonObject();
				
		try{idSalesForce= jsonObj.get("Id").getAsString();}
		catch(Exception ex){idSalesForce="0";}
		String id ;
		String nome     = jsonObj.get("nome").getAsString();
		String organizacao     = jsonObj.get("organizacao").getAsString();
		String cargo     = jsonObj.get("cargo").getAsString();
		String email     = jsonObj.get("email").getAsString();
		String celular     = jsonObj.get("celular").getAsString();
		String telefone     = jsonObj.get("telefone").getAsString();
		//String status = jsonObj.get("statusPessoa").getAsString();
		String proprietario     = jsonObj.get("proprietario").getAsString();
	//	int estagio     = jsonObj.get("estagio").getAsInt();
		String unidade     = jsonObj.get("unidade").getAsString();

		
		
		String[] vetorCamposInsert= {"Name", "Title",  "MobilePhone",  "Phone",  "Email",  "OwnerId", "AccountId","OwnerId"};
		String[] vetorValores= {nome,cargo, celular,telefone,email,proprietario,organizacao,"0051N000005t5P8QAI"};
		String[] vetorConsulta = {"Id"};
		
		if (idSalesForce=="0")
		{
   	
		  // Insere Pessoa
		  DMLSalesForce.insereSalesForce("Contact", vetorCamposInsert, vetorValores);
		  
		  //Busca o Id da Pessoa insereida
		  jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Contact", vetorConsulta,"%20Email='"+email+"'","");
		  JsonArray  jsonArraySelecaoPessoa=jsonObjSelecao.getAsJsonArray("records");
	      JsonElement jsonElementSelecaoPessoa = jsonArraySelecaoPessoa.get(0);
		  JsonObject jsonObjSelecaoPessoa = jsonElementSelecaoPessoa.getAsJsonObject();
		  idSalesForce=jsonObjSelecaoPessoa.get("Id").getAsString();
	
		
          //insere no status de pessoa
		  String[] vetorCamposInsertStatus= {"Pessoa__c", "Status__c",  "Unidade_Negocio__c",  "CreatedById",  "Foco"};
		  String[] vetorValoreStatus= {idSalesForce,"P0", unidade,proprietario,"Market"};
	 	  DMLSalesForce.insereSalesForce("StatusPessoa__c", vetorCamposInsertStatus, vetorValoreStatus);

		}


		jsonObjResult.addProperty("action", "salvarPessoa");
		jsonObjResult.addProperty("retorno", retorno);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}

	public String buscarPessoasOrganizacao(JsonObject jsonObj) {
		List<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		JsonObject jsonObjSelecao = new JsonObject() ;
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;

		String organizacao     = jsonObj.get("organizacao").getAsString();
		String proprietario     = jsonObj.get("proprietario").getAsString();
		String unidade     = jsonObj.get("unidade").getAsString();
		//		Hashtable<String,Object> hash = new Hashtable<String,Object>();
		//		hash.put("proprietario", proprietario);
		//		hash.put("organizacao", organizacao);
		//		PessoaService pessoaService=new PessoaService();
		//		listaPessoa=pessoaService.findByFieldsOrdered(hash,"nome");
		String[] campos=  {"Pessoa__r.Id","Pessoa__r.Name", "Pessoa__r.Title","Pessoa__r.MobilePhone", "Pessoa__r.Phone","Pessoa__r.Email","Status__c","Pessoa__r.AccountId"};
		String condicaoWhere="Pessoa__r.AccountId='"+organizacao+"'"+"%20AND%20Unidade_Negocio__c='"+unidade+"'";
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("StatusPessoa__c", campos,condicaoWhere,"Pessoa__r.Name");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));

		listaPessoa=montaPessoa(jsonArray,"Pessoa__r");
		//Pessoa__r.Ownerid__c='"+proprietario+"'%20AND%20
		
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaPessoa);
		jsonObjResult.addProperty("action", "buscarPessoasOrganizacao");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}


	public String buscarPessoasProprietario(JsonObject jsonObj) {
		JsonObject pessoaJson;
		JsonObject jsonObjSelecao = new JsonObject() ;
		Pessoa pessoa;


		List<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		PessoaService pessoaService=new PessoaService();
		String proprietario     = jsonObj.get("proprietario").getAsString();
		String unidade     = jsonObj.get("unidade").getAsString();
		

		String[] campos=  {"Pessoa__r.Id","Pessoa__r.Name", "Pessoa__r.Title","Pessoa__r.MobilePhone", "Pessoa__r.Phone","Pessoa__r.Email","Status__c","Pessoa__r.AccountId"};
		String condicaoWhere="Pessoa__r.OwnerId__c='"+proprietario+"'%20AND%20Unidade_Negocio__c='"+unidade+"'";
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("StatusPessoa__c", campos,condicaoWhere,"Pessoa__r.Name");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));

		listaPessoa=montaPessoa(jsonArray,"Pessoa__r");

		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaPessoa);
		jsonObjResult.addProperty("action", "buscarPessoasProprietario");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}	

	
	public String buscarPessoasNegocioProprietario(JsonObject jsonObj) {
		JsonObject pessoaJson;
		JsonObject jsonObjSelecao = new JsonObject() ;
		Pessoa pessoa;


		List<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		PessoaService pessoaService=new PessoaService();
		String proprietario     = jsonObj.get("proprietario").getAsString();
		String negocio     = jsonObj.get("negocio").getAsString();


		String[] campos=  {"Pessoa__r.id","Pessoa__r.Name", "Pessoa__r.Title","Pessoa__r.MobilePhone", "Pessoa__r.Phone","Pessoa__r.Email","Pessoa__r.AccountId"};
		String condicaoWhere="ownerid__c='"+proprietario+"'%20AND%20id='"+negocio+"'";
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Opportunity", campos,condicaoWhere,"Pessoa__r.Name");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));

		listaPessoa=montaPessoa(jsonArray,"Pessoa__r");

		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaPessoa);
		jsonObjResult.addProperty("action", "buscarPessoasNegocioProprietario");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}	

	// busca todas as pessoas com negocios de um proprientario
	public String buscarPessoasNegociosProprietario(JsonObject jsonObj) {
		JsonObject pessoaJson;
		JsonObject jsonObjSelecao = new JsonObject() ;
		Pessoa pessoa;


		List<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		PessoaService pessoaService=new PessoaService();
		String proprietario     = jsonObj.get("proprietario").getAsString();
		//String negocio     = jsonObj.get("negocio").getAsString();


		String[] campos=  {"Pessoa__r.id","Pessoa__r.Name", "Pessoa__r.Title","Pessoa__r.MobilePhone", "Pessoa__r.Phone","Pessoa__r.Email","Pessoa__r.AccountId"};
		String condicaoWhere="ownerid__c='"+proprietario+"'";
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Opportunity", campos,condicaoWhere,"Pessoa__r.Name");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));

		listaPessoa=montaPessoa(jsonArray,"Pessoa__r");

		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaPessoa);
		jsonObjResult.addProperty("action", "buscarPessoasNegociosProprietario");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}	



	public String buscarPessoascomSolicitacao(JsonObject jsonObj) {
		JsonObject jsonObjSelecao = new JsonObject() ;
		//Pessoa pessoa;


		List<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		PessoaService pessoaService=new PessoaService();
		
		//listaPessoa=pessoaService.findByFieldOrdered("proprietario",proprietario,"nome");

		String[] campos=  {"Ownerid__c","Owner.Name","Owner.email"};
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("WFOXCRM_SolicitacaoPessoa__c", campos,"","");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));

		for (int i=0;i<jsonArray.size();i++)
		{
			JsonElement jsonElement = jsonArray.get(i);
			JsonObject jsonObjElement = jsonElement.getAsJsonObject();
			JsonObject pessoaJson     = jsonObjElement.get("Owner").getAsJsonObject();
			Pessoa pessoa = new Pessoa();

			String id;
			try{id= jsonObjElement.get("OwnerId").getAsString();}
			catch(Exception e) {id="";}
			pessoa.setId(id);

			String nome;
			try{nome= pessoaJson.get("Name").getAsString();}
			catch(Exception e) {nome="";}
			pessoa.setNome(nome);

			String email;
			try{email= pessoaJson.get("Email").getAsString();}
			catch(Exception e) {email="";}
			pessoa.setEmail(email);

			listaPessoa.add(pessoa);
		}
			Gson gson = new Gson();
			// converte objetos Java para JSON e retorna JSON como String
			lista = gson.toJson(listaPessoa);
			jsonObjResult.addProperty("action", "buscarPessoascomSolicitacao");
			jsonObjResult.addProperty("retorno", lista);
			result = jsonObjResult.toString();
			System.out.println(result);
			return result;

	}


}