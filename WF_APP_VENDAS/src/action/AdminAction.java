package action;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import Beans.Acesso;
import Beans.AcessoEmpresa;
import Beans.Organizacao;
//import bean.Hierarquia;
import Beans.Turma;
import Service.AcessoEmpresaService;
import Service.AcessoService;
import Service.OrganizacaoService;
//import service.HierarquiaService;
//import Service.TurmaService;



public class AdminAction {
/*
	public String salvarTurma(JsonObject jsonObj) {
		String result = "";
		JsonObject jsonObjResult  = new JsonObject() ;
	//	TurmaService turmaService = new TurmaService();
		String nome;
		int empresa;
		int turmaId=0;
		try {
			turmaId = jsonObj.get("turma").getAsInt();} catch (Exception e){}
		    nome=jsonObj.get("nome").getAsString();
		    empresa=jsonObj.get("empresa").getAsInt();
		Turma turma = new Turma();
		turma.setId(turmaId);
		turma.setNome(nome);
		turma.setStatus(jsonObj.get("status").getAsInt());
		turma.setEmpresa(empresa);
		turma.setPercentualRemocao(jsonObj.get("percRemocao").getAsInt());
		turma.setTotalValores(jsonObj.get("numeroValores").getAsInt());
		
		//turma.setDominios(jsonObj.get("dominios").getAsString().replace("|", ";"));
		if (turma.getId()>0){
			turmaService.save(turma);
		} else {
			turmaService.insert(turma);
			AcessoService acessoService = new AcessoService();
			Acesso acesso = new Acesso();
			Hashtable<String,Object> hashT = new Hashtable<String,Object>();
			hashT.put("empresa", empresa);
			hashT.put("nome", nome);
			List<Turma> listTurma = turmaService.findByFields(hashT);
						
			//Cria o facilitador da turma		
			acesso.setNome("Facilitador_Turma"+listTurma.get(0).getId());
			acesso.setTipo(2);
			acesso.setFase(1);
			acesso.setTurma(listTurma.get(0).getId());
			acessoService.insert(acesso);
			
			Hashtable<String,Object> hashAc = new Hashtable<String,Object>();
			hashAc.put("tipo", 2);
			hashAc.put("nome", "Facilitador_Turma"+listTurma.get(0).getId());
			hashAc.put("turma", listTurma.get(0).getId());
			List<Acesso> listAcessoFac = acessoService.findByFields(hashAc);
			acesso.setId(listAcessoFac.get(0).getId());
			acesso.setCodigoAcesso(""+listAcessoFac.get(0).getId());
			acessoService.save(acesso);
		}
		// converte objetos Java para JSON e retorna JSON como String
		jsonObjResult.addProperty("action", "salvarTurma");
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	public String excluirNegocio(JsonObject jsonObj) {
		String result = "";
		JsonObject jsonObjResult  = new JsonObject() ;
		OrganizacaoService empresaService = new OrganizacaoService();
		int id = jsonObj.get("empresa").getAsInt();
		empresaService.delete(id);
		jsonObjResult.addProperty("action", "excluirUnidade");
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	public String buscarNegocios(JsonObject jsonObj) {
		List<Organizacao> listaEmpresa = new ArrayList<Organizacao>();
		JsonObject jsonObjResult = new JsonObject();
		String empresaMae=jsonObj.get("empresamae").getAsString();
		int turma=jsonObj.get("turma").getAsInt();
		String result,lista;
		OrganizacaoService empresaService=new OrganizacaoService();
		//listaEmpresa=empresaService.buscarNegocios(jsonObj);
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaEmpresa);
		jsonObjResult.addProperty("action", "buscarUnidades");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
	public String buscarAcessos(JsonObject jsonObj) {
		List<Acesso> listaAcesso = new ArrayList<Acesso>();
		JsonObject jsonObjResult = new JsonObject();
		String empresaMae=jsonObj.get("empresamae").getAsString();
		int turma=jsonObj.get("turma").getAsInt();
		String result,lista;
		AcessoService AcessoService=new AcessoService();
		listaAcesso=AcessoService.findByTurma(turma);
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaAcesso);
		jsonObjResult.addProperty("action", "buscarAcessos");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	public String salvarUnidade(JsonObject jsonObj) {
		String result = "";
		JsonObject jsonObjResult  = new JsonObject() ;
		OrganizacaoService empService= new OrganizacaoService();
		
		int empresaid = 0;
		
		try {
			
		empresaid = jsonObj.get("id").getAsInt();} catch (Exception e){}
		
		if (empresaid >0){
	//		empService.save(jsonObj.get("id").getAsInt(),jsonObj.get("nome").getAsString(), jsonObj.get("empresaMae").getAsInt());
		} else {
	//	   empService.insert(jsonObj.get("nome").getAsString(), jsonObj.get("empresaMae").getAsInt());
//		}
		// converte objetos Java para JSON e retorna JSON como String
		jsonObjResult.addProperty("action", "salvarNegocio");
		result = jsonObjResult.toString();
		System.out.println(result);
		}
		return result;
	    
	}


	public String salvarAcesso(JsonObject jsonObj) throws Exception {
		String result = "";
		int acessoId=0;
		JsonObject jsonObjResult  = new JsonObject() ;
        AcessoService acessoService = new AcessoService();
        AcessoEmpresaService acessoEmpresaService = new AcessoEmpresaService();
        Acesso acesso = new Acesso();
        AcessoEmpresa acessoEmpresa = new AcessoEmpresa();
        
        try {
		acessoId = jsonObj.get("id").getAsInt();} catch (Exception e){}
		
		int empresa =jsonObj.get("empresa").getAsInt();
		int turma=jsonObj.get("turma").getAsInt();

        acesso.setId(acessoId);
        acesso.setNome(jsonObj.get("nome").getAsString());
        acesso.setTurma(turma);
        acesso.setTipo(jsonObj.get("tipo").getAsInt());
        //turma.setDominios(jsonObj.get("dominios").getAsString().replace("|", ";"));
		if (acesso.getId()>0){
			acessoService.save(acesso);
		} else {
			acessoService.insert(acesso);
		}
		
		Hashtable<String,Object> hashA = new Hashtable<String,Object>();
		hashA.put("tipo", jsonObj.get("tipo").getAsInt());
		hashA.put("nome", jsonObj.get("nome").getAsString());
		hashA.put("turma", turma);
		
		List<Acesso> list = acessoService.findByFields(hashA);
		acesso.setId(list.get(0).getId());
		acesso.setCodigoAcesso(""+acesso.getId());
		acessoService.save(acesso);
		
		Hashtable<String,Object> hash = new Hashtable<String,Object>();
		hash.put("empresa", empresa);
		hash.put("acesso", acesso.getId());
		List<AcessoEmpresa> listAE = acessoEmpresaService.findByFields(hash);
		
		acessoEmpresa.setEmpresa(empresa);
		acessoEmpresa.setFase(1);
		acessoEmpresa.setAcesso(acesso.getId());
		
		if (listAE.size() >0)
		{ 
			acessoEmpresa.setId(listAE.get(0).getId());
			acessoEmpresaService.save(acessoEmpresa);
		}
		else
		{ 
			//acessoEmpresa.setId(0);
			acessoEmpresaService.insert(acessoEmpresa);
			
			//busca facilitador no acesso
			Hashtable<String,Object> hashFac = new Hashtable<String,Object>();
			hashFac.put("turma", turma);
			hashFac.put("tipo", 2);
			List<Acesso> listFac = acessoService.findByFields(hashFac);
			
			//busca facilitador no acessoEmpresa
			Hashtable<String,Object> hashFacEmpresa = new Hashtable<String,Object>();
			hashFacEmpresa.put("empresa", empresa);
			hashFacEmpresa.put("acesso", listFac.get(0).getId());
			List<AcessoEmpresa> listFacEmpresa = acessoEmpresaService.findByFields(hashFacEmpresa);
			
			AcessoEmpresa acessoEmpresaFac = new AcessoEmpresa();
			acessoEmpresaFac.setAcesso(listFac.get(0).getId());
			acessoEmpresaFac.setEmpresa(empresa);
			acessoEmpresaFac.setFase(1);
			
			if (listFacEmpresa.size() > 0)
			{
				acessoEmpresaFac.setId(listFacEmpresa.get(0).getId());
				acessoEmpresaService.save(acessoEmpresaFac);
			}
			else
			{
				acessoEmpresaFac.setId(0);
				acessoEmpresaService.insert(acessoEmpresaFac);
			}
	
		}
			
	
		// converte objetos Java para JSON e retorna JSON como String
		jsonObjResult.addProperty("action", "salvarAcesso");
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	public String excluirAcesso(JsonObject jsonObj) {
		String result = "";
		JsonObject jsonObjResult  = new JsonObject() ;
		AcessoService acessoService = new AcessoService();
		int id = jsonObj.get("Acesso").getAsInt();
		acessoService.delete(id);
		jsonObjResult.addProperty("action", "excluirAcesso");
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
*/	
}