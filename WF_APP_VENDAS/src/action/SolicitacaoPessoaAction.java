package action;


import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import Beans.Pessoa;
import Beans.SolicitacaoOrganizacao;
import Beans.SolicitacaoPessoa;
import Service.SolicitacaoOrganizacaoService;
import Service.SolicitacaoPessoaService;
import webService.DMLSalesForce;




public class SolicitacaoPessoaAction {

	public List<SolicitacaoPessoa> montaSolicitacaoPessoa(JsonArray jsonArray)
	{
		List<SolicitacaoPessoa> listaSolicitacaoPessoa = new ArrayList<SolicitacaoPessoa>();
		SolicitacaoPessoa solicitacaoPessoa= new SolicitacaoPessoa();

		for (int i=0;i<jsonArray.size();i++)
		{
			solicitacaoPessoa = new SolicitacaoPessoa();
			JsonElement jsonElement = jsonArray.get(i);
			JsonObject pessoaJson = jsonElement.getAsJsonObject();
			//JsonObject pessoaJson     = jsonObjElement.get("SolicitacaoPessoa__c").getAsJsonObject();

			String id;
			try{id= pessoaJson.get("Id").getAsString();}
			catch(Exception e) {id="";}
			solicitacaoPessoa.setId(id);


			String nome;
			try {nome = pessoaJson.get("Nome_da_Pessoa__c").getAsString();}
			catch(Exception ex){nome="";}
			solicitacaoPessoa.setNome(nome);

			String cargo;
			try { cargo = pessoaJson.get("Cargo__c").getAsString();}
			catch(Exception ex){cargo="";}
			solicitacaoPessoa.setCargo(cargo);

			String telefone;
			try{telefone=pessoaJson.get("Telefone__c").getAsString();}
			catch(Exception ex) {telefone="";}
			solicitacaoPessoa.setTelefone(telefone);

			String email;
			try{email = pessoaJson.get("Email__c").getAsString();}
			catch(Exception e) {email="";}
			solicitacaoPessoa.setEmail(email);


			int statusPessoa;
			try{statusPessoa = pessoaJson.get("Status_SolicitacaoP__c").getAsInt();}
			catch(Exception e) {statusPessoa=0;}
			solicitacaoPessoa.setStatusSolicitacaoPessoa(statusPessoa);


			
			String organizacao;
			try{organizacao = pessoaJson.get("Organizacao__c").getAsString();}
			catch(Exception e) {organizacao="";}
			solicitacaoPessoa.setOrganizacao(organizacao);

			String celular;
			try{celular = pessoaJson.get("Celular__c").getAsString();}
			catch(Exception e) {celular="";}
			solicitacaoPessoa.setCelular(celular);


			String proprietario;
			try{proprietario = pessoaJson.get("OwnerId__c").getAsString();}
			catch(Exception e) {proprietario="";}
			solicitacaoPessoa.setProprietario(proprietario);
			
			String observacao;
			try{observacao = pessoaJson.get("Observacao__c").getAsString();}
			catch(Exception e) {observacao="";}
			solicitacaoPessoa.setObservacao(observacao);
			
			String data;
			try{data = pessoaJson.get("Data_da_Solicitacao__c").getAsString();}
			catch(Exception e) {data="";}
			solicitacaoPessoa.setDataSolicitacaoPessoa(data);

			String origemPessoa;
			try{origemPessoa = pessoaJson.get("Origem_da_Pessoa__c").getAsString();}
			catch(Exception e) {origemPessoa="";}
			solicitacaoPessoa.setOrigemPessoa(origemPessoa);
			
			String unidade;
			try{unidade = pessoaJson.get("Unidade_Negocio__c").getAsString();}
			catch(Exception e) {unidade="";}
			solicitacaoPessoa.setUnidade(unidade);
			
			listaSolicitacaoPessoa.add(solicitacaoPessoa);
		}
		return listaSolicitacaoPessoa;

	}


	public String avaliarSolicitacaoPessoa(JsonObject jsonObj) throws Exception {
		List<SolicitacaoPessoa> listaSolicitacaoPessoa = new ArrayList<SolicitacaoPessoa>();
		JsonObject jsonObjSelecao = new JsonObject() ;
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		SolicitacaoPessoaService solicitacaoService=new SolicitacaoPessoaService();




		int status=jsonObj.get("status").getAsInt();
		String justificativa=jsonObj.get("justificativa").getAsString();
		String Id=jsonObj.get("solicitacao").getAsString();


		String[] campos= {"Id","Nome_da_Pessoa__c", "Cargo__c",  "Celular__c",  "Telefone__c",  "Email__c",  "OwnerId__c", "Organizacao__c","Status_SolicitacaoP__c","Observacao__c","Data_da_Solicitacao__c","Origem_da_Pessoa__c","Unidade_Negocio__c"};
		String condicaoWhere="Id='"+Id+"'";
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("WFOXCRM_SolicitacaoPessoa__c", campos,condicaoWhere,"Nome_da_Pessoa__c");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		listaSolicitacaoPessoa=montaSolicitacaoPessoa(jsonArray);

		//UPDATE
		String[] vetorCamposAtu= {"Observacao__c","Status_SolicitacaoP__c"};
		String[] vetorValoresAtu= {justificativa,""+status};
		DMLSalesForce.atualizaSalesForce("WFOXCRM_SolicitacaoPessoa__c",Id, vetorCamposAtu, vetorValoresAtu);


		Gson gson = new Gson();
		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaSolicitacaoPessoa);
		JsonParser jsonParser = new JsonParser();
		jsonArray= (JsonArray)jsonParser.parse(lista);
//		JsonObject  jsonObjConv = new JsonObject();
//		
//		jsonObjConv.add("records", jsonArray);


		if(status==1)
		{
			PessoaAction pessoaAction= new PessoaAction();
			pessoaAction.salvarPessoa(jsonArray);
		}
		jsonObjResult.addProperty("action", "avaliarSolicitacao");
		jsonObjResult.addProperty("tipoSolicitacao", "pessoa");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}





	public String salvarSolicitacaoPessoa(JsonObject jsonObj) throws Exception {

		JsonObject jsonObjResult = new JsonObject() ;
		String result;
		SolicitacaoPessoaService solicitacaoPessoaService=new SolicitacaoPessoaService();
		int retorno=0;
		int statusSolicitacaoPessoa=0;
		String id     = jsonObj.get("id").getAsString();
		String nome     = jsonObj.get("nome").getAsString();
		String organizacao     = jsonObj.get("organizacao").getAsString();
		String cargo     = jsonObj.get("cargo").getAsString();
		String email     = jsonObj.get("email").getAsString();
		String celular     = jsonObj.get("celular").getAsString();
		String telefone     = jsonObj.get("telefone").getAsString();
		String proprietario     = jsonObj.get("proprietario").getAsString();
		String observacao     = jsonObj.get("observacao").getAsString();
		String dataSolicitacaoPessoa     = jsonObj.get("dataSolicitacaoPessoa").getAsString();
		String origemPessoa     = jsonObj.get("origemPessoa").getAsString();
		String unidade     = jsonObj.get("unidade").getAsString();

		String[] vetorCamposInsert= {"Nome_da_Pessoa__c", "Cargo__c",  "Celular__c",  "Telefone__c",  "Email__c",  "OwnerId__c", "Organizacao__c","Status_SolicitacaoP__c","Observacao__c","Data_da_Solicitacao__c","Origem_da_Pessoa__c","Unidade_Negocio__c","OwnerId"};
		String[] vetorValores= {nome,cargo, celular,telefone,email,proprietario,organizacao,"pendente",observacao,dataSolicitacaoPessoa,origemPessoa,unidade,"0051N000005t5P8QAI"};
		
		if (id.equals("0")==true)
		 { 
			DMLSalesForce.insereSalesForce("WFOXCRM_SolicitacaoPessoa__c", vetorCamposInsert, vetorValores);
		 }
		else
		{
			DMLSalesForce.atualizaSalesForce("WFOXCRM_SolicitacaoPessoa__c",id, vetorCamposInsert, vetorValores);
		}
		
		jsonObjResult.addProperty("action", "salvarSolicitacaoPessoa");
		jsonObjResult.addProperty("retorno", retorno);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}



	public String buscarSolicitacaoPessoa(JsonObject jsonObj) {
		List<SolicitacaoPessoa> listaSolicitacaoPessoa = new ArrayList<SolicitacaoPessoa>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		JsonObject jsonObjSelecao = new JsonObject() ;
		SolicitacaoPessoaService solicitacaoPessoaService=new SolicitacaoPessoaService();
		String id    = jsonObj.get("id").getAsString();
		//List<Colaborador> listPart = new  ColaboradorDao().findByFields(hash);
		//	listaSolicitacaoPessoa=solicitacaoPessoaService.findByField("id",id);

		String[] campos= {"Id","Nome_da_Pessoa__c", "Cargo__c",  "Celular__c",  "Telefone__c",  "Email__c",  "OwnerId__c", "Organizacao__c","Status_SolicitacaoP__c","Observacao__c","Data_da_Solicitacao__c","Origem_da_Pessoa__c","Unidade_Negocio__c"};
		String condicaoWhere="Id='"+id+"'";
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("WFOXCRM_SolicitacaoPessoa__c", campos,condicaoWhere,"Nome_da_Pessoa__c");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		listaSolicitacaoPessoa=montaSolicitacaoPessoa(jsonArray);



		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaSolicitacaoPessoa);
		jsonObjResult.addProperty("action", "buscarSolicitacaoPessoa");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}


	public String buscarLancamentosSolicitacaoPessoaStatus(JsonObject jsonObj) {
		List<SolicitacaoPessoa> listaSolicitacaoPessoa = new ArrayList<SolicitacaoPessoa>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		JsonObject jsonObjSelecao = new JsonObject() ;
		SolicitacaoPessoaService solicitacaoPessoaService=new SolicitacaoPessoaService();
		String status    = jsonObj.get("status").getAsString();
		String proprietario    = jsonObj.get("proprietario").getAsString();

		String[] campos= {"Id","Nome_da_Pessoa__c", "Cargo__c",  "Celular__c",  "Telefone__c",  "Email__c",  "OwnerId__c", "Organizacao__c","Status_SolicitacaoP__c","Observacao__c","Data_da_Solicitacao__c","Origem_da_Pessoa__c","Unidade_Negocio__c"};
		String condicaoWhere="Status_SolicitacaoP__c='"+status+"'%20AND%20OwnerId__c='"+proprietario+"'";
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("WFOXCRM_SolicitacaoPessoa__c", campos,condicaoWhere,"Nome_da_Pessoa__c");
		
		if (jsonObjSelecao!=null)
		{
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));

		listaSolicitacaoPessoa=montaSolicitacaoPessoa(jsonArray);
		Gson gson = new Gson();
		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaSolicitacaoPessoa);
		}
		else
		{
			lista="";
		}
		
		jsonObjResult.addProperty("action", "buscarSolicitacoesStatusTipo");
		jsonObjResult.addProperty("tipoSolicitacao", "pessoa");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}

	//	public String removerSolicitacaoPessoa(JsonObject jsonObj) {
	//
	//		JsonObject jsonObjResult = new JsonObject() ;
	//		String result;
	//		SolicitacaoPessoaService solicitacaoPessoaService=new SolicitacaoPessoaService();
	//
	//		int solicitacao     = jsonObj.get("lancamento").getAsInt();
	//		solicitacaoPessoaService.delete(solicitacao);
	//
	//		jsonObjResult.addProperty("action", "removerSolicitacaoPessoa");
	//		jsonObjResult.addProperty("retorno", 0);
	//		result = jsonObjResult.toString();
	//		System.out.println(result);
	//		return result;
	//	}





}