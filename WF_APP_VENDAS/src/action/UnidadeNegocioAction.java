package action;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.Colaborador;
import Beans.Organizacao;
import Beans.Pessoa;
import Beans.UnidadeNegocio;
import Beans.Pessoa;
import Beans.Pessoa;
import Service.PessoaService;
import webService.DMLSalesForce;
import Service.PessoaService;




public class UnidadeNegocioAction {




	public List<UnidadeNegocio> montaUnidade(JsonArray jsonArray,String tabela)
	{
		List<UnidadeNegocio> listaUnidade = new ArrayList<UnidadeNegocio>();
		UnidadeNegocio unidade= new UnidadeNegocio();

		for (int i=0;i<jsonArray.size();i++)
		{
			unidade = new UnidadeNegocio();
			JsonElement jsonElement = jsonArray.get(i);
			JsonObject unidadeJson = jsonElement.getAsJsonObject();
	//		JsonObject unidadeJson     = jsonObjElement.get(tabela).getAsJsonObject();

			String id;
			try{id= unidadeJson.get("Id").getAsString();}
			catch(Exception e) {id="";}
			unidade.setId(id);

			String nome;
			try {nome = unidadeJson.get("Name").getAsString();}
			catch(Exception ex){nome="";}
			unidade.setNome(nome);

			

			listaUnidade.add(unidade);
		}
		return listaUnidade;

	}



	public String buscarUnidadesNegocio(JsonObject jsonObj) {
		List<UnidadeNegocio> listaUnidade = new ArrayList<UnidadeNegocio>();
		JsonObject jsonObjSelecao = new JsonObject() ;
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;

		String[] campos=  {"Id","Name"};
		String condicaoWhere="ativo__c=true";
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Unidade_de_Negocio__c", campos,condicaoWhere,"");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));

		listaUnidade=montaUnidade(jsonArray,"Unidade_de_Negocio__c");


		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaUnidade);
		jsonObjResult.addProperty("action", "buscarUnidadesNegocio");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}



}