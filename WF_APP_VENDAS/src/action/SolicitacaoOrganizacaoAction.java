package action;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import Beans.Organizacao;
import Beans.Reembolso;
import Beans.SolicitacaoOrganizacao;
import Beans.SolicitacaoOrganizacao;
import Beans.SolicitacaoOrganizacao;
import Service.ReembolsoService;
import Service.SolicitacaoOrganizacaoService;
import webService.DMLSalesForce;
import Service.SolicitacaoOrganizacaoService;
import Service.SolicitacaoOrganizacaoService;




public class SolicitacaoOrganizacaoAction {


	public List<SolicitacaoOrganizacao> montaSolicitacaoOrganizacao(JsonArray jsonArray, String nomeReferenciaTabela)
	{
		List<SolicitacaoOrganizacao> listaSolicitacaoOrganizacao = new ArrayList<SolicitacaoOrganizacao>();


		for (int i=0;i<jsonArray.size();i++)
		{
			SolicitacaoOrganizacao solicitacaoOrganizacao= new SolicitacaoOrganizacao();
			JsonElement jsonElement = jsonArray.get(i);
			JsonObject solicitacaoOrganizacaoJson = jsonElement.getAsJsonObject();
			//JsonObject solicitacaoOrganizacaoJson     = jsonObjElement.get(nomeReferenciaTabela).getAsJsonObject();

			String id;
			try{id=solicitacaoOrganizacaoJson.get("Id").getAsString();}
			catch(Exception ex){id="";}
			solicitacaoOrganizacao.setId(id);

			String nome;
			try {nome = solicitacaoOrganizacaoJson.get("Nome_da_Organizacao__c").getAsString();
			// do what you want with this byte buffer
			
			
			}
			catch(Exception ex){nome="";}
			solicitacaoOrganizacao.setNome(nome);

			String cep;
			try { cep = solicitacaoOrganizacaoJson.get("CEP__c").getAsString();}
			catch(Exception ex){cep="";}
			solicitacaoOrganizacao.setCep(cep);

			String logradouro;
			try{logradouro=solicitacaoOrganizacaoJson.get("Logradouro__c").getAsString();}
			catch(Exception ex) {logradouro="";}
			solicitacaoOrganizacao.setLogradouro(logradouro);

			String cidade;
			try{cidade = solicitacaoOrganizacaoJson.get("Cidade__c").getAsString();}
			catch(Exception e) {cidade="";}
			solicitacaoOrganizacao.setCidade(cidade);

			String estado;
			try{estado = solicitacaoOrganizacaoJson.get("Estado__c").getAsString();}
			catch(Exception e) {estado="";}
			solicitacaoOrganizacao.setUf(estado);

			String cnpj;
			try{cnpj = solicitacaoOrganizacaoJson.get("CNPJ__c").getAsString();}
			catch(Exception e) {cnpj="";}
			solicitacaoOrganizacao.setCnpj(cnpj);

			String segmento;
			try {segmento = solicitacaoOrganizacaoJson.get("Segmento__c").getAsString();}
			catch(Exception e) {segmento="";}
			solicitacaoOrganizacao.setSegmento(segmento); 

			String proprietario;
			try{proprietario = solicitacaoOrganizacaoJson.get("OwnerId__c").getAsString();}
			catch(Exception e) {proprietario="";}
			solicitacaoOrganizacao.setProprietario(proprietario);


			String observacao;
			try{observacao = solicitacaoOrganizacaoJson.get("Observacao__c").getAsString();}
			catch(Exception e) {observacao="";}
			solicitacaoOrganizacao.setObservacao(observacao);
			
			String data;
			try{data = solicitacaoOrganizacaoJson.get("Data_da_Solicitacao__c").getAsString();}
			catch(Exception e) {data="";}
			solicitacaoOrganizacao.setDataSolicitacaoOrganizacao(data);

			String bairro;
			try{bairro = solicitacaoOrganizacaoJson.get("Bairro__c").getAsString();}
			catch(Exception e) {bairro="";}
			solicitacaoOrganizacao.setBairro(bairro);
			
			
			String unidade;
			try{unidade = solicitacaoOrganizacaoJson.get("Unidade_Negocio__c").getAsString();}
			catch(Exception e) {unidade="";}
			solicitacaoOrganizacao.setUnidade(unidade);

			listaSolicitacaoOrganizacao.add(solicitacaoOrganizacao);
		}
		return listaSolicitacaoOrganizacao;

	}


	public String avaliarSolicitacaoOrganizacao(JsonObject jsonObj) throws Exception {
		List<SolicitacaoOrganizacao> listaSolicitacao = new ArrayList<SolicitacaoOrganizacao>();
		JsonObject jsonObjResult = new JsonObject() ;
		JsonObject jsonObjSelecao = new JsonObject() ;
		String result,lista;



		int status=jsonObj.get("status").getAsInt();
		String justificativa=jsonObj.get("justificativa").getAsString();
		String Id=jsonObj.get("solicitacao").getAsString();
		//UPDATE


		String[] campos= {"Id","Nome_da_Organizacao__c", "Logradouro__c",  "Cidade__c",  "Estado__c",  "CEP__c",  "Segmento__c","OwnerId__c", "Data_da_Solicitacao__c", "Status_SolicitacaoO__c","Observacao__c","CNPJ__c","Bairro__c","Unidade_Negocio__c"};
		String condicaoWhere="id='"+Id+"'";
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("WFOXCRM_SolicitacaoOrganizacao__c", campos,condicaoWhere,"Nome_da_Organizacao__c");
		JsonArray  jsonArray=jsonObjSelecao.getAsJsonArray("records");
		listaSolicitacao=montaSolicitacaoOrganizacao(jsonArray,"WFOXCRM_SolicitacaoOrganizacao__c");

		String[] vetorCamposAtu= {"Observacao__c","Status_SolicitacaoO__c"};
		String[] vetorValoresAtu= {justificativa,""+status};
		DMLSalesForce.atualizaSalesForce("WFOXCRM_SolicitacaoOrganizacao__c",Id, vetorCamposAtu, vetorValoresAtu);


		Gson gson = new Gson();
		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaSolicitacao);
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObjConv = (JsonObject)jsonParser.parse(lista);


		if(status==1)
		{
			OrganizacaoAction organizacaoAction= new OrganizacaoAction();
			organizacaoAction.salvarOrganizacao(jsonObjConv);
		}



		jsonObjResult.addProperty("action", "avaliarSolicitacao");
		jsonObjResult.addProperty("tipoSolicitacao", "organizacao");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}




	public String buscarSolicitacaoOrganizacao(JsonObject jsonObj) {
		List<SolicitacaoOrganizacao> listaSolicitacaoOrganizacao = new ArrayList<SolicitacaoOrganizacao>();
		JsonObject jsonObjResult = new JsonObject() ;
		JsonObject jsonObjSelecao = new JsonObject() ;
		String result,lista;
		SolicitacaoOrganizacaoService solicitacaoOrganizacaoService=new SolicitacaoOrganizacaoService();
		String id    = jsonObj.get("id").getAsString();


		String[] campos= {"Id","Nome_da_Organizacao__c", "Logradouro__c",  "Cidade__c",  "Estado__c",  "CEP__c",  "Segmento__c","OwnerId__c", "Data_da_Solicitacao__c", "Status_SolicitacaoO__c","Observacao__c","CNPJ__c","Bairro__c","Unidade_Negocio__c"};
		String condicaoWhere="id='"+id+"'";
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("WFOXCRM_SolicitacaoOrganizacao__c", campos,condicaoWhere,"Nome_da_Organizacao__c");
		JsonArray  jsonArray=jsonObjSelecao.getAsJsonArray("records");
		listaSolicitacaoOrganizacao=montaSolicitacaoOrganizacao(jsonArray,"WFOXCRM_SolicitacaoOrganizacao__c");

		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaSolicitacaoOrganizacao);
		jsonObjResult.addProperty("action", "buscarSolicitacaoOrganizacao");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}

	public String buscarLancamentosSolicitacaoOrganizacaoStatus(JsonObject jsonObj) {
		List<SolicitacaoOrganizacao> listaSolicitacaoOrganizacao = new ArrayList<SolicitacaoOrganizacao>();
		JsonObject jsonObjResult = new JsonObject() ;
		JsonObject jsonObjSelecao = new JsonObject();
		String result,lista;
		SolicitacaoOrganizacaoService solicitacaoOrganizacaoService=new SolicitacaoOrganizacaoService();
		String status    = jsonObj.get("status").getAsString();
		String proprietario    = jsonObj.get("proprietario").getAsString();


		String[] campos= {"Id","Nome_da_Organizacao__c", "Logradouro__c",  "Cidade__c",  "Estado__c",  "CEP__c",  "Segmento__c","OwnerId__c", "Data_da_Solicitacao__c", "Status_SolicitacaoO__c","Observacao__c","CNPJ__c","Bairro__c","Unidade_Negocio__c"};
		String condicaoWhere="Status_SolicitacaoO__c='"+status+"'%20AND%20OwnerId__c='"+proprietario+"'";
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("WFOXCRM_SolicitacaoOrganizacao__c", campos,condicaoWhere,"Nome_da_Organizacao__c");
		if(jsonObjSelecao !=null)
		{
			JsonArray  jsonArray=jsonObjSelecao.getAsJsonArray("records");
			listaSolicitacaoOrganizacao=montaSolicitacaoOrganizacao(jsonArray,"WFOXCRM_SolicitacaoOrganizacao__c");
			Gson gson = new Gson();

			// converte objetos Java para JSON e retorna JSON como String
			lista = gson.toJson(listaSolicitacaoOrganizacao);

		}
		else
		{
			lista="";
		}
		jsonObjResult.addProperty("action", "buscarSolicitacoesStatusTipo");
		jsonObjResult.addProperty("tipoSolicitacao", "organizacao");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}


	//	public String removerSolicitacaoOrganizacao(JsonObject jsonObj) {
	//
	//		JsonObject jsonObjResult = new JsonObject() ;
	//		String result;
	//		JsonObject jsonObjSelecao = new JsonObject() ;
	//		SolicitacaoOrganizacaoService solicitacaoOrganizacaoService=new SolicitacaoOrganizacaoService();
	//
	//		int solicitacao     = jsonObj.get("lancamento").getAsInt();
	////		solicitacaoOrganizacaoService.delete(solicitacao);
	//
	//		String[] campos= {"Nome_da_Pessoa__c", "Logradouro__c",  "Cidade__c",  "Estado__c",  "CEP__c",  "Segmento__c","OwnerId__c", "Data_da_Solicitacao__c", "Status_SolicitacaoO__c"};
	//		String condicaoWhere="status_da_Sol__c="+status;
	//		jsonObjResult=DMLSalesForce.selecionaSalesForce("WFOXCRM_SolicitacaoOrganizacao__c", campos,condicaoWhere);
	//		JsonArray  jsonArray=jsonObjSelecao.getAsJsonArray("records");
	//		listaSolicitacaoOrganizacao=montaSolicitacaoOrganizacao(jsonArray,"WFOXCRM_SolicitacaoOrganizacao__c");
	//	
	//		
	//		
	//		jsonObjResult.addProperty("action", "removerSolicitacaoOrganizacao");
	//		jsonObjResult.addProperty("retorno", 0);
	//		result = jsonObjResult.toString();
	//		System.out.println(result);
	//		return result;
	//	}

	public String salvarSolicitacaoOrganizacao(JsonObject jsonObj) throws Exception {

		JsonObject jsonObjResult = new JsonObject() ;
		String result;
		SolicitacaoOrganizacaoService solicitacaoOrganizacaoService=new SolicitacaoOrganizacaoService();
		int retorno=0;

		String id     = jsonObj.get("id").getAsString();
		String nome     = jsonObj.get("nome").getAsString();
		String cnpj     = jsonObj.get("cnpj").getAsString();
		cnpj=cnpj.replace(".", "").replace("/","");
		String logradouro     = jsonObj.get("logradouro").getAsString();
		String bairro     = jsonObj.get("bairro").getAsString();
		String cidade     = jsonObj.get("cidade").getAsString();
		String uf     = jsonObj.get("uf").getAsString();
		String cep     = jsonObj.get("cep").getAsString();
		String segmento     = jsonObj.get("segmento").getAsString();
		String observacao= jsonObj.get("observacao").getAsString();
		String dataSolicitacaoOrganizacao= jsonObj.get("dataSolicitacaoOrganizacao").getAsString();
		String proprietario=jsonObj.get("proprietario").getAsString();
		String unidade=jsonObj.get("unidade").getAsString();

		String[] vetorCampos= {"Nome_da_Organizacao__c", "CNPJ__c","Logradouro__c",  "Cidade__c",  "Estado__c",  "CEP__c",  "Segmento__c","OwnerId__c", "Status_SolicitacaoO__c","Observacao__c","Data_da_Solicitacao__c","CNPJ__c","Bairro__c","Unidade_Negocio__c","OwnerId"};
		String[] vetorValores= {nome,cnpj, logradouro,cidade,uf,cep, segmento, proprietario,"pendente",observacao,dataSolicitacaoOrganizacao,cnpj,bairro,unidade,"0051N000005t5P8QAI"};

		if (id.equals("0")==true)
		{
			DMLSalesForce.insereSalesForce("WFOXCRM_SolicitacaoOrganizacao__c", vetorCampos, vetorValores);
		}
		else
		{
			DMLSalesForce.atualizaSalesForce("WFOXCRM_SolicitacaoOrganizacao__c",id, vetorCampos, vetorValores);
		}

		jsonObjResult.addProperty("action", "salvarSolicitacaoOrganizacao");
		jsonObjResult.addProperty("retorno", retorno);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}








}