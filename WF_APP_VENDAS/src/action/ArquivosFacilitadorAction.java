package action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import Bundle.ParamBundle;




public class  ArquivosFacilitadorAction {

	private static final long serialVersionUID = -5385086202856927997L;
	private List<File> listaArquivos;
	private List<String> listaNomes;
	//*** constructor

	public  ArquivosFacilitadorAction() {
	}

	public void criaDiretorio(String turma,String nomeTurma){
	
		//Verifica se existe o diret�rio para o grupo
		File dir_turma = new File(Bundle.ParamBundle.REPORT_PATH+"/"+"turma_"+turma+"-"+nomeTurma);
		if (!dir_turma.exists()){
			dir_turma.mkdir();
		}

		setListaArquivos(dir_turma.listFiles());
		listaNomes = new ArrayList<String>();
		for(File f:listaArquivos){
			if (f.getName().charAt(0)!='.') {
				if (!f.isDirectory()){
					listaNomes.add(dir_turma+"/"+f.getName());
				} else {
					String prefixo = "turma_"+turma+"/";
					File dir_grupo = new File(prefixo+f.getName());
					File[] files = dir_grupo.listFiles();
					for (File g:files){
						listaNomes.add(dir_turma+"/"+f.getName()+"/"+g.getName());
					}
				}
			}
		}
	}
	
	public void consultaDiretorio(String turma, String nomeTurma){
		
		//Verifica se existe o diret�rio para o grupo
		File dir_turma = new File(Bundle.ParamBundle.REPORT_PATH+"/"+"turma_"+turma+"-"+nomeTurma);
		if (dir_turma.exists()){
		
			setListaArquivos(dir_turma.listFiles());
			listaNomes = new ArrayList<String>();
			for(File f:listaArquivos){
				if (f.getName().charAt(0)!='.') {
					if (!f.isDirectory()){
						listaNomes.add(dir_turma+"/"+f.getName());
					} else {
						String prefixo = "turma_"+turma+"/";
						File dir_grupo = new File(prefixo+f.getName());
						File[] files = dir_grupo.listFiles();
						for (File g:files){
							listaNomes.add(dir_turma+"/"+f.getName()+"/"+g.getName());
						}
					}
				}
			}
		}
	}	
	
	

	public List<File> getListaArquivos() {
		if (listaArquivos==null){
			listaArquivos = new ArrayList<File>();
		}
		return listaArquivos;
	}

	
	public List<String> getListaNomes() {
		if (listaNomes==null){
			listaNomes = new ArrayList<String>();
		}
		return listaNomes;
	}
	
	public void setListaArquivos(List<File> listaArquivos) {
		this.listaArquivos = listaArquivos;
	}

	public void setListaArquivos(File[] listaArquivos) {
		this.listaArquivos = new ArrayList<File>();
		for (File f:listaArquivos){
			this.listaArquivos.add(f);
		}
	}
/*
	public StreamedContent getFile(String arq) {
		StreamedContent file = null;
		FacilitadorMB facilitadorMB = (FacilitadorMB)MBAccess.getMB("#{facilitadorMB}");
		int turma = facilitadorMB.getTurmaId();
		try {
			InputStream stream;
			stream = new FileInputStream("turma_"+turma+"/"+arq);
			file = new DefaultStreamedContent(stream, "application/pdf", arq);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return file;
	}

	public List<String> getListaNomes() {
		criaDiretorio(turma);
		return listaNomes;
	}

	public void setListaNomes(List<String> listaNomes) {
		this.listaNomes = listaNomes;
	}
*/
}