package action;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import Beans.Atividade;
import Beans.Organizacao;
import Service.AtividadeService;
import webService.DMLSalesForce;




public class RelacionamentoPessoaAction {

	public List<Atividade> montaAtividade(JsonArray jsonArray, String nomeReferenciaTabela)
	{
		List<Atividade> listaAtividade = new ArrayList<Atividade>();
		Atividade atividade= new Atividade();

		for (int i=0;i<jsonArray.size();i++)
		{
			atividade = new Atividade();
			JsonElement jsonElement = jsonArray.get(i);
			JsonObject jsonObjElement = jsonElement.getAsJsonObject();
			//JsonObject atividadeJson     = jsonObjElement.get(nomeReferenciaTabela).getAsJsonObject();

	
			String idAtividade;
			try{idAtividade=jsonObjElement.get("Id").getAsString();}
			catch(Exception ex){idAtividade="";}
			atividade.setId(idAtividade);
			
			String tipodeAtividade;
			try{tipodeAtividade=jsonObjElement.get("Subject").getAsString();}
			catch(Exception ex){tipodeAtividade="";}
			atividade.setTipoAtividade(tipodeAtividade);
			
			String proprietario;
			try {proprietario = jsonObjElement.get("OwnerId__c").getAsString();}
			catch(Exception ex){proprietario="";}
			atividade.setProprietario(proprietario);

			String objetoAtividade;
			try { objetoAtividade = jsonObjElement.get("WhatId").getAsString();}
			catch(Exception ex){objetoAtividade="";}
			atividade.setNegocio(objetoAtividade);

			String pessoAtividade;
			try{pessoAtividade=jsonObjElement.get("WhoId").getAsString();}
			catch(Exception ex) {pessoAtividade="";}
			atividade.setPessoaAtividade(pessoAtividade);

	
			
			String descricao;
			try{descricao = jsonObjElement.get("Description").getAsString();}
			catch(Exception e) {descricao="";}
			atividade.setDescricao(descricao);
			
			
			String dataAtividade;
			try{dataAtividade = jsonObjElement.get("ActivityDate").getAsString();}
			catch(Exception e) {dataAtividade="";}
			atividade.setDataAtividade(dataAtividade);
			


			listaAtividade.add(atividade);
		}
		return listaAtividade;
}
	
//
//	public String buscarAtividadesOrganizacao(JsonObject jsonObj) {
//		List<Atividade> listaAtividade = new ArrayList<Atividade>();
//		JsonObject jsonObjResult = new JsonObject() ;
//		String result,lista;
//		AtividadeService atividadeService=new AtividadeService();
//		int organizacao    = jsonObj.get("organizacao").getAsInt();
//
//		Hashtable<String,Object> hash = new Hashtable<String,Object>();
//		hash.put("organizacao", organizacao);
//		hash.put("statusAtividade", 1);
//		//List<Colaborador> listPart = new  ColaboradorDao().findByFields(hash);
//		listaAtividade=atividadeService.findByFieldsOrdered(hash,"nome");
//		Gson gson = new Gson();
//
//		// converte objetos Java para JSON e retorna JSON como String
//		lista = gson.toJson(listaAtividade);
//		jsonObjResult.addProperty("action", "buscarAtividadesOrganizacao");
//		jsonObjResult.addProperty("retorno", lista);
//		result = jsonObjResult.toString();
//		System.out.println(result);
//		return result;
//	}
//
//	public String buscarTodosAtividadesOrganizacao(JsonObject jsonObj) {
//		List<Atividade> listaAtividade = new ArrayList<Atividade>();
//		JsonObject jsonObjResult = new JsonObject() ;
//		String result,lista;
//		AtividadeService atividadeService=new AtividadeService();
//		int organizacao    = jsonObj.get("organizacao").getAsInt();
//		//List<Colaborador> listPart = new  ColaboradorDao().findByFields(hash);
//		listaAtividade=atividadeService.findByFieldOrdered("organizacao",organizacao, "nome");
//		Gson gson = new Gson();
//
//		// converte objetos Java para JSON e retorna JSON como String
//		lista = gson.toJson(listaAtividade);
//		jsonObjResult.addProperty("action", "buscarTodosAtividadesOrganizacao");
//		jsonObjResult.addProperty("retorno", lista);
//		result = jsonObjResult.toString();
//		System.out.println(result);
//		return result;
//	}
	
	
	public String buscarAtividade(JsonObject jsonObj) {
		JsonObject jsonObjSelecao = new JsonObject() ;
		List<Atividade> listaAtividade = new ArrayList<Atividade>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		AtividadeService atividadeService=new AtividadeService();
		String id    = jsonObj.get("id").getAsString();
		//listaAtividade=atividadeService.findByField("id",id);
		String[] campos= {"Id","subject", "OwnerId__c",  "WhatId", "WhoId","Description","ActivityDate"};
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Event", campos,"Id='"+id+"'","");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		listaAtividade=montaAtividade(jsonArray,"Event");
		
		
		
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaAtividade);
		jsonObjResult.addProperty("action", "buscarAtividade");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
	public String buscarTodasAtividadesProprietarioPessoa(JsonObject jsonObj) {
		JsonObject jsonObjSelecao = new JsonObject() ;
		List<Atividade> listaAtividade = new ArrayList<Atividade>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		AtividadeService atividadeService=new AtividadeService();
		String proprietario    = jsonObj.get("proprietario").getAsString();
		String pessoa    = jsonObj.get("pessoa").getAsString();
		//listaAtividade=atividadeService.findByField("id",id);
		String condicaoWhere="OwnerId__c='"+proprietario+"'%20AND%20WhoId='"+pessoa+"'";
		String[] campos= {"Id","subject", "OwnerId__c",  "WhatId", "WhoId","Description","ActivityDate"};
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Event", campos,condicaoWhere,"");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		listaAtividade=montaAtividade(jsonArray,"Event");
		
		
		
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaAtividade);
		jsonObjResult.addProperty("action", "buscarTodasAtividadesProprietarioPessoa");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
	
	
	public String buscarTodasAtividadesProprietarioNegocio(JsonObject jsonObj) {
		JsonObject jsonObjSelecao = new JsonObject() ;
		List<Atividade> listaAtividade = new ArrayList<Atividade>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		AtividadeService atividadeService=new AtividadeService();
		String proprietario    = jsonObj.get("proprietario").getAsString();
		String negocio    = jsonObj.get("negocio").getAsString();
		//listaAtividade=atividadeService.findByField("id",id);
		String condicaoWhere="OwnerId__c='"+proprietario+"'%20AND%20WhatId='"+negocio+"'";
		String[] campos= {"Id","subject", "OwnerId__c",  "WhatId", "WhoId","Description","ActivityDate"};
		jsonObjSelecao=DMLSalesForce.selecionaSalesForce("Event", campos,condicaoWhere,"");
		JsonArray  jsonArray=(jsonObjSelecao.getAsJsonArray("records"));
		listaAtividade=montaAtividade(jsonArray,"Event");
		
		
		
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaAtividade);
		jsonObjResult.addProperty("action", "buscarTodasAtividadesProprietarioNegocio");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
//	public String buscarTodosAtividades(JsonObject jsonObj) {
//		List<Atividade> listaAtividade = new ArrayList<Atividade>();
//		JsonObject jsonObjResult = new JsonObject() ;
//		String result,lista;
//		AtividadeService atividadeService=new AtividadeService();
//		//int organizacao    = jsonObj.get("organizacao").getAsInt();
//		listaAtividade=atividadeService.findAllOrdered("nome");
//		Gson gson = new Gson();
//
//		// converte objetos Java para JSON e retorna JSON como String
//		lista = gson.toJson(listaAtividade);
//		jsonObjResult.addProperty("action", "buscarTodosAtividades");
//		jsonObjResult.addProperty("retorno", lista);
//		result = jsonObjResult.toString();
//		System.out.println(result);
//		return result;
//	}

	
	public String removerAtividade(JsonObject jsonObj) {
		
		JsonObject jsonObjResult = new JsonObject() ;
		String result;
		AtividadeService atividadeService=new AtividadeService();
		
		int atividade     = jsonObj.get("atividade").getAsInt();
		atividadeService.delete(atividade);

		jsonObjResult.addProperty("action", "removerAtividade");
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	public String salvarAtividade(JsonObject jsonObj) throws Exception {

		JsonObject jsonObjResult = new JsonObject() ;
		String result;
		AtividadeService atividadeService=new AtividadeService();
		int retorno=0;

		String id     = jsonObj.get("id").getAsString();
		String negocio     = jsonObj.get("negocio").getAsString();
		String pessoa     = jsonObj.get("pessoa").getAsString();
		String dataAtividade= jsonObj.get("dataAtividade").getAsString();
		String descricao= jsonObj.get("descricao").getAsString();
		String tipoAtividade=jsonObj.get("tipoAtividade").getAsString();
		String proprietario=jsonObj.get("proprietario").getAsString();
			
		String[] vetorCamposInsert= {"subject", "OwnerId__c",  "WhatId",  "WhoId",  "Description","ActivityDate","ActivityDateTime","DurationInMinutes","OwnerId"};
		String[] vetorValores= {tipoAtividade,proprietario, negocio,pessoa,descricao,dataAtividade,dataAtividade,"0","0051N000005t5P8QAI"};
		
		if (id.equals("0")==true)
		{

			DMLSalesForce.insereSalesForce("Event", vetorCamposInsert, vetorValores);	
		}
		else
		{
			DMLSalesForce.atualizaSalesForce("Event",id, vetorCamposInsert, vetorValores);

		}		
		
				
		jsonObjResult.addProperty("action", "salvarAtividade");
		jsonObjResult.addProperty("retorno", retorno);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
	
	

}