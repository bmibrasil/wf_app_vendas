package action;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import Beans.TipoHora;
import Service.TipoHoraService;




public class TipoHoraAction {

	

	public String buscarTipoHoras(JsonObject jsonObj) {
		List<TipoHora> listaTipoHora = new ArrayList<TipoHora>();
		JsonObject jsonObjResult = new JsonObject() ;
		String result,lista;
		TipoHoraService tipoHoraService=new TipoHoraService();
		listaTipoHora=tipoHoraService.findAllOrdered("nome");
		Gson gson = new Gson();

		// converte objetos Java para JSON e retorna JSON como String
		lista = gson.toJson(listaTipoHora);
		jsonObjResult.addProperty("action", "buscarTipoHoras");
		jsonObjResult.addProperty("retorno", lista);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
	public String removerTipoHoras(JsonObject jsonObj) {
		
		JsonObject jsonObjResult = new JsonObject() ;
		String result;
		TipoHoraService tipoHoraService=new TipoHoraService();
		
		int tipoHora     = jsonObj.get("tipoHora").getAsInt();
		tipoHoraService.delete(tipoHora);

		jsonObjResult.addProperty("action", "removerTipoHora");
		jsonObjResult.addProperty("retorno", 0);
		result = jsonObjResult.toString();
		System.out.println(result);
		return result;
	}
	
	
	
	
	
	

}