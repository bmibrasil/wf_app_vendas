package Bundle;

public class SystemBundle {
	public static final int Aba_Config=0;
	public static final int Aba_Nomes=1;
	public static final int Aba_Relacoes=2;
	public static final int Aba_Dados=3;
	public static final int Aba_Decisoes=4;
	public static final int Aba_ImpactoEE=5;
	public static final int Aba_DistribuicaoTemas=6;
	public static final int Aba_LE=7;
	public static final int Aba_CAP=8;
	public static final int Aba_Marketshare=9;
	public static final int Aba_Calculos=10;
	public static final int Aba_PessoasChave=11;
	public static final int Aba_LEMercado=12;
	public static final int Aba_TIH=13;
	public static final int Aba_PVF=14;
}
