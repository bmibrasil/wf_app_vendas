package Bundle;

public class ParamBundle {
	public static final String SYSROOTPATH = "../applications/BMI_CURVA_VALOR/debriefing/CurvaValor";
	public static final String ROOTPATH = "C:/servers/CapJotaFiles";
	public static final String TEMPLATE_PATH = SYSROOTPATH+"/templates";
	public static final String REPORT_PATH = ROOTPATH+"/resources/reports";
	public static final String EXPORT_PATH = ROOTPATH+"/resources/exports";
	public static final String IMAGE_PATH = ROOTPATH+"/resources/images";
	public static final String SQL_SCRIPTS_PATH = "C:/Users/BiankaGoncalves(BMI)/git/curva_valor/WebContent/WEB-INF/resources";
}