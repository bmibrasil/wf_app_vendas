package Bundle;

public class Opcoes{
	int a;
	int b;
	int c;
	int d;
	int e;
	
	public Opcoes(int a, int b, int c, int d, int e) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
	}
	
	public Opcoes(int a, int b, int c, int d) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = 0;
	}

	public Opcoes(int a, int b, int c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = 0;
		this.e = 0;
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public int getC() {
		return c;
	}

	public void setC(int c) {
		this.c = c;
	}

	public int getD() {
		return d;
	}

	public void setD(int d) {
		this.d = d;
	}

	public int getE() {
		return e;
	}

	public void setE(int e) {
		this.e = e;
	}
	
}
