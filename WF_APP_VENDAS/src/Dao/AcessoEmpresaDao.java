package Dao;

import java.util.List;


import Beans.AcessoEmpresa;
import Dao.AbstractDAO;

public class AcessoEmpresaDao extends AbstractDAO<AcessoEmpresa> {
	public AcessoEmpresaDao() {
		super();
	}

	public int consultarMenorFaseAcessoTurma(int turma, int acesso){
		String query = "select min(acessoempresa.fase) "
  				       + "from  "
					 + "acessoempresa, acesso "
					 + "where acessoempresa.acesso = acesso.id and acesso.turma=#1 "
					 +  "and acesso.id= #2 ";
		query=query.replace("#1", ""+ turma);
		query=query.replace("#2", ""+ acesso);
		return getIntValueSQL(query);
	}
	
	public int consultarMenorFaseTurma(int turma){
		String query = "select min(acessoempresa.fase) "
  				       + "from  "
					 + "acessoempresa, acesso "
					 + "where acessoempresa.acesso = acesso.id and acesso.turma=#1 "
					 +  "and acesso.tipo <>2 ";
		query=query.replace("#1", ""+ turma);
		return getIntValueSQL(query);
	}
	
	
	public int consultarFaseFacilitador(int turma){
		String query = "select fase "
  				       + "from  "
					 + "acesso "
					 + "where acesso.turma=#1 "
					 +  "and acesso.tipo =2";
		query=query.replace("#1", ""+ turma);
		
		return getIntValueSQL(query);
	}
	
	
	public int consultarMenorFaseGrupo(int turma, int acesso){
		String query = "select min(acessoempresa.fase) "
  				       + "from  "
					 + "acessoempresa, acesso "
					 + "where acessoempresa.acesso = acesso.id and acesso.turma=#1 and acesso.id=#2";
					 
		query=query.replace("#1", ""+ turma);
		query=query.replace("#2", ""+ acesso);
		return getIntValueSQL(query);
	}
	
	//busca as votações de cada grupo
	public List<AcessoEmpresa> buscaAcessoNegocios(int empresa ,int turma){
		String query = "select "
					 + "acessoempresa.* "
					 + "from  "
					 + "acessoempresa, acesso "
					 + "where acesso.id= acessoempresa.acesso and acesso.turma =#1 and "
					 + "acessoempresa.empresa = #2 and acesso.tipo=1 ";
		query=query.replace("#1", ""+ turma);
		query=query.replace("#2", ""+ empresa);
		return executeSQL(query);
	}
	
		
}