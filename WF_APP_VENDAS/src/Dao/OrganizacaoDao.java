package Dao;

import java.util.List;

import Beans.Organizacao;
import Beans.Pessoa;


public class OrganizacaoDao extends AbstractDAO<Organizacao> {
	public OrganizacaoDao() {
		super();
	}

	
	public List<Organizacao> buscaOrganizacaocomPessoa(int proprietario){
		String query = "select distinct organizacao.* from  pessoa, organizacao " 
	                   +"where pessoa.organizacao=organizacao.id and pessoa.proprietario=#1 ";
	           
				query=query.replace("#1", ""+ proprietario);
		
		
		
		return executeSQL(query);
	}	
	
	
	public List<Organizacao> buscaOrganizacaocomNegocio(int proprietario){
		String query = "select distinct organizacao.* from  negocio, organizacao " 
	                   +"where negocio.organizacao=organizacao.id and pessoa.proprietario=#1 ";
	           
				query=query.replace("#1", ""+ proprietario);
		
		
		
		return executeSQL(query);
	}	
	
	
}