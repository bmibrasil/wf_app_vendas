package Dao;

import java.util.List;

public interface GenericDAO<T> {
	
	public List<T> findAll();
	public void save(T content);
	
}