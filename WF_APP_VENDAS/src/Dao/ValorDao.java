package Dao;

import java.sql.ResultSet;
import java.util.List;


import Beans.AcessoEmpresa;
import Beans.Valor;
import Dao.AbstractDAO;

public class ValorDao extends AbstractDAO<Valor> {
	public ValorDao() {
		super();
	}

	public ResultSet consultarGap(int turma , int fase, int empresa) {
		ResultSet result;
		String query = "select "
				 + "valor.id, valor.valor, abs(classificacaoCliente-classificacaoEmpresa) gap "
				 + "from  "
				 + "valor,acesso "
				 + "where valor.acesso= acesso.id and "
				 + "valor.turma = #1  and empresa= #2 and acesso.tipo=2 and valor.fase=#3 "
				 + "order by gap desc"; 
				
		query=query.replace("#1", ""+ turma);
		query=query.replace("#2", ""+ empresa);
		query=query.replace("#3", ""+ fase);
		result=executeRawSQL(query);
		return result;
	}



	public List<Valor> buscaValoresTurma(int turma ,int negocio, int fase){
		String query = "select "
					 + "valor.* "
					 + "from  "
					 + "valor,acesso "
					 + "where valor.acesso= acesso.id and "
					 + "valor.turma = #1  and empresa= #2 and acesso.tipo=2 and valor.fase=#3 ";
		query=query.replace("#1", ""+ turma);
		query=query.replace("#2", ""+ negocio);
		query=query.replace("#3", ""+ fase);
		return executeSQL(query);
	}
	
	public List<Valor> buscaValoresAcesso(int turma , int negocio, int acesso, int fase){
		String query = "select "
					 + "* "
					 + "from  "
					 + "valor "
					 + "where turma in (#1) and empresa= #2 and acesso= #3 and fase= #4 ";
		query=query.replace("#1", ""+ turma);
		query=query.replace("#2", ""+ negocio);
		query=query.replace("#3", ""+ acesso);
		query=query.replace("#4", ""+ fase);
		return executeSQL(query);
	}	
	
	
	
	
	public void atualizaListaAcesso(int turma,int fase, int empresa,int acessoBase,int novoAcesso)
	{
	    
		String query = "insert into valor (valor, acesso, empresa, votacao, classificacaoCliente,classificacaoEmpresa, turma,fase) "
				 + "select valor, #6, empresa, votacao,classificacaoCliente, classificacaoEmpresa, turma, (#5) "
				 + "from  "
				 + "valor "
				 + "where turma =#1 and acesso = #2 and fase=#3 and empresa=#4 ";
	             query=query.replace("#1", ""+ turma);
	             query=query.replace("#2", ""+ acessoBase);
	             query=query.replace("#3", ""+ (fase-1));
	             query=query.replace("#4", ""+ empresa);
	             query=query.replace("#5", ""+ fase);
	             query=query.replace("#6", ""+ novoAcesso);
	             execute(query);
		
	}
	
	
	public void atualizaFaseAcesso(int turma,int fase, int empresa)
	{
	    
		String query = "update valor "
				 + "set fase =#2  "
				 + "where turma =#1 and fase=#4 and empresa=#3 ";
	             query=query.replace("#1", ""+ turma);
	             query=query.replace("#2", ""+ fase);
	             query=query.replace("#3", ""+ empresa);
	             query=query.replace("#4", ""+ (fase-1));
	             execute(query);
		
	}
	
	public	void calculaPontosGrafico(int turma, int empresa, int acesso, int fase){
		ResultSet result =null;
		
		//Calcula a media de cliente e da empresa de cada valor da turma 
		String sql = 
				
				 "update valor, "
				 + "(select valor, avg(classificacaoCliente) classificacaoCliente, avg(classificacaoEmpresa) classificacaoEmpresa "
				+" from " 
				+" valor "
				+" where "
			    +" turma = %1 and acesso <> %3 and fase = %4 and empresa= %2 "
			    + "group by valor) graf "
			    + "set  valor.classificacaoCliente=graf.classificacaoCliente , "
			    + "valor.classificacaoEmpresa=graf.classificacaoEmpresa " 
			    + "where valor.turma=%1 and valor.acesso=%3 and valor.valor=graf.valor and empresa= %2 ";
			    sql=sql.replace("%1", ""+turma);
			    sql=sql.replace("%2", ""+empresa);
			    sql=sql.replace("%3", ""+acesso);
			    sql=sql.replace("%4", ""+fase);
			    execute(sql);
		
	}  

		
}