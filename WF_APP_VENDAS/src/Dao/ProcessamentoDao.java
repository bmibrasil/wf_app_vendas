package Dao;

import java.util.ArrayList;
import java.util.List;

import Beans.Processamento;

public class ProcessamentoDao extends AbstractDAO<Processamento> {
	public ProcessamentoDao() {
		super();
	}

	public List<Processamento> getHerdeiros(Processamento e, int grupo){
		return getHerdeiros(e.getCampo(),grupo);
	}
	
	public List<Processamento> getHerdeiros(String campo, int grupo){
		List<Processamento> result = new ArrayList<Processamento>();
		
		String query = 
			"select "+ 
			    "processamento.id, "+ 
			    "processamento.grupo, "+ 
			    "processamento.campo, "+ 
			    "processamento.valor, "+ 
			    "processamento.conteudo, "+ 
			    "processamento.descricao "+
			"from  "+ 
			    "processamento"+ 
			 "where  "+ 
			    "processamento.campo like '#1' AND "+
			    "processamento.grupo = #2";
		
		query=query.replace("#1", campo);
		query=query.replace("#2", ""+grupo);
		
		result = executeSQL(query);
		
		return result;
	}
	
	public void novoGrupo(int grupo){
		String query = 
				"insert into "
				+ "processamento ("
					+ "grupo,"
					+ "campo,"
					+ "valor,"
					+ "conteudo,"
					+ "descricao"
				+ ") "
				+ "select "
					+ "#1 as grupo,"
					+ "campo,"
					+ "valor,"
					+ "par,"
					+ "conteudo,"
					+ "descricao"
				+ "from processamento where grupo = 0";
		query=query.replace("#1",""+grupo);
		execute(query);
	}
	
}