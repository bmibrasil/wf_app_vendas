package Dao;

import java.util.List;

import Beans.Colaborador;
import Beans.GapValor;


public class ColaboradorDao extends AbstractDAO<Colaborador> {
	public ColaboradorDao() {
		super();
	}


	public List<Colaborador> buscaColaboradorEquipe(int colaborador){
		String query = "select colaborador.* from  colaborador, gestao " 
	                   +"where (colaborador.id=gestao.colaborador and gestao.gestor=#1) "  
				      + "union "  
				      +"select colaborador.* from  colaborador " 
				      +	"where colaborador.id=#1 ";
				query=query.replace("#1", ""+ colaborador);
		
		
		return executeSQL(query);
	}	
	
	public void bloqueiaColaborador (String acesso, String bloqueio)
	{

		String query = 
				"update colaborador "
				+"set bloqueado=#1 "
				+"where colaborador.id = (#2)  ";
						
		query=query.replace("#1",""+bloqueio);
		query=query.replace("#2",""+acesso);
		execute(query);
	}
	
	
}