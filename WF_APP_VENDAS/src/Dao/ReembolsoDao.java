package Dao;

import java.sql.ResultSet;

import Beans.Reembolso;


public class ReembolsoDao extends AbstractDAO<Reembolso> {
	public ReembolsoDao() {
		super();
	}

	public ResultSet consolidaReembolso(String listaColaboradores, String periodo) {
		ResultSet result;
		String query = 
				"SELECT colaborador.nome, CAST(SEC_TO_TIME( SUM( TIME_TO_SEC( `horasTrabalhadas` ) ) ) AS CHAR) totalHoras "
				+"FROM reembolso, colaborador "
				+"WHERE  reembolso.colaborador=colaborador.id AND colaborador.id IN (#1) AND reembolso.periodo = (#2) "
	            +"GROUP BY colaborador.nome "
				+"ORDER BY colaborador.nome ";	
		query=query.replace("#1", ""+ listaColaboradores);
		query=query.replace("#2", ""+ periodo);
		result=executeRawSQL(query);
		return result;
}


public ResultSet buscaResumoHorasProjetoPeriodo(String periodo) {
	ResultSet result;
	String query = "SELECT cliente.nome nomecliente,projeto.nome nomeprojeto, CAST(SEC_TO_TIME( SUM( TIME_TO_SEC( `horasTrabalhadas` ) ) ) AS CHAR) totalHoras "
					+ "FROM reembolso, projeto, tipohora,cliente,colaborador where "
					+" projeto=projeto.id and tipo=tipohora.id and reembolso.cliente =cliente.id and colaborador=colaborador.id "
					+ "AND reembolso.periodo = (#1) "
					+"group by cliente.nome,projeto.nome ";
					query=query.replace("#1", ""+ periodo);
					result=executeRawSQL(query);
					return result;
}


public ResultSet buscaResumoHorasProjetoColaboradorTipoHoraPeriodo(String periodo) {
	ResultSet result;
	String query = "SELECT cliente.nome nomecliente,projeto.nome nomeprojeto, colaborador.nome colaborador , tipohora.nome tipoHora,  CAST(SEC_TO_TIME( SUM( TIME_TO_SEC( `horasTrabalhadas` ) ) ) AS CHAR) totalHoras "
					+ "FROM reembolso, projeto, tipohora,cliente,colaborador where "
					+" projeto=projeto.id and tipo=tipohora.id and reembolso.cliente =cliente.id and colaborador=colaborador.id "
					+ "AND reembolso.periodo = (#1) "
					+"group by nomecliente,nomeprojeto,colaborador,tipohora ";
					query=query.replace("#1", ""+ periodo);
					result=executeRawSQL(query);
					return result;
}


public ResultSet buscaLancamentosColaboradorPeriodo(String dataInicio, String dataFim) {
	ResultSet result;
	String query = "SELECT colaborador.email, reembolso.data, reembolso.inicio, reembolso.termino, tipohora.nome, projeto.centroCusto, projeto.codigoSap "
					+ "FROM reembolso, projeto, tipohora,cliente,colaborador,periodo where "
					+"projeto=projeto.id AND tipo=tipohora.id AND reembolso.cliente =cliente.id AND colaborador=colaborador.id "
					+"AND reembolso.periodo=periodo.id AND periodo.datainicial = '#1' AND periodo.datafinal='#2' ";
					
					query=query.replace("#1", ""+ dataInicio);
					query=query.replace("#2", ""+ dataFim);
					result=executeRawSQL(query);
					return result;
}



public void aprovaPeriodo (String periodo, String acesso)
{

	String query = 
			"update reembolso "
			+"set aprovado=1 "
			+"where reembolso.periodo = (#1) and reembolso.acesso = (#2)  ";
					
	query=query.replace("#1",""+periodo);
	query=query.replace("#2",""+acesso);
	execute(query);
}






}					