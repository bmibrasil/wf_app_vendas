package Dao;

import java.util.List;

import Beans.ParticipanteStatus;
import Dao.AbstractDAO;

public class ParticipanteStatusDao extends AbstractDAO<ParticipanteStatus> {
	public ParticipanteStatusDao() {
		super();
	}

	
	
	public List<ParticipanteStatus> getParticipantesStatus(int turma){
		String query = 
				"select t.id, p.nome, max(e.nome) empresa, max(a.fase) fase ,p.codigoAcesso "  
				+"from  turma t join acesso p on p.turma=t.id " 
				+"left outer join acessoempresa a on a.acesso=p.id "
				+ "left outer join empresa e on e.id=a.empresa "
				+"where  p.turma=#1 "
				+"group by t.id, p.nome, a.empresa,p.codigoAcesso "
				+ "order by p.nome, a.empresa";
				query=query.replace("#1", ""+ turma);
				return executeSQL(query);
	}
}