package Dao;

import java.util.List;

import Beans.SolicitacaoPessoa;



public class SolicitacaoPessoaDao extends AbstractDAO<SolicitacaoPessoa> {
	public SolicitacaoPessoaDao() {
		super();
	}


	public List<SolicitacaoPessoa> buscaColaboradorEquipe(int solicitacaoPessoa){
		String query = "select solicitacaoPessoa.* from  solicitacaoPessoa, gestao " 
	                   +"where (solicitacaoPessoa.id=gestao.solicitacaoPessoa and gestao.gestor=#1) "  
				      + "union "  
				      +"select solicitacaoPessoa.* from  solicitacaoPessoa " 
				      +	"where solicitacaoPessoa.id=#1 ";
				query=query.replace("#1", ""+ solicitacaoPessoa);
		
		
		return executeSQL(query);
	}	
	
	public void bloqueiaColaborador (String acesso, String bloqueio)
	{

		String query = 
				"update solicitacaoPessoa "
				+"set bloqueado=#1 "
				+"where solicitacaoPessoa.id = (#2)  ";
						
		query=query.replace("#1",""+bloqueio);
		query=query.replace("#2",""+acesso);
		execute(query);
	}
	
	
}