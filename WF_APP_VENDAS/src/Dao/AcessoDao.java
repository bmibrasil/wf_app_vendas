package Dao;

import java.util.List;

import Beans.Acesso;
import Dao.AbstractDAO;

public class AcessoDao extends AbstractDAO<Acesso> {
	public AcessoDao() {
		super();
	}

	public int findAcesso(int turma, int empresa) {
		String sql = "select acesso.id"
				   + " from acesso, acessoempresa where "
				   + " acesso.id=acessoempresa.acesso and acesso.turma = #1 and acessoEmpresa.empresa=#2 and acesso.tipo=2";
					sql=sql.replace("#1", ""+ turma);
					sql=sql.replace("#2", ""+ empresa);
		return getIntValueSQL(sql);
	}
	
	
	
	public List<Acesso> listaAcessoAtivos(int turma, int empresa)
	{
		String sql = "select acesso.* "
				   + " from acesso, acessoempresa where "
				   + " acesso.id= acessoempresa.acesso and acessoempresa.empresa= #2 "
				   + "and acesso.turma=#1 and acessoempresa.fase>=1";
				   sql=sql.replace("#1", ""+ turma);
				   sql=sql.replace("#2", ""+ empresa);
				  
		return executeSQL(sql);
	
	}

	
	
	public int verificaStatusTurmaAcesso(String acesso){
		String query = "select "
					 + "turma.status "
					 + "from  "
					 + "acesso,turma "
					 + "where acesso.turma=turma.id and "
					 + "acesso.turma = #1";
		query=query.replace("#1", ""+ acesso);
		return  getIntValueSQL(query);
	}
	
	public int validaDominio(int turma,String email) {
	    String query = "select "
					 + "turma.status "
					 + "from  "
					 + "turma "
					 + "where turma.id = #1  and "
				     + "upper(turma.dominios) like  upper('%#2;%')";
		query=query.replace("#1", ""+ turma);
		query=query.replace("#2", ""+ email);
		return getIntValueSQL(query);
	}
	
}