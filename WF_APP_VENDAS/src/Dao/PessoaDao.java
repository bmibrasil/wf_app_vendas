package Dao;

import java.util.List;

import Beans.Pessoa;



public class PessoaDao extends AbstractDAO<Pessoa> {
	public PessoaDao() {
		super();
	}


	public List<Pessoa> buscaColaboradorEquipe(int pessoa){
		String query = "select pessoa.* from  pessoa, gestao " 
	                   +"where (pessoa.id=gestao.pessoa and gestao.gestor=#1) "  
				      + "union "  
				      +"select pessoa.* from  pessoa " 
				      +	"where pessoa.id=#1 ";
				query=query.replace("#1", ""+ pessoa);
		
		
		return executeSQL(query);
	}	
	
	public void bloqueiaColaborador (String acesso, String bloqueio)
	{

		String query = 
				"update pessoa "
				+"set bloqueado=#1 "
				+"where pessoa.id = (#2)  ";
						
		query=query.replace("#1",""+bloqueio);
		query=query.replace("#2",""+acesso);
		execute(query);
	}
	
	
}