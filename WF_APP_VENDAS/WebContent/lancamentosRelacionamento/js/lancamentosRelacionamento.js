//Variáveis da sessão
var observer;
var config;
var target;
var periodo;
var indiceColaborador;

var vendedor = "0";
var unidade = "0";
var organizacao = 0;
var projeto = 0;
var etapa = 0;
var tipoHora = 0;
var nome="";
var status=0;
var tabelaLancamentos;
var listaNegocios = [];
var listaOrganizacoes = [];
var listaPessoas = [];
var listaLancamentos = [];
var tipoAtividade=1;
var pessoa=0;


function admin_init(){
	// cadastro = sessionStorage.getItem("cadastro");
	//  nome=sessionStorage.getItem("nomeColaborador");


	vendedor=sessionStorage.getItem("vendedor");
	unidade=sessionStorage.getItem("unidade");
	
	lancamentoRelacionamento=sessionStorage.getItem("lancamentoRelacionamento");
	document.getElementById("lancamentosrelacionamento_pessoa").addEventListener("change", mudaPessoa);
	document.getElementById("lancamentosrelacionamento_organizacao").addEventListener("change",buscarPessoasOrganizacao );
	buscarOrganizacoes();
	if ((vendedor==0) || (vendedor==null) || (vendedor==undefined)) 
	{
		window.document.location.href = '../login/login.html';
	}
	else
	{	
		setSelectedValue(document.getElementById("lancamentosrelacionamento_pessoa"),0);
	}

	tabelaLancamentos = document.getElementById("lancamentosrelacionamento_lancamentos");
	document.getElementById("menuop1").addEventListener("click", inserirRelacionamento);
	document.getElementById("menuop2").addEventListener("click", carregarMenuVendas);
	document.getElementById("menuop3").addEventListener("click", sair);
	//buscaOrganizacaocomPessoa();

}


function isMobile()
{
	var userAgent = navigator.userAgent.toLowerCase();
	if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
		return true;
	else
		return false;
}



//PREENCHIMENTO DAS COMBOS





function dataAtualFormatada(data){

	var dia = data.getDate();
	if (dia.toString().length == 1)
		dia = "0"+dia;
	var mes = data.getMonth()+1;
	if (mes.toString().length == 1)
		mes = "0"+mes;
	var ano = data.getFullYear();  

	return ano+"-"+mes+"-"+dia+' 00:00:00.0';
}

function mudaPessoa() {
 	tabelaLancamentos.innerHTML = '';

	buscarTodosRelacionamentosProprietarioPessoa();
	}
	

function buscarPessoasProprietario()
{
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarPessoasProprietario",
			proprietario: vendedor,
			unidade: unidade

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   	
}

function buscarTodosRelacionamentosProprietarioPessoa(lancamentoId){
	var pessoa= document.getElementById("lancamentosrelacionamento_pessoa").value;
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarTodosRelacionamentosProprietarioPessoa",
			pessoa: pessoa,
			proprietario: vendedor,
			unidade :  unidade

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");  

}


function removerLancamento(lancamentoId){
	var lancamento=listaLancamentos[lancamentoId].id;
	var r=confirm("Você deseja realmente remover o lançamento?");
	if (r==true)
	{
		var url = retornaServer();
		var data = {
				page: "vendas",
				action: "removerAtividade",
				lancamento: lancamento

		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");  
	}

}


//function buscaOrganizacaocomPessoa(){
//		var url = retornaServer();
//		var data = {
//				page: "vendas",
//				action: "buscaOrganizacaocomPessoa",
//				proprietario:vendedor,
//				unidade: unidade
//		}
//		url=url+encodeURI(JSON.stringify(data));
//		callWebservice(url,"callback");  
//	
//
//}

function editarLancamento(lancamentoId)
{
	var lancamento=listaLancamentos[lancamentoId].id;
	var achei=0;
	var i=0;
	while ( achei==0)
	{
		if (listaLancamentos[i].id==lancamento)
		{
			achei=1;	  
		}
		i=i+1;
	}
	
	sessionStorage.setItem("lancamentoRelacionamento",listaLancamentos[i-1].id);
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("unidade",unidade);
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../relacionamentoPessoa/relacionamentoPessoa.html';

}

function preencheListaOrganizacoes(){
	var html = '<option value="0">Organizacao...</option>';
	var select = "";

	for (i=0;i<listaOrganizacoes.length;i++){
		if(organizacao==i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaOrganizacoes[i].id+'" '+select+'>' +listaOrganizacoes[i].nome+'</option>';
	}

	document.getElementById("lancamentosrelacionamento_organizacao").innerHTML = html;
	setSelectedValue(document.getElementById("lancamentosrelacionamento_organizacao"),0);
}

function preencheListaPessoas(){
	var html = '<option value="0">Pessoa...</option>';
	var indiceOrganizacao=0;
	var select = "";
	for (i=0;i<listaPessoas.length;i++){
		if(pessoa==i){
			select = "selected";
		}else{
			selected = "";
		}

		for (j=0;j<listaOrganizacoes.length;j++)
		{

			if (listaOrganizacoes[j].id== listaPessoas[i].organizacao)
			{
				indiceOrganizacao=j;
			}

		}

		html += '<option value="'+listaPessoas[i].id+'" '+select+'>' +listaPessoas[i].nome+"-"+listaOrganizacoes[indiceOrganizacao].nome+'</option>';
	}
	document.getElementById("lancamentosrelacionamento_pessoa").innerHTML = html;
	setSelectedValue(document.getElementById("lancamentosrelacionamento_pessoa"),0);

}

function buscarOrganizacoes(){

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarOrganizacoes",
			unidade: unidade


	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}

function buscarPessoasOrganizacao(){

	var proprietario = vendedor;
	e=document.getElementById("lancamentosrelacionamento_organizacao");
	organizacao = e.options[e.selectedIndex].value;

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarPessoasOrganizacao",
			proprietario: proprietario,
			organizacao:organizacao,
			unidade: unidade



	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}

//PROCESSAMENTO DO RETORNO DOS WEBSERVICES

function callback(data){

	
	if(data.action=="buscarPessoasProprietario"){
		listaPessoas=jQuery.parseJSON(data.retorno);
		preencheListaPessoas();

	}

//	if(data.action=="buscaOrganizacaocomPessoa"){
//		listaOrganizacoes=jQuery.parseJSON(data.retorno);
//		buscarPessoasProprietario();
//
//	}

	if(data.action=="buscarTodosRelacionamentosProprietarioPessoa"){
		listaLancamentos=jQuery.parseJSON(data.retorno);
		preencheTabelaLancamentos();

	}

	if(data.action=="buscarPessoasOrganizacao"){
		listaPessoas=jQuery.parseJSON(data.retorno);
		preencheListaPessoas();
	}

	if(data.action=="removerAtividade"){
		location.reload();
	}
	
	if(data.action=="buscarOrganizacoes"){
		listaOrganizacoes=jQuery.parseJSON(data.retorno);
		
		preencheListaOrganizacoes();
	}


}


function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value==valueToSet) {
			selectObj.options[i].selected = true;

		}
	}
	return i;
}




function inserirRelacionamento()
{
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("unidade",unidade);
	sessionStorage.setItem("lancamentoRelacionamento","0");
	
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../relacionamentoPessoa/relacionamentoPessoa.html';

}

function carregarMenuVendas()
{
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("unidade",unidade);
	sessionStorage.setItem("lancamentoRelacionamento","0");
	
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../menuVendas/menuVendas.html';

}





//TRATAMENTO DOS EVENTOS

function preencheTabelaLancamentos(){
	var html = '';
	var status;
	var cargo="";
	var indiceNegocio;
	var indicePessoa;
	var j=0;


	var achei=0;
	var dataFormatada;
	var data;

	if(listaLancamentos.length > 0)
	{
		for(i = 0; i < listaLancamentos.length; i++)
		{
			
			achei=0;
			j=0;
			while (achei ==0)
			{
				if (listaLancamentos[i].pessoaAtividade== listaPessoas[j].id)
				{
					achei=1;
					indicePessoa=j;
				}
				j=j+1;
			}
			//var dataMontada =listaLancamentos[i].dataAtividade.substr(6,4)+"-"+listaLancamentos[i].dataAtividade.substr(3,2)+"-"+listaLancamentos[i].dataAtividade.substr(0,2)+" 00:00:00";
			//data = new Date(dataMontada);
			
			var dataMontada=listaLancamentos[i].dataAtividade.substr(0,10);
			var data = construirData(dataMontada, "00:00:00");
			//data =new Date(listaLancamentos[i].dataAtividade);
			dataFormatada= formataDataPt(data);
			
			var horaInicio = obterHorario(listaLancamentos[i].dataInicio);
			var horaFim = obterHorario(listaLancamentos[i].dataFim);
			
			if (isMobile() ==false)
			{

				html+='<div class="item" id="div'+listaLancamentos[i].id+'">'
				+'<div><b>'
				+" "+dataFormatada;
				html+= "<br>Inicio: " + horaInicio + " - Fim: " + horaFim;
				html+="<br>"+listaPessoas[indicePessoa].nome;
				html+="<br><b><font color=#515caf>Tipo de Atividade: "+listaLancamentos[i].tipoAtividade +"</b></font>"
				+"<br> Descrição:<br>" +listaLancamentos[i].descricao
				+'</div>'
				+'<button class="btnedit" onclick="editarLancamento('+i+')"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:24px"></i></button>'
	//			+'<button class="btndelete" onclick="removerLancamento('+i+')"><i class="fa fa-trash" aria-hidden="true" style="font-size:24px"></i></button>'
				+'</div>';
			}
			else
			{

				html+='<div class="item" id="div'+listaLancamentos[i].id+'">'
				+'<div><b>'
				+" "+dataFormatada;
				html+= "<br>Inicio: " + horaInicio + " - Fim: " + horaFim;
				html+="<br>"+listaPessoas[indicePessoa].nome;
				html+="<br><b><font color=#515caf>Tipo de Atividade: "+listaLancamentos[i].tipoAtividade +"</b></font>" 
				+"<br> Descrição:<br>" +listaLancamentos[i].descricao
				+'</div>'
				+'<button class="action" onclick="editarLancamento('+i+')"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:24px"></i></button>'
		//		+'<button class="action" onclick="removerLancamento('+i+')"><i class="fa fa-trash" aria-hidden="true" style="font-size:24px"></i></button>'
				+'</div>';
			}        	   

		}

		tabelaLancamentos.innerHTML = html;
	}//checkButton();
	else{
		tabelaLancamentos.innerHTML = html;
	}

}



function sair()
{
	localStorage.clear();
	sessionStorage.setItem("vendedor",0);
	sessionStorage.setItem("unidade",0);
	sessionStorage.setItem("lancamentoRelacionamento",0);
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../login/login.html';

}


target = document.querySelectorAll('.item-list')[0];



//cria uma nova instância de observador
observer = new MutationObserver(function(mutations) {
	mutations.forEach(function(mutation) {
		console.log("tipo de mutação" +mutation.type);

		$(function() {$('.example-1').listSwipe();});

	});    
});

//configuração do observador:
config = { attributes: true, childList: true, characterData: true };

//passar o nó alvo pai, bem como as opções de observação
observer.observe(target, config);

//function dragElement(event) {
//var elementToDrag = event.target;
//elementToDrag.style.left = event.gesture.deltaX + 'px';
//};



//function resetElement(event) {
//var elementToReset = event.target;
//elementToReset.style.left = 0;
//};


