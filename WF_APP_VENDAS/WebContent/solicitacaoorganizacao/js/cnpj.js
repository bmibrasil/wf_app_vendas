var VALORES_VALIDACAO_PRIMEIRO_DIGITO_VERIFICADOR = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
var VALORES_VALIDACAO_SEGUNDO_DIGITO_VERIFICADOR = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];

var LISTA_MULTIPLICACOES = [];

var QUANTIDADE_DIGITOS_CORPO_CNPJ = 11;

function validarCNPJ(){
	
	var cnpj = document.getElementById("solicitacaoorganizacao_cnpj").value;

	cnpj = cnpj.replace(/\./g, '');
	cnpj = cnpj.replace("/",'');
	cnpj = cnpj.replace("-", '');
	
	cnpj_primeiro_digito_verificador = cnpj.charAt(cnpj.length - 2);
	cnpj_segundo_digito_verificador = cnpj.charAt(cnpj.length - 1);
	
	cnpj_corpo_primeira_validacao = cnpj.substr(0, cnpj.length - 2);
	cnpj_corpo_segunda_validacao = cnpj.substr(0, cnpj.length - 1);
	
	return (verificarCnpjVazio( cnpj )
			|| ( verificarStringCnpj( cnpj ) 
			&& validarPrimeiroDigito(cnpj_corpo_primeira_validacao, cnpj_primeiro_digito_verificador) 
			&& validarSegundoDigito(cnpj_corpo_segunda_validacao, cnpj_segundo_digito_verificador)) );
	
}

function verificarCnpjVazio( cnpj ){
	
	if( cnpj == "" )	return true;
	
	return false;
	
}

function verificarStringCnpj( cnpj ){
	
	var status = cnpj.match(/^[0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[\-]?[0-9]{2}$/g);
	
	if( status == null ) return false;
		
	return true;
	
}

function validarPrimeiroDigito( cnpj_corpo, cnpj_primeiro_digito_verificador ){
	
	var primeiro_digito_gerado = 0;
	
	LISTA_MULTIPLICACOES = [];
	
	for( var contador = 0; contador < cnpj_corpo.length; contador++ ){
		
		LISTA_MULTIPLICACOES.push( parseInt( cnpj_corpo.charAt(contador) ) * VALORES_VALIDACAO_PRIMEIRO_DIGITO_VERIFICADOR[contador] );
		
	}
	
	primeiro_digito_gerado = somarValoresCnpj() % QUANTIDADE_DIGITOS_CORPO_CNPJ;
	
	if( primeiro_digito_gerado < 2 ){
		
		primeiro_digito_gerado = 0;
		
	}else{
	
		primeiro_digito_gerado = QUANTIDADE_DIGITOS_CORPO_CNPJ - primeiro_digito_gerado;
		
	}
	
	if( cnpj_primeiro_digito_verificador != primeiro_digito_gerado )	return false;
	
	return true;
	
}

function validarSegundoDigito( cnpj_corpo, cnpj_segundo_digito_verificador ){
	
	var segundo_digito_gerado = 0;
	
	LISTA_MULTIPLICACOES = [];
	
	for( var contador = 0; contador < cnpj_corpo.length; contador++ ){
		
		LISTA_MULTIPLICACOES.push( parseInt( cnpj_corpo.charAt(contador) ) * VALORES_VALIDACAO_SEGUNDO_DIGITO_VERIFICADOR[contador] );
		
	}
	
	segundo_digito_gerado = somarValoresCnpj() % QUANTIDADE_DIGITOS_CORPO_CNPJ;
	
	if( segundo_digito_gerado < 2 ){
		
		segundo_digito_gerado = 0;
		
	}else{
	
		segundo_digito_gerado = QUANTIDADE_DIGITOS_CORPO_CNPJ - segundo_digito_gerado;
	
	}
	
	if( cnpj_segundo_digito_verificador != segundo_digito_gerado )	return false;
	
	return true;
	
}

function somarValoresCnpj(  ){
	
	var soma = 0;
	
	for( var contador = 0; contador < LISTA_MULTIPLICACOES.length; contador++ ){
		
		soma += LISTA_MULTIPLICACOES[contador];

	}
	
	return soma;
	
}
