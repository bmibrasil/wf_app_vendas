

var cnpj = 0;
var cep = 0;
var logradouro = 0;
var numero = 0;
var bairro = 0;
var cidade = 0;
var uf = 0;
var segmento = 0;
var id = 0;
var lancamentoSolicitacaoOrganizacao="0";
var vendedor="0";
var unidade="0";


function admin_init(){
	vendedor=sessionStorage.getItem("vendedor");
	unidade=sessionStorage.getItem("unidade");

	lancamentoSolicitacaoOrganizacao=sessionStorage.getItem("lancamentoSolicitacaoOrganizacao");
	document.getElementById("btn_salvar").addEventListener("click", btn_salvar_click);
	document.getElementById("solicitacaoorganizacao_cnpj").addEventListener("keypress", formatarCNPJ);
	//document.getElementById("solicitacaoorganizacao_cnpj").addEventListener("blur", validarCNPJ);
	document.getElementById("solicitacaoorganizacao_cep").addEventListener("keypress", formatarCEP);
	document.getElementById("menuop1").addEventListener("click", carregaLancamentosSolicitacaoOrganizacao);
	document.getElementById("menuop2").addEventListener("click", carregaMenuVendas);
	document.getElementById("menuop3").addEventListener("click", sair);
	var data =new Date();
	var dataFormatada = formataData(data);
	document.getElementById("solicitacaoorganizacao_dataSolicitacaoOrganizacao").value=dataFormatada;
	if (lancamentoSolicitacaoOrganizacao!="0")
	{
		buscarSolicitacaoOrganizacao();
	}
}

//TRATAMENTO DOS EVENTOS

//verifica se foi escolhido valor na combo diferente do default
function verificaValorSelecionadoCombo(str) {
	var selectObj  = document.getElementById(str);
	var valueToSet  = document.getElementById(str).value;
	if (selectObj.options[0].value==valueToSet) {
		return false;
	}
	else{
		return true;
	}

}

//verifica se o campo esta vazio
function verificaPreeenchimento(str) {
	var v = document.getElementById(str).value;
	if ((v == null) || (v == "") || ( v == '0' ))
		return false
		else	
			return true;
}

function formatarCEP()
{
	var teclapres=event;
	var campo=document.getElementById("solicitacaoorganizacao_cep");
	var tecla = teclapres.keyCode;
	var vr = new String(campo.value);
	vr = vr.replace(".", "");
	vr = vr.replace("/", "");
	vr = vr.replace("-", "");
	tam = vr.length + 1;
	if (tecla != 14)
	{
		if (tam == 6)
			campo.value = vr.substr(0, 6) + '-';
		if (tam == 10)
			campo.value = vr.substr(0, 6) + '-' + vr.substr(6, 8);
		if (tam > 10)
			campo.value = vr.substr(0, 6) + '-' + vr.substr(6, 8);
	}
}

function formatarCNPJ(){
	
	var teclapres=event;
	var campo=document.getElementById("solicitacaoorganizacao_cnpj");
	var tecla = teclapres.keyCode;
	var vr = new String(campo.value);
	vr = vr.replace("\.", "");
	vr = vr.replace("/", "");
	vr = vr.replace("-", "");
	tam = vr.length + 1;
	if (tecla != 14)
	{
		if (tam == 3)
			campo.value = vr.substr(0, 2) + '.';
		if (tam == 6)
			campo.value = vr.substr(0, 2) + '.' + vr.substr(2, 5) + '.';
		if (tam == 10)
			campo.value = vr.substr(0, 2) + '.' + vr.substr(2, 3) + '.' + vr.substr(6, 3) + '/';
		if (tam == 14)
			campo.value = vr.substr(0, 2) + '.' + vr.substr(2, 3) + '.' + vr.substr(6, 3) + '/' + vr.substr(9, 4) + '-' + vr.substr(13, 1);
		if (tam > 14)
			campo.value = vr.substr(0, 2) + '.' + vr.substr(2, 3) + '.' + vr.substr(6, 3) + '/' + vr.substr(9, 4) + '-' + vr.substr(13, 1);
	}
}

function validaCampoCnpj(){
	
	if( ( validarCNPJ() ) == true ){
		
		return true;
		
	}else{
		
		alert('Campo CNPJ invalido, por favor verifique');
		return false;
		
	}
	
}

function validaCadastro() {
	if ((verificaPreeenchimento("solicitacaoorganizacao_nome")
			&& verificaPreeenchimento("solicitacaoorganizacao_cep")
			&& verificaPreeenchimento("solicitacaoorganizacao_logradouro")
			&& verificaPreeenchimento("solicitacaoorganizacao_bairro")
			&& verificaPreeenchimento("solicitacaoorganizacao_cidade")
			&& verificaPreeenchimento("solicitacaoorganizacao_uf")
			&& verificaValorSelecionadoCombo("solicitacaoorganizacao_segmento")
	)==true)
	{
		return  true;
	}
	else
	{
		alert('Existem campos que não foram preenchidos, Por favor preencha');
		return false;
	}

}

function btn_salvar_click(){

	if (validaCadastro() == true && validaCampoCnpj() == true )
		
	{
		document.getElementById("btn_salvar").removeEventListener("click", btn_salvar_click);
		salvarSolicitacaoOrganizacao();
	}

}


function salvarSolicitacaoOrganizacao(){

	var dataSolicitacaoOrganizacao = document.getElementById("solicitacaoorganizacao_dataSolicitacaoOrganizacao").value; 
	var nome = document.getElementById("solicitacaoorganizacao_nome").value; 
	var cnpj = document.getElementById("solicitacaoorganizacao_cnpj").value;
	var cep = document.getElementById("solicitacaoorganizacao_cep").value;
	var logradouro = document.getElementById("solicitacaoorganizacao_logradouro").value;
	var bairro = document.getElementById("solicitacaoorganizacao_bairro").value;
	var cidade = document.getElementById("solicitacaoorganizacao_cidade").value;
	var uf = document.getElementById("solicitacaoorganizacao_uf").value;
	var segmento =document.getElementById('solicitacaoorganizacao_segmento').options[document.getElementById('solicitacaoorganizacao_segmento').selectedIndex].innerText;
	cnpj = cnpj.replace(".", "");
	cnpj = cnpj.replace("/", "");
	cnpj = cnpj.replace("-", "");
	cep = cep.replace(".", "");
	cep = cep.replace("/", "");
	cep = cep.replace("-", "");
	var observacao="";
	var status='pendente';

	if (lancamentoSolicitacaoOrganizacao !="0")
	{
		id=lancamentoSolicitacaoOrganizacao;

	}

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "salvarSolicitacaoOrganizacao",
			id: id,
			nome: nome,
			cnpj: cnpj,
			cep: cep,
			logradouro: logradouro,
			bairro:bairro,
			cidade: cidade,
			uf: uf,
			segmento: segmento,
			statusSolicitacaoOrganizacao:status,
			observacao: observacao,
			dataSolicitacaoOrganizacao:dataSolicitacaoOrganizacao,
			proprietario: vendedor,
			unidade: unidade
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}

function buscarSolicitacaoOrganizacao()
{
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarSolicitacaoOrganizacao",
			id: lancamentoSolicitacaoOrganizacao

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   	
}





function editaLancamento()
{
	var cargo="";

	achei=0;
	j=0;


	var data = construirData(listaLancamentos[0].dataSolicitacaoOrganizacao, "00:00:00");
	var dataFormatada= formataData(data);
	document.getElementById('solicitacaoorganizacao_dataSolicitacaoOrganizacao').value=dataFormatada;
	document.getElementById('solicitacaoorganizacao_dataSolicitacaoOrganizacao').innerHTML="";
	document.getElementById('solicitacaoorganizacao_nome').value=listaLancamentos[0].nome;
	document.getElementById('solicitacaoorganizacao_nome').innerHTML="";
	document.getElementById('solicitacaoorganizacao_cnpj').value=listaLancamentos[0].cnpj;
	document.getElementById('solicitacaoorganizacao_cnpj').innerHTML="";
	document.getElementById('solicitacaoorganizacao_cep').value=listaLancamentos[0].cep;
	document.getElementById('solicitacaoorganizacao_cep').innerHTML="";
	document.getElementById('solicitacaoorganizacao_logradouro').value=listaLancamentos[0].logradouro;
	document.getElementById('solicitacaoorganizacao_logradouro').innerHTML="";
	document.getElementById('solicitacaoorganizacao_bairro').value=listaLancamentos[0].bairro;
	document.getElementById('solicitacaoorganizacao_bairro').innerHTML="";
	document.getElementById('solicitacaoorganizacao_cidade').value=listaLancamentos[0].cidade;
	document.getElementById('solicitacaoorganizacao_cidade').innerHTML="";
	document.getElementById('solicitacaoorganizacao_uf').value=listaLancamentos[0].uf;
	document.getElementById('solicitacaoorganizacao_uf').innerHTML="";
	selecionarSelectpeloOption("solicitacaoorganizacao_segmento",listaLancamentos[0].segmento);



}

function carregaLancamentosSolicitacaoOrganizacao()
{
	{
		sessionStorage.setItem("vendedor",vendedor);
		sessionStorage.setItem("unidade",unidade);
		sessionStorage.setItem("lancamentoSolicitacaoOrganizacao","0");
		window.document.location.href = '../lancamentosSolicitacaoOrganizacao/lancamentosSolicitacaoOrganizacao.html';

	}

}


function carregaMenuVendas()
{
	{
		sessionStorage.setItem("vendedor",vendedor);
		sessionStorage.setItem("unidade",unidade);
		sessionStorage.setItem("lancamentoSolicitacaoOrganizacao","0");
		window.document.location.href = '../menuVendas/menuVendas.html';

	}

}

function sair()
{
	sessionStorage.setItem("vendedor","0");
	sessionStorage.setItem("unidade",0);
	sessionStorage.setItem("lancamentoSolicitacaoOrganizacao","0");
	window.document.location.href = '../login/login.html';

}


function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value==valueToSet) {
			selectObj.options[i].selected = true;
			return;
		}
	}
}

function selecionarSelectpeloOption(elementId, valor) {
	var elt = document.getElementById(elementId);
	var opt = elt.getElementsByTagName("option");
	for(var i = 0; i < opt.length; i++) {
		if(opt[i].text == valor) {
			var campo=document.getElementById(elementId);
			setSelectedValue(campo,i);
		}
	}

}

function callback(data){


	if(data.action=="salvarSolicitacaoOrganizacao"){
		alert("Solicitacao de Organização realizada!!");
		document.getElementById("btn_salvar").addEventListener("click", btn_salvar_click);
		location.reload();
	}


	if(data.action=="buscarSolicitacaoOrganizacao"){
		listaLancamentos=jQuery.parseJSON(data.retorno);
		editaLancamento();	
	}
}



