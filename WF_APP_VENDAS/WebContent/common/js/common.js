function openNav() {
	
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    
}

function closeNav() {
	
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    
}

function formataData(dataFormartar){
	
	var retorno = "";

	if( isNaN(dataFormartar) ){
		
		retorno = "  -  -    ";
		
	}else{
		
		var dd = dataFormartar.getDate();
		var mm = dataFormartar.getMonth()+1; 
		var yyyy = dataFormartar.getFullYear();
		
		if(dd<10) 
		{
			dd='0'+dd;
		} 

		if(mm<10) 
		{
			mm='0'+mm;
		} 	
		
		retorno = yyyy + '-' + mm + '-' + dd;
		
	}
	
	return	retorno;
}

function formataDataPt(dataFormartar){
	
	var retorno = "";
	
	if( isNaN(dataFormartar) ){
		
		retorno = "  -  -    ";
		
	}else{
		
		var dd = dataFormartar.getDate();
		var mm = dataFormartar.getMonth()+1; 
		var yyyy = dataFormartar.getFullYear();
		
		if(dd<10) 
		{
			dd='0'+dd;
		} 

		if(mm<10) 
		{
			mm='0'+mm;
		} 
		
		retorno = dd + '-' + mm + '-' + yyyy;
		
	}	

	return	retorno;
}

function formataDataPtBarra(dataFormartar){
		
	var retorno = "";
		
	if( isNaN(dataFormartar) ){
		
		retorno = "  -  -    ";
		
	}else{
		
		var dd = dataFormartar.getDate();
		var mm = dataFormartar.getMonth()+1; 
		var yyyy = dataFormartar.getFullYear();
		
		if(dd<10) 
		{
			dd='0'+dd;
		} 

		if(mm<10) 
		{
			mm='0'+mm;
		} 
		
		retorno = dd + '/' + mm + '/' + yyyy;
		
	}

	return	retorno;
}

function construirData(data, time){
	
	lista = data.split('-');
	
	var dia = lista[2];
	var mes = lista[1];
	var ano = lista[0];
	var fusoHorario = "GMT-0300";
	
	var dataString = mes + " " + dia + " " + ano + " " + time + " " + fusoHorario;
	
	return new Date(dataString);
		
}

function obterHorario( data ){
	
	var data = new Date(data);
	
	var horas = data.getHours();
	var minutos = data.getMinutes();
	
	if( horas < 10 ){
		horas = '0' + horas;
	}
	if( minutos < 10 ){
		minutos = '0' + minutos;
	}
	
	return horas + ":" + minutos;
	
}

function calcularIntervaloTempo( horaInicio, horaFim ){
	
	var inicio = horaInicio.split(":");
	var fim = horaFim.split(":");
	
	var horasI = inicio[0];
	var minutosI = inicio[1];
	
	var horasF = fim[0];
	var minutosF = fim[1];
	
	var quantidadeMinutos = ((horasF - horasI) * 60) + (minutosF - minutosI);
	
	return quantidadeMinutos;
	
}
