function retornaServer(){ 
	
	//return 'http://54.233.171.120/rest_capvase/srv_capvase/ws_capvase/';
	//return 'http://localhost:80/WF_APP_VENDAS/rest_vendas/srv_vendas/ws_vendas/';
	return 'http://vendas.whitefoxapp.com.br/WF_APP_VENDAS/rest_vendas/srv_vendas/ws_vendas/';
	
}

function retornaServerPost(){
	
	//return 'http://54.233.171.120/rest_capvase/srv_capvase/ws_capvase/';
	//return 'http://localhost:80/WF_APP_VENDAS/rest_vendas/srv_vendas/ws_vendas_post';
	return 'http://vendas.whitefoxapp.com.br/WF_APP_VENDAS/rest_vendas/srv_vendas/ws_vendas_post';
	
}


function retornaCaminhoArquivo(){ 
	
	//return 'http://18.228.20.169:80/WF_APP_VENDAS/arquivosReembolso/';
	return 'http://10.10.4.193/:80/WF_APP_VENDAS/arquivosReembolso/';
 
}

function callWebServicePost(dados, callbackMethod ){
	
    $.ajax({
    	
    	method: "POST",
        url: retornaServerPost(),
        contentType: "application/json; charset=utf-8",
        data: dados,
        crossDomain: true,
        processData: false,
        dataType: "json",
        async: false,
    	success:function(result){
             callback(result);
         },
         error:function(xhr, status,error){
            alert("ERRO: " + status + ": " + error);
        }
    }); 
}


function callWebserviceMethod(url,callbackMethod){
	
    url = url + "?" + callbackMethod + "=?";
    
    $.ajax({
    	
        url: url,
        crossDomain: true,
        method: "GET",
        contentType: "application/json; charset=utf-8",
        async: false,
        dataType: 'jsonp',
        jsonp: callbackMethod 
    }); 
}


function callWebservice(url){
	
    callWebserviceMethod(url,"callback");
    
}

function blockBackButton(){
	
	history.pushState(null, document.title, location.href);
	
	window.addEventListener('popstate', function (event){
		
		history.pushState(null, document.title, location.href);
	  
	});
}
