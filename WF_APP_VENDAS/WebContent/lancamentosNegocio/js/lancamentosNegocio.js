//Variáveis da sessão
var listaOrganizacoes = [];
var listaPessoas = [];
var listaLancamentosNegocios = [];
var observer;
var config;
var target;
var periodo;
var indiceColaborador;
var proprietario = 0;
var vendedor = 0;
var unidade = 0;
var lancamentoNegocio = 0;
;

var tabelaLancamentos;
var listaLancamentosNegocios = [];


function admin_init(){
	// cadastro = sessionStorage.getItem("cadastro");
	//  nome=sessionStorage.getItem("nomeColaborador");

	$( "#dialog" ).hide();
	
	vendedor=sessionStorage.getItem("vendedor");
	unidade=sessionStorage.getItem("unidade");
	lancamentoNegocio=sessionStorage.getItem("lancamentoNegocio");
	if ((vendedor==0) || (vendedor==null) || (vendedor==undefined) ) 
	{
		window.document.location.href = '../../login/login.html';
	}

	else
	{	
		setSelectedValue(document.getElementById("lancamentosNegocio_status"),0);
		document.getElementById("lancamentosNegocio_status").addEventListener("change", mudaStatusNegocio);
		document.getElementById("lancamentosNegocio_temperatura").addEventListener("change", mudaStatusNegocio);
		tabelaLancamentos = document.getElementById("lancamentosNegocio_lancamentos");
		//buscaOrganizacaocomPessoa();
		buscarOrganizacoes();
		document.getElementById("menuop1").addEventListener("click", inserirNegocio);
		document.getElementById("menuop2").addEventListener("click", carregarMenuVendas);
		document.getElementById("menuop3").addEventListener("click", sair);

	}



}



function isMobile()
{
	var userAgent = navigator.userAgent.toLowerCase();
	if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
		return true;
	else
		return false;
}


function btn_salvar_click(){
	salvarNegocio();
}

function buscaOrganizacaocomPessoa(){

	var proprietario = vendedor;

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscaOrganizacaocomPessoa",
			proprietario: proprietario,


	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}

function buscarOrganizacoes(){
	
	var proprietario = vendedor;

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarOrganizacoes",
			unidade:unidade
		

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}



function buscarPessoasNegociosProprietario(){

	var proprietario = vendedor;
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarPessoasNegociosProprietario",
			proprietario: proprietario,
			unidade : unidade



	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}


function buscarTodasPessoasOrganizacoes(){

	var proprietario = vendedor;
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarTodasPessoasOrganizacoes",
			proprietario: proprietario,
			unidade : unidade



	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}


function buscarLancamentosNegocioStatus(statusNegocio,temperatura){

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarLancamentosNegocioStatus", 
			proprietario:vendedor,
			statusNegocio:statusNegocio,
			unidade: unidade,
			temperatura:temperatura

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");

}




function editarLancamento(lancamentoId)
{
	var lancamento=listaLancamentosNegocios[lancamentoId].id;
	var achei=0;
	var i=0;
	while ( achei==0)
	{
		if (listaLancamentosNegocios[i].id==lancamento)
		{
			achei=1;	  
		}
		i=i+1;
	}
	sessionStorage.setItem("lancamentoNegocio",listaLancamentosNegocios[i-1].id);
	sessionStorage.setItem("vendedor",vendedor);
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../negocio/negocio.html';

}

function removerLancamento(lancamentoId){

	var lancamento=listaLancamentosNegocios[lancamentoId].id;
	var r=confirm("Você deseja realmente excluir o lançamento?");
	if (r==true)
	{
		var url = retornaServer();
		var data = {
				page: "vendas",
				action: "removerLancamentoNegocio",
				lancamento: lancamento

		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");  
	}

}

function atualizarFaseNegocio(lancamentoId,faseid){
var fase;

	
	
	if (faseid==1)
		{
		  fase='Perdido';
		}
	
	var lancamento=listaLancamentosNegocios[lancamentoId].id;
	$( "#dialog" ).show();
	$( "#dialog" ).dialog();
	document.getElementById("targetSim").addEventListener("click", function(){inserirMotivoPerda(lancamento, fase)},false);
	//$( "#dialog" ).resizable=true;

}



function cancelarMotivoPerda()
{
	$( "#dialog" ).hide();	
}


function inserirMotivoPerda(lancamento, fase)
{
	var motivoPerda=document.getElementById("lancamentosNegocio_motivoPerda").value;
	{
		var url = retornaServer();
		var data = {
				page: "vendas",
				action: "atualizarFaseNegocio",
				lancamento: lancamento,
				fase: fase,
				motivoPerda:motivoPerda

		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");  
	}

}



//PROCESSAMENTO DO RETORNO DOS WEBSERVICES

function callback(data){


	if(data.action=="buscarTodasPessoasOrganizacoes"){
		listaPessoas=jQuery.parseJSON(data.retorno);
		buscarLancamentosNegocioStatus("BQ","");
	}	
	
	if(data.action=="buscarPessoasNegociosProprietario"){
		listaPessoas=jQuery.parseJSON(data.retorno);
		buscarLancamentosNegocioStatus("BQ","");
	}	
	
	
	if(data.action=="buscaOrganizacaocomPessoa"){
		listaOrganizacoes=jQuery.parseJSON(data.retorno);
		//buscarTodasPessoasOrganizacoes();
		buscarPessoasNegociosProprietario();
	}
	
	if(data.action=="buscarOrganizacoes"){
		listaOrganizacoes=jQuery.parseJSON(data.retorno);
		//buscarTodasPessoasOrganizacoes();
		buscarPessoasNegociosProprietario();
	}

	if(data.action=="buscarLancamentosNegocioStatus"){
		listaLancamentosNegocios=jQuery.parseJSON(data.retorno);
		preencheTabelaLancamentosNegocio();

	}
	
	if(data.action=="atualizarFaseNegocio"){
		listaLancamentosNegocios=jQuery.parseJSON(data.retorno);
		$( "#dialog" ).hide();
		location.reload();

	}

	if(data.action=="removerLancamentoNegocio"){
		location.reload();
	}


}




function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value==valueToSet) {
			selectObj.options[i].selected = true;

		}
	}
	return i;
}


function mudaStatusNegocio() {
	tabelaLancamentos.innerHTML = '';
	//status = document.getElementById("lancamentosNegocio_status").innerHTML;
	status =document.getElementById('lancamentosNegocio_status').options[document.getElementById('lancamentosNegocio_status').selectedIndex].innerText;
	temperatura =document.getElementById('lancamentosNegocio_temperatura').options[document.getElementById('lancamentosNegocio_temperatura').selectedIndex].innerText;
	buscarLancamentosNegocioStatus(status,temperatura);

}






function inserirNegocio()
{
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("lancamentoNegocio","0");
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../negocio/negocio.html';

}

function carregarMenuVendas()
{
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("lancamentoNegocio","0");
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../menuVendas/menuVendas.html';

}

function sair()
{
	localStorage.clear();
	sessionStorage.setItem("vendedor","0");
	sessionStorage.setItem("lancamentoNegocio","0");
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../login/login.html';

}




//TRATAMENTO DOS EVENTOS

function preencheTabelaLancamentosNegocio(){
	var html = '';
	var status='';
	var indiceOrganizacao;
	var indicePessoa;
	var j=0;
	var totalLancamentos=0;

	var achei=0;

	var dataFormatada;
	var dataFormatadaEntrega;
	var dataFormatadaFechamento;
	var data ;
	if(listaLancamentosNegocios.length > 0)
	{
		for(i = 0; i < listaLancamentosNegocios.length; i++)
		{

			if(listaLancamentosNegocios[i].organizacao!='')
			{
				achei=0;
				j=0;
				while (achei ==0)
				{
					if (listaLancamentosNegocios[i].organizacao== listaOrganizacoes[j].id)
					{achei=1;
					indiceOrganizacao=j;
					}
					j=j+1;
				}
			}
			if(listaLancamentosNegocios[i].pessoa!='')
			{
				achei=0;
				j=0;
				while (achei ==0)
				{
					if (listaLancamentosNegocios[i].pessoa== listaPessoas[j].id)
					{achei=1;
					indicePessoa=j;
					}
					j=j+1;
				}
			}
			else 
			{
				listaLancamentosNegocios[i].pessoa="SEM PESSOA";
			    indicePessoa=i;
			}
			
			totalLancamentos +=parseFloat(listaLancamentosNegocios[i].potencial);
			
			data = construirData(listaLancamentosNegocios[i].dataAbertura, "00:00:00");
			dataFormatada= formataDataPt(data);
			
			data = construirData(listaLancamentosNegocios[i].dataPrevistaEntrega, "00:00:00");
			dataFormatadaEntrega= formataDataPt(data);
			
			data = construirData(listaLancamentosNegocios[i].dataPrevistaFechamento, "00:00:00");
			dataFormatadaFechamento= formataDataPt(data);
			//totalLancamentos=somaHora(totalLancamentos,listaLancamentosNegocios[i].horasTrabalhadas,false);


			if (isMobile() ==false)
			{

				if ((listaLancamentosNegocios[i].statusNegocio=='BQ') ||(listaLancamentosNegocios[i].statusNegocio=='BM'))
				{

					html+='<div class="item" id="div'+listaLancamentosNegocios[i].id+'">'
					+'<div><b>'
					+dataFormatada 
					+"<br>"+listaLancamentosNegocios[i].numeracao
					+"<br>"+listaLancamentosNegocios[i].nome
					+"<br>"+listaOrganizacoes[indiceOrganizacao].nome;
					if (listaLancamentosNegocios[i].pessoa=='SEM PESSOA')
					{html+="<br>"+listaLancamentosNegocios[i].pessoa;
					}
					else
					{html+="<br>"+listaPessoas[indicePessoa].nome;}
					
					html+="<font color=#515caf> <br>Valor Potencial: "+currency(listaLancamentosNegocios[i].potencial)
					+"<font color=#515caf> <br>Temperatura: "+listaLancamentosNegocios[i].temperatura
					+" </font><br> Escopo: </b>"+listaLancamentosNegocios[i].escopo +"<br>"
					+"<b> Prev. de Inicio: </b>"+dataFormatadaEntrega + " <b>Prev. de Fechamento: </b>"+dataFormatadaFechamento
					+'</div>'
					+'<button class="btnedit" onclick="editarLancamento('+i+')"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:24px"></i></button>'
			        +'<button class="btndelete" onclick="atualizarFaseNegocio('+i+',1)"><i class="fa fa-thumbs-down" aria-hidden="true" style="font-size:24px"></i></button>'
					+'</div>';
				}
				else
				{
				 if ((listaLancamentosNegocios[i].statusNegocio=='BP') ||(listaLancamentosNegocios[i].statusNegocio=='BA'))
				{
					html+='<div class="item" id="div'+listaLancamentosNegocios[i].id+'">'
					+'<div><b>'
					+dataFormatada
					+"<br>"+listaLancamentosNegocios[i].numeracao
					+"<br>"+listaLancamentosNegocios[i].nome
					+"<br>"+listaOrganizacoes[indiceOrganizacao].nome
					+"<br>"+listaPessoas[indicePessoa].nome
					+"<font color=#515caf> <br>Temperatura: "+listaLancamentosNegocios[i].temperatura
					+"<font color=#515caf> <br>Valor Potencial: "+currency(listaLancamentosNegocios[i].potencial)+" </font><br> Escopo: </b>"+listaLancamentosNegocios[i].escopo +"<br>"
					+"<b> Prev. de Inicio: </b>"+dataFormatadaEntrega + "<b> Prev. de Fechamento: </b>"+dataFormatadaFechamento
					+'</div>'
					+'<button class="btnedit" onclick="alertaNegocio()"><i class="fa fa-lock" aria-hidden="true" style="font-size:24px"></i></button>'
				//	+'<button class="btndelete" onclick="alertaNegocio()"><i class="fa fa-lock" aria-hidden="true" style="font-size:24px"></i></button>'
					+'</div>'

				 }
				 else
					 {
					 
						html+='<div class="item" id="div'+listaLancamentosNegocios[i].id+'">'
						+'<div><b>'
						+dataFormatada
						+"<br>"+listaLancamentosNegocios[i].numeracao
						+"<br>"+listaLancamentosNegocios[i].nome
						+"<br>"+listaOrganizacoes[indiceOrganizacao].nome
						+"<br>"+listaPessoas[indicePessoa].nome+ "<font color=#515caf> <br>Valor Potencial: "+currency(listaLancamentosNegocios[i].potencial)+
						+"<font color=#515caf> <br>Temperatura: "+listaLancamentosNegocios[i].temperatura
					    +"</font><br> Escopo: </b>"+listaLancamentosNegocios[i].escopo +"<br>"
						+"<b> Prev. de Inicio: </b>"+dataFormatadaEntrega + "<b> Prev. de Fechamento: </b>"+dataFormatadaFechamento
						+"<br><b> Observação do Administrativo: </b>"+listaLancamentosNegocios[i].obsAdm
						+'</div>'
						+'<button class="btnedit" onclick="alertaNegocio()"><i class="fa fa-lock" aria-hidden="true" style="font-size:24px"></i></button>'
					//	+'<button class="btndelete" onclick="alertaNegocio()"><i class="fa fa-lock" aria-hidden="true" style="font-size:24px"></i></button>'
						+'</div>'

					 
					 }
				}
			}
			else
			{
				if ((listaLancamentosNegocios[i].statusNegocio=='BQ') ||(listaLancamentosNegocios[i].statusNegocio=='BM'))
				{
					html+='<div class="item" id="div'+listaLancamentosNegocios[i].id+'">'
					+'<div><b>'
					+dataFormatada 
					+"<br>"+listaLancamentosNegocios[i].nome
					+"<br>"+listaOrganizacoes[indiceOrganizacao].nome
					+"<br>"+listaPessoas[indicePessoa].nome+ "<font color=#515caf> <br>Valor Potencial: "+currency(listaLancamentosNegocios[i].potencial)
					+"<font color=#515caf> <br>Temperatura: "+listaLancamentosNegocios[i].temperatura
					+" </font><br> Escopo: "+listaLancamentosNegocios[i].escopo +"</b><br>"
					+'</div>'
					+'<button class="btnedit" onclick="editarLancamento('+listaLancamentosNegocios[i].id+')"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:24px"></i></button>'
				//	+'<button class="btndelete" onclick="atualizarFaseNegocio('+listaLancamentosNegocios[i].id+',1)"><i class="fa fa-thumbs-down" aria-hidden="true" style="font-size:24px"></i></button>'
					+'</div>';
				}
				else
				{

					html+='<div class="item" id="div'+listaLancamentosNegocios[i].id+'">'
					+'<div><b>'
					+dataFormatada 
					+"<br>"+listaLancamentosNegocios[i].nome
					+"<br>"+listaOrganizacoes[indiceOrganizacao].nome
					+"<br>"+listaPessoas[indicePessoa].nome+ "<font color=#515caf> <br>Valor Potencial: "+currency(listaLancamentosNegocios[i].potencial)
					+"<font color=#515caf> <br>Temperatura: "+listaLancamentosNegocios[i].temperatura
					+" </font><br> Escopo: "+listaLancamentosNegocios[i].escopo +"</b><br>"
					+'</div>'
					+'<button class="btnedit" onclick="alertaNegocio()"><i class="fa fa-lock" aria-hidden="true" style="font-size:24px"></i></button>'
					//+'<button class="btndelete" onclick="alertaNegocio()"><i class="fa fa-lock" aria-hidden="true" style="font-size:24px"></i></button>'
					+'</div>';

				}
			}        	   


		}

		tabelaLancamentos.innerHTML = html;
		//checkButton();
	}else{
		tabelaLancamentos.innerHTML = html;
	}
	document.getElementById("lancamentosNegocio_totalNegocios").innerHTML="<center><H1>"+ currency(totalLancamentos)+"</H1> Total de Negócios </center>";

}

function currency(numero){
	var formatado  = formatMoney(numero, 2, "R$ ", ".", ",");
    return formatado;
}

function formatMoney(number, places, symbol, thousand, decimal) {
	places = !isNaN(places = Math.abs(places)) ? places : 2;
	symbol = symbol !== undefined ? symbol : "$";
	thousand = thousand || ",";
	decimal = decimal || ".";
	var negative = number < 0 ? "-" : "",
	    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
}

function alertaNegocio()
{
	alert ("Este negocio não pode ser alterado pois já tem proposta!");	
}





target = document.querySelectorAll('.item-list')[0];



//cria uma nova instância de observador
observer = new MutationObserver(function(mutations) {
	mutations.forEach(function(mutation) {
		//console.log("tipo de mutação" +mutation.type);

		$(function() {$('.example-1').listSwipe();});

	});    
});

//configuração do observador:
config = { attributes: true, childList: true, characterData: true };

//passar o nó alvo pai, bem como as opções de observação
observer.observe(target, config);







