var tipoTurma=0;
var email="";
var senha="";
var listaUnidades=[];
var unidade="";

function admin_init()
{
	
	localStorage.clear();
	document.getElementById("login_btLogin").addEventListener("click",login_byLogin);
	buscarUnidadesNegocio();
	/*document.getElementById("login_btLogin").ontouch = login_byLogin;*/

};

function login_byLogin() {
	
	localStorage.clear();
	email = document.getElementById("email").value;
	senha = document.getElementById("senha").value;
	consultarColaborador();
}


function preencheListaUnidades(){
	var html="";
	var select ="";
	for (i=0;i<listaUnidades.length;i++){
		if(listaUnidades[i].nome=="B.Smart"){
			select = "Selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaUnidades[i].id+'" '+select+'>' +listaUnidades[i].nome+'</option>';
	}

	document.getElementById("login_unidade").innerHTML = html;
	
}


function buscarUnidadesNegocio()
{

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarUnidadesNegocio",
		

}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function consultarColaborador()
{
	var unidade =	document.getElementById("login_unidade").value;
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarColaborador",
			colaborador: 0,
			email: email,
			senha:senha,
			unidade:unidade

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}


function callback(data){
	var retorno;
	var acesso;
	var nome;

	if (data.action=="buscarColaborador")
	{
		
		retorno = jQuery.parseJSON(data.retorno);
		if (retorno==0)
		{
			sessionStorage.setItem("vendedor",data.salesForce);
			sessionStorage.setItem("unidade",data.unidade);
			nome = data.primeiroNome+" " + data.ultimoNome;
			sessionStorage.setItem("nomeVendedor",nome);
			//sessionStorage.setItem("bloqueio",data.bloqueado);
			location.href = "../menuVendas/menuVendas.html"; 
		}
		else
		 {
				alert ("Usuário,senha e/ou unidade de negócio incorretos!")
		 }


	}
	if (data.action=="buscarUnidadesNegocio")
	{
	   listaUnidades = jQuery.parseJSON(data.retorno);
	   preencheListaUnidades();
	
	}

}

