var nome = "";
var telefone = 0;
var email = 0;
var cargo = 0;
var vendedor="0";
var unidade="0";
var quantidadeDigitosTelefone = 10;
var quantidadeDigitosCelular = 11;


var lancamentoSolicitacaoPessoa=0;
var listaOrganizacoes = [];
var listaSolicitacoesOrganizacao=[];
var listaLancamentos = [];
var organizacao=0;


var id = 0;
var proprietario;


function admin_init(){

	document.getElementById("btn_salvar").addEventListener("click", btn_salvar_click);
	vendedor=sessionStorage.getItem("vendedor");
	unidade=sessionStorage.getItem("unidade");
	lancamentoSolicitacaoPessoa=sessionStorage.getItem("lancamentoSolicitacaoPessoa");
	document.getElementById("menuop1").addEventListener("click", carregaLancamentosSolicitacaoPessoa);
	document.getElementById("menuop2").addEventListener("click", carregaMenuVendas);
	document.getElementById("menuop3").addEventListener("click", sair);
	var data =new Date();
	var dataFormatada = formataData(data);
	document.getElementById("solicitacaopessoa_dataSolicitacaoPessoa").value=dataFormatada; 
	buscarOrganizacoes();


}

//TRATAMENTO DOS EVENTOS

//verifica se foi escolhido valor na combo diferente do default
function verificaValorSelecionadoCombo(str) {
	var selectObj  = document.getElementById(str);
	var valueToSet  = document.getElementById(str).value;
	if (selectObj.options[0].value==valueToSet) {
		return false;
	}
	else{
		return true;
	}

}

//verifica se o campo esta vazio
function verificaPreeenchimento(str) {
	var v = document.getElementById(str).value;
	if ((v == null) || (v == "") || ( v == '0' ))
		return false
		else	
			return true;
}

function verificarEmail( str ){
	
	var v = document.getElementById(str).value;
	
	var status = v.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
	
	if( status == null ) return false;
	
	return true;
	
}

function verificarNumeroTelefoneCelular( str ){
	
	var v = document.getElementById(str).value;

	var status = v.match(/^([0-9]{2}[ ]?[0-9]{4,5}[\-]?[0-9]{4}?)$/g);
	
	if( status == null ) return false;
		
	return true;
	
}


/*
A função Mascara tera como valores no argumento os dados inseridos no input (ou no evento onkeypress)
onkeypress="mascara(this, '## ####-####')"
onkeypress = chama uma função quando uma tecla é pressionada, no exemplo acima, chama a função mascara e define os valores do argumento na função
O primeiro valor é o this, é o Apontador/Indicador da Mascara, o '## ####-####' é o modelo / formato da mascara
no exemplo acima o # indica os números, e o - (hifen) o caracter que será inserido entre os números, ou seja, no exemplo acima o telefone ficara assim: 11-4000-3562
para o celular de são paulo o modelo deverá ser assim: '## #####-####' [11 98563-1254]
para o RG '##.###.###.# [40.123.456.7]
para o CPF '###.###.###.##' [789.456.123.10]
Ou seja esta mascara tem como objetivo inserir o hifen ou espaço automáticamente quando o usuário inserir o número do celular, cpf, rg, etc 

lembrando que o hifen ou qualquer outro caracter é contado tambem, como: 11-4561-6543 temos 10 números e 2 hifens, por isso o valor de maxlength será 12
<input type="text" name="telefone" onkeypress="mascara(this, '## ####-####')" maxlength="12">
neste código não é possivel inserir () ou [], apenas . (ponto), - (hifén) ou espaço
 */
function mascara(t, mask){
	var i = t.value.length;
	var saida = mask.substring(1,0);
	var texto = mask.substring(i)
	if (texto.substring(0,1) != saida){
		t.value += texto.substring(0,1);
	}
}

function validaEmail(){
	
	if( verificarEmail("solicitacaopessoa_email") == true ){
		
		return true;
		
	}else{
		
		alert('Campo email invalido, por favor verifique');
		return false;
		
	}
	
}

function validaTelefone(){
	
	if( (verificarNumeroTelefoneCelular("solicitacaopessoa_telefone") 
			|| verificarNumeroTelefoneCelular("solicitacaopessoa_celular")) == true ){
		
		return true;
		
	}else{
		
		alert('Campo telefone invalido, Por favor verifique');
		return false;
		
	}
	
}


function validaCadastro() {
	if ( (verificaPreeenchimento("solicitacaopessoa_nome")
			&& verificaValorSelecionadoCombo("solicitacaopessoa_cargo")
			&& verificaValorSelecionadoCombo("solicitacaopessoa_organizacao")
	)==true)
	{
		return  true;
	}
	else
	{
		alert('Existem campos que não foram preenchidos, Por favor preencha');
		return false;
	}

}

function btn_salvar_click(){
	if (validaCadastro() == true && validaTelefone() == true && validaEmail() == true)
	{
		document.getElementById("btn_salvar").removeEventListener("click", btn_salvar_click);
		salvarSolicitacacoPessoa();
	}
}


function preencheListaOrganizacao(){
	var html = '<option value="0">Organizacao...</option>';
	var select = "";


	for (i=0;i<listaOrganizacoes.length;i++){
		if(organizacao==i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaOrganizacoes[i].id+'" '+select+'>' +listaOrganizacoes[i].nome+'</option>';
	}

	document.getElementById("solicitacaopessoa_organizacao").innerHTML = html;
	setSelectedValue(document.getElementById("solicitacaopessoa_organizacao"),0);
}

function buscarOrganizacoes()
{
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarOrganizacoes",
			unidade:unidade

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   	
}


function buscarSolicitacaoPessoa()
{
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarSolicitacaoPessoa",
			id: lancamentoSolicitacaoPessoa

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   	
}



function salvarSolicitacacoPessoa(){

	var nome = document.getElementById("solicitacaopessoa_nome").value;
	var telefone = document.getElementById("solicitacaopessoa_telefone").value;
	var celular = document.getElementById("solicitacaopessoa_celular").value;
	var email = document.getElementById("solicitacaopessoa_email").value;
	cargo =document.getElementById('solicitacaopessoa_cargo').options[document.getElementById('solicitacaopessoa_cargo').selectedIndex].innerText;
	var organizacao  =document.getElementById('solicitacaopessoa_organizacao').options[document.getElementById('solicitacaopessoa_organizacao').selectedIndex].innerText;
	var dataSolicitacaoPessoa= document.getElementById("solicitacaopessoa_dataSolicitacaoPessoa").value; 
	origemPessoa =document.getElementById('solicitacaopessoa_origemPessoa').options[document.getElementById('solicitacaopessoa_origemPessoa').selectedIndex].innerText; 
	
	var observacao='';

	if (lancamentoSolicitacaoPessoa!="0")
	{
		id=lancamentoSolicitacaoPessoa;

	}
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "salvarSolicitacaoPessoa",
			id: id,
			nome: nome,
			organizacao:organizacao,
			telefone: telefone,
			celular: celular,
			email: email,
			cargo: cargo,
			proprietario: vendedor,
			unidade: unidade,
			observacao: observacao,
			dataSolicitacaoPessoa:dataSolicitacaoPessoa,
			origemPessoa: origemPessoa



	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}


function buscarSolicitacoesStatusTipo(tipoSolicitacao){


	status='pendente';
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarSolicitacoesStatusTipo", 
			proprietario:vendedor,
			unidade:unidade,
			status: status,
			tipoSolicitacao:tipoSolicitacao

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");

}


function carregaLancamentosSolicitacaoPessoa()
{
	{
		sessionStorage.setItem("vendedor",vendedor);
		sessionStorage.setItem("unidade",unidade);
		sessionStorage.setItem("lancamentoSolicitacaoPessoa","0");
		window.document.location.href = '../lancamentosSolicitacaoPessoa/lancamentosSolicitacaoPessoa.html';

	}

}

function carregaMenuVendas()
{
	{
		sessionStorage.setItem("vendedor",vendedor);
		sessionStorage.setItem("unidade",unidade);
		sessionStorage.setItem("lancamentoSolicitacaoPessoa","0");
		window.document.location.href = '../menuVendas/menuVendas.html';

	}

}

function sair()
{
	sessionStorage.setItem("vendedor","0");
	sessionStorage.setItem("unidade","0");
	sessionStorage.setItem("lancamentoSolicitacaoPessoa","0");
	window.document.location.href = '../login/login.html';

}


function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value==valueToSet) {
			selectObj.options[i].selected = true;
			return;
		}
	}
}

function selecionarSelectpeloOption(elementId, valor) {
	var elt = document.getElementById(elementId);
	var opt = elt.getElementsByTagName("option");
	for(var i = 0; i < opt.length; i++) {
		if(opt[i].text == valor) {
			var campo=document.getElementById(elementId);
			setSelectedValue(campo,i);
		}
	}

}

function editaLancamento()
{
	var cargo="";

	achei=0;
	j=0;

	var data = construirData(listaLancamentos[0].dataSolicitacaoPessoa, "00:00:00");
	var dataFormatada= formataData(data);
	document.getElementById('solicitacaopessoa_dataSolicitacaoPessoa').value=dataFormatada;
	document.getElementById('solicitacaopessoa_dataSolicitacaoPessoa').innerHTML="";
	document.getElementById('solicitacaopessoa_nome').value=decodeURIComponent(listaLancamentos[0].nome);
	document.getElementById('solicitacaopessoa_nome').innerHTML="";
	setSelectedValue(document.getElementById("solicitacaopessoa_organizacao"),decodeURIComponent(listaLancamentos[0].organizacao));
	document.getElementById('solicitacaopessoa_telefone').value=listaLancamentos[0].telefone;
	document.getElementById('solicitacaopessoa_telefone').innerHTML="";
	document.getElementById('solicitacaopessoa_celular').value=listaLancamentos[0].celular;
	document.getElementById('solicitacaopessoa_celular').innerHTML="";
	document.getElementById('solicitacaopessoa_email').value=listaLancamentos[0].email;
	document.getElementById('solicitacaopessoa_email').innerHTML="";
	selecionarSelectpeloOption("solicitacaopessoa_cargo",listaLancamentos[0].cargo);
	selecionarSelectpeloOption("solicitacaopessoa_origemPessoa",decodeURIComponent (listaLancamentos[0].origemPessoa));
}
function callback(data){


	if(data.action=="salvarSolicitacaoPessoa"){
		alert("Solicitacao de Pessoa realizada!!");
		document.getElementById("btn_salvar").addEventListener("click", btn_salvar_click);
		window.location.reload(false);
	}

	if(data.action=="buscarSolicitacoesStatusTipo"){
		var tamanho=listaOrganizacoes.length;
		if (data.retorno !="")
		{
			listaSolicitacoesOrganizacao = jQuery.parseJSON(data.retorno);
			for(i=0;i<listaSolicitacoesOrganizacao.length;i++)
			{
				listaSolicitacoesOrganizacao
				listaSolicitacoesOrganizacao[i].nome='(Em Solicitação)'+listaSolicitacoesOrganizacao[i].nome;
				listaOrganizacoes[tamanho]= listaSolicitacoesOrganizacao[i];
				tamanho=tamanho+1;

			}
		}
		preencheListaOrganizacao();

		if (lancamentoSolicitacaoPessoa!="0")
		{   
			buscarSolicitacaoPessoa();
		}

	}



	if(data.action=="buscarSolicitacaoPessoa"){
		listaLancamentos=jQuery.parseJSON(data.retorno);
		editaLancamento();	
	}


	if(data.action=="buscarOrganizacoes"){
		listaOrganizacoes=jQuery.parseJSON(data.retorno);
		buscarSolicitacoesStatusTipo(1);



	}
}

