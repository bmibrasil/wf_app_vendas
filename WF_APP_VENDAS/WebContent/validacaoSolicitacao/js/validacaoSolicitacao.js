//Variáveis da sessão
var listaOrganizacoes = [];
var listaPessoas = [];
var listaLancamentosSolicitacao = [];
var observer;
var config;
var target;
var periodo;
var vendedor = 0;
var cliente = 0;
var projeto = 0;
var etapa = 0;
var tipoHora = 0;
var nome="";
var status=0;
var permissoes='';
var tipoSolicitacao;
var indiceOrganizacao;
var indicePessoa;

var tabelaLancamentos;



function admin_init(){
	// cadastro = sessionStorage.getItem("cadastro");
	//  nome=sessionStorage.getItem("nomeColaborador");


	vendedor=sessionStorage.getItem("vendedor");
	permissoes=sessionStorage.getItem("permissoes");
	//||(permissoes.substring(2)==0)
	if ((vendedor==0) || (vendedor==null) || (vendedor==undefined)) 
	{
		window.document.location.href = '../login/login.html';
	}

	else
	{	
		setSelectedValue(document.getElementById("validacaosolicitacao_tipoSolicitacao"),0);
		setSelectedValue(document.getElementById("validacaosolicitacao_status"),0);
		document.getElementById("validacaosolicitacao_status").addEventListener("change", buscarSolicitacoesStatusTipo);
		document.getElementById("validacaosolicitacao_tipoSolicitacao").addEventListener("change", buscarSolicitacoesStatusTipo);

		tabelaLancamentos = document.getElementById("validacaosolicitacao_lancamentos");
		buscarOrganizacoescomSolicitacao();
		document.getElementById("menuop1").addEventListener("click", carregaSolicitacao);
		document.getElementById("menuop3").addEventListener("click", sair);
	}



}



function isMobile()
{
	var userAgent = navigator.userAgent.toLowerCase();
	if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
		return true;
	else
		return false;
}


function btn_salvar_click(){
	salvarSolicitacao();
}


function buscarOrganizacoescomSolicitacao(){

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarOrganizacoescomSolicitacao"
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");


}

function buscarPessoascomSolicitacao(){

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarPessoascomSolicitacao"
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");


}




function buscarPessoascomSolicitacao(){

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarPessoascomSolicitacao"
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");


}



function buscarSolicitacoesStatusTipo(){
	status= 	document.getElementById("validacaosolicitacao_status").value;
	tipoSolicitacao = 	document.getElementById("validacaosolicitacao_tipoSolicitacao").value;
	var url = retornaServer();
	var data = {
			page: "vendas",
			status: status,
			action: "buscarSolicitacoesStatusTipo",
			tipoSolicitacao: tipoSolicitacao
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");
}



function aprovarSolicitacao(lancamentoId)
{
	lancamento=listaLancamentosSolicitacao[lancamentoId].id;
	var valorAvaliacao=0;
	var r=prompt("Aprovar o solicitacao? Entre com a justificativa" , "ok");
	if (r!=null)
	{
		valorAvaliacao=1;
		avaliarLancamentoSolicitacao(lancamento, valorAvaliacao,r);	
	}

}


function reprovarSolicitacao(lancamentoId)
{
	lancamento=listaLancamentosSolicitacao[lancamentoId].id;
	var valorAvaliacao=0;
	var r=prompt("Reprovar o solicitacao? Entre com a justificativa","");
	if (r!=null)
	{
		valorAvaliacao=2;
		avaliarLancamentoSolicitacao(lancamento, valorAvaliacao,r);	
	}
}


function avaliarLancamentoSolicitacao(lancamentoId, valorAvaliacao,justificativa)
{
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "avaliarSolicitacao",
			solicitacao:lancamentoId,
			status: valorAvaliacao,
			justificativa:justificativa,
			tipoSolicitacao: tipoSolicitacao

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");  
}




function editarLancamento(lancamentoId)
{  lancamento=listaLancamentosSolicitacao[lancamentoId].id;
var achei=0;
var i=0;
while ( achei==0)
{
	if (listaLancamentosSolicitacao[i].id==lancamento)
	{
		achei=1;	  
	}
	i=i+1;
}

sessionStorage.setItem("vendedor",listaLancamentosSolicitacao[i-1].vendedor);
sessionStorage.setItem("solicitacao",listaLancamentosSolicitacao[i-1].id);
//mais tarde você pode parar de observar
observer.disconnect();
if (tipoSolicitacao==1)
{
	window.document.location.href = '../solicitacapessoa/solicitacaopessoa.html';
}

else

{	window.document.location.href = '../solicitacaoorganizacao/solicitacaoorganizacao.html';}
}

//PROCESSAMENTO DO RETORNO DOS WEBSERVICES

function callback(data){
	if(data.action=="buscarSolicitacoesStatusTipo"){
		listaLancamentosSolicitacao = jQuery.parseJSON(data.retorno);
		preencheTabelaLancamentosSolicitacao();
	}


	if(data.action=="buscarOrganizacoescomSolicitacao"){
		listaOrganizacoes = jQuery.parseJSON(data.retorno);
		buscarPessoascomSolicitacao();
	}

	if(data.action=="buscarPessoascomSolicitacao"){
		listaPessoas = jQuery.parseJSON(data.retorno);
		buscarSolicitacoesStatusTipo();
	}


	if(data.action=="avaliarSolicitacao"){
		location.reload();
	}

}

//function salvarSolicitacao(){
//var e =document.getElementById("solicitacao_cliente");
//var clienteid = e.options[e.selectedIndex].value;
//e=document.getElementById("solicitacao_projeto");
//var projetoid = e.options[e.selectedIndex].value;
////e=document.getElementById("solicitacao_tipoReemb");
////var tipohoraid = e.options[e.selectedIndex].value;
//e=document.getElementById("solicitacao_etapa");
//var etapaid = e.options[e.selectedIndex].value;
//var data = document.getElementById("solicitacao_data").value;
////var horaInicio = document.getElementById("solicitacao_horaInicial").value;
////var horaTermino = document.getElementById("solicitacao_horaFinal").value;
////var horaTotal = document.getElementById("solicitacao_horaTotal").value;
//var observacao = document.getElementById("solicitacao_observacao").value;
////var periodo = document.getElementById("solicitacao_periodo").value;
//var id=0;
//var url = retornaServer();
//var data = {
//page: "solicitacao",
//action: "salvarSolicitacao",
//id: id,
//data:data,
//vendedor:vendedor,
//cliente:clienteid,
//projeto: projetoid,
//tipoHora: tipohoraid,
//diaSemana:"",
//etapa: etapaid,
//inicio: horaInicio,
//termino: horaTermino,
//horasTrabalhadas: horaTotal,
//descricao: descricao,
//periodo: periodo

//}
//url=url+encodeURI(JSON.stringify(data));
//callWebservice(url,"callback");


//}




function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value==valueToSet) {
			selectObj.options[i].selected = true;

		}
	}
	return i;
}



function carregaSolicitacao()
{
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("lancamentoSolicitacao","0");
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../solicitacao/solicitacao.html';

}


//TRATAMENTO DOS EVENTOS

function preencheTabelaLancamentosSolicitacao(){
	var html = '';
	var status='';

	var j=0;


	var achei=0;

	if(listaLancamentosSolicitacao.length > 0)
	{
		for(i = 0; i < listaLancamentosSolicitacao.length; i++)
		{
			if (tipoSolicitacao==0)
			{


				achei=0;
				j=0;
				while (achei ==0)
				{
					if (listaLancamentosSolicitacao[i].organizacao== listaOrganizacoes[j].id)
					{
						achei=1;
						indiceOrganizacao=j;
					}
					j=j+1;	
				}

			}


			achei=0;
			j=0;
			while (achei ==0)
			{
				if (listaLancamentosSolicitacao[i].proprietario== listaPessoas[j].id)
				{
					achei=1;
					indicePessoa=j;
				}
				j=j+1;
			}

			if (listaLancamentosSolicitacao[i].statusSolicitacaoPessoa ==0)
				status="Pendente";
			if (listaLancamentosSolicitacao[i].statusSolicitacaoPessoa ==1)
				status="Aprovado";
			if (listaLancamentosSolicitacao[i].statusSolicitacaoPessoa ==2)
				status="Reprovado";


			if (isMobile() ==false)
			{
				if (tipoSolicitacao==0)
				{	  

					if (listaLancamentosSolicitacao[i].statusSolicitacaoPessoa==0)
					{

						html+='<div class="item" id="div'+listaLancamentosSolicitacao[i].id+'">'
						+'<div><b>'
						+listaOrganizacoes[indiceOrganizacao].nome+"<br>"
						+ "<font color=#515caf>Nova Pessoa: "+listaLancamentosSolicitacao[i].nome+ "<br> Email: " +listaLancamentosSolicitacao[i].email+'</font></b>'
						+"<br>Solicitante: "+listaPessoas[indicePessoa].nome +"<br>"+'</div>';

						html+='<button class="btnedit" onclick="editarLancamento('+i+')"><i class="fa fa-search" aria-hidden="true" style="font-size:24px"></i></button>';


						html+='<button class="btndelete" onclick="reprovarSolicitacao('+i+')"><i class="fa fa-times-circle" aria-hidden="true" style="font-size:24px"></i></button>'
						+'<button class="btnaprova" onclick="aprovarSolicitacao('+i+')"><i class="fa fa-check" aria-hidden="true" style="font-size:24px"></i></button>'
						+'</div>';

					}
					else
					{
						html+='<div class="item" id="div'+listaLancamentosSolicitacao[i].id+'">'
						+'<div><b>'
						+listaLancamentosSolicitacao[i].nome+"<font color=#515caf><br>CNPJ: "+listaLancamentosSolicitacao[i].cnpj+'</font></b>'
						+"<br>Endereco: "+listaLancamentosSolicitacao[i].logradouro+ "-"+listaLancamentosSolicitacao[i].cidade+"-"+listaLancamentosSolicitacao[i].estado+"<br>"
						+'</div>';


						html+='<button class="btnedit" onclick="editarLancamento('+i+')"><i class="fa fa-search" aria-hidden="true" style="font-size:24px"></i></button>';


						html+='<button class="btndelete" onclick="reprovarSolicitacao('+i+')"><i class="fa fa-times-circle" aria-hidden="true" style="font-size:24px"></i></button>'
						+'<button class="btnaprova" onclick="aprovarSolicitacao('+i+')"><i class="fa fa-check" aria-hidden="true" style="font-size:24px"></i></button>'
						+'</div>';
					}


				}
				else
				{
					if (tipoSolicitacao==0)
					{
						html+='<div class="item" id="div'+listaLancamentosSolicitacao[i].id+'">'
						+'<div><b>'
						+listaColaboradores[indiceColaborador].nome+"<br>"
						+listaLancamentosSolicitacao[i].data+ "-" +listaTipoSolicitacao[indiceTipoSolicitacao].nome+ "<font color=#515caf> <br>Valor Solicitado: "+listaLancamentosSolicitacao[i].valorsolicitado+"- Valor Previsto: "+listaLancamentosSolicitacao[i].valorsolicitacao+'</font></b>'
						+"<br>"+listaProjetos[indiceProjeto].codigoSap+"-"+listaProjetos[indiceProjeto].nome +"<br>"+'</div>';

						html+='Comprovantes:<br>';

						for (j=0;j < listaLancamentosSolicitacao[i].numeroArquivos;j++)
						{
							html+='<a href="'+retornaCaminhoArquivo()+'solicitacao_'+listaLancamentosSolicitacao[i].id+ '_'+j+'.png"' + 'target="parent">Solicitacao'+listaLancamentosSolicitacao[i].id+ '_'+j+'</a><br>';

						}

						html+='<button class="btnedit" onclick="editarLancamento('+i+')"><i class="fa fa-search" aria-hidden="true" style="font-size:24px"></i></button>'
						+'<button class="btndelete" onclick="alertaAvaliado()"><i class="fa fa-lock" aria-hidden="true" style="font-size:24px"></i></button>';
						+'</div>'
					}
					else
					{
						html+='<div class="item" id="div'+listaLancamentosSolicitacao[i].id+'">'
						+'<div><b>'
						+listaColaboradores[indiceColaborador].nome+"<br>"
						+listaLancamentosSolicitacao[i].data+ "-" +listaTipoSolicitacao[indiceTipoSolicitacao].nome+ "<font color=#515caf> <br>Valor Solicitado: "+listaLancamentosSolicitacao[i].valorsolicitado+"- Valor Previsto: "+listaLancamentosSolicitacao[i].valorsolicitacao+'</font></b>'
						+"<br>"+listaProjetos[indiceProjeto].codigoSap+"-"+listaProjetos[indiceProjeto].nome +"<br>"+'</div>';

						html+='Comprovantes:<br>';

						for (j=0;j < listaLancamentosSolicitacao[i].numeroArquivos;j++)
						{
							html+='<a href="'+retornaCaminhoArquivo()+'solicitacao_'+listaLancamentosSolicitacao[i].id+ '_'+j+'.png"' + 'target="parent">Solicitacao'+listaLancamentosSolicitacao[i].id+ '_'+j+'</a><br>';

						}

						html+='<button class="btnedit" onclick="editarLancamento('+i+')"><i class="fa fa-search" aria-hidden="true" style="font-size:24px"></i></button>'
						+'<button class="btndelete" onclick="alertaAvaliado()"><i class="fa fa-lock" aria-hidden="true" style="font-size:24px"></i></button>';
						+'</div>'

					}
				}
			}
			else
			{

				if (tipoSolicitacao==0)
				{

					if (listaLancamentosSolicitacao[i].statussolicitacao==0)
					{

						html+='<div class="item" id="div'+listaLancamentosSolicitacao[i].id+'">'
						+'<button class="btnedit" onclick="editarLancamento('+listaLancamentosSolicitacao[i].id+')"><i class="fa fa-search" aria-hidden="true" style="font-size:24px"></i></button>'
						+'<div><b>'
						+listaColaboradores[indiceColaborador].nome+"<br>"
						+listaLancamentosSolicitacao[i].data+ "-" +listaTipoSolicitacao[indiceTipoSolicitacao].nome+ "<font color=#515caf> <br>Valor Solicitado: "+listaLancamentosSolicitacao[i].valorsolicitado+"- Valor Previsto: "+listaLancamentosSolicitacao[i].valorsolicitacao+'</font></b>'
						+"<br>"+listaProjetos[indiceProjeto].codigoSap+"-"+listaProjetos[indiceProjeto].nome +"<br>"+'</div>';

						html+='Comprovantes:<br>';

						for (j=0;j < listaLancamentosSolicitacao[i].numeroArquivos;j++)
						{
							html+='<a href="'+retornaCaminhoArquivo()+'solicitacao_'+listaLancamentosSolicitacao[i].id+ '_'+j+'.png"' + 'target="parent">Solicitacao'+listaLancamentosSolicitacao[i].id+ '_'+j+'</a><br>';

						}
						html+='<button class="btdelete" onclick="reprovarSolicitacao('+i+')"><i class="fa fa-times-circle" aria-hidden="true" style="font-size:24px"></i></button>'
						+'<button class="btnaprova" onclick="aprovarSolicitacao('+i+')"><i class="fa fa-check" aria-hidden="true" style="font-size:24px"></i></button>'
						+'</div>';
					}
					else
					{

						html+='<div class="item" id="div'+listaLancamentosSolicitacao[i].id+'">'
						+'<button class="btnedit" onclick="editarLancamento('+i+')"><i class="fa fa-search" aria-hidden="true" style="font-size:24px"></i></button>'
						+'<div><b>'
						+listaColaboradores[indiceColaborador].nome+"<br>"
						+listaLancamentosSolicitacao[i].data+ "-" +listaTipoSolicitacao[indiceTipoSolicitacao].nome+ "<font color=#515caf> <br>Valor Solicitado: "+listaLancamentosSolicitacao[i].valorsolicitado+"- Valor Previsto: "+listaLancamentosSolicitacao[i].valorsolicitacao+'</font></b>'
						+"<br>"+listaProjetos[indiceProjeto].codigoSap+"-"+listaProjetos[indiceProjeto].nome +"<br>"+'</div>';

						html+='Comprovantes:<br>';

						for (j=0;j < listaLancamentosSolicitacao[i].numeroArquivos;j++)
						{
							html+='<a href="'+retornaCaminhoArquivo()+'solicitacao_'+listaLancamentosSolicitacao[i].id+ '_'+j+'.png"' + 'target="parent">Solicitacao'+listaLancamentosSolicitacao[i].id+ '_'+j+'</a><br>';

						}					
						html+='<button class="btdelete" onclick="alertaAvaliado()"><i class="fa fa-lock" aria-hidden="true" style="font-size:24px"></i></button>'
							+'</div>';

					}
				}
				else
				{
					if (listaLancamentosSolicitacao[i].statussolicitacao==0)
					{

						html+='<div class="item" id="div'+listaLancamentosSolicitacao[i].id+'">'
						+'<button class="btnedit" onclick="editarLancamento('+listaLancamentosSolicitacao[i].id+')"><i class="fa fa-search" aria-hidden="true" style="font-size:24px"></i></button>'
						+'<div><b>'
						+listaColaboradores[indiceColaborador].nome+"<br>"
						+listaLancamentosSolicitacao[i].data+ "-" +listaTipoSolicitacao[indiceTipoSolicitacao].nome+ "<font color=#515caf> <br>Valor Solicitado: "+listaLancamentosSolicitacao[i].valorsolicitado+"- Valor Previsto: "+listaLancamentosSolicitacao[i].valorsolicitacao+'</font></b>'
						+"<br>"+listaProjetos[indiceProjeto].codigoSap+"-"+listaProjetos[indiceProjeto].nome +"<br>"+'</div>';

						html+='Comprovantes:<br>';

						for (j=0;j < listaLancamentosSolicitacao[i].numeroArquivos;j++)
						{
							html+='<a href="'+retornaCaminhoArquivo()+'solicitacao_'+listaLancamentosSolicitacao[i].id+ '_'+j+'.png"' + 'target="parent">Solicitacao'+listaLancamentosSolicitacao[i].id+ '_'+j+'</a><br>';

						}
						html+='<button class="btdelete" onclick="reprovarSolicitacao('+i+')"><i class="fa fa-times-circle" aria-hidden="true" style="font-size:24px"></i></button>'
						+'<button class="btnaprova" onclick="aprovarSolicitacao('+i+')"><i class="fa fa-check" aria-hidden="true" style="font-size:24px"></i></button>'
						+'</div>';
					}
					else
					{

						html+='<div class="item" id="div'+listaLancamentosSolicitacao[i].id+'">'
						+'<button class="btnedit" onclick="editarLancamento('+listaLancamentosSolicitacao[i].id+')"><i class="fa fa-search" aria-hidden="true" style="font-size:24px"></i></button>'
						+'<div><b>'
						+listaColaboradores[indiceColaborador].nome+"<br>"
						+listaLancamentosSolicitacao[i].data+ "-" +listaTipoSolicitacao[indiceTipoSolicitacao].nome+ "<font color=#515caf> <br>Valor Solicitado: "+listaLancamentosSolicitacao[i].valorsolicitado+"- Valor Previsto: "+listaLancamentosSolicitacao[i].valorsolicitacao+'</font></b>'
						+"<br>"+listaProjetos[indiceProjeto].codigoSap+"-"+listaProjetos[indiceProjeto].nome +"<br>"+'</div>';

						html+='Comprovantes:<br>';

						for (j=0;j < listaLancamentosSolicitacao[i].numeroArquivos;j++)
						{
							html+='<a href="'+retornaCaminhoArquivo()+'solicitacao_'+listaLancamentosSolicitacao[i].id+ '_'+j+'.png"' + 'target="parent">Solicitacao'+listaLancamentosSolicitacao[i].id+ '_'+j+'</a><br>';

						}					
						html+='<button class="btdelete" onclick="alertaAvaliado()"><i class="fa fa-lock" aria-hidden="true" style="font-size:24px"></i></button>'
							+'</div>';

					}	 
				}
			}        	   
		}



		tabelaLancamentos.innerHTML = html;
		//checkButton();
	}else{
		tabelaLancamentos.innerHTML = html;
	}

}


//function btn_salvar_click(){
//salvarSolicitacao();
//}


function alertaAvaliado()
{
	alert ("Este solicitacao já foi avaliado!");	
}


function sair()
{
	localStorage.clear();
	sessionStorage.setItem("vendedor","0");
	sessionStorage.setItem("lancamento","0");

	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../../login/login.html';

}


target = document.querySelectorAll('.item-list')[0];



//cria uma nova instância de observador
observer = new MutationObserver(function(mutations) {
	mutations.forEach(function(mutation) {
		console.log("tipo de mutação" +mutation.type);

		$(function() {$('.example-1').listSwipe();});

	});    
});

//configuração do observador:
config = { attributes: true, childList: true, characterData: true };

//passar o nó alvo pai, bem como as opções de observação
observer.observe(target, config);



