var comboTurma;
$(document).ready(function(){
    document.getElementById("logout_link").onclick = menu_logout;
    buscaTurma();
    comboTurma=document.getElementById("comboTurma")
    comboTurma.onclick= carregaParticipantes;
    document.getElementById("botaoRelatorio").onclick = processaRelatorio;

});

function menu_logout() {
    localStorage.clear();
    window.document.location.href = '../login/login.html';
}

function menu_openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function menu_closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    document.body.style.backgroundColor = "white";
}


function carregaParticipantes()
{
        //alert("value:" + comboTurma.value+ " texto: " +comboTurma.text);
        var turma=comboTurma.value;
        var url = 'http://localhost:8080/BMI_AVASE_SERVER_V2/rest/server/webservice/';
        var data = {
        page: "facilitador",
        action: "buscaParticipantesStatus",
        turma: turma
        }
        url=url+encodeURI(JSON.stringify(data));

       callWebservice(url,"callbackTurma");
    
}


function processaRelatorio()
{
        //alert("value:" + comboTurma.value+ " texto: " +comboTurma.text);
        var turma=comboTurma.value;
        var url = 'http://localhost:8080/BMI_AVASE_SERVER_V2/rest/server/webservice/';
        var data = {
        page: "facilitador",
        action: "processaRelatorio",
        turma: turma
        }
        url=url+encodeURI(JSON.stringify(data));

       callWebservice(url,"callbackTurma");
    
}



function MontaPagina(data)
{  // alert(data);
    //alert(data.listaTurmas);
    //alert(data.listaTurmas[0].nome);
    var comboTurma = document.getElementById("comboTurma");
    var opt = document.createElement('option');
    opt.value=0;
    opt.text='';
    opt.innerHTML = ""+opt.text;
    comboTurma.appendChild(opt);    
    for (var k=1;k<=data.listaTurmas.length;k++)
    {
        var opt = document.createElement('option');
        //alert(data    [k].nome);
        var idTurma=data.listaTurmas[k-1].id;
        var opcao=data.listaTurmas[k-1].nome;
        opt.value=idTurma;
        opt.text = opcao;
        opt.innerHTML = ""+opt.text;
        comboTurma.appendChild(opt);    
        var progressao = data.listaTurmas[k-1].progressao;
        var ckProgressao = document.getElementById("progressao");
        var txEstagio = document.getElementById("estagio");
        if(progressao==0) 
                ckProgressao.checked =true;

        txEstagio.value=""+data.listaTurmas[k-1].estagio;    

    }
}

function MostraParticipantesTurma(data)
{
 
    var table = document.getElementById("tabelaParticipante");
   // Captura a quantidade de linhas já existentes na tabela
    var numOfRows = table.rows.length;
    //alert(numOfRows);
    //alert(numOfRows);
// Captura a quantidade de colunas da última linha da tabela
   var  numOfCols = table.rows[numOfRows - 1].cells.length;
   //alert(data.nome);
   
    
    if (numOfRows >1)
    {  
     for(j=numOfRows-1; j>=1; j--)
     {
        table.deleteRow(j);
     }
        
    }
    //alert(data.listaParticipantes.length);
    for (var k=numOfRows;k<=data.listaParticipantes.length;k++)  
    {
        // Insere uma linha no fim da tabela.
       var newRow = table.insertRow(k);
       //newRow.className='tr';
   // Faz um loop para criar as colunas
 
            // Insere uma coluna na nova linha 
         var newCell = newRow.insertCell(0);
        newCell.innerHTML = ""+data.listaParticipantes[k-1].participante;
        
        var newCell = newRow.insertCell(1);
        newCell.innerHTML = ""+data.listaParticipantes[k-1].empresa;
           
        
        if (data.listaParticipantes[k-1].posicao==0)
        {
               var newCell = newRow.insertCell(2);
               var img = document.createElement('img');   
               img.src = "images/bt_atrib_cult.png";
               img.style.width='20%';
               newCell.appendChild(img);
    
              
               var newCell = newRow.insertCell(3);
               var img = document.createElement('img');
               img.src = "images/bt_alav_comp.png";
               img.style.width='20%';
               newCell.appendChild(img);
            
               var newCell = newRow.insertCell(4);
               var img = document.createElement('img');
               img.src = "images/bt_sist_org.png";
               img.style.width='20%';
               newCell.appendChild(img);
          
               var newCell = newRow.insertCell(5);
               var img = document.createElement('img');
               img.src = "images/bt_comp_essenc.png";
               img.style.width='20%';
               newCell.appendChild(img);
         }
        
        if (data.listaParticipantes[k-1].posicao>=1)
        {
               var newCell = newRow.insertCell(2);
               var img = document.createElement('img');
               img.src = "images/bt_atrib_cult_finalizado.png";
               img.style.width='20%';
               newCell.appendChild(img);
         }
        
        if (data.listaParticipantes[k-1].posicao>=2)
         {      var newCell = newRow.insertCell(3);
               var img = document.createElement('img');
               img.src = "images/bt_alav_comp_finalizado.png";
               img.style.width='20%';
               newCell.appendChild(img);
         }
        
        if (data.listaParticipantes[k-1].posicao>=3)
          {     var newCell = newRow.insertCell(4);
               var img = document.createElement('img');
               img.src = "images/bt_sist_org_finalizado.png";
               img.style.width='20%';
               newCell.appendChild(img);
          }
        if (data.listaParticipantes[k-1].posicao==4)
           {    var newCell = newRow.insertCell(5);
               var img = document.createElement('img');
               img.src = "images/bt_comp_essenc_finalizado.png";
               img.style.width='20%';
               newCell.appendChild(img);
           }
    
}

}

    

function buscaTurma()
{
        var url = 'http://localhost:8080/BMI_AVASE_SERVER_V2/rest/server/webservice/';
        var data = {
        page: "facilitador",
        action: "buscaTurmas",
        }
        url=url+encodeURI(JSON.stringify(data));

       callWebservice(url);
}


function callback(data){
          
       //alert(data.Pessoa);
    if (data.action=="buscaTurmas")   
    {var resultado =data;
           
    //alert(data.page);         
 
     MontaPagina(data);
      
    }
    
    if (data.action=="buscaParticipantesStatus")   
    {var resultado =data;
           
           
    //alert("mosta");
     MostraParticipantesTurma(data);
      
    }
    
    
    if (data.action=="processaRelatorio")   
    {var resultado =data;
           
           
    //alert("mosta");
     //MostraParticipantesTurma(data);
      
    }
              
}





