//Variáveis da sessão
var listaColaboradores = [];
var listaCargo = [];
var listaOrganizacoes = [];
var listaLancamentos = [];
var listaSolicitacaoOrganizacao =[];
var observer;
var config;
var target;
var periodo;
var indiceColaborador;

var vendedor = "0";
var unidade = "0";
var projeto = 0;
var etapa = 0;
var tipoHora = 0;
var nome="";
var status=0;
var tabelaLancamentos;
var lancamentoSolicitacaoPessoa=0;



function admin_init(){
	// cadastro = sessionStorage.getItem("cadastro");
	//  nome=sessionStorage.getItem("nomeColaborador");


	vendedor=sessionStorage.getItem("vendedor");
	unidade=sessionStorage.getItem("unidade");



	if ((vendedor==0) || (vendedor==null) || (vendedor==undefined)) 
	{
		window.document.location.href = '../login/login.html';
	}
	else
	{	
		setSelectedValue(document.getElementById("lancamentossolicitacaopessoa_status"),0);
		document.getElementById("lancamentossolicitacaopessoa_status").addEventListener("change",function(){ buscarSolicitacoesStatusTipo(document.getElementById("lancamentossolicitacaopessoa_status").value)},false);
		tabelaLancamentos = document.getElementById("lancamentossolicitacaopessoa_lancamentos");
		document.getElementById("menuop1").addEventListener("click", inserirSolicitacaoPessoa);
		document.getElementById("menuop2").addEventListener("click", carregarMenuVendas);
		document.getElementById("menuop3").addEventListener("click", sair);
		buscarOrganizacoes();


	}
}


function isMobile()
{
	var userAgent = navigator.userAgent.toLowerCase();
	if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
		return true;
	else
		return false;
}



//PREENCHIMENTO DAS COMBOS





function dataAtualFormatada(data){

	var dia = data.getDate();
	if (dia.toString().length == 1)
		dia = "0"+dia;
	var mes = data.getMonth()+1;
	if (mes.toString().length == 1)
		mes = "0"+mes;
	var ano = data.getFullYear();  

	return ano+"-"+mes+"-"+dia+' 00:00:00.0';
}



function buscarOrganizacoes(){

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarOrganizacoes",
			unidade : unidade

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");


}

function buscarSolicitacaoPessoa()
{
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarSolicitacaoPessoa",
			id: lancamentoSolicitacaoPessoa

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   	
}





function buscarSolicitacoesStatusTipo(tipo){


	var status=document.getElementById("lancamentossolicitacaopessoa_status").options[document.getElementById("lancamentossolicitacaopessoa_status").selectedIndex].text;
	
	
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarSolicitacoesStatusTipo", 
			proprietario:vendedor,
			unidade: unidade,
			status: status,
			tipoSolicitacao:tipo

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");

}

function removerLancamento(lancamentoId){
	var lancamento=listaLancamentos[lancamentoId].id;
	var r=confirm("Você deseja realmente remover o lançamento?");
	if (r==true)
	{
		var url = retornaServer();
		var data = {
				page: "vendas",
				action: "removerSolicitacaoPessoa",
				lancamento: lancamento

		}
		url=url+encodeURI(JSON.stringify(data));
		callWebservice(url,"callback");  
	}

}


function editarLancamento(lancamentoId)
{
	var lancamento=listaLancamentos[lancamentoId].id;
	var achei=0;
	var i=0;
	while ( achei==0)
	{
		if (listaLancamentos[i].id==lancamento)
		{
			achei=1;	  
		}
		i=i+1;
	}
	sessionStorage.setItem("lancamentoSolicitacaoPessoa",listaLancamentos[i-1].id);
	sessionStorage.setItem("vendedor",listaLancamentos[i-1].proprietario);
	sessionStorage.setItem("unidade",listaLancamentos[i-1].unidade);

	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../solicitacaopessoa/solicitacaopessoa.html';

}

//PROCESSAMENTO DO RETORNO DOS WEBSERVICES

function callback(data){



	if(data.action=="buscarSolicitacoesStatusTipo"){
		if (data.retorno !="")
		{
			if (data.tipoSolicitacao=='pessoa')
			{
				listaLancamentos = jQuery.parseJSON(data.retorno);
				preencheTabelaLancamentos();
			}
			if (data.tipoSolicitacao=='organizacao')
			{

				listaSolicitacaoOrganizacao= jQuery.parseJSON(data.retorno);
				tamanho=listaOrganizacoes.length;
				for(i=0;i<listaSolicitacaoOrganizacao.length;i++)
				{
					listaSolicitacaoOrganizacao[i].nome='(Em Solicitação)'+listaSolicitacaoOrganizacao[i].nome;
					listaOrganizacoes[tamanho]= listaSolicitacaoOrganizacao[i];
					tamanho=tamanho+1;

				}
				buscarSolicitacoesStatusTipo(0);

			}	
		}
	}


	if(data.action=="buscarPessoas"){
		if (data.retorno!=""){
			listaPessoas = jQuery.parseJSON(data.retorno);
		}
	}

	if(data.action=="buscarOrganizacoes"){
		listaOrganizacoes = jQuery.parseJSON(data.retorno);
		buscarSolicitacoesStatusTipo(1);
	}



	if(data.action=="removerLancamentoSolicitacaoPessoa"){
		location.reload();
	}


}


function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value==valueToSet) {
			selectObj.options[i].selected = true;

		}
	}
	return i;
}




function inserirSolicitacaoPessoa()
{
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("unidade",unidade);

	sessionStorage.setItem("lancamentoSolicitacaoPessoa","0");
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../solicitacaopessoa/solicitacaopessoa.html';

}

function carregarMenuVendas()
{
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("unidade",unidade);

	sessionStorage.setItem("lancamentoSolicitacaoPessoa","0");
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../menuVendas/menuVendas.html';

}





//TRATAMENTO DOS EVENTOS

function preencheTabelaLancamentos(){
	var html = '';
	var status;
	var cargo="";
	var indiceOrganizacao;
	var indicePessoa;
	var j=0;
	var totalLancamentos='00:00';

	var achei=0;
	var dataFormatada;
	var data ;
	if(listaLancamentos.length > 0)
	{
		for(i = 0; i < listaLancamentos.length; i++)
		{   
			data = construirData(listaLancamentos[i].dataSolicitacaoPessoa, "00:00:00");
			dataFormatada= formataDataPt(data);
			achei=0;
			j=0;


			if (isMobile() ==false)
			{

				html+='<div class="item" id="div'+listaLancamentos[i].id+'">'
				+'<div><b>'
				+" "+dataFormatada
				+"<br><b><font color=#515caf>Pessoa: " +listaLancamentos[i].nome +"</font></b>"
				+"<br>" +listaLancamentos[i].organizacao  
				+"<br>" +listaLancamentos[i].cargo 
				+"<br>" +listaLancamentos[i].email
				+"<br>Observação:" +listaLancamentos[i].observacao+"<br>"
				+'</div>'
				+'<button class="btnedit" onclick="editarLancamento('+i+')"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:24px"></i></button>'
				//		+'<button class="btndelete" onclick="removerLancamento('+i+')"><i class="fa fa-trash" aria-hidden="true" style="font-size:24px"></i></button>'
				+'</div>';
			}
			else
			{

				html+='<div class="item" id="div'+listaLancamentos[i].id+'">'

				+'<div><b>'
				+" "+dataFormatada 
				+"<br><b><font color=#515caf>Pessoa: " +listaLancamentos[i].nome +"</font></b>"
				+"<br>" +listaLancamentos[i].organizacao 
				+"<br>" +listaLancamentos[i].cargo
				+"<br>" +listaLancamentos[i].email 
				+"<br>Observação:" +listaLancamentos[i].observacao+"<br>"
				+'</div>'
				+'<button class="action" onclick="editarLancamento('+i+')"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size:24px"></i></button>'
				//		+'<button class="action" onclick="removerLancamento('+i+')"><i class="fa fa-trash" aria-hidden="true" style="font-size:24px"></i></button>'
				+'</div>';
			}        	   
		}



		tabelaLancamentos.innerHTML = html;
	}//checkButton();
	else{
		tabelaLancamentos.innerHTML = html;
	}

}



function sair()
{
	localStorage.clear();
	sessionStorage.setItem("vendedor","0");
	sessionStorage.setItem("unidade","0");

	sessionStorage.setItem("lancamentoSolicitacaoPessoa","0");
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../login/login.html';

}


target = document.querySelectorAll('.item-list')[0];



//cria uma nova instância de observador
observer = new MutationObserver(function(mutations) {
	mutations.forEach(function(mutation) {
		console.log("tipo de mutação" +mutation.type);

		$(function() {$('.example-1').listSwipe();});

	});    
});

//configuração do observador:
config = { attributes: true, childList: true, characterData: true };

//passar o nó alvo pai, bem como as opções de observação
observer.observe(target, config);

//function dragElement(event) {
//var elementToDrag = event.target;
//elementToDrag.style.left = event.gesture.deltaX + 'px';
//};



//function resetElement(event) {
//var elementToReset = event.target;
//elementToReset.style.left = 0;
//};


