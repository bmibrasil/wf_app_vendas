var organizacao = 0;
var negocio = 0;
var data = "";
var diaSemana;
var tipo = 0;
var descricao = 0;
var id = 0;
var listaNegocios = [];
var listaOrganizacoes = [];
var listaPessoas = [];
var listaLancamento = [];
var pessoa=0;
var tipoAtividade=1;
var unidade=0;
var vendedor="";
var nomeVendedor="";

function admin_init(){

	vendedor=sessionStorage.getItem("vendedor");
	nomeVendedor=sessionStorage.getItem("nomeVendedor");
	unidade=sessionStorage.getItem("unidade");
	lancamentoRelacionamento=sessionStorage.getItem("lancamentoRelacionamento");
	
	
	document.getElementById("relacionamento_data").addEventListener("change", buscaData);
	//document.getElementById("relacionamento_pessoa").addEventListener("change", selecionaOrganizacao);
	
	document.getElementById("relacionamento_organizacao").addEventListener("change",buscarPessoasOrganizacao );
	buscarOrganizacoes();
	dataAtual();
	buscaData();
	document.getElementById("btn_salvar").addEventListener("click", btn_salvar_click);
	document.getElementById("menuop1").addEventListener("click", carregaLancamentosRelacionamento);
	document.getElementById("menuop2").addEventListener("click", carregaMenuVendas);
	document.getElementById("menuop3").addEventListener("click", sair);
}

//TRATAMENTO DOS EVENTOS

//verifica se foi escolhido valor na combo diferente do default
function verificaValorSelecionadoCombo(str) {
	var selectObj  = document.getElementById(str);
	var valueToSet  = document.getElementById(str).value;
	if (selectObj.options[0].value==valueToSet) {
		return false;
	}
	else{
		return true;
	}

}

//verifica se o campo esta vazio
function verificaPreeenchimento(str) {
	var v = document.getElementById(str).value;
	if ((v == null) || (v == "") || (v == '0'))
		return false
		else	
			return true;
}

function validarHorario() {
	
	var horaInicio = document.getElementById('relacionamento_hora_inicio').value;
	var horaFim = document.getElementById('relacionamento_hora_fim').value;
	
	if ( calcularIntervaloTempo( horaInicio, horaFim ) >= 0 ){
		return true;
	}else{
		alert('Campos de Data com valores Invalidos, Certifique-se de que os horarios estejam preenchidos corretamente');
		return false;
	}

}

function validaCadastro() {
	if ((( 
			(verificaValorSelecionadoCombo("relacionamento_organizacao")))
			&&verificaValorSelecionadoCombo("relacionamento_pessoa")
			&& verificaPreeenchimento("relacionamento_data")
			&& verificaPreeenchimento("relacionamento_hora_inicio")
			&& verificaPreeenchimento("relacionamento_hora_fim")
			&& verificaPreeenchimento("relacionamento_diaSemana")
			&& verificaValorSelecionadoCombo("relacionamento_tipo")
			&& verificaPreeenchimento("relacionamento_descricao")
	)==true)
	{
		return  true;
	}
	else
	{
		alert('Existem campos que não foram preenchidos, Por favor preencha');
		return false;
	}

}
	
function carregaLancamentosRelacionamento()
{
	{
		sessionStorage.setItem("vendedor",vendedor);
		sessionStorage.setItem("nomeVendedor",nomeVendedor);
		sessionStorage.setItem("lancamentoRelacionamento","0");
		window.document.location.href = '../lancamentosRelacionamento/lancamentosRelacionamento.html';

	}

}

function carregaMenuVendas()
{
	{
		sessionStorage.setItem("vendedor",vendedor);
		sessionStorage.setItem("nomeVendedor",nomeVendedor);
		sessionStorage.setItem("lancamentoRelacionamento","0");
		window.document.location.href = '../menuVendas/menuVendas.html';

	}

}

function sair()
{
	sessionStorage.setItem("vendedor","0");
	sessionStorage.setItem("nomeVendedor","0");
	sessionStorage.setItem("lancamentoRelacionamento","0");

	window.document.location.href = '../login/login.html';

}

function dataAtual(data){
	var data =new Date();
	document.getElementById("relacionamento_data").valueAsDate=data; 
	document.getElementById("relacionamento_data").innerHTML="";

}


function buscaData()
{
	var dataSel=document.getElementById("relacionamento_data").value;
	diaSemanaData(dataSel);
}


function diaSemanaData(dataEscolhida)
{

	var dataEsc=dataEscolhida;
	var dt1   = parseInt(dataEsc.substring(8,10));
	var mon1  = parseInt(dataEsc.substring(5,7));
	var yr1   = parseInt(dataEsc.substring(0,4));
	var dataNova= new Date(yr1, mon1-1, dt1,0,0,0);

	var dia= dataNova.getDay();
	var semana=new Array(6);
	semana[0]='Domingo';
	semana[1]='Segunda-Feira';
	semana[2]='Terça-Feira';
	semana[3]='Quarta-Feira';
	semana[4]='Quinta-Feira';
	semana[5]='Sexta-Feira';
	semana[6]='Sábado';
	document.getElementById("relacionamento_diaSemana").value=semana[dia];
	document.getElementById("relacionamento_diaSemana").innerHTML="";
	return dia;
}              


function btn_salvar_click(){
	if (validaCadastro() == true && validarHorario() == true )
	{
		document.getElementById("btn_salvar").removeEventListener("click", btn_salvar_click);
		salvarAtividade();
	}
}


function selecionaOrganizacao()
{
	
	var indice= document.getElementById("relacionamento_pessoa").selectedIndex;
	for (j=0;j<listaOrganizacoes.length;j++)
	{

		if (listaOrganizacoes[j].id== listaPessoas[indice-1].organizacao)
		{
			indiceOrganizacao=j;
		}

	}	
	
	setSelectedValue(document.getElementById('relacionamento_organizacao'), listaOrganizacoes[indiceOrganizacao].id);

}

function preencheListaOrganizacoes(){
	var html = '<option value="0">Organizacao...</option>';
	var select = "";

	for (i=0;i<listaOrganizacoes.length;i++){
		if(organizacao==i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaOrganizacoes[i].id+'" '+select+'>' +listaOrganizacoes[i].nome+'</option>';
	}

	document.getElementById("relacionamento_organizacao").innerHTML = html;
	setSelectedValue(document.getElementById("relacionamento_organizacao"),0);
}

function preencheListaPessoas(){
	var html = '<option value="0">Pessoa...</option>';
	var indiceOrganizacao=0;
	var select = "";
	for (i=0;i<listaPessoas.length;i++){
		if(pessoa==i){
			select = "selected";
		}else{
			selected = "";
		}


		html += '<option value="'+listaPessoas[i].id+'" '+select+'>'+listaPessoas[i].nome+'</option>';

	}
	document.getElementById("relacionamento_pessoa").innerHTML = html;
	setSelectedValue(document.getElementById("relacionamento_pessoa"),0);
}

function buscarOrganizacoes(){

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarOrganizacoes",
			unidade: unidade


	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}

function buscarPessoasOrganizacao(){

	var proprietario = vendedor;
	e=document.getElementById("relacionamento_organizacao");
	organizacao = e.options[e.selectedIndex].value;

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarPessoasOrganizacao",
			proprietario: proprietario,
			organizacao:organizacao,
			unidade: unidade



	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}

function buscarAtividade()
{
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarAtividade",
			id: lancamentoRelacionamento

	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   	
}

function buscarPessoasProprietario()
{

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarPessoasProprietario",
			proprietario: vendedor,
			unidade: unidade
			
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   	
}


function buscaOrganizacaocomPessoa(){

	var proprietario = vendedor;

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscaOrganizacaocomPessoa",
			proprietario: proprietario


	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}


function salvarAtividade(){

	

    negocio='';
	pessoa=	document.getElementById("relacionamento_pessoa").value;
	
	var data = document.getElementById("relacionamento_data").value;	
	var diaSemana = document.getElementById("relacionamento_diaSemana").value;
	
	var horaInicio = document.getElementById("relacionamento_hora_inicio").value;
	var horaFim = document.getElementById("relacionamento_hora_fim").value;
	
	var tipoAtividade = document.getElementById('relacionamento_tipo').options[document.getElementById('relacionamento_tipo').selectedIndex].innerText;
	var descricao = document.getElementById("relacionamento_descricao").value;	
	
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "salvarAtividade",
			id: encodeURIComponent(lancamentoRelacionamento),
			negocio: encodeURIComponent(negocio),
			pessoa:encodeURIComponent(pessoa),
			dataAtividade: encodeURIComponent(data),
			horaInicio: encodeURIComponent(horaInicio),
			horaFim: encodeURIComponent(horaFim),
			tipoAtividade: encodeURIComponent(tipoAtividade),
			descricao: encodeURIComponent(descricao),
			proprietario: encodeURIComponent(vendedor),
			nomeProprietario: encodeURIComponent(nomeVendedor)

	}
	url=url+JSON.stringify(data);
	
	console.log(url);
	
	callWebservice(url,"callback");   

}

function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value==valueToSet) {
			selectObj.options[i].selected = true;
			return;
		}
	}
}


function editaLancamento()
{
	var cargo="";
	var indiceNegocio=0;
	var indicePessoa=0;

//	achei=0;
//	j=0;
//	while (achei ==0)
//	{
//		if (listaLancamentos[0].pessoa== listaPessoas[j].id)
//		{achei=1;
//		indicePessoa=j;
//		}
//		j=j+1;
//	}

	
	//var data = new Date(listaLancamentos[0].dataAtividade+" 00:00:00");
	//var data = new Date(listaLancamentos[0].dataAtividade.substr(0,10)+" 00:00:00");
	//var dataMontada =listaLancamentos[0].dataAtividade.substr(6,4)+"-"+listaLancamentos[0].dataAtividade.substr(3,2)+"-"+listaLancamentos[0].dataAtividade.substr(0,2)+" 00:00:00";
	var dataMontada=listaLancamentos[0].dataAtividade.substr(0,10);
	var data = construirData(dataMontada, "00:00:00");
	var dataFormatada= formataData(data);
	
	var horaInicio = obterHorario(listaLancamentos[0].dataInicio);
	var horaFim = obterHorario(listaLancamentos[0].dataFim);
	
	document.getElementById('relacionamento_data').value=dataFormatada;
	document.getElementById('relacionamento_data').innerHTML="";
	diaSemanaData(dataFormatada);
	
	document.getElementById('relacionamento_hora_inicio').value=horaInicio;
	document.getElementById('relacionamento_hora_inicio').innerHTML="";
	
	document.getElementById('relacionamento_hora_fim').value=horaFim;
	document.getElementById('relacionamento_hora_fim').innerHTML="";

	setSelectedValue(document.getElementById('relacionamento_pessoa'),listaLancamentos[0].pessoaAtividade);
	selecionaOrganizacao();
	document.getElementById('relacionamento_descricao').value=listaLancamentos[0].descricao;
	document.getElementById('relacionamento_descricao').innerHTML="";
	selecionarSelectpeloOption("relacionamento_tipo", listaLancamentos[0].tipoAtividade)
}


function selecionarSelectpeloOption(elementId, valor) {
	var elt = document.getElementById(elementId);
	var opt = elt.getElementsByTagName("option");
	for(var i = 0; i < opt.length; i++) {
		if(opt[i].text == valor) {
			var campo=document.getElementById(elementId);
			setSelectedValue(campo,i);
		}
	}

}

function callback(data){


	if(data.action=="salvarAtividade"){
		alert("Atividade salva com sucesso!!");
		document.getElementById("btn_salvar").addEventListener("click", btn_salvar_click);
		window.location.reload(false);
	}

	
	if(data.action=="buscarPessoasProprietario"){
		listaPessoas=jQuery.parseJSON(data.retorno);
		preencheListaPessoas();
		if (lancamentoRelacionamento!="0")
		{
			buscarAtividade();
		}
	}

	if(data.action=="buscaOrganizacaocomPessoa"){
		listaOrganizacoes=jQuery.parseJSON(data.retorno);
		buscarPessoasProprietario();
	}
	
	if(data.action=="buscarPessoasOrganizacao"){
		listaPessoas=jQuery.parseJSON(data.retorno);
		preencheListaPessoas();
	}

	if(data.action=="buscarAtividade"){
		listaLancamentos=jQuery.parseJSON(data.retorno);
		editaLancamento();	
	}
	
	if(data.action=="buscarOrganizacoes"){
		listaOrganizacoes=jQuery.parseJSON(data.retorno);
		preencheListaOrganizacoes();
		buscaOrganizacaocomPessoa();
	}

}