var organizacao = 0;
var pessoa = 0;
var data = "";
var contato = 0;
var rua = 0;
var descricao = 0;
var briefing = 0;
var id = 0;
var diaSemana;
var listaPessoas = [];
var listaOrganizacoes = [];
var listaNegocio = [];
var vendedor=0;
var unidade=0;
var lancamentoNegocio="0";


function admin_init(){

	vendedor=sessionStorage.getItem("vendedor");
	unidade=sessionStorage.getItem("unidade");
	lancamentoNegocio=sessionStorage.getItem("lancamentoNegocio");
	document.getElementById("negocio_data").addEventListener("change", buscaData);
	dataAtual();
	buscaData();
	document.getElementById("btn_salvar").addEventListener("click", btn_salvar_click);
	document.getElementById("negocio_organizacao").addEventListener("change",buscarPessoasOrganizacao );
	//buscaOrganizacaocomPessoa();
	buscarOrganizacoes();
	document.getElementById("menuop1").addEventListener("click", carregaLancamentosNegocio);
	document.getElementById("menuop2").addEventListener("click", carregaMenuVendas);
	document.getElementById("menuop3").addEventListener("click", sair);
	


}

//TRATAMENTO DOS EVENTOS

//verifica se foi escolhido valor na combo diferente do default
function verificaValorSelecionadoCombo(str) {
	var selectObj  = document.getElementById(str);
	var valueToSet  = document.getElementById(str).value;
	if (selectObj.options[0].value==valueToSet) {
		return false;
	}
	else{
		return true;
	}

}

//verifica se o campo esta vazio
function verificaPreeenchimento(str) {
	var v = document.getElementById(str).value;
	if ((v == null) || (v == "") || (v == '0'))
		return false
		else	
			return true;
}

function verificarPotenciaValor(str){
	
	var v = document.getElementById(str).value;
	
	var status = v.match(/^([0-9]*[\.]*[0-9]*)$/g);
	
	console.log(status);
	
	if( status == null )	return false;
	
	return true;
}

function validaPotencialValor(){
	
	if (verificarPotenciaValor('negocio_potencial') == true){
		
		return  true;
		
	}else{
		
		alert('Potencial valor possui virgular(,), por favor troque por ponto(.)');
		return false;
		
	}
	
}

function validaCadastro() {
	if ((verificaPreeenchimento("negocio_data")
			&& verificaPreeenchimento("negocio_diaSemana")
			&& verificaValorSelecionadoCombo("negocio_organizacao")
			&& verificaValorSelecionadoCombo("negocio_pessoa")
			&& verificaPreeenchimento("negocio_escopo")
			&& verificaPreeenchimento("negocio_nome")
			&& verificaPreeenchimento("negocio_publico")
			&& verificaPreeenchimento("negocio_temperatura")
			&& verificaPreeenchimento("negocio_qtepessoas")
			&& verificaPreeenchimento("negocio_solucao")
			&& verificaPreeenchimento("negocio_dataentrega")
			&& verificaPreeenchimento("negocio_datafechamento")
			//&& verificaValorSelecionadoCombo("negocio_limite")

			//&& verificaPreeenchimento("negocio_potencial")
			//&& verificaPreeenchimento("negocio_briefing")
	)==true)
	{
		return  true;
	}
	else
	{
		alert('Existem campos que não foram preenchidos, Por favor preencha');
		return false;
	}

}


function mascara(t, mask){
	var i = t.value.length;
	var saida = mask.substring(1,0);
	var texto = mask.substring(i)
	if (texto.substring(0,1) != saida){
		t.value += texto.substring(0,1);
	}
}




function dataAtual(data){
	var data = new Date();
	document.getElementById("negocio_data").valueAsDate=data; 
	document.getElementById("negocio_data").innerHTML="";
	document.getElementById("negocio_dataentrega").valueAsDate=data;
	document.getElementById("negocio_dataentrega").innerHTML="";
	document.getElementById("negocio_datafechamento").valueAsDate=data;
	document.getElementById("negocio_datafechamento").innerHTML="";

}


function buscaData()
{
	var dataSel=document.getElementById("negocio_data").value;
	diaSemanaData(dataSel);
}


function diaSemanaData(dataEscolhida)
{

	var dataEsc=dataEscolhida;
	var dt1   = parseInt(dataEsc.substring(8,10));
	var mon1  = parseInt(dataEsc.substring(5,7));
	var yr1   = parseInt(dataEsc.substring(0,4));
	var dataNova= new Date(yr1, mon1-1, dt1,0,0,0);

	var dia= dataNova.getDay();
	var semana=new Array(6);
	semana[0]='Domingo';
	semana[1]='Segunda-Feira';
	semana[2]='Terça-Feira';
	semana[3]='Quarta-Feira';
	semana[4]='Quinta-Feira';
	semana[5]='Sexta-Feira';
	semana[6]='Sábado';
	document.getElementById("negocio_diaSemana").value=semana[dia];
	document.getElementById("negocio_diaSemana").innerHTML="";
	return dia;
}


function btn_salvar_click(){
	if (validaCadastro() == true && validaPotencialValor() == true)
	{
		document.getElementById("btn_salvar").removeEventListener("click", btn_salvar_click);
		salvarNegocio();
	}
}

function preencheListaOrganizacoes(){
	var html = '<option value="0">Organizacao...</option>';
	var select = "";

	for (i=0;i<listaOrganizacoes.length;i++){
		if(organizacao==i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaOrganizacoes[i].id+'" '+select+'>' +listaOrganizacoes[i].nome+'</option>';
	}

	document.getElementById("negocio_organizacao").innerHTML = html;
	setSelectedValue(document.getElementById("negocio_organizacao"),0);
}

function preencheListaPessoas(){
	var html = '<option value="0">Pessoa...</option>';
	var select = "";
	for (i=0;i<listaPessoas.length;i++){
		if(pessoa==i){
			select = "selected";
		}else{
			select = "";
		}
		html += '<option value="'+listaPessoas[i].id+'" '+select+'>' +listaPessoas[i].nome+'</option>';
	}

	document.getElementById("negocio_pessoa").innerHTML = html;
	setSelectedValue(document.getElementById("negocio_pessoa"),0);
}


function buscaOrganizacaocomPessoa(){

	var proprietario = vendedor;
	var unidade = unidade;

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscaOrganizacaocomPessoa",
			proprietario: proprietario,
			unidade: unidade,


	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}

function buscarOrganizacoes(){

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarOrganizacoes",
			unidade: unidade


	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}

function buscarPessoasOrganizacao(){

	var proprietario = vendedor;
	e=document.getElementById("negocio_organizacao");
	organizacao = e.options[e.selectedIndex].value;

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarPessoasOrganizacao",
			proprietario: proprietario,
			organizacao:organizacao,
			unidade: unidade



	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}

function buscarTodasPessoasOrganizacoes(){

	var proprietario = vendedor;
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarTodasPessoasOrganizacoes",
			proprietario: proprietario,
			unidade: unidade



	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}

function buscarPessoasNegociosProprietario(){

	var proprietario = vendedor;
	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarPessoasNegociosProprietario",
			proprietario: proprietario,
			unidade : unidade



	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");   

}


function salvarNegocio(){
	if ( document.getElementById("negocio_pessoa").options[document.getElementById("negocio_pessoa").selectedIndex].value!="0")
	{
		var dataAbertura = document.getElementById("negocio_data").value;
		var diaSemana = document.getElementById("negocio_diaSemana").value;
		var nome = document.getElementById("negocio_nome").value;
		e=document.getElementById("negocio_organizacao");
		var organizacao = e.options[e.selectedIndex].value;
		e=document.getElementById("negocio_pessoa");
		var pessoa = e.options[e.selectedIndex].value;
		var escopo = document.getElementById("negocio_escopo").value;
		
		var briefing = document.getElementById("negocio_briefing").value;
		
		var potencial = document.getElementById("negocio_potencial").value;
		var briefingAprovado=document.getElementById("negocio_briefingAprovado").checked;
		var publico =document.getElementById('negocio_publico').options[document.getElementById('negocio_publico').selectedIndex].innerText; 
		var temperatura =document.getElementById('negocio_temperatura').options[document.getElementById('negocio_temperatura').selectedIndex].innerText; 

		var solucao = document.getElementById('negocio_solucao').options[document.getElementById('negocio_solucao').selectedIndex].innerText;
		var dataPrevistaEntrega = document.getElementById("negocio_datafechamento").value;
		var dataPrevistaFechamento = document.getElementById("negocio_dataentrega").value;
		var quantidadePessoas = document.getElementById("negocio_qtepessoas").value;
		var quantidadeTurmas = document.getElementById("negocio_qteturmas").value;
		var limite = document.getElementById('negocio_limite').options[document.getElementById('negocio_limite').selectedIndex].innerText;

	//	if (briefingAprovado==false)
	//	{
			var data_jason = ""; //retornaServerPost();
			var data = {
					page: "vendas",
					action: "salvarNegocio",
					id: lancamentoNegocio,
					dataAbertura: dataAbertura,
					diaSemana: diaSemana,
					organizacao: organizacao,
					pessoa: pessoa,
					nome: nome,
					escopo: escopo,
					briefing:briefing,
					potencial: potencial,
					proprietario:vendedor,
					publicoAlvo: publico,
					tipoSolucao: solucao,
					dataPrevistaEntrega:dataPrevistaEntrega,
					dataPrevistaFechamento:dataPrevistaFechamento,
					quantidadePessoas:quantidadePessoas,
					quantidadeTurmas:quantidadeTurmas,
					limiteTempoCliente:limite,
					unidadeNegocio: unidade,
					temperatura: temperatura

			}
			
			data_jason = data_jason+JSON.stringify(data);
			callWebServicePost(data_jason,"callback");  
		//}

		//else
		//{
	//		alert("O negócio não pode ser mais alterado porque o Briefing já esta aprovado. Contate o BDM!");
//		}
	}
	else
	{
		alert("Voce não pode salvar um negócio que não tem pessoa associada!")
	}
}


function buscarNegocio(){

	var url = retornaServer();
	var data = {
			page: "vendas",
			action: "buscarNegocio", 
			id: lancamentoNegocio
	}
	url=url+encodeURI(JSON.stringify(data));
	callWebservice(url,"callback");

}


function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value==valueToSet) {
			selectObj.options[i].selected = true;
			return;
		}
	}
}

function carregaLancamentosNegocio()
{
	{
		sessionStorage.setItem("vendedor",vendedor);
		sessionStorage.setItem("lancamentosNegocio","0");
		window.document.location.href = '../lancamentosNegocio/lancamentosNegocio.html';

	}


}


function carregaMenuVendas()
{
	{
		sessionStorage.setItem("vendedor",vendedor);
		sessionStorage.setItem("lancamentosNegocio","0");
		window.document.location.href = '../menuVendas/menuVendas.html';

	}

}

function inserirNegocio()
{
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("lancamentoNegocio","0");
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../lancamentoNegocio/lancamentoNegocio.html';

}

function carregarMenuVendas()
{
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("lancamentoNegocio","0");
	// mais tarde você pode parar de observar
	observer.disconnect();
	window.document.location.href = '../menuVendas/menuVendas.html';

}


function sair()
{
	sessionStorage.setItem("vendedor","0");
	sessionStorage.setItem("lancamentosNegocio","0");
	window.document.location.href = '../login/login.html';

}

function editaNegocio()
{
	var i=0;
	var achei=0;
	var indiceOrganizacao=0;
	var indicePessoa=0;


	while (achei==0)
	{
		if (listaOrganizacoes[i].id==listaNegocio[0].organizacao)
		{	
			indiceOrganizacao=i;
			achei=1;
			setSelectedValue(document.getElementById("negocio_organizacao"),listaOrganizacoes[indiceOrganizacao].id);
		}
		i=i+1;
	}
	if (listaNegocio[0].pessoa!="")
	{	

		i=0;
		achei=0;
		while (achei==0)
		{
			if (listaPessoas[i].id==listaNegocio[0].pessoa)
			{	
				indicePessoa=i;
				achei=1;
				setSelectedValue(document.getElementById("negocio_pessoa"),listaPessoas[indicePessoa].id);
			}
			i=i+1;
		}
	}
	else{
	
		setSelectedValue(document.getElementById("negocio_pessoa"),0);}
	var solucao;

	switch (listaNegocio[0].tipoSolucao) {
	case  "Consultoria":
		setSelectedValue (document.getElementById("negocio_solucao"),1);
		break;
	case "Treinamento":
		setSelectedValue (document.getElementById("negocio_solucao"),2);
		break;
	case "Tecnologia":
		setSelectedValue (document.getElementById("negocio_solucao"),3);
		break;
	case "Blended":
		setSelectedValue (document.getElementById("negocio_solucao"),4);
		break;
	} 

	switch (listaNegocio[0].publicoAlvo) {
	case  "Colaboradores":
		setSelectedValue (document.getElementById("negocio_publico"),1);
		break;
	case "Coordenadores/Gerentes":
		setSelectedValue (document.getElementById("negocio_publico"),2);
		break;
	case "Diretores/Presidente":
		setSelectedValue (document.getElementById("negocio_publico"),3);
		break;
	case "Conselho":
		setSelectedValue (document.getElementById("negocio_publico"),4);
		break;
	}
	
	
	switch (listaNegocio[0].temperatura) {
	case  "Fria":
		setSelectedValue (document.getElementById("negocio_temperatura"),1);
		break;
	case "Morna":
		setSelectedValue (document.getElementById("negocio_temperatura"),2);
		break;
	case "Quente":
		setSelectedValue (document.getElementById("negocio_temperatura"),3);
		break;
	} 


	switch (listaNegocio[0].limiteTempoCliente) {
	case  "Limite...":
		setSelectedValue (document.getElementById("negocio_limite"),0);
		break
	case  "Não":
		setSelectedValue (document.getElementById("negocio_limite"),1);
		break;
	case  "Sim":
		setSelectedValue (document.getElementById("negocio_limite"),2);
		break;
	}

	var data = construirData(listaNegocio[0].dataAbertura, "00:00:00");
	var dataFormatada= formataData(data);
	document.getElementById("negocio_data").value=dataFormatada;
	
	var data = construirData(listaNegocio[0].dataPrevistaFechamento, "00:00:00");
	var dataFormatadaF= formataData(data);
	document.getElementById("negocio_datafechamento").value=dataFormatadaF;
	
	var data = construirData(listaNegocio[0].dataPrevistaEntrega, "00:00:00");
	var dataFormatadaE= formataData(data);
	document.getElementById("negocio_dataentrega").value=dataFormatadaE;





	document.getElementById("negocio_nome").value=listaNegocio[0].nome;
	diaSemanaData(dataFormatada);
	document.getElementById("negocio_potencial").value=listaNegocio[0].potencial;
	document.getElementById("negocio_qteturmas").value=listaNegocio[0].quantidadeTurmas;
	document.getElementById("negocio_qtepessoas").value=listaNegocio[0].quantidadePessoas;
	setSelectedValue(document.getElementById("negocio_limite"),listaNegocio[0].limiteTempoCliente);
	document.getElementById("negocio_escopo").value=listaNegocio[0].escopo;
	document.getElementById("negocio_briefing").value=listaNegocio[0].briefing;
	if(listaNegocio[0].briefingAprovado==false)
	{
		document.getElementById("negocio_briefingAprovado").checked=false;
	}
	else
	{
		document.getElementById("negocio_briefingAprovado").checked=true;
	}


	document.getElementById("negocio_briefingAprovado").setAttribute("disabled","disabled");
	
}


function callback(data){


	if(data.action=="salvarNegocio"){
		retorno=data.retorno;
		alert ("Negócio salvo com sucesso");
		document.getElementById("btn_salvar").addEventListener("click", btn_salvar_click);
		location.reload();

	}


	if(data.action=="buscarNegocio"){
		listaNegocio=jQuery.parseJSON(data.retorno);
		editaNegocio();

	}

	if(data.action=="buscarTodasPessoasOrganizacoes"){
		listaPessoas=jQuery.parseJSON(data.retorno);
		preencheListaPessoas();
		buscarNegocio();

	}
	
	if(data.action=="buscarPessoasNegociosProprietario"){
		listaPessoas=jQuery.parseJSON(data.retorno);
		preencheListaPessoas();
		buscarNegocio();
	}	

	if(data.action=="buscarPessoasOrganizacao"){
		listaPessoas=jQuery.parseJSON(data.retorno);
		preencheListaPessoas();
	}	

	if(data.action=="buscaOrganizacaocomPessoa"){
		listaOrganizacoes=jQuery.parseJSON(data.retorno);
		preencheListaOrganizacoes();
		if(lancamentoNegocio!="0")
		{
			buscarTodasPessoasOrganizacoes();	
		}
	}

	if(data.action=="buscarOrganizacoes"){
		listaOrganizacoes=jQuery.parseJSON(data.retorno);
		
		preencheListaOrganizacoes();
		if(lancamentoNegocio!="0")
		{
			buscarPessoasNegociosProprietario();
			//buscarTodasPessoasOrganizacoes();	
		}
	}

}

