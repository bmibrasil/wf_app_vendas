//Variáveis da sessão
var listaTurmas = [];
var listaEmpresas = [];
var listaUnidades = [];
var listaParticipantes = [];
var listaNegociosParticipante = [];
var turma=0;
var turmaPos=0;
var empresa=0;
var empresaPos=0;
var unidade=0;
var tableUnidade;
var tableParticipante;

function admin_init() {
    //turma = sessionStorage.getItem("turma");
    buscarTurmas();
    buscarEmpresas();
    document.getElementById("admin_salvar").addEventListener("click", admin_salvar_turma_click);    
    document.getElementById("admin_salvar_unidade").addEventListener("click", admin_salvar_unidade_click);
    document.getElementById("admin_excluir_unidade").addEventListener("click", admin_excluir_unidade_click);
    document.getElementById("admin_salvar_participante").addEventListener("click", admin_salvar_participante_click);
    document.getElementById("admin_excluir_participante").addEventListener("click", admin_excluir_participante_click);
}

//TRATAMENTO DOS EVENTOS
function admin_salvar_turma_click(){
    salvarTurma();
}

function admin_salvar_unidade_click(){
    salvarNegocio();
}

function admin_excluir_unidade_click(){
    excluirNegocio();
}

function admin_excluir_participante_click(){
    excluirAcesso();
}

function admin_salvar_participante_click(){
    salvarAcesso();
}

function mudaTurma() {
    
    var e = document.getElementById("admin_turmas");
    turmaid = e.options[e.selectedIndex].value;
    var cont=0;
    var achei=-1;
    while ((cont <listaTurmas.lenght ) || (achei ==-1) && (turmaid !=0))
    {
        	    
                if (listaTurmas[cont].id==turmaid)
        	    	{
        	    	  achei=cont;
        	    	}
        	  cont++;
    }
    turmaPos=achei;
    turma=turmaid;
    sessionStorage.setItem("turma",achei);
    buscarNegocios();
    buscarAcessos();
    update_admin();
//    window.location.reload(false);
}

function mudaEmpresa() {
    var e = document.getElementById("admin_turma_empresa");
    empresaid = e.options[e.selectedIndex].value;
    var cont=0;
    var achei=-1;
    empresa=empresaid;
    sessionStorage.setItem("empresa",empresaid);
 //   update_admin();
//    window.location.reload(false);
}

function preencheListaTurma() {
    var html = '<option value="0">nova turma ...</option>';
    var selected = "";
    for (i=1;i<=listaTurmas.length;i++) { //inicia em 1 para não incluir o "Template"
        if (turma==i) {
            selected = " selected";
        } else {
            selected = "";
        }
        html += '<option value="'+listaTurmas[i-1].id+'" '+selected+'>'+listaTurmas[i-1].nome+'</option>';
    }
    setSelectedValue(document.getElementById("admin_turmas"),turma);
    document.getElementById("admin_turmas").innerHTML = html;
  
}


function preencheListaEmpresa() {
    var html = '<option value="0">nova empresa ...</option>';
    var selected = "";
    for (i=1;i<=listaEmpresas.length;i++) { //inicia em 1 para não incluir o "Template"
        if (empresa==i) {
            selected = " selected";
        } else {
            selected = "";
        }
        html += '<option value="'+listaEmpresas[i-1].id+'" '+selected+'>'+listaEmpresas[i-1].nome+'</option>';
    }
    setSelectedValue(document.getElementById("admin_participante_negocio"),empresa);
    //document.getElementById("admin_participante_negocio").innerHTML = html;
    document.getElementById("admin_turma_empresa").innerHTML = html;

}

function preencheListaNegocios() {
    var html = '<option value="0">novo negocio ...</option>';
    var selected = "";
    for (i=1;i<=listaUnidades.length;i++) { //inicia em 1 para não incluir o "Template"
        if (empresa==i) {
            selected = " selected";
        } else {
            selected = "";
        }
        html += '<option value="'+listaUnidades[i-1].id+'" '+selected+'>'+listaUnidades[i-1].nome+'</option>';
    }
    setSelectedValue(document.getElementById("admin_participante_negocio"),empresa);
    document.getElementById("admin_participante_negocio").innerHTML = html;
}

function preencheListaUnidades() {
    var html = 
            "<thead>"+
            "    <th style='text-align:center'>id</th>"+
            "    <th style='text-align:left;'>negocio</th>"+
            "</thead>";
                    
    for (i=0;i<listaUnidades.length;i++) { 
        html +=        
            "<tr>"+
            "    <td style='text-align:center;'>"+listaUnidades[i].id+"</td>"+
            "    <td>"+listaUnidades[i].nome+"</td>"+
            "</tr>";
    }
    document.getElementById("unidades").innerHTML = html;
}


function preencheListaParticipantes() {
    
    var cont =0;               
    var achei=-1;
    var html = 
            "<thead>"+
            "    <th style='text-align:center'>id</th>"+
            "    <th style='text-align:left;'>nome</th>"+
           // "    <th style='text-align:center;'>empresa/negocio</th>"+
            "    <th style='text-align:center;'>acesso</th>"+
            "</thead>";
     for (i=0;i<listaParticipantes.length;i++) 
     { 

//         while ((cont <listaEmpresas.lenght ) || (achei ==-1))
//         {
//
//            if (listaEmpresas[cont].id==listaParticipantes[i].empresa)
//             {
//               achei=cont;
//             }
//            cont++;
//         }
           html +=        
                "<tr>"+
                "    <td style='text-align:center;'>"+listaParticipantes[i].id+"</td>"+
                "    <td>"+listaParticipantes[i].nome+"</td>"+
              //  "  <td style='text-align:center;'>"+listaEmpresas[achei].nome+"</td>"+
                "  <td style='text-align:center;'>"+listaParticipantes[i].codigoAcesso+"</td>"+
                "</tr>";

      //    cont=0;
     //     achei=-1;
     }
      document.getElementById("participantes").innerHTML = html;

}

function preencheListaParticipantes() {
    
    var cont =0;               
    var achei=-1;
    var html = 
            "<thead>"+
            "    <th style='text-align:center'>id</th>"+
            "    <th style='text-align:left;'>nome</th>"+
           // "    <th style='text-align:center;'>empresa/negocio</th>"+
            "    <th style='text-align:center;'>acesso</th>"+
            "</thead>";
     for (i=0;i<listaParticipantes.length;i++) 
     { 

//         while ((cont <listaEmpresas.lenght ) || (achei ==-1))
//         {
//
//            if (listaEmpresas[cont].id==listaParticipantes[i].empresa)
//             {
//               achei=cont;
//             }
//            cont++;
//         }
           html +=        
                "<tr>"+
                "    <td style='text-align:center;'>"+listaParticipantes[i].id+"</td>"+
                "    <td>"+listaParticipantes[i].nome+"</td>"+
              //  "  <td style='text-align:center;'>"+listaEmpresas[achei].nome+"</td>"+
                "  <td style='text-align:center;'>"+listaParticipantes[i].codigoAcesso+"</td>"+
                "</tr>";

    
     }
      document.getElementById("participantes").innerHTML = html;

}

function preencheListaNegociosParticipante() {
    
    var cont =0;               
    var achei=-1;
    var html = 
            "<thead>"+
            "    <th style='text-align:center'>id</th>"+
           // "    <th style='text-align:left;'>nome</th>"+
            "    <th style='text-align:center;'>empresa/negocio</th>"+
           // "    <th style='text-align:center;'>acesso</th>"+
            "</thead>";
     for (i=0;i<listaNegociosParticipante.length;i++) 
     { 

         while ((cont <listaUnidades.lenght ) || (achei ==-1))
         {

            if (listaUnidades[cont].id==listaNegociosParticipante[i].empresa)
             {
               achei=cont;
             }
            cont++;
         }
           html +=        
                "<tr>"+
                "    <td style='text-align:center;'>"+document.getElementById("admin_participante_id").value+"</td>"+
                "  <td style='text-align:center;'>"+listaUnidades[achei].nome+"</td>"+
                "</tr>";

           cont=0;
           achei=-1;
     }
      document.getElementById("negociosParticipante").innerHTML = html;

}

function update_admin() {
   sessionStorage.setItem("turma",turma);
    if (turma!=0)
    {
    	var achei=-1;
        var cont=0;
        document.getElementById("admin_turma_id").value       = listaTurmas[turmaPos].id;
        document.getElementById("admin_turma_nome").value     = listaTurmas[turmaPos].nome;
        document.getElementById("admin_turma_numValores").value     = listaTurmas[turmaPos].totalValores;
        document.getElementById("admin_turma_percentualRemocao").value     = listaTurmas[turmaPos].percentualRemocao;
        document.getElementById("admin_unidade_turma").value    = turma;
        document.getElementById("admin_unidade_empresaMae").value = listaEmpresas[empresaPos].nome;
        document.getElementById("admin_unidade_turma").innerHTML = "";
        document.getElementById("admin_unidade_empresaMae").innerHTML = "";

        var combo=document.getElementById("admin_turma_status");
        //preenche os status da turma
        for (var i = 0; i < combo.options.length; i++)
	      
         {
		 
         if (combo.options[i].value == listaTurmas[turmaPos].status)
		   {
			combo.options[i].selected = "true";
			break;
		   }
	     }
        
        
        
        //preenche a empresa mae da turma
            var comboEmp=document.getElementById("admin_turma_empresa");
        //preenche os status da turma
         for (var i = 0; i < comboEmp.options.length; i++)
	      
         {
		 
         if (comboEmp.options[i].value == listaTurmas[turmaPos].empresa)
		   {
			comboEmp.options[i].selected = "true";
            empresaPos=i-1;
			break;
		   }
	     }
        
              
        
   
    }
     else {
        document.getElementById("admin_turma_id").value       = "";
        document.getElementById("admin_turma_nome").value     = "";
        document.getElementById("admin_turma_status").value   = "";
        document.getElementById("admin_turma_empresa").value = "";
        document.getElementById("admin_turma_numValores").value     = "";
        document.getElementById("admin_turma_percentualRemocao").value     = "";
        
        document.getElementById("admin_unidade_id").value    = "";
        document.getElementById("admin_unidade_nome").value = "";
        document.getElementById("admin_unidade_turma").value    = "";
        document.getElementById("admin_unidade_empresaMae").value = "";
         document.getElementById("admin_participante_id").value = "";
         document.getElementById("admin_participante_nome").value = "";
         document.getElementById("admin_participante_negocio").value = "";
    }
}

function update_unidade(){
    var elementos = Array();
    var selecionado = false;
    elementos = tableUnidade.rows('.selected').data()[0];
    for (i=0;i<tableUnidade.rows('.selected').data().length;i++){
        elementos[i]=tableUnidade.rows('.selected').data()[i][0]; 
    }
    if (elementos!=undefined) {
        unidade  = elementos[0];
        nome     = elementos[1];
       
     
        sessionStorage.setItem("unidade",unidade);
        
        document.getElementById("admin_unidade_id").value       = unidade;
        document.getElementById("admin_unidade_nome").value     = nome;
        document.getElementById("admin_unidade_turma").value    = turma;
        document.getElementById("admin_unidade_empresaMae").value = listaEmpresas[empresaPos].nome;
        
    } else {
        sessionStorage.setItem("unidade",0);
        document.getElementById("admin_unidade_id").value       = "";
        document.getElementById("admin_unidade_nome").value     = "";
         document.getElementById("admin_unidade_turma").value    = turma;
         document.getElementById("admin_unidade_empresaMae").value = listaEmpresas[empresaPos].nome;
      
    }
}


function update_participante(){
    var elementos = Array();
    var selecionado = false;
    elementos = tableParticipante.rows('.selected').data()[0];
    for (i=0;i<tableParticipante.rows('.selected').data().length;i++){
        elementos[i]=tableParticipante.rows('.selected').data()[i][0]; 
    }
    if (elementos!=undefined) {
        id  = elementos[0];
        nome     = elementos[1];
        empresa    = elementos[2];
             
        //sessionStorage.setItem("unidade",unidade);
        
        document.getElementById("admin_participante_id").value       = id;
        document.getElementById("admin_participante_nome").value     = nome;
        var unidadeSel=-1;
        var i=0;
        //procura o id da unidade
//        while ((i<listaUnidades.length) || (unidadeSel==-1))
//            {
//                if(empresa==listaUnidades[i].nome)
//                {
//                    unidadeSel=listaUnidades[i].id;
//                }
//                i++;
//                
//            }
        
//        var combo=document.getElementById("admin_participante_negocio");
//        //preenche os status da turma
//        for (var i = 0; i < combo.options.length; i++)
//	      
//         {
//		 
//         if (combo.options[i].value == unidadeSel)
//		   {
//			combo.options[i].selected = "true";
//			break;
//		   }
//	     }
//        
//        
//        
       
        
        
    } else {
      //sessionStorage.setItem("unidade",0);
        document.getElementById("admin_participante_id").value       = "";
        document.getElementById("admin_participante_nome").value     = "";
        //document.getElementById("admin_participante_negocio").value    = "";
      
    }
    buscarNegociosParticipante();
}


function update_negocioParticipante(){
    var elementos = Array();
    var selecionado = false;
    elementos = tableNegociosParticipante.rows('.selected').data()[0];
    for (i=0;i<tableNegociosParticipante.rows('.selected').data().length;i++){
        elementos[i]=tableNegociosParticipante.rows('.selected').data()[i][0]; 
    }
    if (elementos!=undefined) {
        id  = elementos[0];
        //nome     = elementos[1];
        //empresa    = elementos[2];
             
        //sessionStorage.setItem("unidade",unidade);
        
        //document.getElementById("admin_participante_id").value       = id;
       // document.getElementById("admin_participante_nome").value     = nome;
        var unidadeSel=-1;
        var i=0;
        //procura o id da unidade
        while ((i<listaUnidades.length) || (unidadeSel==-1))
            {
                if(empresa==listaUnidades[i].nome)
                {
                    unidadeSel=listaUnidades[i].id;
                }
                i++;
                
            }
        
        var combo=document.getElementById("admin_participante_negocio");
        //preenche os status da turma
        for (var i = 0; i < combo.options.length; i++)
	      
         {
		 
         if (combo.options[i].value == unidadeSel)
		   {
			combo.options[i].selected = "true";
			break;
		   }
	     }
        
        
        
       
        
        
    } else {
      //sessionStorage.setItem("unidade",0);
        document.getElementById("admin_participante_id").value       = "";
        document.getElementById("admin_participante_nome").value     = "";
        document.getElementById("admin_participante_negocio").value    = "";
      
    }
}

function setSelectedValue(selectObj, valueToSet) {
    for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].value==valueToSet) {
            selectObj.options[i].selected = true;
            return;
        }
    }
}


// PROCESSAMENTO DO RETORNO DOS WEBSERVICES

function callback(data) {
    if (data.action=="buscarTurmas") {
        listaTurmas = jQuery.parseJSON(data.retorno);
        preencheListaTurma();
        update_admin();

    }
    
    if (data.action=="buscarEmpresas") {
        listaEmpresas = jQuery.parseJSON(data.retorno);
        preencheListaEmpresa();
        update_admin();
   
   
      }
    
    if (data.action=="salvarTurma") {
        buscarTurmas();
        update_admin();
    }
    
    
    if (data.action=="excluirNegocio") {
        document.getElementById("unidades").innerHTML = "";
        window.location.reload(false);
    }
    if (data.action=="buscarNegocios") {
        listaUnidades = jQuery.parseJSON(data.retorno);
        preencheListaUnidades();
        preencheListaNegocios();
        update_admin();
        activateUnidadesTable();
    }
    
    if (data.action=="buscarNegociosParticipante") {
        listaNegociosParticipante = jQuery.parseJSON(data.retorno);
        preencheListaNegociosParticipante();
       // preencheListaNegocios();
        update_admin();
        activateNegociosParticipanteTable();
        
        
    }
    
    
    if (data.action=="buscarAcessos") {
        listaParticipantes = jQuery.parseJSON(data.retorno);
        preencheListaParticipantes();
        update_admin();
        activateParticipantesTable();
    }
    
    if (data.action=="excluirAcesso") {
        document.getElementById("participantes").innerHTML = "";
        window.location.reload(false);
    }
    
     if (data.action=="salvarAcesso") {
        preencheListaParticipantes();
        document.getElementById("participantes").innerHTML = "";
        window.location.reload(false);
    }
    
    if (data.action=="salvarNegocio"){
        window.location.reload(false);
    }
}

function buscarTurmas() {
    var url = retornaServer();
    var data = {
        page:   "admin",
        action: "buscarTurmas"
    }
    url=url+encodeURI(JSON.stringify(data));
    callWebservice(url,"callback");
}

function buscarEmpresas() {
    var url = retornaServer();
    var data = {
        page:   "admin",
        action: "buscarEmpresas"
    }
    url=url+encodeURI(JSON.stringify(data));
    callWebservice(url,"callback");
}

function buscarNegocios() {
    if (turma>0){
        var url = retornaServer();
        var data = {
            page:   "admin",
            action: "buscarNegocios",
            turma: turma,
            empresamae: listaTurmas[turmaPos].empresa
        }
        url=url+encodeURI(JSON.stringify(data));
        callWebservice(url,"callback");
    } else {
        listaUnidades = [];
        listaParticipantes=[];
        
        //preencheListaUnidades();
        //preencheListaNegocios();
    }
}


function buscarAcessos() {
    if (turma>0){
        var url = retornaServer();
        var data = {
            page:   "admin",
            action: "buscarAcessos",
            turma: turma,
            
        }
        url=url+encodeURI(JSON.stringify(data));
        callWebservice(url,"callback");
    } else {
        listaParticipantes = [];
        preencheListaParticipantes();
    }
}
function salvarTurma() {
    var url = retornaServer();
     var data = {
        page:   "admin",
        action: "salvarTurma",
        turma: turma,
        nome: document.getElementById("admin_turma_nome").value,
        status: document.getElementById("admin_turma_status").value,
        empresa: document.getElementById("admin_turma_empresa").value,
        numeroValores:document.getElementById("admin_turma_numValores").value,
        percRemocao:document.getElementById("admin_turma_percentualRemocao").value,
    
    }
    url=url+encodeURI(JSON.stringify(data));
    callWebservice(url,"callback");
}

function salvarNegocio(){
    var url = retornaServer();
    var data = {
        page:     "admin",
        action:   "salvarNegocio",
        id:       document.getElementById("admin_unidade_id").value,
        nome:      document.getElementById("admin_unidade_nome").value,
        empresaMae: listaEmpresas[empresaPos].id
        
    }
    url=url+encodeURI(JSON.stringify(data));
    callWebservice(url,"callback");
}

function salvarAcesso(){
    var url = retornaServer();
    nome     = document.getElementById("admin_participante_nome").value;
    superior = empresa;
    var data = {    
        page:     "admin",
        action:   "salvarAcesso",
        id:       document.getElementById("admin_participante_id").value,
        nome:     nome,
        turma:   turma,
        tipo: 1,
        empresa:  document.getElementById("admin_participante_negocio").value,
        
    }
    url=url+encodeURI(JSON.stringify(data));
    callWebservice(url,"callback");
}

function excluirNegocio(){
    var url = retornaServer();
    var elementos = Array();
    elementos = tableUnidade.rows('.selected').data()[0];
    for (i=1;i<tableUnidade.rows('.selected').data().length;i++){ elementos[i]=tableUnidade.rows('.selected').data()[i][0]; }
    var data = {
        page:   "admin",
        action: "excluirUnidade",
        empresa: elementos[0]
   }
    url=url+encodeURI(JSON.stringify(data));
    callWebservice(url,"callback");
}


function excluirAcesso(){
    var url = retornaServer();
    var elementos = Array();
    elementos = tableParticipante.rows('.selected').data()[0];
    for (i=1;i<tableParticipante.rows('.selected').data().length;i++){ elementos[i]=tableParticipante.rows('.selected').data()[i][0]; }
    var data = {
        page:   "admin",
        action: "excluirParticipante",
        participante: elementos[0]
   }
    url=url+encodeURI(JSON.stringify(data));
    callWebservice(url,"callback");
}

function buscarNegociosParticipante(){
    var url = retornaServer();
    var elementos = Array();
    elementos = tableParticipante.rows('.selected').data()[0];
    for (i=1;i<tableParticipante.rows('.selected').data().length;i++)
    { elementos[i]=tableParticipante.rows('.selected').data()[i][0]; }
    var data = {
        page:   "admin",
        action: "buscarNegociosParticipante",
        acesso: elementos[0]
   }
    url=url+encodeURI(JSON.stringify(data));
    callWebservice(url,"callback");
}

function lostfocus(){
    var x = Number(event.target.value);
    if (x>100) x=100;
    event.target.value = x;
    event.target.value = event.target.value+"%";
}

function getfocus(){
    event.target.value = event.target.value.replace(/%/g, "");
}
                  
function activateUnidadesTable() {
    tableUnidade = $('#unidades').DataTable( {
        retrieve: true,
        destroy: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese-Brasil.json"
        },
        paging: false,
        searching: false,
        bSort: false
    });
    
    $('#unidades tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            tableUnidade.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        update_unidade();
    } );

}


                  
function activateParticipantesTable() {
    tableParticipante = $('#participantes').DataTable( {
        retrieve: true,
        destroy: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese-Brasil.json"
        },
        paging: false,
        searching: false,
        bSort: false
    });
    
    $('#participantes tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            tableParticipante.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        update_participante();
    } );

}

function activateNegociosParticipanteTable() {
    tableNegociosParticipante = $('#negociosParticipante').DataTable( {
        retrieve: true,
        destroy: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese-Brasil.json"
        },
        paging: false,
        searching: false,
        bSort: false
    });
    
    $('#negociosParticipante tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
        	tableNegociosParticipante.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
 
    } );
    
    
    buscarEmpresas();
}
