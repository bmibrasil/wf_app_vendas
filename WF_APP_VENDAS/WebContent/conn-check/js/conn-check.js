var connected = false;
var checking = false;
var intervalID = setInterval(verificaConexao, 5000);

function contactServer(){
    connected = false;
    var url = retornaServer();
    var data = {
        page:   "application",
        action: "verificaConexao",
        jsonp: "callback_header"
    }
    url=url+encodeURI(JSON.stringify(data));
    callWebserviceMethod(url,"callback_header");
}

function callback_header(data){
    var retorno;
    var acesso;
    if (data.action=="verificaConexao"){
        //dataObj = jQuery.parseJSON(data.conexao);
        connected = true;
        checking = false;
    }
}

function verificaConexao(){
    if (connected==false){
        if (checking==true){
                document.getElementById("myModal").style.display = "block";
        }
    } else {
        document.getElementById("myModal").style.display = "none";
    }
    checking = true;
    contactServer();
}