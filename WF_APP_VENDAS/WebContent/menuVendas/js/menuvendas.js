//Variáveis da sessão
var listaColaboradores = [];
var listaClientes = [];
var listaLancamento = [];
var listaProjetos = [];
var listaEtapas = [];
var listaTipoHoras = [];
var HorasTotal=0;
var txtHoraInicial=0;

var vendedor = "0";
var nomeVendedor = "0";
var unidade="0";
var id=0;

var bloqueio=0;
var permissoes="";


/**/

function admin_init(){

	//alert(getCurrentUserEmail());
	// cadastro = sessionStorage.getItem("cadastro");
	//  nome=sessionStorage.getItem("nomeColaborador");

	
	vendedor=sessionStorage.getItem("vendedor");
	nomeVendedor=sessionStorage.getItem("nomeVendedor");
	unidade=sessionStorage.getItem("unidade");
	
	
	if ((vendedor==0) || (vendedor==null) || (vendedor==undefined)) 
	{
		window.document.location.href = '../login/login.html';
	}
	senha=sessionStorage.getItem("senha");
	bloqueio=sessionStorage.getItem("bloqueio");
    permissoes=sessionStorage.getItem("permissoes");
	
	//buscarClientes();
	document.getElementById("menuop1").addEventListener("click", sair);
	document.getElementById("menuVendas_solicitacaoorganizacao").addEventListener("click", inserirSolicitacaoOrganizacao);
	document.getElementById("menuVendas_solicitacaopessoa").addEventListener("click", inserirSolicitacaoPessoa);
	document.getElementById("menuVendas_atividade").addEventListener("click", inserirAtividade);
	document.getElementById("menuVendas_negocio").addEventListener("click", inserirNegocio);
	document.getElementById("menuVendas_relacionamento").addEventListener("click", inserirRelacionamentoPessoa);

}



function atribuirValoresAmbiente()
{
	
	sessionStorage.setItem("unidade",unidade);
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("nomeVendedor",nomeVendedor);
	sessionStorage.setItem("permissoes",permissoes);
	sessionStorage.setItem("bloqueio",bloqueio);
	
}

function inserirSolicitacaoOrganizacao()
{
	atribuirValoresAmbiente();
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("unidade",unidade);
	sessionStorage.setItem("lancamentoSolicitacaoOrganizacao",0);
	window.document.location.href = '../solicitacaoorganizacao/solicitacaoorganizacao.html';

}


function inserirSolicitacaoPessoa()
{
	atribuirValoresAmbiente();
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("unidade",unidade);
	sessionStorage.setItem("lancamentoSolicitacaoPessoa",0);
	window.document.location.href = '../solicitacaopessoa/solicitacaopessoa.html';

}

function inserirAtividade()
{
	atribuirValoresAmbiente();
	sessionStorage.setItem("lancamentoAtividade",0);
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("unidade",unidade);

	window.document.location.href = '../lancamentosAtividade/lancamentosAtividade.html';

}

function inserirRelacionamentoPessoa()
{
	atribuirValoresAmbiente();
	sessionStorage.setItem("lancamentoAtividade",0);
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("unidade",unidade);

	window.document.location.href = '../lancamentosRelacionamento/lancamentosRelacionamento.html';

}



function inserirNegocio()
{
	atribuirValoresAmbiente();
	sessionStorage.setItem("lancamentoNegocio",0);
	sessionStorage.setItem("vendedor",vendedor);
	sessionStorage.setItem("unidade",unidade);
	window.document.location.href = '../lancamentosNegocio/lancamentosNegocio.html';

}

function sair()
{
	localStorage.clear();
	sessionStorage.setItem("vendedor","0");
	sessionStorage.setItem("unidade",0);
	sessionStorage.setItem("lancamentoSolicitacaoOrganizacao","0");
	sessionStorage.setItem("lancamentoSolicitacaoPessoa","0");
	sessionStorage.setItem("lancamentoAtividade","0");
	sessionStorage.setItem("Negocio","0");
	// mais tarde você pode parar de observar
	window.document.location.href = '../login/login.html';

}

