var id = 0;
var negocio = 0;
var tipo = 0;
var descricao = 0;
var negocio_pessoa = 0;
var conexaoAtividade = 1;

var listaNegocios = [];
var listaOrganizacoes = [];
var listaPessoas = [];
var listaLancamento = [];


var diaSemana;
var data = "";
var vendedor = "";
var nomeVendedor = "";

function admin_init(){

	vendedor = sessionStorage.getItem("vendedor");
	nomeVendedor = sessionStorage.getItem("nomeVendedor");
	lancamentoAtividade = sessionStorage.getItem("lancamentoAtividade");
	conexaoAtividade = sessionStorage.getItem("conexaoAtividade");
	
	document.getElementById("atividade_data").addEventListener("change", buscaData);
	document.getElementById("atividade_negocio").addEventListener("change", buscarPessoasNegocioProprietario);
	
	dataAtual();
	buscaData();
	
	document.getElementById("btn_salvar").addEventListener("click", btn_salvar_click);
	document.getElementById("menuop1").addEventListener("click", carregaLancamentosAtividade);
	document.getElementById("menuop2").addEventListener("click", carregaMenuVendas);
	document.getElementById("menuop3").addEventListener("click", sair);
	
	buscaOrganizacaocomNegocio();
}

//TRATAMENTO DOS EVENTOS

//verifica se foi escolhido valor na combo diferente do default
function verificaValorSelecionadoCombo(str) {
	
	var selectObj = document.getElementById(str);
	var valueToSet = document.getElementById(str).value;
	
	if (selectObj.options[0].value == valueToSet)
		return false;
	else
		return true;
	
	

}

//verifica se o campo esta vazio
function verificaPreeenchimento(str) {
	
	var v = document.getElementById(str).value;
	
	if ((v == null) || (v == "") || (v == '0'))
		return false
	else	
		return true;
	
}


function validarHorario() {
	
	var horaInicio = document.getElementById('atividade_hora_inicio').value;
	var horaFim = document.getElementById('atividade_hora_fim').value;
	
	if ( calcularIntervaloTempo( horaInicio, horaFim ) >= 0 ){
		return true;
	}else{
		alert('Campos de Data com valores Invalidos, Certifique-se de que os horarios estejam preenchidos corretamente');
		return false;
	}

}

function validaCadastro() {
	if( ( (verificaValorSelecionadoCombo("atividade_negocio") && (verificaValorSelecionadoCombo("atividade_pessoa") ) )
			&& verificaPreeenchimento("atividade_data")
			&& verificaPreeenchimento("atividade_hora_inicio")
			&& verificaPreeenchimento("atividade_hora_fim")
			&& verificaPreeenchimento("atividade_diaSemana")
			&& verificaValorSelecionadoCombo("atividade_tipo")
			&& verificaPreeenchimento("atividade_descricao") ) == true ){
		return  true;
	
	}else{
		
		alert('Existem campos que não foram preenchidos, Por favor preencha');
		return false;
		
	}

}

function carregaLancamentosAtividade(){
	
	{
		sessionStorage.setItem("vendedor",vendedor);
		sessionStorage.setItem("nomeVendedor",nomeVendedor);
		sessionStorage.setItem("lancamentoAtividade","0");
		window.document.location.href = '../lancamentosAtividade/lancamentosAtividade.html';
	}

}

function carregaMenuVendas(){
	
	{
		sessionStorage.setItem("vendedor",vendedor);
		sessionStorage.setItem("nomeVendedor",nomeVendedor);
		sessionStorage.setItem("lancamentoAtividade","0");
		sessionStorage.setItem("conexaoAtividade","0");
		window.document.location.href = '../menuVendas/menuVendas.html';
	}

}

function sair(){
	
	sessionStorage.setItem("vendedor","0");
	sessionStorage.setItem("nomeVendedor","0");
	sessionStorage.setItem("lancamentoAtividade","0");
	sessionStorage.setItem("conexaoAtividade","0");
	window.document.location.href = '../login/login.html';

}

function dataAtual(data){
	
	var data = new Date();
	
	document.getElementById("atividade_data").valueAsDate=data; 
	document.getElementById("atividade_data").innerHTML="";

}


function buscaData(){
	
	var dataSel = document.getElementById("atividade_data").value;
	
	diaSemanaData(dataSel);
	
}


function diaSemanaData(dataEscolhida){

	var dataEsc = dataEscolhida;
	var dt1   = parseInt(dataEsc.substring(8,10));
	var mon1  = parseInt(dataEsc.substring(5,7));
	var yr1   = parseInt(dataEsc.substring(0,4));
	var dataNova = new Date(yr1, mon1-1, dt1,0,0,0);

	var dia = dataNova.getDay();
	var semana = new Array(6);
	
	semana[0] = 'Domingo';
	semana[1] = 'Segunda-Feira';
	semana[2] = 'Terça-Feira';
	semana[3] = 'Quarta-Feira';
	semana[4] = 'Quinta-Feira';
	semana[5] = 'Sexta-Feira';
	semana[6] = 'Sábado';
	
	document.getElementById("atividade_diaSemana").value=semana[dia];
	document.getElementById("atividade_diaSemana").innerHTML = "";
	
	return dia;
}              


function btn_salvar_click(){
	
	if (validaCadastro() == true && validarHorario() == true){
		
		document.getElementById("btn_salvar").removeEventListener("click", btn_salvar_click);
		
		salvarAtividade();
		
	}
	
}


function preencheListaNegocios(){
	
	var html = '<option value="0">Negocio...</option>';
	var indiceOrganizacao = 0;
	var select = "";
	
	for(i = 0; i < listaNegocios.length; i++){
		
		if(negocio == i){
			
			select = "selected";
			
		}else{
			
			selected = "";
			
		}

		for (j = 0; j < listaOrganizacoes.length; j++){

			if (listaOrganizacoes[j].id == listaNegocios[i].organizacao){
				
				indiceOrganizacao = j;
				
			}

		}

		html += '<option value="' + listaNegocios[i].id + '" ' + select + '>' + 
				listaOrganizacoes[indiceOrganizacao].nome + " - " + listaNegocios[i].nome + '</option>';
	}

	document.getElementById("atividade_negocio").innerHTML = html;
	setSelectedValue(document.getElementById("atividade_negocio"), 0);
}


function preencheListaPessoas(){
	
	var html = '<option value="0">Pessoa...</option>';
	var indiceOrganizacao = 0;
	var select = "";
	
	for (i = 0; i < listaPessoas.length; i++){
		
		if(negocio==i){
			
			select = "selected";
			
		}else{
			
			selected = "";
			
		}

		for (j = 0; j < listaOrganizacoes.length; j++){

			if (listaOrganizacoes[j].id == listaPessoas[i].organizacao){
				
				indiceOrganizacao = j;
				
			}

		}
		html += '<option value="' + listaPessoas[i].id + '" ' + select + '>' + 
				listaOrganizacoes[indiceOrganizacao].nome + " - " + listaPessoas[i].nome + '</option>';

	}
	
	document.getElementById("atividade_pessoa").innerHTML = html;
	setSelectedValue(document.getElementById("atividade_pessoa"), 0);
}


function buscarAtividade(){
	
	var url = retornaServer();
	
	var data = {
			
			page: "vendas",
			action: "buscarAtividade",
			id: lancamentoAtividade

	}
	
	url = url + encodeURI(JSON.stringify(data));
	
	callWebservice(url,"callback");   	
}

function buscarPessoasNegocioProprietario(){
	
	negocio = document.getElementById("atividade_negocio").value;

	var url = retornaServer();
	
	var data = {
			
			page: "vendas",
			action: "buscarPessoasNegocioProprietario",
			proprietario: vendedor,
			negocio:negocio

	}
	
	url = url + encodeURI(JSON.stringify(data));
	
	callWebservice(url,"callback");   	
}

function buscarNegociosProprietario(){
	
	var url = retornaServer();
	
	var data = {
			
			page: "vendas",
			action: "buscarNegociosProprietario",
			proprietario: vendedor,

	}
	
	url = url + encodeURI(JSON.stringify(data));
	
	callWebservice(url,"callback");   	
}

function buscaOrganizacaocomNegocio(){

	var proprietario = vendedor;

	var url = retornaServer();
	
	var data = {
			
			page: "vendas",
			action: "buscaOrganizacaocomNegocio",
			proprietario: proprietario

	}
	
	url = url + encodeURI(JSON.stringify(data));
	
	callWebservice(url,"callback");   

}


function salvarAtividade(){

	var negocio_pessoa = 0;

	pessoa = document.getElementById("atividade_pessoa").value;
	negocio = document.getElementById("atividade_negocio").value;
	
	var horaInicio = document.getElementById("atividade_hora_inicio").value;
	var horaFim = document.getElementById("atividade_hora_fim").value;

	var data = document.getElementById("atividade_data").value + ' 00:00:00';
	var diaSemana = document.getElementById("atividade_diaSemana").value;
	
	var tipoAtividade = document.getElementById('atividade_tipo').options[document.getElementById('atividade_tipo').selectedIndex].innerText;
	var descricao = document.getElementById("atividade_descricao").value;
    
	var url = retornaServer();
	
	var data = {
			
			page: "vendas",
			action: "salvarAtividade",
			id: encodeURIComponent(lancamentoAtividade),
			negocio: encodeURIComponent(negocio),
			pessoa:encodeURIComponent(pessoa),
			dataAtividade: encodeURIComponent(data),
			horaInicio: encodeURIComponent(horaInicio),
			horaFim: encodeURIComponent(horaFim),
			tipoAtividade: encodeURIComponent(tipoAtividade),
			descricao: encodeURIComponent(descricao),
			proprietario: encodeURIComponent(vendedor),
			nomeProprietario: nomeVendedor

	}
	
	url = url + JSON.stringify(data);
	
	callWebservice(url,"callback");   

}

function setSelectedValue(selectObj, valueToSet) {
	
	for (var i = 0; i < selectObj.options.length; i++) {
		
		if (selectObj.options[i].value == valueToSet) {
			
			selectObj.options[i].selected = true;
			
			return;
			
		}
		
	}
	
}


function editaLancamento(){
	
	var cargo = "";
	
	setSelectedValue(document.getElementById("atividade_negocio"),listaLancamentos[0].negocio);
	buscarPessoasNegocioProprietario();
	//var dataMontada =listaLancamentos[0].dataAtividade.substr(6,4)+"-"+listaLancamentos[0].dataAtividade.substr(3,2)+"-"+listaLancamentos[0].dataAtividade.substr(0,2)+" 00:00:00";
	var dataMontada=listaLancamentos[0].dataAtividade.substr(0,10);
	var data = construirData(dataMontada, "00:00:00");
	var dataFormatada= formataData(data);
	
	var horaInicio = obterHorario(listaLancamentos[0].dataInicio);
	var horaFim = obterHorario(listaLancamentos[0].dataFim);
	
	document.getElementById('atividade_hora_inicio').value=horaInicio;
	document.getElementById('atividade_hora_inicio').innerHTML="";
	
	document.getElementById('atividade_hora_fim').value=horaFim;
	document.getElementById('atividade_hora_fim').innerHTML="";
	
	document.getElementById('atividade_data').value=dataFormatada;
	document.getElementById('atividade_data').innerHTML="";
	
	diaSemanaData(dataFormatada);
	
	document.getElementById('atividade_diaSemana').innerHTML="";
	
	document.getElementById('atividade_descricao').value=listaLancamentos[0].descricao;
	document.getElementById('atividade_descricao').innerHTML="";
	
	selecionarSelectpeloOption('atividade_tipo',listaLancamentos[0].tipoAtividade);
    selecionarSelectpeloOption("atividade_pessoa",listaLancamentos[0].pessoaAtividade);
	
}


function selecionarSelectpeloOption(elementId, valor) {
	
	var elt = document.getElementById(elementId);
	
	var opt = elt.getElementsByTagName("option");
	
	for(var i = 0; i < opt.length; i++) {
		
		if(opt[i].text == valor) {
			
			var campo=document.getElementById(elementId);
			
			setSelectedValue(campo,i);
			
		}
		
	}

}

function callback(data){

	if(data.action == "salvarAtividade"){
		
		alert("Atividade salva com sucesso!!");
		document.getElementById("btn_salvar").addEventListener("click", btn_salvar_click);
		window.location.reload(false);
		
	}

	if(data.action=="buscarNegociosProprietario"){
		
		listaNegocios=jQuery.parseJSON(data.retorno);
		preencheListaNegocios();
		
		if (lancamentoAtividade!="0"){
			
			buscarAtividade();
			
		}
		
	}
	
	if(data.action=="buscarPessoasNegocioProprietario"){
		
		listaPessoas=jQuery.parseJSON(data.retorno);
		preencheListaPessoas();
		
		if (lancamentoAtividade!="0"){
			
			setSelectedValue(document.getElementById("atividade_pessoa"), listaLancamentos[0].pessoaAtividade);
			
			//buscarAtividade();
			
		}
		
	}

	if(data.action=="buscaOrganizacaocomNegocio"){
		
		listaOrganizacoes=jQuery.parseJSON(data.retorno);
		buscarNegociosProprietario();
		
	}

	if(data.action=="buscarAtividade"){
		
		listaLancamentos=jQuery.parseJSON(data.retorno);
		editaLancamento();	
		
	}

}